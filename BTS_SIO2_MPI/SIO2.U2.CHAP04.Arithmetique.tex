% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
\usepackage{ProfSio}
\useproflyclib{piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={CHAPITRE~},numdoc={4},titre={Arithmétique}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\part{CH04 - Rappels d'arithmétique}

\section{Diviseurs, multiples, nombres premiers}

\subsection{Diviseurs et multiples}

\begin{cdefi}
On dit que $a$ est un \textbf{diviseur} de $b$ ou que $b$ est un \textbf{multiple} de $a$ s’il existe un \uline{entier} $k$ tel que $b = k \times a$.

On dit aussi que  $b$ est \textbf{divisible} par $a$.
\end{cdefi}

\begin{cprop}
On a les résultats suivants :
\begin{center}
	\begin{tblr}{hlines,vlines,width=0.975\linewidth,cells={font=\sffamily},colspec={Q[m,c]X[m,c]},row{1}={bg=lightgray}}
		Un entier naturel est divisible par : & Si : \\
		2& son chiffre des unités est 0 ou 2 ou 4 ou 6 ou 8\\ 
		3& la somme de ses chiffres est divisible par 3\\
		4& le nombre formé des deux derniers chiffres est divisible par 4\\
		5& son chiffre des unités est 0 ou 5\\
		9& la somme de ses chiffres est divisible par 9\\ 
		10& son chiffre des unités est 0 \\
	\end{tblr}
%	\renewcommand{\arraystretch}{1.25}
%	\textsf{\begin{tabularx}{\linewidth}{|c|Y|}
%		\hline
%		\cellcolor{lightgray}Un entier naturel est divisible par : & \cellcolor{lightgray}Si : \\ \hline
%		2& son chiffre des unités est 0 ou 2 ou 4 ou 6 ou 8\\ \hline
%		3& la somme de ses chiffres est divisible par 3\\ \hline
%		4& le nombre formé des deux derniers chiffres est divisible par 4\\ \hline
%		5& son chiffre des unités est 0 ou 5\\ \hline
%		9& la somme de ses chiffres est divisible par 9\\ \hline
%		10& son chiffre des unités est 0 \\ \hline
%	\end{tabularx}}
\end{center}
\end{cprop}

\subsection{Nombres premiers}

\begin{cdefi}[Pluriel]
Un entier naturel $p$ est \textbf{premier} s’il admet \uline{exactement} deux diviseurs : 1 et lui-même.
\end{cdefi}

\begin{cprop}[ComplementTitre={ - (test de primalité)}]
Si un entier n'admet aucun diviseur premier inférieur ou égal à sa racine carrée, alors il est premier.
\end{cprop}

\begin{cthm}
Tout entier naturel strictement supérieur à 1 peut être écrit comme un produit de nombres premiers de façon unique, à l'ordre près des facteurs. On appelle ce produit la \og décomposition en facteurs premiers \fg.
\end{cthm}

\begin{cmethode}
Pour trouver le \textbf{nombre} de diviseurs d'un entier N à partir de sa décomposition en facteurs premiers, on multiplie les exposants, augmentés de 1, des facteurs premiers apparaissant dans la décomposition de N.
\end{cmethode}

\begin{cmethode}
Pour déterminer la liste de tous les diviseurs d'un entier N, on utilise un \textbf{arbre} (de type \textit{multiplicatif}).

Cet arbre permet également de retrouver le nombre de diviseurs de l'entier N.
\end{cmethode}

\newpage

\section{Division euclidienne, congruences}

\subsection{Division euclidienne}

\begin{cdefi}
$a$ et $b$ étant deux entiers relatifs tels que $b \neq 0$, il existe un unique couple $(q;r)$  d’entiers relatifs tel que : \[\begin{dcases} a=b \times q+r \\ 0 \pp r < |b| \end{dcases}.\]
L’entier $q$ est le quotient et l’entier $r$ est le reste de cette division.
\end{cdefi}

\begin{cmethode}[ComplementTitre={ - calculatrice}]
La partie entière donne le quotient de la division euclidienne de $a$ par $b$.
	
La fonction \calg{MOD} ou \calg{reste} ou \calg{remainder} donne le reste de la division euclidienne de $a$ par $b$ :
\begin{center}
	\includegraphics[height=2cm]{graphics/chap04_diveucl_35}~~\includegraphics[height=2cm]{graphics/chap04_diveucl_83ce}~~\includegraphics[height=2cm]{graphics/chap04_diveucl_82}
\end{center}
\end{cmethode}

\begin{cprop}[Pluriel]
$b$ divise $a$ si et seulement si le reste de la division euclidienne de $a$ par $b$ est égal à 0.
\end{cprop}

\subsection{Congruences}

\begin{cdefi}
Soit $n$ un entier supérieur ou égal à 2, et soient $a$ et $b$ deux entiers quelconques de $\Z$.

On dit que $a$ et $b$ sont \textbf{congrus} modulo $n$ si et seulement si  $a-b$ est un multiple de  $n$, c’est-à-dire s’il existe un entier relatif $k$ tel que $a-b=kn$.
\end{cdefi}

\begin{cnota}
Cela se note $a \equiv b \Modulo{n}$ et on lit : « $a$ est congru à $b$ modulo $n$ ».
\end{cnota}

\begin{cdefi}
Soit $n$ un entier supérieur ou égal à 2, et soient $a$ et $b$ deux entiers quelconques de $\Z$.
	
On dit que $a$ et $b$ sont congrus modulo $n$ si et seulement si $a$ et $b$ ont le même reste dans la division par $n$.
\end{cdefi}

\begin{ccscq}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $a \equiv 0 \Modulo{n} \ssi a$ est un multiple de $n$.
	\item Si $a=nq+r$ avec $0 \pp r < n$  alors $a \equiv r \Modulo{n}$.
	\item Si $a \equiv r \Modulo{n}$ avec $0 \pp r<n$  alors  $r$ est le reste dans la division de $a$ par $n$.
\end{itemize}
$r$ est le plus petit entier positif  congru à $a$ modulo $n$.
\end{ccscq}

\begin{cprop}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Si $a \equiv b \Modulo{n}$ et $b \equiv c \Modulo{n}$ alors $a \equiv c \Modulo{n}$.
	\item Si $a \equiv b \Modulo{n}$ alors $a+c \equiv b+c \Modulo{n}$ et $ac \equiv bc \Modulo{n}$.
\end{itemize}
\end{cprop}

\begin{cthm}
Si $a$, $a'$, $b$, $b'$ sont des entiers tels que $a \equiv a' \Modulo{n}$ et $b \equiv b' \Modulo{n}$ alors :
\begin{itemize}[leftmargin=*]
	\item $a+b \equiv a'+b' \Modulo{n}$ et $a-b \equiv a'-b' \Modulo{n}$ ;
	\item $a \times b \equiv a' \times b' \Modulo{n}$ ;
	\item $k \times a' \equiv k \times a \Modulo{n}$ avec $k$ un entier ;
	\item $a^k \equiv (a')^k \Modulo{n}$ avec $k$ un entier.
\end{itemize}
\end{cthm}

\begin{ccalco}
On peut travailler sur les congruences en utilisant le reste de la div. eucl. (via \ccalg{reste}/\ccalg{remainder}/\ccalg{MOD}).
\end{ccalco}

\section{PGCD}

\subsection{Définitions}

\begin{cdefi}
Le PGCD des deux entiers $a$ et $b$, noté $PGCD(a;b)$ est le plus grand des diviseurs communs à $a$ et à $b$.
\end{cdefi}

\begin{cmethode}
Pour calculer $PGCD(a;b)$ :
\begin{itemize}[leftmargin=*]
	\item on écrit la décomposition en facteurs premiers de $a$ et de $b$ ;
	\item on multiplie entre eux les facteurs premiers communs aux deux décompositions, affectés du plus petit exposant figurant dans ces décompositions.
\end{itemize} 
\end{cmethode}

\begin{ccalco}
\begin{center}
	\includegraphics[height=2cm]{graphics/chap04_pcgd_35}~~\includegraphics[height=2cm]{graphics/chap04_pgcd_83ce}~~\includegraphics[height=2cm]{graphics/chap04_pgcd_82}
\end{center}
\end{ccalco}

\begin{cdefi}[ComplementTitre={ - Propriétés }]
$a$ et $b$ sont dits premiers entre eux lorsque leur seul diviseur commun est 1.

\smallskip

$a$ et $b$ sont premiers entre eux si et seulement si leur PGCD est égal à 1. 

Deux nombres premiers distincts sont premiers entre eux.
\end{cdefi}

\subsection{Algorithme d'Euclide}

\begin{cmethode}
Pour calculer $PGCD(a;b$) où $a>b$ :
\begin{itemize}[leftmargin=*]
	\item on effectue la division euclidienne de $a$ par $b$ :  on obtient un reste $r$ avec  $r<b$ :
	\begin{itemize}
		\item si $r=0$, alors $a$ est divisible par $b$ et $PGCD(a;b)=b$ ;
		\item si $r \neq 0$, alors $PGCD(a;b)=PGCD(b;r)$ ;
	\end{itemize}
	\item on effectue la division euclidienne de $b$ par $r$ : on obtient un nouveau reste $r_1$ avec $r_1<r$ :
	\begin{itemize}
		\item si $r_1=0$ alors $b$ est divisible par $r$ et $PGCD(a;b)=PGCD(b;r)=r_1$ ;
		\item si $r_1 \neq 0$ alors on réitère le processus avec $r$ et $r_1$ ;
	\end{itemize}
	\item on s'arrête lorsque l'on trouve un reste égal à 0 ; le $PGCD(a;b)$ est alors le dernier reste non nul obtenu.
\end{itemize}
\end{cmethode}

\end{document}