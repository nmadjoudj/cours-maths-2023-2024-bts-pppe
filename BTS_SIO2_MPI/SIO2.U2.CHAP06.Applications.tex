% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage{ProfSio}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{wrapstuff}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={EXOS~},numdoc={6},titre={Arithmétique},mois=Novembre,annee=2023]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - \DonneesMois{} \DonneesAnnee}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers


\begin{document}

\pagestyle{fancy}

\part{CH06 - Produit cartésien, applications}

\section{Produit cartésien de deux ensembles}

\subsection{Rappels}

\begin{cdefi}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Un \underline{ensemble} désigne une collection d'objets, qui sont appelés les éléments de l'ensemble.
	
	Un ensemble fini se note entre accolades.
	\item Pour tout ensemble $E$, si $x$ est un élément de $E$, on dit que $x$ \underline{appartient} à $E$, et on note $x \in E$.
	\item Si le nombre d'éléments appartenant à un ensemble $E$ est fini, on dit que $E$ est un ensemble fini et on appelle \underline{cardinal} de $E$ le nombre d'éléments de $E$, noté $\card(E)$.
	\item On dit qu'un ensemble $A$ est \underline{inclus} dans un ensemble $E$ si tout élément de $A$ est aussi élément de $E$.
	
	On dit alors que $A$ est un \underline{sous-ensemble} ou une \underline{partie} de $E$, et on note $A \subset E$.
	\item L'\underline{ensemble vide}, noté $\emptyset$ est l'ensemble ne contenant aucun élément.
\end{itemize}
\end{cdefi}

\subsection{Produit cartésien}

\begin{cdefi}
Soient $E$ et $F$ deux ensembles.

Le \underline{produit cartésien} de $E$ et $F$ est l'ensemble de tous les couples formés par un élément de $E$ en première position, et un élément de $F$ en deuxième position. Il est noté $E \times F$ (lire \og $E$ croix $F$ \fg) : \[E \times F = \{(x;y),~x \in E \wedge y \in F\}.\]
\end{cdefi}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{descartes}{}Les produits cartésiens doivent leur nom à \textbf{René Descartes}, qui, en créant la géométrie analytique, a le premier utilisé ce que nous appelons maintenant $\R^2 = \R \times \R$ pour représenter le plan euclidien, et $\R^3 = \R \times \R \times \R$ pour représenter l'espace euclidien tri-dimensionnel
\end{chistoire}

\begin{crmq}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $E \times E$ peut également être noté $E^2$.
	\item Attention, le produit cartésien n'est pas commutatif : $E \times F$ n'est pas le même ensemble que $F \times E$.
\end{itemize}
\end{crmq}

\begin{cprop}
Si $E$ et $F$ sont deux ensembles finis, alors {$\card(E \times F) = \card(E) \times \card(F)$}.
\end{cprop}

\begin{crmq}
La notion de produit cartésien se généralise, et on peut définir le produit cartésien de $n$ ensembles, où $n$ est un entier supérieur ou égal à 2.

Si on note $E_1$, $E_2$, \dots, $E_n$ ces ensembles, alors $E_1 \times E_2 \times \dots \times E_n$ est l'ensemble des $n$-uplets $(x_1, x_2,\dots,x_n)$ où pour tout $i$, $x_i$ est un élément de l'ensemble $E_i$.

Si tous les ensembles en cause sont finis, alors la propriété précédente se généralise en :

\hfill{}$\card(E_1 \times E_2 \times \dots \times E_n) = \card(E_1) \times \card(E_2) \times \dots \times \card(E_n)$.\hfill{}~
\end{crmq}

\newpage

\section{Applications d'un ensemble dans un autre}

\subsection{Généralités}

\begin{cdefi}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Une \underline{application} $f$ d'un ensemble $E$ vers un ensemble $F$ est une relation binaire de $E$ vers $F$ qui, à tout élément $x$ de $E$, associe un unique élément de $F$ noté $f(x)$.
	\item $f(x)$ s'appelle l'\textit{image} de $x$ par $f$. Et si $f(x) = y$, alors on dit que $x$ est un \textit{antécédent} de $y$ par $f$.
	\item On note $f$ : \begin{minipage}{2cm} $E \rightarrow F$\\ $x \mapsto f(x)$ \end{minipage}
\end{itemize}
\end{cdefi}

\begin{cillustr}[ModifLabel={ \&{} Exemple}]
\begin{center}
	\begin{tikzpicture}[scale=0.9]
		\draw[thick] (0.5,1) circle[x radius=0.5,y radius=1] ;
		\draw (0.5,0) node[below,font=\sffamily] {E} ;
		\draw[thick] (2.5,1) circle[x radius=0.6,y radius=0.81] ;
		\draw (2.5,0) node[below,font=\sffamily] {F} ;
		\node[inner sep=2pt] (debx) at (0.5,1.25) {} ;
		\node[inner sep=2pt] (finx) at (2.5,0.75) {} ;
		\filldraw[darkgray] (debx) circle[radius=2pt] node[above] {$x$} ;
		\filldraw[darkgray] (finx) circle[radius=2pt] node[above] {$f(x)$} ;
		\draw[thick,purple,->,>=latex] (debx) to[bend right=20] node[midway,below,text=purple] {$f$} (finx)  ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{crmq}
Une manière très pratique, quand on peut le faire, de travailler sur une application est de raisonner sur son \textbf{diagramme sagittal} (ou une compréhension de son digramme sagittal) :
\begin{itemize}
	\item la \og patate \fg{} de gauche est celle de l'ensemble de départ (E) ;
	\item la \og patate \fg{} de droite est celle de l'ensemble d'arrivée (F) ;
	\item les \og flèches \fg{} traduisent les images (et donc les antécédents !).
\end{itemize}
\end{crmq}

\begin{cexemple}
Si $E = \EcritureEnsemble[\strut]{1/2/3}$ et $F = \EcritureEnsemble[\strut]{1/4}$, alors $f$, définie par $f(x) = x^2$ n'est pas une application.
\begin{center}
	\begin{tikzpicture}[x=0.75cm,y=0.75cm,font=\small\sffamily]
		%ENSEMBLES
		\draw[thick] (0.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (0.5,0.25) node[below] {E} ;
		\draw[thick] (2.5,1.5) circle[x radius=0.45,y radius=1] ;
		\draw (2.5,0.25) node[below] {F} ;
		%VALEURS
		\node[inner sep=2pt] (Ea) at (0.5,2) {1} ;
		\node[inner sep=2pt] (Eb) at (0.5,1.5) {2} ;
		\node[inner sep=2pt] (Ec) at (0.5,1) {3} ;
		\node[inner sep=2pt] (Fa) at (2.5,1.75) {1} ;
		\node[inner sep=2pt] (Fb) at (2.5,1.25) {4} ;
		%APPLI
		\draw[thick,purple,->,>=latex] (Ea) -- (Fa);
		\draw[thick,purple,->,>=latex] (Eb) -- (Fb);
		\draw[purple] (1.5,2.25) node {$f$} ;
		\draw (2.95,1.5) node[right=8pt] {On constate que l'élément \textsf{3} de \textsf{E} n'a pas d'image !} ;
	\end{tikzpicture}	
\end{center}
\end{cexemple}

\begin{cexemple}
Si $E = \EcritureEnsemble[\strut]{-1/1/2/3}$ et $F = \EcritureEnsemble[\strut]{0/1/4/5/9}$ alors $f$, définie par $f(x) = x^2$ est une application.
\begin{center}
	\begin{tikzpicture}[x=0.6cm,y=0.6cm,font=\small\sffamily]
		%ENSEMBLES
		\draw[thick] (0.75,2.5) circle[x radius=0.75,y radius=2.25] ;
		\draw (0.75,0.25) node[below] {E} ;
		\draw[thick] (4,2.5) circle[x radius=0.75,y radius=2.25] ;
		\draw (4,0.25) node[below] {F} ;
		%VALEURS
		\node[inner sep=2pt] (Ea) at (0.75,4) {-1} ;
		\node[inner sep=2pt] (Eb) at (0.75,3) {1} ;
		\node[inner sep=2pt] (Ec) at (0.75,2) {2} ;
		\node[inner sep=2pt] (Ed) at (0.75,1) {3} ;
		\node[inner sep=2pt] (Fa) at (4,4) {4} ;
		\node[inner sep=2pt] (Fb) at (4,3.25) {1} ;
		\node[inner sep=2pt] (Fc) at (4,2.5) {0} ;
		\node[inner sep=2pt] (Fd) at (4,1.75) {5} ;
		\node[inner sep=2pt] (Fe) at (4,1) {9} ;
		%APPLI
		\draw[thick,purple,->,>=latex] (Ea) -- (Fb);
		\draw[thick,purple,->,>=latex] (Eb) -- (Fb);
		\draw[thick,purple,->,>=latex] (Ec) -- (Fa);
		\draw[thick,purple,->,>=latex] (Ed) -- (Fe);
		\draw[purple] (2.375,4.25) node {$f$} ;
		\draw (4.75,2.5) node[right=8pt] {On constate que tous les éléments de \textsf{E} ont une (seule) image !} ;
	\end{tikzpicture}
\end{center}
\end{cexemple}

\begin{cexemple}
Une entreprise associe à chaque client un numéro de client.
\end{cexemple}

\begin{crmq}[Pluriel]
Si $f$ est une application de $E$ dans $F$, chaque élément $x \in E$ possède une \textbf{unique} image.

\hspace{5mm}$\rhd$ Autrement dit, de tout élément de l'ensemble de départ, il \og part une (seule) flèche \fg{} !

En revanche, un élément $y$ de $F$ peut avoir zéro, un ou plusieurs antécédents.
\end{crmq}

\begin{cillustr}
Le diagramme sagittal ci dessous représente une application $f$ de $E = \EcritureEnsemble[\strut]{1/2/3}$ dans $F = \EcritureEnsemble[\strut]{a/b/c/d}$.
\begin{center}
	\begin{tikzpicture}
		%ENSEMBLES
		\draw[thick] (0.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (0.5,0.15) node[below] {\point{E}} ;
		\draw[thick] (2.5,1.5) circle[x radius=0.45,y radius=1.35] ;
		\draw (2.5,0.15) node[below] {\point{F}} ;
		%ELEMENTS
		\node[inner sep=2pt] (Ea) at (0.5,2) {\ppoint{1}} ;
		\node[inner sep=2pt] (Eb) at (0.5,1.5) {\ppoint{2}} ;
		\node[inner sep=2pt] (Ec) at (0.5,1) {\ppoint{3}} ;
		\node[inner sep=2pt] (Fa) at (2.5,2.25) {\ppoint{a}} ;
		\node[inner sep=2pt] (Fb) at (2.5,1.75) {\ppoint{b}} ;
		\node[inner sep=2pt] (Fc) at (2.5,1.25) {\ppoint{c}} ;
		\node[inner sep=2pt] (Fd) at (2.5,0.75) {\ppoint{d}} ;
		\draw[purple] (1.5,2.5) node[text=purple] {$f$} ;
		%ARCS
		\foreach \deb/\fin in {Ea/Fb,Eb/Fb,Ec/Fd} \draw[very thick,purple,->,>=latex] (\deb)--(\fin) ;	
	\end{tikzpicture}
\end{center}
\end{cillustr}

\subsection{Image directe et image réciproque d'une partie d'ensemble}

\begin{ccadre}[Compteur=false]
Dans toute cette partie, $f$ est une application d'un ensemble $E$ dans un ensemble $F$.
\end{ccadre}

\begin{cdefi}
Soit $A$ une partie de $E$. On appelle \underline{image de A} par $f$ l'ensemble constitué des images des éléments de $A$.

Cet ensemble est noté $f(A)$ : $$f(A) = \{\strut f(x),~x \in A \} =  \{\strut y\in F,~\exists x \in A,~f(x) = y\}.$$
\end{cdefi}

\begin{cmethode}[ModifLabel={s concrètes}]
Autrement dit, sur un diagramme sagittal, l'image directe de A est l'ensemble de tous les éléments de F \og atteints par une flèche \fg{} venant d'un élément de A !
\end{cmethode}

\begin{crmq}
Attention à ne pas confondre l'image d'un élément, qui est un élément, et l'image d'un ensemble, qui est un ensemble.
\end{crmq}

\begin{cdefi}
Soit $B$ une partie de $F$. On appelle \underline{image réciproque} de $B$ par $f$ l'ensemble des éléments de $E$ dont l'image est un élément de $B$.

Cet ensemble est noté $f^{-1}(B)$ : $$f^{-1}(B) = \{\strut x \in E,~f(x) \in B\}.$$
\end{cdefi}

\begin{cmethode}[ModifLabel={s concrètes}]
\begin{itemize}[leftmargin=*]
	\item Autrement dit, sur un diagramme sagittal, l'image réciproque de B est l'ensemble des tous les éléments de E pour lesquels la \og flèche pointe vers un élément de B \fg{} !
	\item $f^{-1}(B)$ est en fait l'ensemble des antécédents des éléments de $B$.
\end{itemize}
\end{cmethode}

\subsection{Injection, surjection et bijection}

\begin{ccadre}[Compteur=false]
Dans toute cette partie, $f$ est une application d'un ensemble $E$ dans un ensemble $F$.
\end{ccadre}

\begin{cdefi}
L'application $f$ est dite \underline{injective} si chaque élément de $F$ a \textbf{au plus} un antécédent par $f$.

Cela revient à dire que deux éléments distincts de $E$ ont deux images distinctes par $f$ : $$\forall (x ; x') \in E^2,~x \neq x' \Rightarrow f(x) \neq f(x').$$
$f$ est donc injective si deux éléments distincts de $E$ ont deux images distinctes par $f$.
\end{cdefi}

\begin{cmethode}[ModifLabel={ simple}]
Sur un diagramme sagittal, on repère une application \textbf{injective} lorsque :

\hspace{5mm}$\rhd$ tout élément de l'ensemble d'arrivée (F) est atteint par au \textbf{maximum} (donc 0 ou 1) \textbf{une flèche}.
\end{cmethode}

\begin{cillustr}[ComplementTitre={ - Remarque}]
\begin{center}
	\begin{tikzpicture}[font=\small\sffamily]
		%ENSEMBLES
		\draw[thick] (0.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (0.5,0.15) node[below] {E} ;
		\draw[thick] (2.5,1.5) circle[x radius=0.45,y radius=1.35] ;
		\draw (2.5,0.15) node[below] {F} ;
		%ELEMENTS
		\node[inner sep=2pt] (Ea) at (0.5,2) {{1}} ;
		\node[inner sep=2pt] (Eb) at (0.5,1.5) {{2}} ;
		\node[inner sep=2pt] (Ec) at (0.5,1) {{3}} ;
		\node[inner sep=2pt] (Fa) at (2.5,2.25) {{a}} ;
		\node[inner sep=2pt] (Fb) at (2.5,1.75) {{b}} ;
		\node[inner sep=2pt] (Fc) at (2.5,1.25) {{c}} ;
		\node[inner sep=2pt] (Fd) at (2.5,0.75) {{d}} ;
		\draw[purple] (1.5,2.5) node {\textcolor{purple}{$f$}} ;
		%ARCS
		\foreach \deb/\fin in {Ea/Fc,Eb/Fb,Ec/Fd} \draw[very thick,purple,->,>=latex] (\deb)--(\fin) ;
		\draw (3,2) node [right=8pt] {Il y a donc (forcément) plus d'éléments dans F que dans E !} ;
		\draw (3,1) node [right=8pt] {Ainsi card(F)\,$\mathsf{\pg}$\,card(E) !} ;	
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{crmq}
En cryptographie, il est \textit{nécessaire} d'utiliser des fonctions de cryptage injectives : chaque message crypté doit avoir un unique message en clair pour antécédent, sinon on ne pourrait pas opérer de décryptage.

D'une manière générale, dès que l'on veut coder des informations sans perte, on utilise des fonctions
injectives.
\end{crmq}

\begin{cdefi}
L'application $f$ est dite \underline{surjective} si chaque élément de $F$ a \textbf{au moins} un antécédent par $f$ : $$\forall y \in F,~\exists x \in E, f(x) = y.$$
\end{cdefi}

\begin{cmethode}[ModifLabel={ simple}]
Sur un diagramme sagittal, on repère une application \textbf{surjective} lorsque :

\hspace{5mm}$\rhd$ tout élément de l'ensemble d'arrivée (F) est atteint par au \textbf{minimum une flèche}.
\end{cmethode}

\begin{cillustr}[ComplementTitre={ - Remarque}]
\begin{center}
	\begin{tikzpicture}[font=\small\sffamily]
		%ENSEMBLES
		\draw[thick] (2.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (0.5,0.15) node[below] {E} ;
		\draw[thick] (0.5,1.5) circle[x radius=0.45,y radius=1.35] ;
		\draw (2.5,0.15) node[below] {F} ;
		%ELEMENTS
		\node[inner sep=2pt] (Ea) at (2.5,2) {{1}} ;
		\node[inner sep=2pt] (Eb) at (2.5,1.5) {{2}} ;
		\node[inner sep=2pt] (Ec) at (2.5,1) {{3}} ;
		\node[inner sep=2pt] (Fa) at (0.5,2.25) {{a}} ;
		\node[inner sep=2pt] (Fb) at (0.5,1.75) {{b}} ;
		\node[inner sep=2pt] (Fc) at (0.5,1.25) {{c}} ;
		\node[inner sep=2pt] (Fd) at (0.5,0.75) {{d}} ;
		\draw[purple] (1.5,2.5) node {\textcolor{purple}{$f$}} ;
		%ARCS
		\foreach \deb/\fin in {Fa/Ea,Fb/Ea,Fc/Eb,Fd/Ec} \draw[very thick,purple,->,>=latex] (\deb)--(\fin) ;
		\draw (3,2) node [right=8pt] {Il y a donc (forcément) plus d'éléments dans E que dans F !} ;
		\draw (3,1) node [right=8pt] {Ainsi card(E)\,$\mathsf{\pg}$\,card(F) !} ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cdefi}
L'application $f$ est dite \underline{bijective} si elle est à la fois \textit{injective} et \textit{surjective}, c'est-à-dire si chaque élément de F a exactement un antécédent par $f$.
\end{cdefi}

\begin{cmethode}[ModifLabel={ simple}]
Sur un diagramme sagittal, une \textbf{application bijective} se caractérise par le fait que tout élément de l'ensemble d'arrivée est atteint par \textbf{exactement une} \og flèche \fg{} !
\end{cmethode}

\begin{cprop}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Si $f$ est une bijection de $E$ vers $F$, alors $f(E) = F$ et $f^{-1}(F)=E$.
	\item Si $f$ est une bijection de $E$ vers $F$ et si $E$ et $F$ sont des ensembles finis, alors $E$ et $F$ ont même cardinal.
\end{itemize}
\end{cprop}

\begin{cillustr}[ComplementTitre={ - Remarque}]
\begin{center}
	\begin{tikzpicture}[font=\small\sffamily]
		%ENSEMBLES
		\draw[thick] (0.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (0.5,0.15) node[below] {E} ;
		\draw[thick] (2.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (2.5,0.15) node[below] {F} ;
		%ELEMENTS
		\node[inner sep=2pt] (Ea) at (0.5,2.25) {a} ;
		\node[inner sep=2pt] (Eb) at (0.5,1.75) {b} ;
		\node[inner sep=2pt] (Ec) at (0.5,1.25) {c} ;
		\node[inner sep=2pt] (Ed) at (0.5,0.75) {d} ;
		\node[inner sep=2pt] (Fa) at (2.5,2.25) {1} ;
		\node[inner sep=2pt] (Fb) at (2.5,1.75) {2} ;
		\node[inner sep=2pt] (Fc) at (2.5,1.25) {3} ;
		\node[inner sep=2pt] (Fd) at (2.5,0.75) {4} ;
		\draw[purple] (1.5,2.5) node {\textcolor{purple}{$f$}} ;
		%ARCS
		\foreach \deb/\fin in {Ea/Fb,Eb/Fd,Ec/Fa,Ed/Fc} \draw[very thick,purple,->,>=latex] (\deb)--(\fin) ;
		\draw (3,2) node [right=8pt] {Il y a donc autant d'éléments dans E que dans F !} ;
		\draw (3,1) node [right=8pt] {Ainsi card(E)\,$\mathsf{=}$\,card(F) !} ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cdefi}
Si $f$ est une bijection de $E$ vers $F$, alors il existe une unique application de $F$ dans $E$ qui à chaque élément de $F$ associe son unique antécédent dans $E$ par $f$.

Cette application est également bijective, et est appelée \underline{application réciproque} de $f$. Elle est notée $f^{-1}$.
\end{cdefi}

\begin{cillustr}
Avec l'exemple précédent :
\begin{center}
	\begin{tikzpicture}[font=\small\sffamily]
		%ENSEMBLES
		\draw[thick] (0.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (0.5,0.15) node[below] {F} ;
		\draw[thick] (2.5,1.5) circle[x radius=0.5,y radius=1.25] ;
		\draw (2.5,0.15) node[below] {E} ;
		%ELEMENTS
		\node[inner sep=2pt] (Ea) at (2.5,2.25) {a} ;
		\node[inner sep=2pt] (Eb) at (2.5,1.75) {b} ;
		\node[inner sep=2pt] (Ec) at (2.5,1.25) {c} ;
		\node[inner sep=2pt] (Ed) at (2.5,0.75) {d} ;
		\node[inner sep=2pt] (Fa) at (0.5,2.25) {1} ;
		\node[inner sep=2pt] (Fb) at (0.5,1.75) {2} ;
		\node[inner sep=2pt] (Fc) at (0.5,1.25) {3} ;
		\node[inner sep=2pt] (Fd) at (0.5,0.75) {4} ;
		\draw[purple] (1.5,2.5) node {\textcolor{purple}{$f^{-1}$}} ;
		%ARCS
		\foreach \deb/\fin in {Fa/Ec,Fb/Ea,Fc/Ed,Fd/Eb} \draw[very thick,purple,->,>=latex] (\deb)--(\fin) ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\subsection{Composition d'applications}

\begin{cdefi}
Soient $E$, $F$ et $G$ trois ensembles, et soient $f$ une application de $E$ dans $F$ et $g$ une application de $F$ dans $G$.

L'application \underline{composée de $f$ par $g$} est l'application de $E$ dans $G$ qui à tout élément $x$ de $E$ associe $g(f(x))$. Cette application se note $g \circ f$ (lire \og $g$ rond $f$ \fg) : $$\forall x \in E,~g \circ f (x) = g(f(x)).$$
\end{cdefi}

\begin{cillustr}[ComplementTitre={ - Propriétés}]
\begin{center}
	\begin{tikzpicture}[y=0.86cm]
		%ENSEMBLES
		\draw[thick] (1,2) circle[x radius=1,y radius=1.75] ;
		\draw (1,3.75) node[above] {\point{E}} ;
		\draw[thick] (5,1.5) circle[x radius=1,y radius=1] ;
		\draw (5,0.5) node[below] {\point{F}} ;
		\draw[thick] (8.5,2.5) circle[x radius=1.5,y radius=1] ;
		\draw (8.5,3.5) node[above] {\point{G}} ;
		%ELEMENTS
		\node[inner sep=2pt] (Ex) at (1,2.75) {} ;
		\node[inner sep=2pt] (Fx) at (5,1.25) {} ;
		\node[inner sep=2pt] (Gx) at (8.5,2.25) {} ;
		\filldraw[darkgray] (Ex) circle[radius=2pt] node[above,font=\small] {$x$} ;
		\filldraw[darkgray] (Fx) circle[radius=2pt] node[above=3pt,font=\small] {$f(x)$} ;
		\filldraw[darkgray] (Gx) circle[radius=2pt] node[above right=0pt,font=\small] {$g(f(x))$} ;
		%arcs
		\draw[very thick,purple,->,>=latex] (Ex) to[bend right=5] node[midway,below] {\ppoint{\textcolor{purple}{$f$}}} (Fx) ;
		\draw[very thick,purple,->,>=latex] (Fx) to[bend left=5] node[midway,above] {\ppoint{\textcolor{purple}{$g$}}} (Gx) ;
		\draw[very thick,purple,->,>=latex] (Ex) to[bend left=25] node[midway,above] {\ppoint{\textcolor{purple}{$g \circ f$}}} (Gx) ;
	\end{tikzpicture}
\end{center}
\begin{itemize}[leftmargin=*]
	\item La composée de deux injections est une injection.
	\item La composée de deux surjections est une surjection.
	\item La composée de deux bijections est une bijection. De plus $(g \circ f)^{-1} = f^{-1} \circ g^{-1}$.
\end{itemize}
\end{cillustr}

\section{Exemples \og Type BTS \fg}

\begin{cexo}[SousTitre=(Extrait Polynésie 2016)]
Un administrateur réseau a assigné une adresse IP (Internet Protocol) à chaque ordinateur à l'aide d'un
logiciel installé sur le serveur. Il obtient le tableau suivant :

\begin{center}
	\begin{tblr}{stretch=1.2,width=0.9\linewidth,hlines,vlines,colspec={X[m,c]X[m,c]X[m,c]},cells={font=\footnotesize\ttfamily},row{1}={font=\bfseries\footnotesize\ttfamily,bg=lightgray!50}}
		Adresse MAC& N\up*{o} de l'ordinateur &Adresse IP\\
		00:FF:B4:A9:96:11 &1 &172.16.0.21\\
		00:FF:B4:B0:45:1A &2 &172.16.0.22\\
		00:FF:B4:00:C5:DE &3 &172.16.0.23\\
		00:EE:B5:01:32:C4 &4 &172.16.0.24\\
		00:EE:B5:01:32:C5 &5 &172.16.0.25\\
		00:EE:B5:01:32:C6 &6 &172.16.0.26\\
		00:FF:B4:00:C5:DF &7 &172.16.0.27\\
		00:FF:B4:00:02:98 &8 &172.16.0.28\\
		00:EE:B5:01:34:CA &9 &172.16.0.29\\
	\end{tblr}
\end{center}

On considère l'application $f$ qui, à un numéro d'ordinateur, associe la dernière partie de l'adresse IP.

Cette dernière partie est un entier variant de 2 à 255. Par exemple, $f(1) = 21$. \[f :\: \left\lbrace 1 ; 2 ; 3 ; 4 ; 5 ; 6 ; 7 ; 8 ; 9 \rule{0pt}{9pt}\right\rbrace  \longmapsto  \left\lbrace 2 ; 3 ; \ldots ; 255\rule{0pt}{9pt}\right\rbrace.\]
\begin{enumerate}
	\item Justifier le fait que cette application est injective.
	\item Cette application est-elle surjective ? Justifier.
	\item À la suite d'une opération informatique, le poste dont l'adresse MAC est \texttt{00:FF:B4:00:C5:DF} obtient l'adresse IP suivante : \texttt{172.16.0.23}. Les autres postes gardent leur adresse IP précédente.
	
	On a alors une nouvelle application  $g : \:\left\lbrace 1 ; 2 ; 3 ; 4 ; 5 ; 6 ; 7 ; 8 ; 9\rule{0pt}{9pt}\right\rbrace \longmapsto \left\lbrace 2 ; 3 ; \ldots ; 255\rule{0pt}{9pt}\right\rbrace$.
	
	L'application $g$  est-elle injective ? Justifier.
\end{enumerate}
\end{cexo}

\end{document}