% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage{ProfSio}
\useproflyclib{ecritures}
\usepackage{customenvs}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{wrapstuff}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={EXOS~},numdoc={4},titre={Arithmétique},mois=Novembre,annee=2023]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - \DonneesMois{} \DonneesAnnee}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\part{CH04 - Arithmétique - Exercices v2}

\smallskip

\begin{cintro}[Compteur=false]
Il s'agit ici d'exercices de base, afin de maîtriser les compétences et techniques de base.

Les exercices \textit{en situation} sont prévus pour le fiche d'exercices II !
\end{cintro}

\smallskip

\begin{Exercice}[Type=Niveau/0,CodeDebut=\medskip]%exo1
Écrire la division euclidienne de :
%
\begin{MultiCols}[Type=enum](4)
	\item $425$ par $4$ ;
	\item $\num{1001}$ par $9$ ;
	\item $\num{7297}$ par $26$ ;
	\item $\num{427}$ par $401$.
\end{MultiCols}
\end{Exercice}

\vspace*{0.25cm}

\begin{Exercice}[Type=Niveau/1,CodeDebut=\medskip]%exo12
Déterminer, en justifiant, si les entiers suivants sont premiers :
%
\begin{MultiCols}[Type=enum](4)
	\item \num{1789} ;
	\item \num{503} ;
	\item \num{483}.
\end{MultiCols}
\end{Exercice}

\vspace*{0.25cm}

\begin{Exercice}[Type=Niveau/1,CodeDebut=\smallskip]%exo3
\begin{enumerate}
	\item Déterminer la décomposition en produit de facteurs premiers de :
	%
	\begin{MultiCols}[Type=enum](3)
		\item 88 ;
		\item 221 ;
		\item \num{117000}.
	\end{MultiCols}
	\item Déterminer, en détaillant la démarche, la liste des diviseurs de :
	%
	\begin{MultiCols}[Type=enum](3)
		\item 88 ;
		\item 221 ;
		\item 396.
	\end{MultiCols}
\end{enumerate}
\end{Exercice}

\vspace*{0.25cm}

\begin{Exercice}[Type=Niveau/1,CodeDebut=\smallskip]
\begin{enumerate}
	\item Par la méthode de votre choix, déterminer le PGCD des entiers suivants :
	%
	\begin{MultiCols}[Type=enum](3)
		\item 96 et 728 ;
		\item \num{1071} et \num{2200} ;
		\item 657 et 405.
	\end{MultiCols}
	\item Les entiers suivants sont-ils premiers entre eux ? Justifier.
	%
	\begin{MultiCols}[Type=enum](2)
		\item \num{8753} et 147 ;
		\item \num{54865} et \num{44685}.
	\end{MultiCols}
\end{enumerate}
\end{Exercice}

\vspace*{0.25cm}

\begin{Exercice}[Type=Niveau/2,CodeDebut=\smallskip]%exo5
\begin{enumerate}
	\item Compléter les congruences suivantes :
	%
	\begin{MultiCols}[Type=enum](2)
		\item $352 \equiv \ldots \Modulo{7}$ ;
		\item $367 \equiv \ldots \Modulo{15}$ ;
		\item $147 \equiv \ldots \Modulo{8}$ ;
		\item $781 \equiv \ldots \Modulo{10}$ ;
		\item $842 \equiv \ldots \Modulo{5}$ ;
		\item $57 \equiv \ldots \Modulo{13}$.
	\end{MultiCols}
	\item Compléter par le plus petit entier positif :
	%
	\begin{MultiCols}[Type=enum](2)
		\item $120\equiv \ldots \Modulo{7}$ ;
		\item $45\equiv \ldots \Modulo{7}$. 
	\end{MultiCols}
	\item En utilisant les propriétés des congruences et les résultats de la question précédente, compléter :
	%
	\begin{MultiCols}[Type=enum](2)
		\item $240\equiv \ldots \Modulo{7}$ ;
		\item $120^2\equiv \ldots \Modulo{7}$ ;
		\item $120+45\equiv \ldots \Modulo{7}$ ;
		\item $3 \times 120+45\equiv \ldots \Modulo{7}$ ;
		\item $210 \equiv \ldots \Modulo{7}$
		\item $45^2\equiv \ldots \Modulo{7}$. 
	\end{MultiCols}
\end{enumerate}
\end{Exercice}

\vspace*{0.25cm}

\begin{Exercice}[Type=Niveau/3,CodeDebut=\medskip]%exo6
\textit{Les questions suivantes sont indépendantes.}
%
\begin{enumerate}
	\item 
	\begin{enumerate}
		\item Réduire $287$ modulo $11$.
		\item En déduire que $287^{500} \equiv 1 \Modulo{11}$.
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item Simplifier les congruences $39 \equiv \ldots \Modulo{7}$ et $25 \equiv \ldots\Modulo{7}$.
		\item En déduire une réduction modulo $7$ de $39^{200}-25^{200}$, autrement dit $39^{200}-25^{200} \equiv \ldots \Modulo{7}$.
		\item Justifier que  $39^{200}-25^{200}$ est divisible par $7$.
	\end{enumerate}
\end{enumerate}
\end{Exercice}

\end{document}