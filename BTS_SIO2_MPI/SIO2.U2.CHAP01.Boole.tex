% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={CHAPITRE~},numdoc={1},titre={Calcul booléen}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}
%divers

\begin{document}

\usetikzlibrary{shapes.geometric}
\ifdef{\petitkarno}{}%
{%
	\newcommand\petitkarno[2][]{%
		\begin{tikzpicture}[scale=0.5]
			\draw (2,0) node[font=\footnotesize,darkgray,below=0pt,inner sep=1pt] {$\mathtt{\strut#2}$} ;
			#1
			\draw[] (0,0) rectangle (4,2);
			\draw[] (0,1)--(4,1);
			\draw[] (1,0)--(1,2);
			\draw[] (2,0)--(2,2);
			\draw[] (3,0)--(3,2);
		\end{tikzpicture}%
	}
}

\pagestyle{fancy}

\part{CH01 - Calculs booléens}

\section{Algèbre de Boole}

\subsection{Définition}

\begin{cintro}[Compteur=false]
Un phénomène \textbf{binaire} est constitué d'éléments se présentant sous la forme de deux états distincts, notés 1 et 0, qui s'excluent mutuellement. Le cas le plus élémentaire est celui d'un interrupteur d'un circuit électrique, qui peut être ouvert (état 0) ou fermé (état 1).

Cet exemple très simple est pourtant au fondement même de l'informatique (avec les \textbf{transistors}).

Ce type de situation peut être modélisé par l'utilisation de règles logiques qui reviennent à effectuer un calcul dans le cadre d'une certaine structure qu'on appelle, en mathématiques, une \textbf{algèbre de Boole}.
\end{cintro}

\begin{chistoire}[Compteur=false,ComplementTitre={ - Anecdote}]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{Boole}{}\textit{George Boole} (1815/1864, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{GB}\!) enseignait au \textit{Queen's College} à Cork et un jour qu'il faisait le trajet de 3~km à pied depuis son domicile et qu'il pleuvait, il attrapa une pneumonie. Sa femme, pensant qu'un remède doit ressembler à la cause, le mit au lit et lui jeta des bassines d'eau dessus pendant plusieurs jours ! Ce qui eut pour effet d'achever prématurément une carrière brillante\dots
\end{chistoire}

\subsection{Structure d'algèbre de Boole}

\begin{cdefi}
Un ensemble $B$ non vide muni de deux lois de composition interne (addition et multiplication), d'une opération unaire, notée $a \mapsto \overline{a}$, et possédant deux éléments privilégiés, notés 0 et 1, a une structure d'algèbre de Boole si les propriétés suivantes sont vraies : $\forall a \in B$, $\forall b \in B$, $\forall c \in B$,

\begin{itemize}
	\item $(a+b)+c = a+(b+c)$ \tabto{6cm} associativité de $+$
	\item $(ab)c=a(bc)$ \tabto{6cm} associativité de $\times$
	\item $a+b = b+a$ \tabto{6cm} commutativité de $+$
	\item $a \times b = b \times a$ \tabto{6cm} commutativité de $\times$
	\item $a+0=a$ et $a \times 1 = a$ \tabto{6cm} élément neutre de $+$ et $\times$
	\item $a+bc = a+(bc) = (a+b)(a+c)$ \tabto{6cm} distributivité de $+$ sur $\times$
	\item $a (b+c) = (ab + ac)$ \tabto{6cm} distributivité de $\times$ sur $+$
	\item $a+ \overline{a} = 1$ et $a \times \overline{a} = 0$ \tabto{6cm} compléments pour $+$ et $\times$
\end{itemize}
\end{cdefi}

\begin{crmq}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Par convention, l'opération $\times$ a priorité sur l'opération $+$.
	\item On peut noter $ab$ au lieu de $a \times b$.
\end{itemize}
\end{crmq}

\begin{cnota}
On peut noter une analogie entre les opérateurs de calculs booléens et les opérateurs de logiques (propositionnelle ou ensembliste) :

{\renewcommand{\arraystretch}{1.3}\small
\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
		\hline
		Propositions & $\vee$ & $\wedge$ & $\Leftrightarrow$ & $\neg$ & V & F & $P$ & $Q$ \\
		\hline
		Sous-ensembles de E & $\cup$ & $\cap$ & $=$ & $\overline{A}$ & E & $\emptyset$ & $A$ & $B$ \\
		\hline
		Algèbre de Boole & $+$ & $ \times$ & $=$ & $\overline{a}$ & 1 & 0 & $a$ & $b$ \\
		\hline
	\end{tabular}
\end{center}}
\end{cnota}

\section{Propriétés}

\subsection{Calculs et loi}

\begin{cprop}[Pluriel]
Soit $B$ un ensemble muni d'une structure d'algèbre de Boole.

Des axiomes ci-dessus, on peut déduire les propriétés suivantes ; $\forall a \in B$, $\forall b \in B$, $\forall c \in B$,

\smallskip
\begin{minipage}{0.26\linewidth}
	\begin{itemize}
		\item $a+a=a$
		\item $a+1=1$
		\item $a + ab =a$
	\end{itemize}
\end{minipage}
\begin{minipage}{0.26\linewidth}
	\begin{itemize}
		\item $a \ a=a$
		\item $a \times 0=0$
		\item $a (a+b)=a$
	\end{itemize}
\end{minipage}
\begin{minipage}{0.36\linewidth}
	\begin{itemize}
		\item $\overline{\overline{a}} =a$
		\item Loi de De Morgan : $\overline{ab} = \overline{a}+\overline{b}$
		\item Loi de De Morgan : $\overline{a+b} = \overline{a} \overline{b}$.
	\end{itemize}
\end{minipage}
\end{cprop}

\subsection{Lien avec les tables de vérité}

\begin{crmq}
Les variables considérées dans une algèbre de Boole ne peuvent prendre que deux valeurs : 0 ou 1.

On peut donc utiliser une table pour illustrer (ou démontrer) les propriétés.

\begin{center}
	\begin{tblr}{width=4.5cm,hlines,vlines,colspec={X[m,c]X[m,c]X[m,c]},row{1}={bg=gray!25}}
		$a$ & $b$ & $a+b$ \\
		0 & 0 & 0 \\
		0 & 1 & 1 \\
		1 & 0 & 1 \\
		1 & 1 & 1 \\
	\end{tblr}
	\hspace{1cm}
	\begin{tblr}{width=4.5cm,hlines,vlines,colspec={X[m,c]X[m,c]X[m,c]},row{1}={bg=gray!25}}
		$a$ & $b$ & $ab$ \\
		0 & 0 & 0 \\
		0 & 1 & 0 \\
		1 & 0 & 0 \\
		1 & 1 & 1 \\
	\end{tblr}
	\hspace{1cm}
	\begin{tblr}{width=3cm,hlines,vlines,colspec={X[m,c]X[m,c]},row{1}={bg=gray!25}}
		$a$ & $\overline{a}$ \\
		0 & 1 \\
		1 & 0 \\
	\end{tblr}
\end{center}
\end{crmq}

\begin{crmq}[Pluriel]
On visualise immédiatement que l'addition correspond à OU, la multiplication à ET et la complémentation à NON

\smallskip

Dans une expression, la complémentation est prioritaire sur la multiplication, qui est elle même prioritaire sur l'addition
\end{crmq}

\begin{cexemple}
Soit l'expression $a+\overline{b}c$ pour $a=0$, $b=1$ et $c=0$ :

\hspace{5mm}{}$0+1 \times \overline{0} = 0+1 \times 1 = 0+1=1$
\end{cexemple}

\begin{cdemo}[Compteur=false,ComplementTitre={ - Lois de De Morgan}]
En utilisant une table de vérité :

\begin{center}
	\begin{tblr}{width=10.cm,hlines,vlines,colspec={*{7}{X[m,c]}},row{1}={bg=gray!25},cell{2-Z}{4,7}={bg=cyan!25}}
		$a$ & $b$ & $a+b$ & $\overline{a+b}$ & $\overline{a}$ & $\overline{b}$ & $\overline{a}\overline{b}$\\
		0&0&0&1&1&1&$1$ \\
		0&1&1&0&1&0&$0$ \\
		1&0&1&0&0&1&$0$ \\
		1&1&1&0&0&0&$0$ \\
	\end{tblr}
\end{center}

\end{cdemo}

\subsection{Tableaux de Karnaugh}

\begin{cdefi}[Compteur=false]
Les tables de Karnaugh (prononcer : \textsf{karno}) sont des représentations graphiques astucieuses qui facilitent grandement l'utilisation pratique des règles de calcul dans une algèbre de Boole, en permettant de visualiser des expressions booléennes. Conformément au programme, on se limitera aux cas de deux et trois variables
\end{cdefi}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{karnaugh}{}Maurice Karnaugh (1925 --, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{US}\!) est un spécialiste de physique et mathématique (algèbre de Boole), il développe son célèbre diagramme en 1954 alors qu'il travaillait aux \textit{Bell Labs}. Il a ensuite travaillé chez \textit{IBM} (1966-1989).
\end{chistoire}

\begin{cmethode}[s]
\begin{tikzpicture}
	\tikzset{kar/.style={blue,font=\small}}
	\draw[thick] (0,0) rectangle (2,2);
	\draw[thick] (0,1)--(2,1);
	\draw[thick] (1,0)--(1,2);
	\node at (-0.33,0.5) {$\strut\mathtt{\overline{a}}$};
	\node at (-0.33,1.5) {$\strut\mathtt{a}$};
	\node at (0.5,2.33) {$\strut\mathtt{b}$};
	\node at (1.5,2.33) {$\strut\mathtt{\overline{b}}$};
	%\KarnaughD
	\draw[kar] (0.5,0.5) node {$\strut\overline{\tt a}\,{\tt b}$} ;
	\draw[kar] (1.5,0.5) node {$\strut\overline{\tt a}\,\overline{\tt b}$} ;
	\draw[kar] (0.5,1.5) node {$\strut{\tt a}\,{\tt b}$} ;
	\draw[kar] (1.5,1.5) node {$\strut{\tt a}\,\overline{\tt b}$} ;
	\draw (1,3) node[font=\small\sffamily] {Tableau de Karnaugh à deux variables } ;
	\node at (2,-0.54) {$\vphantom{\strut \overline{\tt c}}$};
\end{tikzpicture}
\hfill
\begin{tikzpicture}
	\tikzset{kar/.style={blue,font=\small}}
	%	NŒUDS
	\coordinate (A00) at (0,0) ;
	\coordinate (A10) at (1,0) ;
	\coordinate (A20) at (2,0) ;
	\coordinate (A30) at (3,0) ;
	\coordinate (A40) at (4,0) ;
	\coordinate (A01) at (0,1) ;
	\coordinate (A11) at (1,1) ;
	\coordinate (A21) at (2,1) ;
	\coordinate (A31) at (3,1) ;
	\coordinate (A41) at (4,1) ;
	\coordinate (A02) at (0,2) ;
	\coordinate (A12) at (1,2) ;
	\coordinate (A22) at (2,2) ;
	\coordinate (A32) at (3,2) ;
	\coordinate (A42) at (4,2) ;
	%	rectangles
	\draw[thick] (0,0) rectangle (4,2);
	\draw[thick] (0,1)--(4,1);
	\draw[thick] (1,0)--(1,2);
	\draw[thick] (2,0)--(2,2);
	\draw[thick] (3,0)--(3,2);
	\draw[thick,decorate,decoration={brace,amplitude=5pt}](0,2.05)--(2,2.05) ;
	\draw[thick,decorate,decoration={brace,amplitude=5pt}](2,2.05)--(4,2.05) ;
	\draw[thick,decorate,decoration={brace,amplitude=5pt,mirror}](1,-0.05)--(3,-0.05) ;
	\node at (-0.33,0.5) {$\strut\mathtt{\overline{a}}$};
	\node at (-0.33,1.5) {$\strut\mathtt{a}$};
	\node at (1,2.5) {$\strut\mathtt{b}$};
	\node at (3,2.5) {$\strut\mathtt{\overline{b}}$};
	\node at (0.5,-0.54) {$\strut\mathtt{c}$};
	\node at (3.5,-0.54) {$\strut\mathtt{c}$};
	\node at (2,-0.54) {$\strut\mathtt{\overline{c}}$};
	%\KarnaughDefT
	\draw[kar] (0.5,0.5) node {$\strut\overline{\tt a}\,{\tt b}\,{\tt c}$} ;
	\draw[kar] (1.5,0.5) node {$\strut\overline{\tt a}\,{\tt b}\,\overline{\tt c}$} ;
	\draw[kar] (2.5,0.5) node {$\strut\overline{\tt a}\,\overline{\tt b}\,\overline{\tt c}$} ;
	\draw[kar] (3.5,0.5) node {$\strut\overline{\tt a}\,\overline{\tt b}\,{\tt c}$} ;
	\draw[kar] (0.5,1.5) node {$\strut{\tt a}\,{\tt b}\,{\tt c}$} ;
	\draw[kar] (1.5,1.5) node {$\strut{\tt a}\,{\tt b}\,\overline{\tt c}$} ;
	\draw[kar] (2.5,1.5) node {$\strut{\tt a}\,\overline{\tt b}\,\overline{\tt c}$} ;
	\draw[kar] (3.5,1.5) node {$\strut{\tt a}\,\overline{\tt b}\,{\tt c}$} ;
	\draw (2,3) node[font=\small\sffamily] {Tableau de Karnaugh à trois variables} ;
	%cylindre explicatif
	\draw (5.25,1) node {équivalent à} ;
	\def\lgt{0.9} \def\hellips{0.2} \def\centre{7.5} \def\angl{7.5}
	\draw[thick] ({\centre-\lgt},0) -- ({\centre-\lgt},2) ;
	\draw[thick] ({\centre+\lgt},0) -- ({\centre+\lgt},2);
	\draw[thick] (\centre,{0-\hellips}) -- (\centre,{2-\hellips});
	\draw[thick] ({\centre-\lgt},0) arc (180:360:{\lgt} and \hellips) ;
	\draw[thick] (\centre,2) circle[x radius=\lgt,y radius=\hellips] ;
	\draw[thick] ({\centre-\lgt},1) arc (180:360:{\lgt} and \hellips) ;
	\draw[thick] ({\centre-\lgt+0.1},-0.1) -- ({\centre-\lgt+0.1},1.9);
	\draw[thick] ({\centre+\lgt-0.1},-0.1) -- ({\centre+\lgt-0.1},1.9);
	\draw[kar] ({\centre-0.45*\lgt},0.33) node[rotate=-\angl] {$\strut\overline{\tt a}\,\overline{\tt b}\,{\tt c}$} ;
	\draw[kar] ({\centre+0.45*\lgt},0.33) node[rotate=\angl] {$\strut\overline{\tt a}\,{\tt b}\,{\tt c}$} ;
	\draw[kar] ({\centre-0.45*\lgt},1.33) node[rotate=-\angl] {$\strut{\tt a}\,\overline{\tt b}\,{\tt c}$} ;
	\draw[kar] ({\centre+0.45*\lgt},1.33) node[rotate=\angl] {$\strut{\tt a}\,{\tt b}\,{\tt c}$} ;
\end{tikzpicture}
%\vspace{-0.25cm}

Il faut imaginer le tableau de Karnaugh à trois variables comme un \og cylindre \fg{}, avec les 2 \og colonnes {\ttfamily c} qui se touchent \fg.
\end{cmethode}

\begin{cidee}[Compteur=false,ComplementTitre={ générale}]
L'idée d'un tableau de Karnaugh est :
\begin{itemize}
	\item de \og colorier \fg{} les cases relatives à une expression booléenne ;
	\item de \og regrouper \fg{} les cases coloriées (avec \textit{superposition} possible) en \textit{blocs} :
	\begin{itemize}
		\item de 4 cases contiguës qui donnent \textbf{1} variable ;
		%
		\begin{center}
			\petitkarno[{\filldraw[lightgray] (0,1) rectangle (4,2);}]{a}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (0,0) rectangle (4,1);}]{\overline{a}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (0,0) rectangle (2,2);}]{b}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (2,0) rectangle (4,2);}]{\overline{b}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (1,0) rectangle (3,2);}]{c}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (0,0) rectangle (1,2); \filldraw[lightgray] (3,0) rectangle (4,2);}]{\overline{c}}
		\end{center}
		\vspace{-0.25cm}
		\item de 2 cases contigües qui donnent \textbf{2} variables ;
		%
		\begin{center}
			\petitkarno[{\filldraw[lightgray] (0,1) rectangle (2,2);}]{a\,b}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (1,1) rectangle (3,2);}]{a\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (2,1) rectangle (4,2);}]{a\,\overline{b}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (0,0) rectangle (2,1);}]{\overline{a}\,b}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (1,0) rectangle (3,1);}]{\overline{a}\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (2,0) rectangle (4,1);}]{\overline{a}\,\overline{b}}%
			
			\smallskip
			
			\petitkarno[{\filldraw[lightgray] (0,1) rectangle (1,2);\filldraw[lightgray] (3,1) rectangle (4,2);}]{a\,c}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (0,0) rectangle (1,1);\filldraw[lightgray] (3,0) rectangle (4,1);}]{\overline{a}\,c}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (0,0) rectangle (1,2);}]{b\,c}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (1,0) rectangle (2,2);}]{b\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (2,0) rectangle (3,2);}]{\overline{b}\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (3,0) rectangle (4,2);}]{\overline{b}\,c}
		\end{center}
		\vspace{-0.25cm}
		\item les \og cases isolées \fg{} correspondent à \textbf{3} variables :
		%
		\begin{center}
			\petitkarno[{\filldraw[lightgray] (0,1) rectangle (1,2);}]{a\,b\,c}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (1,1) rectangle (2,2);}]{a\,b\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (2,1) rectangle (3,2);}]{a\,\overline{b}\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (3,1) rectangle (4,2);}]{a\,\overline{b}\,c}
			
			\smallskip
			
			\petitkarno[{\filldraw[lightgray] (0,0) rectangle (1,1);}]{\overline{a}\,b\,c}
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (1,0) rectangle (2,1);}]{\overline{a}\,b\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (2,0) rectangle (3,1);}]{\overline{a}\,\overline{b}\,\overline{c}}%
			\hspace{5mm}%
			\petitkarno[{\filldraw[lightgray] (3,0) rectangle (4,1);}]{\overline{a}\,\overline{b}\,c}
		\end{center}
	\end{itemize}
\end{itemize}
\end{cidee}

%\begin{cexemple}
%Ici le ''ou'' est traduit par l'addition et le ''et'' est traduit par la multiplication.
%
%\smallskip
%
%Une société de distribution de gaz décide de recruter en interne des collaborateurs pour sa filiale en Extrême-Orient.
%Pour chaque employé, on définit les variables booléennes suivantes :
%\begin{itemize}
%	\item $a=1$ si l'employé a plus de cinq ans d'ancienneté dans l'entreprise, et $a=0$ sinon,
%	\item $b=1$ s'il possède un BTS Service Informatique aux Organisations (BTS SIO), et $b=0$ sinon,
%	\item $c=1$ s'il parle couramment l'anglais, et $c=0$ sinon. 
%\end{itemize}
%La direction des ressources humaines décide que pourront postuler les employés :
%\begin{itemize}
%	\item qui satisfont aux trois conditions,
%	\item ou qui ont moins de 5 ans d'ancienneté mais qui maîtrisent l'anglais,
%	\item ou qui ne maîtrisent pas l'anglais mais qui possèdent un BTS SIO.
%\end{itemize}
%\begin{enumerate}[leftmargin=*,noitemsep]
%	\item on écrit une expression booléenne E traduisant les conditions de la direction ;
%	\item on représente la situation dans un tableau de Karnaugh ;
%	\item à l'aide du tableau de Karnaugh, on donne une expression simplifiée de $E$ (on fait les regroupements les plus gros possibles dans le tableau) ;
%	\item on déduit du résultat précédent une version simplifiée des règles de la direction ;
%	\item on retrouve ce résultat par le calcul booléen. 
%\end{enumerate}
%\end{cexemple}

\end{document}