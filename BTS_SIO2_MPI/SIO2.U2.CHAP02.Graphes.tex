% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={CHAPITRE~},numdoc={2},titre={Graphes}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}
%divers

\begin{document}

\pagestyle{fancy}

\part{CH02 - Graphes}

\section{Exemples}

\begin{cexemple}[Compteur=false]
On peut commencer par le problème des \og sept ponts de Königsberg \fg{} (exemple historique lié à Euler) :
\begin{center}
	\includegraphics[scale=0.5]{graphics/Konigsberg_bridges}
\end{center}
La ville de Königsberg (aujourd'hui Kaliningrad) est construite autour de deux îles situées sur le Pregel et reliées entre elles par un pont. Six autres ponts relient les rives de la rivière à l'une ou l'autre des deux îles, comme représentés sur le plan ci-dessus. Le problème consiste à déterminer s'il existe ou non une promenade dans les rues de Königsberg permettant, à partir d'un point de départ au choix, de passer une et une seule fois par chaque pont, et de revenir à son point de départ, étant entendu qu'on ne peut traverser le Pregel qu'en passant sur les ponts.
\end{cexemple}

\begin{chistoire}[Compteur=false,ComplementTitre={ - Anecdote}]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{euler}{}\textit{Leonhard Euler} (1707/1783, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{CH}\!) est considéré par certains comme le plus grand mathématicien de tous les temps. Terriblement prolifique, il publie plus de 800 pages par an !! Doté d'une mémoire fabuleuse, il calcula une nuit d'insomnie les puissances 6\up{ème} de tous les entiers de 1 à 100 et s'en souvint plusieurs jours plus tard. Une fièvre brutale lui fit perdre son œil droit.

À la cour de Berlin, Frédéric le Grand, préférant les esprits brillants comme Voltaire aux scientifiques efficaces, le traite de \og cyclope mathématique \fg. Euler a résolu ce problème des \og sept ponts de Königsberg \fg{} en 1735.
\end{chistoire}

\begin{cexemple}
Dans une ville, on considère 4 carrefours A, B, C, D reliés par des rues à sens unique ou à double sens. On a les renseignement suivants : 
\begin{itemize}
	\item une rue à sens unique va de A à B ;
	\item une rue à sens unique va de A à C;
	\item une rue à sens unique va de C à D ;
	\item une rue à sens unique va de D à D sans passer par A, B ou C ;
	\item une rue en double sens relie A et D ;
	\item une rue en double sens relie B et C.
\end{itemize}
On peut représenter cette situation par :
\begin{itemize}[leftmargin=*]
	\item un graphique ;
	\item un tableau des successeurs ;
	\item un tableau de prédécesseurs ;
	\item un tableau à double entrée ;
	\item une matrice d'adjacence.
\end{itemize}
\end{cexemple}

\section{Définitions, vocabulaire}

\subsection{Graphe}

\begin{cdefi}[Pluriel]
Un \textbf{graphe} est un ensemble de points appelé sommets et de liaisons appelées arcs. (Dans notre étude, ces arcs seront simples et orientés. Ils pourront être pondérés)

\smallskip

Une \textbf{boucle} est un arc dont les extrémités sont confondues

\smallskip

Un \textbf{chemin} est une suite d'arcs tels que chaque fin d'arc est le début de l'arc suivant (s'il y en a un).

\smallskip

Un \textbf{circuit} est un chemin dont le premier et le dernier sommet sont identiques.

\smallskip

Un \textbf{chemin hamiltonien} est un chemin qui passe une fois et une seule par chacun des sommets du graphe.
\end{cdefi}

\begin{cexemple}
L'exemple introductif peut-être représenté par le graphe $\mathscr{G}$ :
\begin{center}
	\tikzset{sommet/.style={draw,circle,thick,minimum size=12pt,font=\large\sffamily}}
	\tikzset{arete/.style={thick}}
	\begin{tikzpicture}[scale=0.9]
		\node[sommet] (A) at (0,3) {A};
		\node[sommet] (B) at (3,3) {B};
		\node[sommet] (C) at (3,0) {C};
		\node[sommet] (D) at (0,0) {D};
		\draw[arete,->,>=Stealth] (A) -- (B) ;
		\draw[arete,->,>=Stealth] (A) -- (C) ;
		\draw[arete,->,>=Stealth] (C) -- (D) ;
		\draw[arete,->,>=Stealth] (D) to [out=-90,in=-180,loop,looseness=6] (D) ;
		\draw[arete,->,>=Stealth] (A) to [bend left=15] (D) ;
		\draw[arete,->,>=Stealth] (D) to [bend left=15] (A) ;
		\draw[arete,->,>=Stealth] (B) to [bend left=15] (C) ;
		\draw[arete,->,>=Stealth] (C) to [bend left=15] (B) ;
	\end{tikzpicture}
\end{center}
\vspace{-0.75cm}
\end{cexemple}

\subsection{Matrice d'adjacence}

\begin{cdefi}
Soit $\mathscr{G}$ un graphe dont les sommets sont $A_1$, $A_2$, \dots, $A_n$

La matrice d'adjacence du graphe $\mathscr{G}$ est la matrice carrée d'ordre $n$, notée $M=\left( a_{ij} \right)$ telle que $$a_{ij}=\begin{dcases} 1 \text{ si } A_i-A_j \text{ est un arc du graphe} \\ 0 \text{ sinon} \end{dcases}$$
\end{cdefi}

\begin{cexemple}
La matrice M du graphe de l'exemple introductif est : \[M = \bordermatrix{~&\textsf{\footnotesize A}&\textsf{\footnotesize B}&\textsf{\footnotesize C}&\textsf{\footnotesize D}\cr \textsf{\footnotesize A}&0&1&1&1\cr \textsf{\footnotesize B}&0&0&1&0 \cr \textsf{\footnotesize C}&0&1&0&1 \cr \textsf{\footnotesize D}&1&0&0&1\cr}\]

\end{cexemple}

\begin{crmq}[Pluriel]
Pour interpréter plus facilement les matrices, il est commode de \og border \fg{} la matrice avec les sommets.

\smallskip

Il faut pas oublier le \og sens de lecture \fg{} de la matrice d'adjacence :
\begin{itemize}
	\item les arcs existants vont des \textsf{lignes} vers les \textsf{colonnes} ;
	\item une ligne donne les \textsf{successeurs} ;
	\item une colonne donne les \textsf{prédécesseurs} ;
	\item la diagonale donne les \textsf{boucles}.
\end{itemize}
\end{crmq}

\newpage

\section{Applications}

\subsection{Longueur d'un chemin}

\begin{cdefi}
La \textbf{longueur} d'un chemin est le nombre d'arcs qui le constituent.

Un chemin de $n$ sommets est de longueur $n-1$.
\end{cdefi}

\begin{cprop}
Si M est la matrice d'adjacence d'un graphe simple orienté de sommets $A_1$, $A_2$, \dots, $A_n$, le nombre de chemins de longueur $p$ d'un sommet $A_i$  à un sommet $A_j$ est le nombre situé ligne $i$ et colonne $j$ dans la matrice $M^p$.
\end{cprop}

\begin{cexemple}
Pour le graphe donné en exemple introductif, on a, par exemple, \[M^{\mathcolor{red}{3}} = \bordermatrix{~&\textsf{\footnotesize A}&\textsf{\footnotesize B}&\textsf{\footnotesize C}&\textsf{\footnotesize D}\cr \textsf{\footnotesize A}&2&2&2&4\cr \textsf{\footnotesize B}&1&0&1&1 \cr \textsf{\footnotesize C}&1&\mathcolor{blue}{2}&1&3 \cr \textsf{\footnotesize D}&2&2&2&4\cr}\]
%
De ce fait, on peut affirmer qu'il y a \textcolor{blue}{2} chemins de longueur \textcolor{red}{3} allant de \textsf{C} à \textsf{B} :

\hspace{5mm}$\bullet~~\mathsf{C-B-C-B}$ ;

\hspace{5mm}$\bullet~~\mathsf{C-D-A-B}$.

\begin{center}
	\includegraphics[height=2.5cm]{chap02_calc_a}~~\includegraphics[height=2.5cm]{chap02_calc_b}
\end{center}
\end{cexemple}

\begin{cpython}[Compteur=false]
En \calgpython{}, on peut -- grâce au module \cpy{numpy} -- calculer le nombre de chemins de longueur donnée :

\begin{CodePiton}[Filigrane,Largeur=\linewidth]{}
import numpy as np

def nb_chemins(coeffs,puiss,deb,fin) :
	M=np.mat(coeffs)**puiss
	res = M.item((deb-1,fin-1))
	print(M)
	print(f"Il y a {res} chemin(s) de lg {puiss} allant du som num {deb} au som num {fin}")
\end{CodePiton}
%
\begin{python}
import numpy as np
def nb_chemins(coeffs,puiss,deb,fin) :
	M=np.mat(coeffs)**puiss
	res = M.item((deb-1,fin-1))
	print(M)
	print(f"Il y a {res} chemin(s) de longueur {puiss} allant du sommet num {deb} au sommet num {fin}")
\end{python}

\begin{ConsolePiton}<Largeur=\linewidth,Alignement=center>{}
nb_chemins('[0 1 1 1 ; 0 0 1 0 ; 0 1 0 1 ; 1 0 0 1]',3,3,2)
\end{ConsolePiton}
\end{cpython}

\begin{cdefi}
On note $M^{\left[n\right]}$  la $n$-ième puissance booléenne de la matrice M.

Elle se calcule comme $M^n$ en utilisant l'addition et la multiplication booléenne.
\end{cdefi}

\begin{cprop}
Il existe au-moins un chemin de longueur $p$ du sommet $A_i$ au sommet $A_j$ ssi le coefficient $(ij)$ de la matrice $M^{\left[p\right]}$ est égale à 1.
\end{cprop}

\subsection{Fermeture transitive}

\begin{cdefi}
La fermeture transitive du graphe $\mathscr{G}$ est le graphe $\widehat{\mathscr{G}}$ constitué des sommets et des arcs de $\mathscr{G}$ auxquels on ajoute, si nécessaire, les arcs $(A;B)$ pour lesquels il existe un chemin (de longueur quelconque) allant du sommet A au sommet B.
\end{cdefi}

\begin{cthm}
La matrice $\widehat{M}$ de la fermeture transitive d'un graphe $\mathscr{G}$ à $n$ sommets est donnée par : \[\widehat{M} = M \oplus M^{\left[2\right]} \oplus M^{\left[3\right]} \oplus \dots \oplus M^{\left[n\right]}\] avec $\oplus$ l'addition booléenne et $[i]$ la puissance booléenne.

On dit que $\widehat{M}$ est la matrice d'accessibilité du graphe $\mathscr{G}$
\end{cthm}

\begin{cexemple}
Pour le graphe donné en exemple introductif, on a \[M+M^2+M^3+M^4 = \bordermatrix{~&\textsf{\footnotesize A}&\textsf{\footnotesize B}&\textsf{\footnotesize C}&\textsf{\footnotesize D}\cr \textsf{\footnotesize A}&7&8&8&15\cr \textsf{\footnotesize B}&2&3&3&5 \cr \textsf{\footnotesize C}&5&5&5&10 \cr \textsf{\footnotesize D}&8&7&7&15\cr}.\]
%
De ce fait, on obtient $\widehat{M}=\bordermatrix{~&\textsf{\footnotesize A}&\textsf{\footnotesize B}&\textsf{\footnotesize C}&\textsf{\footnotesize D}\cr \textsf{\footnotesize A}&\mathcolor{red}{1}&1&1&1\cr \textsf{\footnotesize B}&\mathcolor{red}{1}&\mathcolor{red}{1}&1&\mathcolor{red}{1} \cr \textsf{\footnotesize C}&\mathcolor{red}{1}&1&\mathcolor{red}{1}&1 \cr \textsf{\footnotesize D}&1&\mathcolor{red}{1}&\mathcolor{red}{1}&1\cr}$.

Concrètement, de chacun des sommets, on peut aller à tous les autres.
\begin{center}
	\includegraphics[height=2.5cm]{chap02_calc_ferm_a}~~\includegraphics[height=2.5cm]{chap02_calc_ferm_b}
\end{center}
%
Pour construire le graphe $\widehat{\mathscr{G}}$, on complète le graphe $\mathscr{G}$ avec les \textcolor{red}{nouvelles arêtes venant des 1 de} $\mathcolor{red}{\widehat{M}}$ :
\vspace{-0.75cm}
\begin{center}
	\tikzset{sommet/.style={draw,circle,thick,minimum size=12pt,font=\large\sffamily}}
	\tikzset{arete/.style={thick,->,>=Stealth}}
	\tikzset{ferm/.style={red,thick,->,>=Stealth}}
	\begin{tikzpicture}[scale=0.9]
		\node[sommet] (A) at (0,3) {A};
		\node[sommet] (B) at (3,3) {B};
		\node[sommet] (C) at (3,0) {C};
		\node[sommet] (D) at (0,0) {D};
		\draw[arete] (A) to [bend left=15] (B) ;
		\draw[arete] (A) to [bend left=15] (C) ;
		\draw[arete] (C) to [bend left=15] (D) ;
		\draw[arete] (D) to [out=-90,in=-180,loop,looseness=6] (D) ;
		\draw[arete] (A) to [bend left=15] (D) ;
		\draw[arete] (D) to [bend left=15] (A) ;
		\draw[arete] (B) to [bend left=15] (C) ;
		\draw[arete] (C) to [bend left=15] (B) ;
		%fermeture transitive
		\draw[ferm] (A) to [out=90,in=180,loop,looseness=6] (A) ;
		\draw[ferm] (B) to [bend left=15] (A) ;
		\draw[ferm] (B) to [out=90,in=0,loop,looseness=6] (B) ;
		\draw[ferm] (B) to [bend left=15] (D) ;
		\draw[ferm] (C) to [bend left=15] (A) ;
		\draw[ferm] (C) to [out=-90,in=0,loop,looseness=6] (C) ;
		\draw[ferm] (D) to [bend left=15] (B) ;
	\end{tikzpicture}
\end{center}
\vspace{-0.75cm}
\end{cexemple}

\begin{cpython}[Compteur=false]
En \calgpython{}, on peut -- grâce au module \cpy{numpy} -- calculer (sans la passer en booléen\ldots) la :

\begin{CodePiton}[Style=Classique,Filigrane,Largeur=16cm]{}
def fermeture_transitive(coeffs) :
	M=np.mat(coeffs)
	nb = M.shape[1]
	for i in range(2,nb+1) :
		M += np.mat(coeffs)**i
	return M
\end{CodePiton}
%
\begin{python}
import numpy as np

def fermeture_transitive(coeffs) :
	M=np.mat(coeffs)
	nb = M.shape[1]
	for i in range(2,nb+1) :
		M += np.mat(coeffs)**i
	return M
	

\end{python}

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
FT = fermeture_transitive('[0 1 1 1 ; 0 0 1 0 ; 0 1 0 1 ; 1 0 0 1]')
FT
\end{ConsolePiton}
\end{cpython}

\subsection{Circuits}

\begin{cthm}[Pluriel]
Si chaque sommet du graphe possède au-moins un prédécesseur, alors le graphe contient un circuit.

Donc, si chaque colonne de la mat. d'adj. contient au-moins une fois \og 1 \fg{}, alors il y a un circuit dans le graphe.

\smallskip

De même, si chaque sommet possède au-moins un successeur, alors le graphe contient un circuit.

Donc, si chaque ligne de la mat. d'adj. contient au-moins une fois \og 1 \fg{}, alors il y a un circuit dans le graphe.
\end{cthm}

\begin{cthm}
Le graphe $\mathscr{G}$ est sans circuit si et seulement si sa fermeture transitive $\widehat{\mathscr{G}}$ n'a pas de boucle, c'est-à-dire si et seulement si la diagonale de $\widehat{M}$ ne contient que des 0.
\end{cthm}

\section{Compléments}

\subsection{Niveau d'un sommet}

\begin{cdefi}[ComplementTitre={ - Méthode}]
Dans un graphe sans circuit, le niveau d'un sommet $X$ est la longueur du plus long chemin d'extrémité $X$.

\smallskip

On peut représenter un graphe ordonné par niveaux, avec comme effet de n'avoir aucun arc entre deux sommets d'un même niveau (autrement dit aucun arc \og vertical \fg) !
\end{cdefi}

\begin{cmethode}
Pour déterminer le niveau des sommets d'un graphe sans circuit :
\begin{itemize}[itemsep=2pt]
	\item $N_0$ est constitué des sommets sans prédécesseur ;
	\item on \og barre \fg{} les sommets de $N_0$ ;
	\item $N_1$ est constitué des (nouveaux) sommets sans prédécesseur ;
	\item on \og barre \fg{} les sommets de $N_1$ ;
	\item $N_2$ est constitué des (nouveaux) sommets sans prédécesseur ;
	\item etc
\end{itemize}
On peut ensuite représenter le graphe, de manière \og \textit{horizontale} \fg, de sorte que les arcs iront \textbf{toujours} de la gauche vers la droite : \hspace{2cm}$N_0$\hspace{2cm}$N_1$\hspace{2cm}$N_2$\hspace{2cm}etc
\end{cmethode}

\subsection{Graphes valués}

\begin{cdefi}
Un graphe \textbf{valué} (ou \textbf{pondéré}) est un graphe pour lequel on a affecté une valeur à chacun des arcs.
\end{cdefi}

\begin{cprop}
La \textbf{valeur} d'un chemin est égale à la somme des valeurs des arcs de ce chemin 
\end{cprop}

\begin{cdefi}[ComplementTitre={ - Propriété}]
Un chemin \textbf{optimal} est un chemin de valeur (ou poids) minimale ou maximale d'une sommet à l'autre.

On peut utiliser l'algorithme de \textbf{Dijkstra} ou le \textbf{marquage de Ford} pour déterminer une plus courte chaîne entre deux sommets.
\end{cdefi}

\subsection{Arborescence}

\begin{cdefi}
Une arborescence est un graphe qui possède un sommet et un seul nommé \textbf{racine} tel que tout sommet peut être atteint par un chemin et un seul issu de cette racine.

\smallskip

On peut utiliser une arborescence pour : des arbres généalogiques, des arbres de probas, hierarchiques, etc
\end{cdefi}

\subsection{Le Pagerank}

\begin{cidee}[Compteur=false]
Le web peut être vu comme un graphe (orienté) où les sommets sont les pages web et les arcs sont les hyperliens entre les pages.

L’intuition qui se cache derrière l’algorithme \textit{PageRank}, de Google, est que la présence d’un lien vers une certaine page constitue un « vote » en faveur de cette page. L'algorithme \textit{PageRank} utilise donc des matrices carrées \textit{gigantesques} pour \og prédire \fg{} la popularité d'une page web.

\begin{center}
	\includegraphics[height=6cm]{pagerank.png}
\end{center}
\end{cidee}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{pagebrin}{}L’idée de PageRank est officiellement présentée pour la première fois en 1998 par Sergey Brin et Larry Page, les fondateurs de Google, dans « The Anatomy of a Large-Scale Hypertexual Web Search Engine ».

Le premier brevet, \textit{Method for Node Ranking in a Linked Database}, est cependant déposé dès janvier 1997 avant d’être enregistré le 9 janvier 1998. Il est d’abord la propriété de l'Université Stanford, qui octroie ensuite la licence à Google la même année (amendée en 2000 et 2003), deux mois après sa fondation.
\end{chistoire}

\end{document}