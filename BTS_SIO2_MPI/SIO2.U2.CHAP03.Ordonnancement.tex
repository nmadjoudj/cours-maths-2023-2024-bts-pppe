% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage{ProfSio}
\useproflyclib{piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={CHAPITRE~},numdoc={3},titre={Ordonnancement}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}
%divers

\begin{document}

\NewDocumentCommand\TacheSimpleMPM{ m m m }{%
	\node[inner sep=0pt] (#1) at (#2) {\begin{tblr}{hlines={0.75pt},vlines={0.75pt},colspec={Q[0.75cm,m,c]},cells={font=\footnotesize\sffamily}}#3\end{tblr}} ;
}
\NewDocumentCommand\ArcSimpleMPM{ m m m }{%
	\draw[blue,line width=0.75pt,->,>=latex] (#1) -- (#2) node[midway,inner sep=2pt,font=\footnotesize\sffamily,purple,fill=white] {#3} ;
}

\pagestyle{fancy}

\part{CH03 - Ordonnancement}\index{MPM}

\section{Méthode MPM}

\subsection{Intro}

\begin{cintro}[Compteur=false]
La réalisation d'un projet passe souvent par l'exécution de différentes tâches. Certaines de ces tâches peuvent être réalisées simultanément, mais d'autres nécessitent d'être réalisées dans un ordre précis.\\
Faire l'\uline{ordonnancement} d'un projet consiste à organiser ce projet en respectant les contraintes d'antériorité des tâches, tout en minimisant la durée totale de réalisation.
\end{cintro}

\begin{cexemple}[Compteur=false]
On souhaite accomplir les tâches A, B, C, D, E, F et G ; on donne la durée de chacune, ainsi que les tâches qui doivent être réalisées auparavant : 
\begin{center}
	\begin{tblr}{width=14cm,hlines,vlines,colspec={Q[m,l]*{7}{X[m,c]}},stretch=1.1}
		Tâches & A & B & C & D & E & F & G \\
		Tâche(s) antérieure(s) & - & - & A & A,B & C & C,D & E,F \\
		Durée des tâches (en h) &3 &2 &4 &5 &3 &4 &3 \\
	\end{tblr}
\end{center}
On lit par exemple que la tâche E, qui dure 3 heures, ne peut être réalisée que si C l'est aussi. 
\end{cexemple}

\begin{crmq}[Compteur=false]
Le programme de BTS donne le choix entre deux méthode d'ordonnancement : le méthode \textbf{PERT} et la méthode \textbf{MPM}. La méthode présentée ici est la méthode MPM (Méthode des Potentiels Métra).
\end{crmq}

\subsection{La méthode MPM}

\begin{cmethode}[Compteur=false]
Pour construire un graphe d'ordonnancement, on effectue les étapes suivantes :
\begin{enumerate}[leftmargin=*]
	\item On commence par déterminer le \textbf{niveau} de chaque tâche dans le graphe.
	\item On représente le projet par un graphe pondéré, dans lequel :
	\begin{itemize}[leftmargin=*]
		\item chaque tâche est représentée par un sommet, et les sommets sont alignés verticalement par niveau,
		\item les arcs représentent les contraintes d'antériorité (un arc va de $i$ à $j$ si faire $j$ nécessite d'avoir fait $i$),
		\item la valeur de chaque arc est la durée de la tâche à l'origine de l'arc,
		\item deux sommets (ce ne sont pas des tâches) sont placés aux extrémités du graphe : \textsf{Début} et \textsf{Fin}).
	\end{itemize}
\end{enumerate}
\end{cmethode}

\begin{cidee}[Pluriel,Compteur=false]
On peut alors se poser certaines questions :
\begin{itemize}[leftmargin=*]
	\item à quel moment peut-on commencer une tâche ? 
	\item peut-on retarder le moment de démarrer certaines tâches sans que cela n'impacte la durée du projet ? 
\end{itemize} 
\end{cidee}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{roy}{}La méthode des potentiels métra ou MPM, est une technique de gestion de projet, inventée par le Français Bernard Roy en 1958, pour l'usine de fabrication de vilebrequins Mavilor. \textit{Bernard Roy} (1934-2017, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{FR}\!) est un chercheur, professeur émérite de mathématiques appliquées aux sciences de gestion à l'Université Paris-Dauphine. Il est considéré comme l'un des pionniers de la recherche opérationnelle en France.
\end{chistoire}

\begin{chistoire}[Compteur=false]
La méthode PERT (Program Evaluation and Review Technique) est une méthode d’ordonnancement et d’optimisation pour la réalisation de projets comportant un grand nombre de tâches. Elle a été créée en 1958 à la demande de la marine américaine pour son programme de missiles balistiques nucléaires miniaturisés Polaris, afin de  rattraper le retard sur l’URSS (projet avec 9\,000 sous-traitants, 250 fournisseurs). Le délai initial était de 7 ans, et grâce au PERT il est passé à 4 ans !
\end{chistoire}

\begin{cillustr}
\hfill\begin{tikzpicture}[scale=0.9]
	%SOMMETS
	\TacheSimpleMPM{Début}{1,1.25}{Début}
	\TacheSimpleMPM{A}{4,2.5}{A}
	\TacheSimpleMPM{B}{4,0}{B}
	\TacheSimpleMPM{C}{7,2.5}{C}
	\TacheSimpleMPM{D}{7,0}{D}
	\TacheSimpleMPM{E}{10,2.5}{E}
	\TacheSimpleMPM{F}{10,0}{F}
	\TacheSimpleMPM{G}{13,1.25}{G}
	\TacheSimpleMPM{Fin}{16,1.25}{Fin}
	%ARCS
	\ArcSimpleMPM{Début}{A}{0}
	\ArcSimpleMPM{Début}{B}{0}
	\ArcSimpleMPM{A}{C}{3}
	\ArcSimpleMPM{A}{D}{3}
	\ArcSimpleMPM{B}{D}{2}
	\ArcSimpleMPM{C}{E}{4}
	\ArcSimpleMPM{C}{F}{4}
	\ArcSimpleMPM{D}{F}{5}
	\ArcSimpleMPM{E}{G}{3}
	\ArcSimpleMPM{F}{G}{4}
	\ArcSimpleMPM{G}{Fin}{3}
\end{tikzpicture}\hfill~
\end{cillustr}

\section{Histoires de dates}

\subsection{Dates au plus tôt}

\begin{cdefi}
La \uline{date de début au plus tôt} d'une tâche est la date minimale à laquelle on peut commencer une tâche, car toutes les tâches antérieures sont terminées.
\end{cdefi}

\begin{crmq}[Compteur=false]
Si on note $t(i)$ la date de début au plus tôt de la tâche $i$, et $d(i)$ la durée de la tâche $i$, on a \[t(j) = \max(t(i) + d(i)) \quad \text{pour $i$ parcourant l'ensemble des prédécesseurs de $j$}.\]
\end{crmq}

\begin{cexemple}
Dans l'exemple précédent :
\begin{itemize}[leftmargin=*]
	\item $t(A)=t(B)=0$
	\item $t(C)=t(A)+d(A)=0+3=3$
	\item $t(D)=\text{max}(t(A)+d(A);t(B)+d(B))=\text{max}(0+3;0+2)=3$
	\item $t(E)=t(C)+d(C)=3+4=7$
	\item $t(F)=\text{max}(t(C)+d(C);t(D)+d(D))=\text{max}(3+4;3+5)=8$
	\item $t(G)=\text{max}(t(E)+d(E);t(F)+d(F))=\text{max}(7+3;8+4)=12$
	\item $t(\text{Fin})=t(G)+d(G)=12+3=15$
\end{itemize}
La date au plus tôt du sommet \textbf{Fin} est la \uline{durée totale} du projet.
\end{cexemple}

\subsection{Dates au plus tard}

\begin{cdefi}
La \uline{date de début au plus tard} d'une tâche est la date maximale à laquelle on peut commencer une tâche sans que cela repousse la fin du projet. (Cela nécessite donc de lire le graphe en partant de la fin !)
\end{cdefi}

\begin{crmq}[Compteur=false]
Si on note $T(i)$ la date de début au plus tard de la tâche $i$, et $d(i)$ la durée de la tâche $i$, on a \[ T(j) = \min(T(k) - d(j)) \quad \text{pour $k$ parcourant l'ensemble des successeurs de $j$}.\]
\end{crmq}

\begin{cexemple}
Dans l'exemple précédent, en commençant par la fin :
\begin{itemize}[leftmargin=*]
	\item $T(\text{Fin})=t(\text{Fin})=15$
	\item $T(G)=T(\text{Fin})-d(G)=15-3=12$
	\item $T(F)=T(G)-d(F)=12-4=8$
	\item $T(E)=T(G)-d(E)=12-3=9$
	\item $T(D)=T(F)-d(D)=8-5=3$
	\item $T(C)=\text{min}(T(E)-d(C)\,;\,T(F)-d(C))=\text{min}(9-4\,;\,8-4)=4$
	\item $T(B)=T(D)-d(B)=3-2=1$
	\item $T(A)=\text{min}(T(D)-d(A)\,;\,T(C)-d(A))=\text{min}(3-3\,;\,4-3)=0$
\end{itemize}
\end{cexemple}

\begin{crmq}[Compteur=false]
Lorsqu'on trace un diagramme d'ordonnancement, on ajoute pour chaque tâche sa date de début au plus tôt et sa date de début au plus tard.

\hfill\begin{GrapheMPM}
	\MPMPlaceNotice(0,0)
\end{GrapheMPM}\hfill~
\end{crmq}

\begin{cillustr}
\hfill\begin{GrapheMPM}
	\MPMPlaceNotice(1,5)
	\MPMPlaceTaches{ (1,1.25)(Début)(0,0) / (4,2.5)(A)(0,0) / (4,0)(B)(0,1) / (7,2.5)(C)(3,4) / (7,0)(D)(3,3)  / (10,2.5)(E)(7,9) / (10,0)(F)(8,8) /(13,1.25)(G)(12,12) / (16,1.25)(Fin)(15,15)}
	\MPMPlaceDurees{ Début>A,0 / Début>B,0 / A>C,3 / A>D,3 / B>D,2 / C>E,4 / C>F,4 / D>F,5 / E>G,3 / F>G,4 / G>Fin,3 }
\end{GrapheMPM}\hfill~
\end{cillustr}

\section{Histoires de marges et de criticité}

\subsection{Un peu de criticité}

\begin{cdefi}[Pluriel]
$\bullet~~$On appelle \textbf{tâche critique} une tâche dont les dates au plus tôt et au plus tard sont les mêmes.\\
Elles ne tolèrent aucun retard.\\
$\bullet~~$Un \textbf{chemin critique} est un chemin reliant le début à la fin, et qui n'est constitué que de tâches critiques.
\end{cdefi}

\begin{cexemple}
Dans le graphe ordonnancé précédent, A--D--F--G est un chemin critique.
\end{cexemple}

\subsection{Un peu de marges}

\begin{cdefi}
La \textbf{marge totale} (notée souvent MT) d'une tâche est le délai maximum dont peut éventuellement disposer la tâche, sans retarder la date de fin du projet.

\smallskip

C'est donc la différence entre la date au plus tard et la date au plus tôt de la tâche : $$MT(i)=T(i)-t(i).$$ 
\end{cdefi}

\begin{cexemple}
Dans l'exemple précédent :
\begin{itemize}[leftmargin=*]
	\item $MT(G)=T(G)-t(G)=12-12=0$
	\item $MT(F)=T(F)-t(F)=8-8=0$
	\item $MT(E)=T(E)-t(E)=9-7=2$
	\item $MT(D)=T(D)-t(D)=3-3=0$
	\item $MT(C)=T(C)-t(C)=4-3=1$
	\item $MT(B)=T(B)-t(B)=1-0=1$
	\item $MT(A)=T(A)-t(A)=0-0=0$
\end{itemize}
\end{cexemple}

\begin{crmq}[Compteur=false]
La marge totale d'une tâche critique est nulle.
\end{crmq}

\begin{cdefi}
La \textbf{marge libre} (souvent notée ML) est le retard maximum que l'on peut accepter sur la date de début de la tâche sans retarder la date de début au plus tôt des tâches suivantes.

\smallskip

Elle est obtenue en faisant : \[\text{min(dates au plus tôt suivantes) – durée de la tâche – date au plus tôt de la tâche.}\]
\end{cdefi}

\begin{cexemple}
\begin{itemize}[leftmargin=*]
	\item A a deux successeurs qui sont C et D :
	
	$ML(A)=\text{min}(t(C)\,;\,t(D))-d(A)-t(A)=3-3-0=0$
	\item $ML(B)=t(D)-d(B)-t(B)=3-2-0=1$
	\item C a deux successeurs qui sont E et F :
	
	$ML(C)=\text{min}(t(E)\,;\,t(F))-d(C)-t(C)=7-4-3=0$
	\item $ML(D)=t(F)-d(D)-t(D)=8-5-3=0$
	\item $ML(E)=t(G)-d(E)-t(E)=12-3-7=2$
	\item $ML(F)=t(G)-d(F)-t(F)=12-4-8=0$
	\item $ML(G)=t(\text{Fin})-d(G)-t(G)=15-3-12=0$
\end{itemize}
\end{cexemple}

\begin{crmq}
Sur le graphe d'ordonnancement, on peut éventuellement rajouter les marges totales et libres :

\hfill\begin{GrapheMPM}
	\MPMPlaceNotice*(0,0)
\end{GrapheMPM}\hfill~
\end{crmq}

\begin{cillustr}
Voilà le graphe ordonnancé avec les dates et marges :

\hfill\begin{GrapheMPM}
	\MPMPlaceNotice*(1,5)
	\MPMPlaceTaches{ (1,1.25)(Début)(0,0) / (4,2.5)(A)(0,0,0,0) / (4,0)(B)(0,1,1,1) / (7,2.5)(C)(3,4,1,0) / (7,0)(D)(3,3,0,0)  / (10,2.5)(E)(7,9,2,2) / (10,0)(F)(8,8,0,0) /(13,1.25)(G)(12,12,0,0) / (16,1.25)(Fin)(15,15)}
	\MPMPlaceDurees{ Début>A,0 / Début>B,0 / A>C,3 / A>D,3 / B>D,2 / C>E,4 / C>F,4 / D>F,5 / E>G,3 / F>G,4 / G>Fin,3 }
\end{GrapheMPM}\hfill~
%\begin{center}
%	\def\larg{0.24}
%	\begin{tikzpicture}
%		%NOTICE
%		\mpmtnoticec{1}{5}
%		%SOMMETS
%		\mpmtdeb{1}{1.75}{Deb}{0}{0}
%		\mpmtsomc{4}{3.5}{A}{A}{0}{0}{0}{0}
%		\mpmtsomc{4}{0}{B}{B}{0}{1}{1}{1}
%		\mpmtsomc{7}{3.5}{C}{C}{3}{4}{1}{0}
%		\mpmtsomc{7}{0}{D}{D}{3}{3}{0}{0}
%		\mpmtsomc{10}{3.5}{E}{E}{7}{9}{2}{2}
%		\mpmtsomc{10}{}{F}{F}{8}{8}{0}{0}
%		\mpmtsomc{13}{1.75}{G}{G}{12}{12}{0}{0}
%		\mpmtfin{16}{1.75}{Fin}{15}{15}
%		%ARCS
%		\arcts{Deb}{A}{0}
%		\arcts{Deb}{B}{0}
%		\arcts{A}{C}{3}
%		\arcts{A}{D}{3}
%		\arcts{B}{D}{2}
%		\arcts{C}{E}{4}
%		\arcts{C}{F}{4}
%		\arcts{D}{F}{5}
%		\arcts{E}{G}{3}
%		\arcts{F}{G}{4}
%		\arcts{G}{Fin}{3}
%	\end{tikzpicture}
%\end{center}
\end{cillustr}

\begin{crmq}[Pluriel]
Dans les exercices de BTS, on demande toujours les dates, la marge totale et le(s) chemin(s) critique(s).

Il est parfois demandé de travailler sur la marge de certaines tâches, mais rarement de \textit{tout} calculer !

\medskip

Il existe une autre marge, moins souvent utilisée, la \textbf{marge certaine} (souvent notée MC) d'une tâche, qui indique le retard que l'on peut admettre dans sa réalisation (quelle que soit sa date de début) sans allonger la durée optimale du projet.
\end{crmq}

\section{Complément Python}

\begin{cpython}
Un professeur de Mathématiques en BTS SIO a développé un module \calgpython{}, nommé \cpy{grapheMPM} (sources disponibles sur \cshell{\href{https://github.com/TeddyBoomer/grapheMPM}{github}}) qui, combiné avec le logiciel \cshell{GraphViz}, permet de déterminer le graphe d'ordonnement (MPM) d'un projet, en utilisant des \cpy{dictionnaires} pour définir les prédécesseurs et les durées.

\begin{CodePiton}[Largeur=14cm,Style=Classique]{}
from grapheMPM import GrapheMPM
# dico des prédecesseurs
p = {"A":"","B":"","C":"A","D":"A","E":"BD","F":"D","G":"E","H":"FGC"}
# dico des pondérations
w = {"A":2,"B":2,"C":2,"D":4,"E":1,"F":2,"G":2,"H":1}
G = GrapheMPM(pred=p, pond=w, marges=True)
G.earliestdate()
G.latestdate()
G.makeGraphviz()
G.gv.render("ex-full-nomarge")
\end{CodePiton}

\begin{center}
	\includegraphics[height=4cm]{chap03_exos_corr5_grapheMPM}
\end{center}
\end{cpython}

\end{document}