% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
\usepackage{ProfSio}
\useproflyclib{piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{tabularx}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={CHAPITRE~},numdoc={5},titre={Codage.décimaux}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\part{CH05 - Codage des décimaux}

\section{Codage d'un décimal entre 0 et 1}

\subsection{Rappel}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{leibniz}{}Même si on a utilisé des codes binaires dès l’Antiquité pour transmettre des informations, le système binaire tel que nous le connaissons a été inventé à la fin du XVII\up{e} siècle par Gottfried Wilhelm Leibniz (1646/1716, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{DE}\!). Philosophe, mathématicien et savant universel, il cherchait une méthode permettant de traduire les concepts linguistiques de la logique (vrai/faux) en un système mathématique ; il a utilisé pour cela les uns et les zéros, qui nous sont familiers depuis.

Quelques décennies auparavant, le philosophe anglais Francis Bacon s’était déjà demandé comment représenter un texte au moyen d’un code binaire. Un troisième savant, George Boole, a finalement élaboré quelque cent-cinquante ans après Leibniz le système binaire de l’algèbre booléenne, un système qui utilise des opérateurs logiques et qui est d’une importance fondamentale pour l’informatique moderne.

Indépendamment de ces acquis européens dans le domaine du calcul binaire, on trouve des systèmes similaires dans d’autres régions du monde : ainsi le \textit{Yi King}, un ouvrage chinois datant du III\up{e} millénaire av. J.‑C., utilise un code binaire, lequel se réfère à la dualité fondamentale du yin et du yang.\hfill{{\tiny \textit{Souce} : IONOS}}
\end{chistoire}

\begin{ccadre}[Compteur=false]
Le nombre $0,38457$ écrit en base 10 signifie : $0,38457 = 3 \times10^{-1} + 8 \times 10^{-2} + 4 \times 10^{-3} + 5 \times 10^{-4} + 7 \times 10^{-5}$.

Il est donc exprimé à partir de puissances entières d'exposants négatifs de la base : $10^{-1}$ , $10^{-2}$, $10^{-3}$, $10^{-4}$, etc.

On peut obtenir les chiffres successifs en effectuant des \textbf{multiplications} par 10 (décaler la virgule à droite et en gardant le chiffre des unités :
\begin{itemize}
	\item dixièmes : $0,38457 \times 10 = 3,8457 \rightarrow$ chiffre 3
	\item centièmes : $0,8457 \times 10 = 8,457 \rightarrow$ chiffre 8
	%\item millièmes : $0,457 \times 10 = 4,57 \rightarrow$ chiffre 4
	\item etc
\end{itemize}
\end{ccadre}

\begin{crmq}[Compteur=false]
Cette méthode qui parait évidente en base 10 sera réutilisée pour traduire le décimal en base 2.

En binaire, on va exprimer ce nombre à partir des puissances de deux d'exposant négatif.
\end{crmq}

\subsection{En binaire}

\begin{ccadre}[Compteur=false]
En binaire, on va exprimer un nombre décimal à partir des puissances de deux d'exposant négatif :
\begin{center}
	%\renewcommand{\arraystretch}{1.1}
	\begin{tblr}{hlines,vlines,stretch=1.15,colspec={*{8}{X[m,c]}X[0.25,m,c]}}
		$2^{-1}$ & $2^{-2}$ & $2^{-3}$ & $2^{-4}$ & $2^{-5}$ & $2^{-6}$ & $2^{-7}$ & $2^{-8}$ & \dots \\
		$0,5$ & $0,25$ & $0,125$ & $0,0625$ & $0,03125$ & $0,015625$ & $0,0078125$ & $0,00390625$ & \dots
		\\
	\end{tblr}
\end{center}
\end{ccadre}

\begin{cmethode}
Pour effectuer le codage, on effectue des multiplications successives par 2, ce qui revient en binaire à décaler la virgule à droite et à traiter les décimales une par une :
\begin{itemize}
	\item on note le chiffre des unités ;
	\item on multiplie par deux la partie décimale (après la virgule) ;
	\item on note le chiffre des unités (0 ou 1 en binaire) et on l'enlève ;
	\item on recommence.
\end{itemize}
\end{cmethode}

\begin{cexemple}
Effectuons le codage de 0,351565 sur 8 bits :

\smallskip

%\renewcommand{\arraystretch}{1.1}
\begin{tblr}{hlines,vlines,width=\linewidth,colspec={*{5}{X[m,c]}}}
	Nombre décimal à coder & Partie décimale & On multiplie par 2 & Chiffre des unités & Codage binaire sur 8 bits \\
	$0,351565$ & & & 0 & $0,$ \\
	& $0,351565$& $0,703125$ & 0 & $0,0$ \\
	& $0,703125$ & $1,40625$ & 1 & $0,01$ \\
	& $0,40625$ & $0,8125$ & 0 & $0,010$ \\
	& $0,8125$ & $1,625$ & 1 & $0,0101$ \\
	& $0,625$ & $1,25$ & 1 & $0,01011$ \\
	& $0,25$ & $0,5$ & 0  & $0,010110$ \\
	& $0,5$ & 1 & 1 & $0,0101101$ \\
	& $0$ & 0 & 0 & $0,01011010$ \\
\end{tblr}

\medskip

L'écriture est \textbf{exacte}, on a rajouté un 0 pour coder sur 8 bits. Finalement on a : \[ (0,351565)_{10}= (0,01011010)_2.\]
Et on a bien $0,25 + 0,0625 + 0,03125 + 0,0078125 = 0,351565$
\end{cexemple}

\begin{crmq}[ModifLabel={ importante}]
Certains nombres auront une partie décimale finie, d'autres non. On sait par exemple que $\nicefrac{ 1}{3}$ ou $\sqrt{3}$ n'ont pas d'écriture décimale finie. Par exemple $(0,1)_{10}$ se traduit par une écriture infinie en binaire :

\begin{itemize}[noitemsep]
	\item $\phantom{0,1 \times 2  =\:\,}$0,1 unités 0 $\rightarrow$ 0 ;
	\item $0,1 \times 2  = 0,2$ unités 0 $\rightarrow 0,0$ ;
	\item $0,2 \times 2 = 0,4$ unités 0 $\rightarrow 0,00$ ;
	\item $0,4 \times 2 = 0,8$ unités 0 $\rightarrow 0,000$ ;
	\item $0,8 \times 2 = 1,6$ unités 1 $\rightarrow 0,0001$ ;
	\item $0,6 \times 2 = 1,2$ unités 1 $\rightarrow 0,00011$ ;
	\item $0,2 \times 2 = 0,4$ unités 0 $\rightarrow 0,000110$ ;
	\item on recommence à l'étape 3 donc la séquence \textbf{0011} va se reproduire indéfiniment !
\end{itemize}

Le codage sur 8 bits est alors : $0,00011001$ et on a $(0,1)_{10}  = (0,00011001)_2$.

Et on a $0,0625 + 0,03125 + 0,00390625 = 0,09765625$ qui est une valeur approchée par défaut de 0,1.
\end{crmq}

\begin{cexemple}[Pluriel]
Coder en binaire sur 8 bits : $a=(0,578125)_{10}$ et $b=(0,85)_{10}$.

\hfill{}$a=(0,10010100)_2$ et $b=(0,11011001)_2$

Convertir en décimal : $c=(0,10110000)_2$ et $d=(0,1101001)_2$.

\hfill{}$c=0,5+0,125+0,0625=0,6875$ et $d=0,5+0,25+0,0625+0,0078125$
\end{cexemple}

\section{Codage d'un décimal quelconque}

\subsection{Bit de signe}

\begin{cdefi}
Dans les normes de codage, on réserve toujours le premier bit pour le signe :

\smallskip

\hfill{}0 pour un nombre positif \quad et \quad 1 pour un nombre négatif.\hfill~

\smallskip

Ainsi le codage de $-14,35156510$ commencera par le bit de signe 1.
\end{cdefi}

\subsection{Codage en virgule fixe}

\begin{cexemple}[ComplementTitre={ - Méthode}]
Pour coder $(14,351565)_{10}$ on sépare :
\begin{itemize}
	\item la partie entière $(14)_{10}  = (1110)_2$ ;
	\item la partie décimale $(0,351565)_{10}  = (0,01011010)_2$.
\end{itemize} 
On a alors $(14,351565)_{10}  = (1110,01011010)_2$.  
\end{cexemple}

\begin{cexemple}[Pluriel]
Coder en virgule fixe $a = (7,853)_{10}$ et $b = (17,52)_{10}$.

\hfill{}$a=111,11011010_2$ et $b=10001,10000101_2$
\end{cexemple}

\subsection{Codage en virgule flottante, norme IEEE754}

\begin{cintro}[Compteur=false]
En réalité, les décimaux sont codés en \textbf{virgule flottante} : on décale la virgule pour avoir un nombre dont l'écriture binaire sera 1,\dots\dots{} de manière analogue à l'écriture des nombres réels en notation scientifique.
\end{cintro}

\begin{crappel}[Pluriel,Compteur=false]
On a, par exemple :
\begin{itemize}
	\item $345,753 = 3,45753 \times 10^2$ ;
	\item $0,00235 = 2,35 \times 10^{-3}$.
\end{itemize}
On se ramène à une partie entière comprise strictement entre 0 et 10, en multipliant par une puissance de 10 pour rétablir la place de la virgule.
\end{crappel}

\begin{cmethode}
En \textbf{binaire}, le décalage de la virgule se fait en multipliant par des puissances de deux.  Il s'agit donc pour coder en virgule flottante de trouver l'exposant qui ramènera la virgule à sa place.

Il y aura donc après le bit de signe un \textbf{codage de l'exposant} (code sur un octet dans le cas le plus simple qui nous concerne).
\end{cmethode}

\begin{cexemple}
On a vu que $(14,351565)_{10}  = (1110,01011010)_2$.

Or $1110,01011010 = 1,11001011010 \times 2^3$.

L'écriture n'est pas correcte en binaire (le chiffre 3 ne doit pas apparaître) mais c'est bien l'exposant 3 que l'on doit retenir.

\smallskip

Le nombre binaire $1,11001011010$ s'appelle la \textbf{mantisse}. On convient d'\textbf{ignorer le 1 qui précède la virgule}.

Ainsi la mantisse sera 11001011010, et elle sera complétée à droite par des zéros de manière à occuper 23 bits.

\smallskip

Donc pour  coder 14,3515625, on a 
\begin{itemize}
	\item 0 pour le bit de signe ;
	\item 3 pour le bit d'exposant ;
	\item et $1,11001011010$ (la \textbf{mantisse}).
\end{itemize}
\textbf{Pour des raisons pratiques, on ajoute 127 à l'exposant lorsqu'il est codé sur un octet.}

\smallskip

Dans notre exemple, l'exposant sera $127+3=130$ codé par $10000010$.

\smallskip

\textbf{Finalement, on code la partie entière et la partie décimale en binaire, on détermine l'exposant. Puis  on code la mantisse en convenant d’ignorer le 1 avant la virgule.}
\end{cexemple}

\begin{cillustr}
Cela donne 1 bit de signe, 8 bits d'exposant et 23 bits de mantisse pour le codage en \textbf{32 bits}.
%
\begin{center}
	\begin{tikzpicture}[x=0.5cm,y=0.5cm]
		\draw[thick,black,fill=purple!50!white] (0,0) rectangle (1,1) ;
		\draw[thick,black,fill=CouleurVertForet!50!white] (1,0) rectangle (9,1) ;
		\draw[thick,black,fill=red!50!white] (9,0) rectangle (33,1) ;
		\foreach \x in {0,1,...,33} \draw[thick] (\x,0) -- (\x,1) ;
		\draw[thick,decorate,decoration={brace,amplitude=15pt,mirror}](9,1)--(1,1) node {};
		\draw[thick,decorate,decoration={brace,amplitude=15pt,mirror}](33,1)--(9,1) node {};
		\draw (0.5,2.5) node[font=\small\sffamily,inner sep=0pt] {signe} ;
		\draw (5,2.5) node[font=\small\sffamily,inner sep=0pt] {exposant} ;
		\draw (21,2.5) node[font=\small\sffamily,inner sep=0pt] {mantisse} ;
		\draw[thick,->,>=latex] (0.5,2.1) -- (0.5,1) ;
		\foreach \x in {0,23,31,32} \draw (32.5-\x,0) node[below,font=\footnotesize\sffamily] {\x} ;
	\end{tikzpicture}
\end{center}

\end{cillustr}

\begin{cexemple}
Ainsi, le codage de 14,3515625 aura : 
\begin{itemize}
	\item 0 pour bit de signe ;
	\item $3+127 = 130$ soit $10000010$ pour exposant ; 
	\item 11001011010 comme mantisse.
\end{itemize}
D’où son codage en binaire selon la norme IEEE754 : ${\textcolor{purple}{0}}{\textcolor{CouleurVertForet}{10000010}}\textcolor{red}{11001011010000000000000}.$
\begin{center}
	\includegraphics[width=0.6\linewidth]{graphics/chp05_ieee754}
	
	{\tiny \url{https://www.h-schmidt.net/FloatConverter/IEEE754.html}}
\end{center}
\end{cexemple}


\begin{cexemple}[Pluriel]
Coder avec la norme IEEE754 sur 32 bits : $a = 523,5$ et $b = -118,625$.

Donner l'écriture décimale des flottants codés en 32 bits par :
\begin{itemize}
	\item $c={\mathcolor{purple}{0}}{\textcolor{CouleurVertForet}{10100010}}\mathcolor{red}{10011001100000000000000}$ ;
	\item $d={\mathcolor{purple}{1}}{\textcolor{CouleurVertForet}{10001000}}\mathcolor{red}{11111011110000001010010}$.
\end{itemize}
%
\hfill{}$a={\textcolor{purple}{0}}{\textcolor{CouleurVertForet}{10001000}}\textcolor{red}{00000101110000000000000}$

\hfill{}$b={\textcolor{purple}{1}}{\textcolor{CouleurVertForet}{10000101}}\textcolor{red}{11011010100000000000000}$

\hfill{}$c=54962159616$ et $d=-1015,5050048828125$
\end{cexemple}

\begin{chistoire}[Compteur=false,SousTitre={- Pourquoi Ariane 5 avait-elle explosé en plein vol ? -}]
\lettrine[findent=.5em,nindent=0pt,lines=6,image,novskip=0pt]{chap05_ariane}{}{\polancienne KOUROU, Guyane, 4 juin 1996, 37 secondes après le décollage la fusée explose en plein ciel à 4\,000~m d'altitude. Comment cela a-t-il pu se produire ? Après enquête, les ingénieurs du CNES se sont aperçu que par mesure d'économie, le logiciel de navigation de la fusée Ariane 5 était celui qui avait été conçu pour Ariane 4. Mais cela à suffit pour créer une incompatibilité entre le logiciel et le matériel.\\ Tout tenait à une seule petite variable : celle allouée à l'accélération horizontale. En effet, l'accélération maximum d'Ariane 4 était d'environ 64, la variable a été codée sur 8 bits. [\dots] En base binaire, cela nous fait 2\up{8}=256 valeurs possibles (256 combinaisons de 8 bits), suffisant pour coder la valeur 64 qui s'écrit 1000000 et nécessite 8 bits. Mais voilà, Ariane 5 était plus véloce : son accélération pouvait atteindre la valeur 300 (qui vaut 100101100 en binaire et nécessite 9 bits). Ainsi, la variable codée sur 8 bits a connu un \textbf{dépassement de capacité} puisque son emplacement mémoire n'était pas assez grand pour accepter une valeur aussi grande, il aurait fallu la coder sur un bit de plus, à savoir 9, ce qui nous aurait fait 2\up{9}=512 comme valeur limite, alors suffisant pour coder la valeur 300. De ce dépassement de capacité, a résulté une valeur absurde dans la variable, ne correspondant pas à la réalité. Par effet domino, le logiciel face à des valeurs vraiment pas normales décida de l'autodestruction de la fusée.\\
Un tel bogue informatique a coûté plus d'un \textbf{milliard de Francs}. Il aurait pu être évité si les ingénieurs avaient révisés tous les logiciels. Avoir décidé de faire cette économie leur a coûté très cher.}

\hfill{}{\tiny \url{https://cyberzoide.developpez.com/pourquoi/index.php3?page=ariane5}}
\end{chistoire}

\end{document}