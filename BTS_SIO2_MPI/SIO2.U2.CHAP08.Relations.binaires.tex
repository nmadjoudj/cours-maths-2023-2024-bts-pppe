% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage{ProfSio}
\useproflyclib{ecritures,piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{wrapstuff}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[U2]},typedoc={CHAPITRE~},numdoc={8},titre={Relations binaires}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\DeclareMathOperator*{\card}{Card}
\def\calr{\mathscr{R}}

\begin{document}

\pagestyle{fancy}

\part{CH08 - Relations binaires}

\section{Généralités}

\subsection{Exemple introductif}

\begin{cillustr}
On s'intéresse à l'emploi du temps d'un élève, donné ci-dessous :

\begin{center}
	\begin{tblr}{hlines,vlines,width=15cm,colspec={*{5}{X[c]}},cells={font=\sffamily}}
		Lundi & Mardi & Mercredi & Jeudi & Vendredi \\
		Anglais & Informatique & Mathématiques & Informatique & Français \\
		Éco/Droit & Informatique & Anglais & Informatique & Mathématiques \\
		PPE & Français & Informatique & Éco/Droit & PPE \\
		PPE & Anglais & Informatique & Éco/Droit & PPE \\
	\end{tblr}
\end{center}

Cet emploi du temps met en relation des matières et des jours. Par exemple la matière Mathématiques et en relation avec Mercredi et avec Vendredi. Cela définit une relation binaire.\\
Pour décrire cette relation, on peut donner tous les couples de la forme $(x\,;\,y)$ où $x$ est une matière enseignée le jour $y$ : $\left\lbrace\right.$(Anglais ; Lundi) ; (Éco/Droit ; Lundi) ; \dots ; (PPE ; Vendredi) $\left.\right\rbrace$.

\smallskip

On peut décrire cette relation à l'aide d'un diagramme sagittal :

\begin{center}
	\begin{tikzpicture}[y=1cm]
		\draw (2,4.5) node {\point{Matières}} ; \draw (7,4.5) node {\point{Jours}} ;
		\draw[very thick] (2,2) circle[x radius=1,y radius=2] ;
		\draw[very thick] (7,2) circle[x radius=1,y radius=2] ;
		\draw [line width=2pt,->,>=latex] (3.15,4.5)--(5.85,4.5) ;
		\draw (4.5,4.5) node[above] {$\calr$} ;
		\node (ANTE) at (2,2) {\renewcommand{\arraystretch}{1.15}\textsf{\begin{tabular}{c}Anglais\\Éco/Droit\\PPE\\Info\\Français\\Maths\end{tabular}}} ;
		\node (IMG) at (7,2) {\renewcommand{\arraystretch}{1.3}\textsf{\begin{tabular}{c}Lundi\\Mardi\\Mercredi\\Jeudi\\Vendredi\end{tabular}}} ;
	\end{tikzpicture}
	
\end{center}

On peut également décrire cette relation à l'aide d'un tableau à double entrée ou à l'aide de la matrice associée (en notant 1 si deux éléments sont en relation et 0 sinon) :

\begin{center}
	\begin{tblr}{width=13cm,colspec={c*{5}{X[c]}},vline{1}={2-Z}{solid},vline{2-Z}={solid},hline{1}={2-Z}{solid},hline{2-Z}={solid},cells={font=\sffamily}}
		& Lundi & Mardi & Mercredi & Jeudi & Vendredi \\
		Anglais &&&&& \\
		Éco/Droit &&&&& \\
		PPE &&&&&\\
		Informatique &&&&& \\
		Français &&&&& \\
		Mathématiques &&&&& \\
	\end{tblr}
\end{center}
\end{cillustr}

\begin{crmq}
L'aspect \og tableau à double entrée \fg{} permet de faire un premier lien avec les matrices et les graphes, même si la suite du chapitre ne sera pas forcément lié à ces deux notions précédemment étudiées.
\end{crmq}

\subsection{Généralités}

\begin{cdefi}[Pluriel]
Soient $E$ et $F$ deux ensembles.
%
\begin{itemize}[leftmargin=*]
	\item Une relation binaire $\calr$ de $E$ vers $F$ est la donnée d'une partie $G$ du produit cartésien $E \times F$.
	\item $G$ est appelée graphe de la relation.
	\item Un couple $(x ; y)$ appartient à $G$ ssi $x$ est en relation avec $y$. On note alors $x \mathscr{R} y$.
\end{itemize}
\end{cdefi}

\begin{crmq}
Dans ce qui suit, on s'intéressera uniquement au cas où les deux ensembles $E$ et $F$ sont confondus.

Définir une relation binaire sur un ensemble $E$ revient alors à donner une partie de $E \times E$.
\end{crmq}

\begin{cillustr}
On s'intéresse à un réseau de voies ferrées (à sens unique !) entre des villes a, b, c et d. Le réseau est ci-dessous.

On note $E$ l'ensemble des villes : $E = \{a ; b ; c ; d\}$. De plus on dit qu'une ville $x$ est en relation avec une ville $y$ si il existe une voie ferrée allant de $x$ vers $y$. Par exemple : $a \mathscr{R} b$.
\begin{center}
	\tikzset{sommet/.style={draw,blue,circle,thick,minimum size=10pt,font=\sffamily}}
	\tikzset{arete/.style={purple,line width=1pt,->,>=latex}}
	\begin{tikzpicture}[transform shape,scale=1.5]
		\node[sommet] (A) at (1,1.75) {a};
		\node[sommet] (B) at (3,1.25) {b};
		\node[sommet] (C) at (2.5,0) {c};
		\node[sommet] (D) at (0,0.25) {d};
		\draw[arete] (A) -- (B) ;
		\draw[arete] (B) -- (D) ;
		\draw[arete] (A) -- (C) ;
		\draw[arete] (D) -- (C) ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{crmq}
Le fait de travailler sur des parties de $E \times E$ permet de représenter les relations binaires par des graphes.

\smallskip

Dans le cas où on travaille sur des parties de $E \times F$, la représentation sera plutôt sous forme d'un diagramme sagittal.
\end{crmq}

\begin{cdefi}[Pluriel]
Soit $\calr$ une relation binaire définie sur un ensemble $E$ non vide.
\begin{itemize}[leftmargin=*]
	\item $\calr$ est dite \underline{réflexive} si : $\forall x \in E$, $x \calr x$.
	\item $\calr$ est dite \underline{transitive} si $\forall (x ; y ; z) \in E^3$, $(x \calr y \wedge y \calr z) \Rightarrow x \calr z$.
	\item $\calr$ est dite \underline{symétrique} si $\forall (x ; y) \in E^2$, $x \calr y \Rightarrow y \calr x$.
	\item $\calr$ est dite \underline{antisymétrique} si $\forall (x ; y) \in E^2$, $(x \calr y \wedge  y \calr x) \Rightarrow y = x$.
\end{itemize}
\end{cdefi}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item L'inégalité \textit{large} $\geq$ sur $\R$ est \og RTAS \fg{} alors que l'inégalité \textit{stricte} $>$ sur $\R$ est uniquement \og T \fg
	\item Sur $\N$, la congruence modulo $n$, où $n$ est un entier strictement positif est \og RTS \fg.
\end{itemize}
\end{cexemple}

\pagebreak

\begin{crmq}[Pluriel]
Concrètement, on peut retenir les propriétés des relations binaires :
%
\begin{itemize}
	\item $\calr$ est \underline{réflexive} : tout élément est en relation avec lui même.
	\item $\calr$ est \underline{transitive} : à chaque fois qu’on peut « enchaîner » les relations, le « raccourci » est présent aussi.
	\item $\calr$ est \underline{symétrique} : si $x \calr y$ alors $y \calr x$ de sorte que les éléments en relation le sont « dans les deux sens ».
	\item $\calr$ est \underline{antisymétrique} : si une relation est vraie « dans les deux sens » alors c’est qu’elle ne concerne qu’un élément (il n’y a pas de double flèche reliant deux éléments différents mais, peut-être, des boucles).
\end{itemize}
\end{crmq}

\section{Relations particulières}

\subsection{Relation d'équivalence}

\begin{cdefi}
Soit $\calr$ une relation binaire définie sur un ensemble $E$ non vide. $\calr$ est une \underline{relation d'équivalence} si et seulement si elle est réflexive, transitive et symétrique (\og RTS \fg)
\end{cdefi}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item D'après ce que l'on a vu ci-dessus, la congruence modulo $n$ sur $\N$ est une relation d'équivalence.
	\item Les entreprises peuvent être regroupées selon leur taille :
	\begin{itemize}
		\item les très petites entreprises comptent moins de 10 salariés,
		\item les petites ou moyennes entreprises comptent entre 10 et 250 salariés,
		\item et les grandes entreprises comptent plus de 250 salariés.
	\end{itemize}
	Soit $E$ l'ensemble des entreprises. Soit $\calr$ la relation définie sur $E$ par $x \calr y$ si les entreprises $x$ et $y$ ont la même taille. $\calr$ est une relation d'équivalence.
\end{itemize}
\end{cexemple}

\begin{crmq}
Une relation d'équivalence sur un ensemble E permet de regrouper les éléments de E
en \textbf{classes d'équivalences}.
\end{crmq}

\subsection{Relation d'ordre}

\begin{cdefi}
Soit $\calr$ une relation binaire définie sur un ensemble $E$ non vide. $\calr$ est une \underline{relation d'ordre} si et seulement si elle est réflexive, transitive et antisymétrique (\og RTAS \fg).
\end{cdefi}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item D'après ce que l'on a vu ci-dessus, l'inégalité large $\geq$ sur $\R$ est une relation d'ordre.
	\item On décide de classer les entreprises selon leur taille. Pour deux entreprises $x$ et $y$, on note $x \calr y$ si le nombre de salariés de $x$ est inférieur ou égal au nombre de salariés de $y$. La relation ainsi définie est une relation d’ordre sur l'ensemble des entreprises.
	\item Soit $E$ un ensemble. Sur $\mathscr{P}(E)$, on définit la relation $\calr$ par $A \calr B \ssi A \subset B$. $\calr$ est une relation d'ordre sur $\mathscr{P}(E)$.
\end{itemize}
\end{cexemple}

\begin{cdefi}
Une relation d'ordre $\calr$ définie sur un ensemble non vide $E$ est une relation d'\underline{ordre total} si : $$\forall (x ; y) \in E^2, \: (x \calr y) \vee (y \calr x).$$
Dans le cas contraire, $\calr$ est une relation d'\underline{ordre partiel}.
\end{cdefi}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item La relation $\geq$ est une relation d'ordre total sur $\R$.
	\item La relation $\subset$ est une relation d'ordre partiel sur $\mathscr{P}(E)$.
\end{itemize}
\end{cexemple}

\begin{crmq}
Lorsqu'on veut effectuer un tri, il est nécessaire de disposer d'une relation d'ordre.
\end{crmq}

\begin{cexo}[Compteur=false]
On considère $\R^2$ et on l’identifie à l’ensemble des points du plan muni d’un repère.

On décide de noter $\preceq$ la relation suivante : $(x_1;y_1) \preceq (x_2;y_2)$ si et seulement si $\begin{dcases}x_1 \leqslant x_2 \\ \text{et} \\ y_1 \leqslant y_2\end{dcases}$.

\begin{Centrage}
	\begin{tikzpicture}[xmin=-1,xmax=7,ymin=-1,ymax=5]
		\GrilleTikz\AxesTikz[ElargirOx=0,ElargirOy=0]
		\AxexTikz[AffGrad=false]{-1,0,...,6}\AxeyTikz[AffGrad=false]{-1,0,...,4}
		\AxexTikz{1}\AxeyTikz{1}
		%points
		\draw[very thick,gray,densely dashed] (1,0)|-(0,2) ;
		\draw[thick,red,fill=white] (1,2) circle[radius=1.75pt] node[above right] {B} ;
		\draw[very thick,gray,densely dashed] (2,0)|-(0,4) ;
		\draw[thick,red,fill=white] (2,4) circle[radius=1.75pt] node[above right] {A} ;
		\draw[very thick,gray,densely dashed] (5,0)|-(0,3) ;
		\draw[thick,red,fill=white] (5,3) circle[radius=1.75pt] node[above right] {C} ;
	\end{tikzpicture}
\end{Centrage}

alors $\preceq$ est une relation d’ordre, mais c’est ordre n’est pas total :

\begin{itemize}
	\item On a bien $(1;2) \preceq (2;4)$ ;
	\item mais on ne peut pas comparer $(2;4)$ et $(5;3)$.
\end{itemize} 
\end{cexo}

\end{document}