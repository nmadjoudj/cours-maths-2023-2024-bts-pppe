% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton,ecritures}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{WriteOnGrid}
\usepackage{vectorlogos}
\GenMacroLogoVect{\logopythonvect}{python}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U2]},typedoc=TD~,mois=Mars,annee=2024,numdoc=06,titre={TD06}]
%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - \DonneesMois{} \DonneesAnnee}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\part{TD06 - Chiffrements, systèmes}

\smallskip

\nomprenomtcbox

\smallskip

\begin{EnvtExo}[Decoration=Image/goku_ssj2,CodeDebut=\medskip]%exo1
On souhaite ici étudier différentes méthodes de chiffrement-déchiffrement permettant de crypter des caractères.

Dans toute la suite, les caractères qui nous intéresseront seront les 26 majuscules de notre alphabet.

Et chacune des 26 lettres de l’alphabet sera associée à l’un des entiers compris entre 0 et 25 selon le tableau de correspondance donné ci-dessous :

\smallskip

\begin{tblr}{hlines,vlines,width=\linewidth,colspec={c*{13}{X[m,c]}},cells={font=\small\ttfamily},column{1}={font=\bfseries\footnotesize\ttfamily}}
	Lettre&A&B&C&D&E&F&G&H&I&J&K&L&M\\
	Rang&0&1&2&3&4&5&6&7&8&9&10&11&12\\
	Lettre&N&O&P&Q&R&S&T&U&V&W&X&Y&Z\\
	Rang&13&14&15&16&17&18&19&20&21&22&23&24&25\\
\end{tblr}

\medskip

\textbf{-- Méthode n°1 : chaque lettre est codée par la valeur de convertie en écriture binaire.}

\medskip

\textit{Ex.} Pour G, la valeur de $x$ est 6 qui s’écrit 110 en binaire. La lettre G est donc codée par 110.

\begin{enumerate}
	\item Déterminer le codage de la lettre N.
	\item Déterminer la lettre qui est codée par 10111.
\end{enumerate}

\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x3]<\CoulSeyes>

\medskip

\textbf{-- Méthode n°2 : chaque lettre est codée par la valeur de $\grasmaths{10x}$ convertie en écriture hexadécimale.}

\medskip

\textit{Ex.} Pour G, la valeur de $x$ est 6 donc $10x = 60$ qui s’écrit 3C en hexadécimal. La lettre G est donc codée par 3C.

\begin{enumerate}[resume]
	\item Déterminer le codage de la lettre N.
	\item Déterminer la lettre qui est codée par B4.
\end{enumerate}

\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x3]<\CoulSeyes>

\medskip

\textbf{-- Méthode n°3 : chaque lettre correspondant à la valeur $\grasmaths{x}$ est codée par la lettre correspond à la valeur de $\grasmaths{y}$ égale au reste de la division euclidienne de $\grasmaths{7x+4}$ par 26.}

\medskip

\textit{Ex.} Pour G, la valeur de $x$ est 6, et $7x+4=7\times6+4=46 \equiv 20 \Modulo{26}$ donc $y=20$ qui correspond à la lettre U. La lettre G est donc codée par la lettre U.

\begin{enumerate}[resume]
	\item Justifier que la lettre N est codée par la lettre R.
	\item Déterminer le codage du mot « GNU » en codant dans l’ordre chacune des trois lettres du mot GNU.
\end{enumerate}

\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x5]<\CoulSeyes>
\end{EnvtExo}

\pagebreak

\begin{EnvtExo}[Decoration=Image/goku_ssj2,CodeDebut=\medskip]%exo2
En informatique, le code ASCII associe à certains caractères (lettre, chiffre, signe de ponctuation\ldots) un entier compris entre 0 et 255 que l'on appelle son code ASCII. La fonction code du tableur renvoie le code ASCII du caractère. L'extrait de tableur ci-dessous donne le codage de quelques caractères.

\medskip

\scalebox{0.875}[1]{\footnotesize \sffamily
	\begin{tabular}{|l|*{26}{c|}}
		\hline
		Lettre &A&B&C&D&E&F&G&H&I&J&K&L&M&N&O&P&Q&R&S&T&U&V&W&X&Y&Z \\ \hline
		Code ASCII : \textit{n} &65&66&67&68&69&70&71&72&73&74&75&76&77&78&79&80&81&82&83&84&85&86&87&88&89&90 \\ \hline
		\textit{p}=\textit{f(n)} &199&206&&&&&&&&&&&&&&&&&&&&&&&& \\ \hline
	\end{tabular}
}

\medskip

On décide de chiffrer (crypter) une lettre à partir de son code ASCII en utilisant la fonction $f$ définie pour tout entier $n$ tel que $0 \pp n < 256$ par : $f(n)$ est le reste de la division euclidienne de $7n$ par 256, c'est-à-dire que si on note $p = f (n)$, alors $p \equiv 7n \Modulo{256}$ où $p = f (n)$, avec $p$ entier tel que $0  \pp p <256$.

\smallskip

Par exemple, le code ASCII de la lettre A est 65. On a $7 \times 65 = 199 \Modulo{256}$ et $0 \pp 199 < 256$ donc la lettre A est chiffrée par 199.

\medskip

\textbf{Partie A - Chiffrement}

\begin{enumerate}
	\item Vérifier que la lettre \og B \fg{} est chiffrée par le nombre 206.
	\item Déterminer le cryptage du mot \og BTS \fg. (On séparera chaque code de lettre par un espace).
\end{enumerate}

\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x5]<\CoulSeyes>

\smallskip

\textbf{Partie B - Déchiffrement}

\medskip

Pour déchiffrer un entier $p$ compris entre 0 et 255 (inclus), on calcule le reste de la division euclidienne de $183 \times p$ par 256 ; autrement dit $n \equiv 183 \times p \Modulo{256}$ avec $n$ entier tel que $0 \pp n < 256$.

\smallskip

Par exemple, pour $p=20$, on a $183 \times 20 \equiv 76 \Modulo{256}$ et donc la valeur chiffrée correspond à la lettre L.

\begin{enumerate}
	\item Déterminer la lettre correspondant à la valeur chiffrée 27. On détaillera la réponse.
	\item Donner le mot de trois lettres correspondant au code chiffré des trois entiers : 234 255 34.
\end{enumerate}

\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x5]<\CoulSeyes>

\smallskip

\textbf{Partie C - Justification}

\begin{enumerate}
	\item Prouver que $183 \times 7 \equiv 1 \Modulo{256}$.
	\item Justifier que $f(n) \equiv 7n \: \Modulo{256} \Rightarrow 183 \times f (n) \equiv n \Modulo{256}$.
\end{enumerate}

\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x5]<\CoulSeyes>
\end{EnvtExo}

\pagebreak

\begin{EnvtExo}[Decoration=Image/goku_ssj1,CodeDebut=\medskip]%exo3
Résoudre, en détaillant la démarche, le système $\systeme{2x+3y-5z=4,x+y-z=4,x-2y=10}$.

\medskip

\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x6]<\CoulSeyes>
\end{EnvtExo}

\medskip

\begin{EnvtExo}[Type=Perso/Zone de réponse libre,Decoration=Icone/\faPen,CodeDebut=\medskip,Compteur=false]%zonelibre
\AffQuadrillage[Grille=Seyes,AffBarre,Marge=2,NbCarreaux=23x21]<\CoulSeyes>
\end{EnvtExo}

\end{document}