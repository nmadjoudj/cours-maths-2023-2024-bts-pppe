% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton,ecritures}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U2]},typedoc=CHAPITRE~,numdoc=03,titre={Divisibilité}]
%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}

\begin{document}

\pagestyle{fancy}

\part{CH03 - Arithmétique - Divisibilité, division euclidienne}

\section{Introduction}

\begin{cintro}[Compteur=false]
L’\textbf{arithmétique} est une branche des mathématiques qui étudie les nombres entiers et les propriétés des opérations sur ces nombres.

\smallskip

On note $\N$ l’ensemble des entiers naturels (entiers positifs ou nul) et $\Z$ l’ensemble des entiers relatifs (entiers positifs ou négatifs ou nul).

L’ensemble $\N$ est bien entendu contenu dans $\Z$ : $\N \subset \Z$.
\end{cintro}

\section{Opérations sur les entiers}

\subsection{Opérations classiques}

\begin{cdefi}
Il existe deux opérations dans $\N$ et dans $\Z$ : l'addition ($+$) et la multiplication ($\times$).
\end{cdefi}

\begin{cprop}[Pluriel]
Pour l'addition :

L'\textbf{opposé} d'un nombre $a$ est le nombre $b$ tel que $a+b=0$.

\begin{itemize}
	\item Dans $\N$, seul 0 admet un opposé (lui-même).
	\item Dans $\Z$, chaque entier a un opposé : $-2$ est l'opposé de 2 ; $-(-5)=5$ est l'opposé de $-5$.
\end{itemize}

Pour la multiplication :

L'\textbf{inverse} d'un nombre $a$ est le nombre $b$ tel que $a \times b=1$.

\begin{itemize}
	\item Dans $\N$, seul 1 admet un inverse (lui-même).
	\item Dans $\Z$, seuls 1 et $-1$ admettent un inverse.
\end{itemize}
\end{cprop}

\subsection{La relation \og divise \fg}

\begin{cdefi}
On dit que $a$ est un \textbf{diviseur} de $b$ ou que $b$ est un \textbf{multiple} de $a$ s’il existe un \uline{entier} $k$ tel que \[ b = k \times a. \]
On dit aussi que  $b$ est \textbf{divisible} par $a$.
\end{cdefi}

\begin{cexemple}[s]
$91 = 7 \times 13$ : donc 7 (ou 13) divise 91.

\smallskip

$-54 = 9 \times (-6) = (-27) \times 2$ : donc 9, $-6$, $-27$ ou 2 sont des diviseurs de $-54$.
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut étudier une divisibilité en utilisant le \textit{reste}, donné par \cpy{\%} qui doit être nul :

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
91%13
7%3
\end{ConsolePiton}
\end{cpython}

\begin{crmq}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Tout entier est divisible par 1 et par lui-même ; 1 est un diviseur de chaque entier.
	\item Tout entier $n$ est un diviseur de 0 (car $0 \times n=0$).
	\item Tout entier est multiple de lui-même.
	\item Tout entier est multiple de 1.
\end{itemize}
\end{crmq}

\subsection{Propriétés}

\begin{cprop}[Pluriel]
Dans $\Z$ :
\begin{itemize}
	\item Si $a$ divise $b$ et $b$ divise $c$ alors :  $a$ divise $c$ (transitivité).
	\item Si $a$ divise $b$ alors $a$ divise $bc,$ quel que soit l'entier $c$.
	\item Si $c$ divise $a$ et $c$ divise $b$ alors : $c$ divise $a+b$ et $c$ divise $a-b$.
\end{itemize}
Dans $\N*$ :
\begin{itemize}
	\item Si $a$ divise $b$ alors : $a \pp b$.
	\item Si $a$ divise $b$ et $b$ divise $a$ alors : $a=b$.
\end{itemize}
\end{cprop}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item 7 divise 21 et 21 divise 105 donc 7 divise 105.
	
	\smallskip
	\item 8 divise 40 donc 8 divise également 80, 120, etc\dots
	
	\smallskip
	\item 6 divise 42 et 12 donc 6 divise $42+12=54$ et $42-12=30$.
	
	\smallskip
\end{itemize}
\end{cexemple}

\subsection{Critères de divisibilité}

\begin{cprop}
On a les résultats suivants :
\begin{center}
	\begin{tblr}{hlines,vlines,width=0.95\linewidth,colspec={Q[m,c]X[m,c]},stretch=1.5,row{1}={bg=lightgray!50},cells={font=\sffamily}}
		Un entier naturel est divisible par : & Si : \\
		2&son chiffre des unités est 0, 2, 4, 6 ou 8 \\
		3&la somme de ses chiffres est divisible par 3 \\
		4&la nombre formé par ses deux derniers chiffres est divisible par 4 \\
		5&son chiffre des unités est 0 ou 5 \\
		9&la somme de ses chiffres est divisible par 9 \\
		10&son chiffre des unités est 0 \\
		11&la somme alternée de ses chiffres est divisible par 11 \\
	\end{tblr}
\end{center}
\end{cprop}

\begin{cexemple}[Pluriel]
Tester la divisibilité des nombres suivants à l'aide des critères du tableau :
\begin{itemize}[leftmargin=*]
	\item 2310 : divisible par 2, 3, 5, 10 et 11.
	
	\smallskip
	\item 324 : divisible par 2, 3, 4 et 9. 
\end{itemize}
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut \og balayer \fg{} un ensemble pour tester des divisibilités :

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
liste = [2,3,5,10,11]
for div in liste :
	if (2310)%div == 0 :
		print(f"{div} divise 2310",end=" // ")

\end{ConsolePiton}

On peut également définir une \calg{fonction booléenne} pour tester une divisibilité :

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
def divise(n,d):
	if n%d == 0 :
		return True
	else :
		return False

liste = [2,3,5]
for div in liste :
	print(f"{div} divise 2310 : {divise(2310,div)}",end=" // ")
	
\end{ConsolePiton}
\end{cpython}

\section{Division euclidienne}

\subsection{Exemples}

\begin{cexemple}
Six personnes jouent avec un jeu de 32 cartes. L'une d'entre elle distribue les cartes de telle sorte que les 6 joueurs aient le même nombre de pièces. Est-il possible de répartir équitablement toutes les cartes ? Combien chaque joueur en aura-t-il ? Combien en restera-t-il ?

\smallskip

$6 \times 5 \pp 32 \pp 6 \times 6$ et $32=6 \times 5+2$ : chaque joueur aura 5 cartes, et il en restera 2.
\end{cexemple}

\begin{cexemple}
Dimitri a 172 billes. Il décide de les ranger dans des boîtes qui peuvent en contenir 12. Combien de boîtes va-t-il remplir complètement ? Combien mettra-t-il  de billes dans la dernière boîte ?

\smallskip

$172 = 12 \times 14+4$ donc il y aura 14 boîtes complètes, et 4 billes dans la dernière.
\end{cexemple}

\begin{cexemple}
Un confiseur vend des dragées par sachets de 50. Aujourd’hui, il a fabriqué 1\,230 dragées. Combien de sachets pourra-t-il remplir ? Restera-t-il des dragées ? Si oui, combien ? 

\smallskip

$1\,230=50 \times 24+30$ donc il y aura 24 sachets complets, et 30 dragées restantes.
\end{cexemple}

\subsection{Le principe}

\begin{cdefi}
$a$ et $b$ étant deux entiers relatifs tels que $b \neq 0$, il existe un unique couple $(q;r)$  d’entiers relatifs tel que : \[\begin{dcases} a=b \times q+r \\ 0 \pp r < |b| \end{dcases}.\]
L’entier $q$ est le quotient et l’entier $r$ est le reste de cette division.
\end{cdefi}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Écrire l’égalité qui définit la division euclidienne de 99 par 5.
	
	\smallskip
	
	\hspace{5mm}on a $99 = 5 \times 19 + 4$, donc $q=19$ et $r=4$ (avec $0 \pp 4 < 5$ !).
	\item On sait que $133=32 \times 4+5$. Donner le quotient et le reste de la division euclidienne de :
	
	\smallskip
	
	\hspace{5mm}133 par 32 : on a $133=32 \times 4+5$ avec $0 \pp 5 < 32$, donc $q=4$ et $r=5$. 
	
	\smallskip
	
	\hspace{5mm}133 par 4 : $133=4 \times 32+5=4 \times 32 + 4 + 1=4 \times 33 + 1$ avec $0 \pp 1 < 4$, donc $q=33$ et $r=1$. 
\end{itemize}	
\end{cexemple}

\begin{cmethode}[ModifLabel={ calculatrice}]
La partie entière donne le quotient de la division euclidienne de $a$ par $b$.

La fonction \ccalc{MOD()}/\ccalt{reste()}/\ccalt{remainder()}/\ccaln{rem()} donne le reste de la division euclidienne de $a$ par $b$.
\begin{center}
	\includegraphics[height=2cm]{graphics/chap03_diveucl_35}~~\includegraphics[height=2cm]{graphics/chap03_diveucl_83}~~\includegraphics[height=2cm]{graphics/chap03_diveucl_82}~~\includegraphics[height=2cm]{graphics/chap03_diveucl_nwks}
\end{center}
\end{cmethode}

\begin{cpython}
En \calgpython{}, on peut utiliser le \textit{quotient entier} et le \textit{reste} ou bien une \calg{fonction} : 

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
a,b = 99,5
quotient,reste  = a//b,a%b
print(f"{a}={b}*{quotient}+{reste}")

def diveucl(a,b) :
	return a//b,a%b

diveucl(99,5)
\end{ConsolePiton}
\end{cpython}

\subsection{Cas particuliers}

\begin{cprop}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item \textit{Nombres pairs – Nombres impairs - }le reste d’une division euclidienne par 2 est 0 ou 1 donc :
	\begin{itemize}
		\item tout nombre pair peut s’écrire sous la forme $2 k$ avec $k$ nombre entier ;
		\item tout nombre impair peut s’écrire sous la forme $2 k+ 1$ avec $k$ nombre entier.
	\end{itemize}
	\item $b$ divise $a$ si et seulement si le reste de la division euclidienne de $a$ par $b$ est égal à 0.
	\item \textit{Disjonction de cas - }Tout nombre s'écrit sous la forme (à partir d'une valeur de référence $b$):
	
	$(kb)$ ou $(kb+1)$ $\ldots$ ou $(kb+(b-1))$.
\end{itemize}
\end{cprop}

\end{document}