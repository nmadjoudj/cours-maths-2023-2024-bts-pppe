% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton,ecritures}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{customenvs}
\usepackage{worldflags}
\usepackage{vectorlogos}
\GenMacroLogoVect{\logopythonvect}{python}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U2]},typedoc=CHAPITRE~,numdoc=06,titre={Congruences}]
%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\ifdef{\cc}{}{\newcommand{\cc}{\cellcolor{lightgray!50}}}
\ifdef{\Modulo}{}{\newcommand{\Modulo}[1]{\:[#1]}}

\pagestyle{fancy}

\part{CH06 - Arithmétique - Congruences}

\section{Introduction}

\begin{cintro}[Compteur=false]
Si l’on considère la division euclidienne par 3, on peut classer les entiers positifs ou nuls en 3 \textbf{familles} :

\begin{itemize}
	\item les nombres dont le reste de la division par 3 est 0 : $E_0=\{\,0;3;6;9;12;15;\dots\,\}$ ;
	
	ces nombres s'écrivent $3k$ où $k \in \N$ ; ce sont les multiples de 3. 
	\item les nombres dont le reste de la division par 3 est 1 : $E_1=\{\,1;4;7;10;13;16;\dots\,\}$ ;
	
	ces nombres s'écrivent $3k+1$ où $k \in \N$ ; ce sont les multiples de 3 auxquels on a ajouté 1.
	\item les nombres dont le reste de la division par 3 est 2 : $E_2=\{\,2;5;8;11;14;17;\dots\,\}$ ;
	
	ces nombres s’écrivent $3k+2$ où $k \in \N$ ; ce sont les multiples de 3 auxquels on a ajouté 2.
\end{itemize}

On retrouve la même idée que la \textbf{disjonction de cas} avec la division euclidienne.

Cette notion s'étend aux entiers négatifs. On peut en effet considérer les nombres qui s'écrivent :

\begin{itemize}
	\item $3k$ où $k \in \Z$ : $F_0=\{\,\dots;-15;-12;-9;-6;-3;0;3;6;9;\dots\,\}$
	\item $3k+1$ où $k \in \Z$ : $F_1=\{\,\dots;-14;-11;-8;-5;-2;1;4;7;10;\dots\,\}$
	\item $3k+2$ où $k \in \Z$ : $F_2=\{\,\dots;-13;-10;-7;-4;-1;2;5;8;11;\dots\,\}$
\end{itemize}

On dit alors que :

\begin{itemize}
	\item les entiers de l'ensemble $F_0$ sont \textbf{congrus} à 0 modulo 3 (par exemple, $-15$ est congru à 0 modulo 3) ;
	\item deux entiers quelconques de $F_0$ sont \textbf{congrus} entre eux modulo 3 (par exemple, $-8$ et 10).
	
	\medskip
	\item les entiers de l'ensemble $F_1$ sont \textbf{congrus} à 1 modulo 3 (par exemple, 7 est congru à 1 modulo 3) ;
	\item deux entiers quelconques de $F_1$ sont \textbf{congrus} entre eux modulo 3 (par exemple, 4  et $-11$).
	
	\medskip
	\item les entiers de l'ensemble $F_2$ sont \textbf{congrus} à 2 modulo 3 (par exemple, $-7$ est congru à 2  modulo 3) ;
	\item deux entiers quelconques de $F_2$ sont \textbf{congrus} entre eux modulo 3 (par exemple, 5  et 11).
\end{itemize}
\end{cintro}

\section{Définitions et propriétés}

\subsection{Définitions}

\begin{cdefi}[ComplementTitre={ - Théorique}]
Soit $n$ un entier supérieur ou égal à 2, et soient $a$ et $b$ deux entiers quelconques de $\Z$.

On dit que $a$ et $b$ sont \textbf{congrus} modulo $n$ si et seulement si  $a-b$ est un multiple de $n$, c’est-à-dire s’il existe un entier relatif $k$ tel que $a-b=kn$.
\end{cdefi}

\begin{cnota}
Cela se note $a \equiv b \Modulo{n}$ et on lit : « $a$ est \textbf{congru} à $b$ \textbf{modulo} $n$ ».
\end{cnota}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $25 \equiv 18 \Modulo{7}$ car $25-18=7$
	\item $25 \equiv -3 \Modulo{7}$ car $25+3=4 \times 7$
	\item $25 \equiv 4 \Modulo{7}$ car $25-4=3 \times 7$
\end{itemize}
\end{cexemple}

\begin{cdefi}[ComplementTitre={ - Pratique}]
Soit $n$ un entier supérieur ou égal à 2, et soient $a$ et $b$ deux entiers quelconques de $\Z$.

On dit que $a$ et $b$ sont congrus modulo $n$ si et seulement si $a$  et  $b$ ont le \textbf{même reste} dans la division par $n$.
\end{cdefi}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $25 \equiv 18 \Modulo{7}$ car 25 et 18 ont le même reste (4) dans la division par 7.
	\item $65 \equiv 17 \Modulo{7}$ car 65 et 17 ont le même reste (5) dans la division par 12.
\end{itemize}
\end{cexemple}

\begin{ccscq}
\begin{itemize}[leftmargin=*]
	\item $a \equiv 0 \Modulo{n} \ssi a$ est un multiple de $n$.
	\item Si $a=nq+r$ avec $0 \pp r < n$  alors $a \equiv r \Modulo{n}$.
	\item Si $a \equiv r \Modulo{n}$ avec $0 \pp r<n$  alors  $r$ est le reste dans la division de $a$ par $n$.
\end{itemize}
\end{ccscq}

\begin{crmq}[ComplementTitre={ - Méthode}]
\begin{itemize}[leftmargin=*]
	\item Il faut bien se souvenir du fait que $r$ est le plus petit entier positif  congru à $a$ modulo $n$.
	\item Concrètement, la calculatrice permet, comme pour le reste de la division euclidienne, de déterminer la congruence la plus utile.
\end{itemize}
\end{crmq}

\begin{ccalco}[ComplementTitre={ - Méthode}]
On a, par exemple : $50 \equiv 0 \Modulo{10}$ car $50 = 10 \times 5 + 0$ ; $100 \equiv 1 \Modulo{9}$ car $100 = 9 \times 11 + 1$ ou $25 \equiv 4 \Modulo{7}$ car $25 = 7 \times 3 + 4$.
\begin{center} 
	\includegraphics[height=2.5cm]{./graphics/chap06_mod_82}~~\includegraphics[height=2.5cm]{./graphics/chap06_mod_83}~~\includegraphics[height=2.5cm]{./graphics/chap06_mod_35}
	
	\smallskip
	
	\includegraphics[height=2.5cm]{./graphics/chap06_mod_90}~~\includegraphics[height=2.5cm]{./graphics/chap06_mod_nwks}
\end{center}
\end{ccalco}

\begin{cexo}
Vrai ou Faux ? Justifier.

\begin{MultiCols}[Type=enum](4)
	\item $17 \equiv 42 \Modulo{5}$ ;
	\item $175 \equiv 2 \Modulo{7}$ ;
	\item $15 \equiv 3 \Modulo{7}$ ;
	\item $544 \equiv 4 \Modulo{10}$ ;
	\item $151 \equiv 1 \Modulo{2}$ ;
	\item $32\,464 \equiv 1 \Modulo{2}$ ;
	\item $25 \equiv 43 \Modulo{17}$ ;
	\item $100 \equiv 1 \Modulo{11}$ ;
	\item $1\,000 \equiv 1 \Modulo{11}$ ;
	\item $-1\,000 \equiv 1 \Modulo{11}$ ;
	\item $12\,345 \equiv 5 \Modulo{10}$
	\item $7\,852 \equiv 1 \Modulo{4}$.c
\end{MultiCols}
\end{cexo}

\begin{cpython}
Comme on l'a déjà vu, le \og modulo \fg{} (le plus petit) peut être obtenu en \logopythonvect{} avec l'opérateur \textit{reste} \cpy{\%} :

\begin{ConsolePiton}<Alignement=center,Largeur=16cm>{}
50%10
100%9
25%7
\end{ConsolePiton}
\end{cpython}

\subsection{Propriétés}

\begin{cprop}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Si $a \equiv b \Modulo{n}$ et $b \equiv c \Modulo{n}$ alors $a \equiv c \Modulo{n}$. \hfill~\textcolor{CouleurVertForet}{Transitivité}
	\item Si $a \equiv b \Modulo{n}$ alors $a+c \equiv b+c \Modulo{n}$ et $ac \equiv bc \Modulo{n}$.
\end{itemize}
\end{cprop}

\begin{cthm}
Si $a$, $a'$, $b$, $b'$ sont des entiers tels que $a \equiv a' \Modulo{n}$ et $b \equiv b' \Modulo{n}$ alors :
\begin{itemize}[leftmargin=*]
	\item $a+b \equiv a'+b' \Modulo{n}$ et $a-b \equiv a'-b' \Modulo{n}$ ;\hfill~\textcolor{CouleurVertForet}{Compatibilité avec $+$ et $-$}
	\item $a \times b \equiv a' \times b' \Modulo{n}$ ;\hfill~\textcolor{CouleurVertForet}{Compatibilité avec $\times$}
	\item $k \times a' \equiv k \times a \Modulo{n}$ avec $k$ un entier ;
	\item $a^k \equiv (a')^k \Modulo{n}$ avec $k$ un entier. \hfill~\textcolor{CouleurVertForet}{Compatibilité avec la puissance}
\end{itemize}
\end{cthm}

\begin{casavoir}
La grande idée des congruences est de \og réduire \fg{} les nombres entiers sur lesquels on travaille, en sachant qu'il y a deux congruences qui sont très \textit{intéressantes} :
\begin{itemize}
	\item la plus petite (positive), celle qui est liée au reste $r$ de la division euclidienne ;
	\item la plus petite (négative), obtenue par une toute petite soustraction.
\end{itemize}
$\rhd~25 \equiv 4 \Modulo{7} \equiv -3 \Modulo{7}$ (car $4-7=-3$ !)

$\rhd~100 \equiv 1 \Modulo{9} \equiv -8 \Modulo{9}$ (car $1-9=-8$ !)

$\rhd~111 \equiv 7 \Modulo{8} \equiv -1 \Modulo{8}$ (car $7-8=-1$ !)
\end{casavoir}

\begin{cpython}
En \logopythonvect{}, on peut déterminer facilement les deux congruences utiles :

\begin{ConsolePiton}<Alignement=center,Largeur=16cm>{}
def reductions_modulo(n,a):
	pos  = n%a
	neg = pos - a
	return pos,neg

reductions_modulo(100,9)
\end{ConsolePiton}
\end{cpython}

\begin{cexemple}[Pluriel]
On a $21 \equiv 1 \Modulo{5}$ et $ 9 \equiv -1 \Modulo{5}$, donc (sans calculatrice) :

\begin{MultiCols}[Type=enum](2)
	\item $21+9 \equiv 1+(-1) \Modulo{5}$ soit $30 \equiv 0  \Modulo{5}$ ;
	\item $21-9 \equiv 1-(-1) \Modulo{5}$ soit $12 \equiv 2  \Modulo{5}$ ;
	\item $21 \times 9 \equiv 1 \times (-1) \Modulo{5}$ soit $189 \equiv -1  \Modulo{5}$ ;
	\item $21 \times 8 \equiv 1 \times 8 \Modulo{5}$ soit  $168 \equiv 8  \Modulo{5}$ ;
	\item $9^2 \equiv (-1)^2 \Modulo{5}$ soit $81 \equiv 1 \Modulo{5}$ ;
	\item $21^{250} \equiv 1^{250} \Modulo{5} \equiv 1 \Modulo{5}$ soit $21^{250} \equiv 1 \Modulo{5}$.
\end{MultiCols}
\end{cexemple}

\begin{cexo}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Sans utiliser de calculatrice et sans effectuer l'addition, montrer que $1\,207+485$ est un multiple de 12.
	
	\hspace{5mm}$\rhd~1\,207 \equiv 7 \Modulo{12}$ et $485 \equiv 5 \Modulo{12}$
	
	\hspace{5mm}$\rhd$~ainsi, $1\,207 + 485 \equiv 7 + 5 \Modulo{12} \equiv 12 \Modulo{12} \equiv 0 \Modulo{12}$, CQFD !
	\item De même montrer que $9^{10}-5^{10}$ est un multiple de 7. 
	
	\hspace{5mm}$\rhd~9 \equiv 2 \Modulo{7}$ donc $9^{10} \equiv 2^{10} \Modulo{7}$
	
	\hspace{5mm}$5\equiv -2 \Modulo{7}$ donc $5^{10} \equiv (-2)^{10} \Modulo{7} \equiv 2^{10} \Modulo{7}$ ;
	
	\hspace{5mm}$\rhd$~ainsi $9^{10}-5^{10} \equiv 2^{10} - 2^{10} \Modulo{7} \equiv 0 \Modulo{7}$, CQFD !
\end{itemize}
\end{cexo}

\begin{cpython}
En \logopythonvect{}, on peut travailler sur de grands entiers, sans problème :

\begin{ConsolePiton}<Alignement=center,Largeur=16cm>{}
(1207+485)%12
(9**10-5**10)%7
\end{ConsolePiton}
\end{cpython}

\begin{cexo}[Compteur=false]
\begin{enumerate}[leftmargin=*]
	\item Compléter par le plus petit entier positif : 
	
	\begin{enumerate}
		\item $120\equiv \dots \Modulo{7}$ ;
		\item $45\equiv \dots \Modulo{7}$. 
	\end{enumerate}
	\item En utilisant les propriétés des congruences et les résultats de la question précédente, compléter :
	
	\begin{MultiCols}[Type=enum](3)
		\item $240\equiv \dots \Modulo{7}$ ;
		\item $360\equiv \dots \Modulo{7}$ ;
		\item $ 120^2\equiv \dots \Modulo{7}$ ;
		\item $120+45\equiv \dots \Modulo{7}$ ;
		\item $3 \times 120+45\equiv \dots \Modulo{7}$ ;
		\item $45^2\equiv \dots \Modulo{7}$.
	\end{MultiCols}
	\item Donner les restes de la division euclidienne par 7 des nombres suivants :
	
	\begin{MultiCols}[Type=enum](3)
		\item 50 ;
		\item $50^{100}$ ;
		\item 100 ;
		\item $100^2$ ;
		\item $50^{100}+100+100^2$;
		\item $100^2 \times 50^{100}$.
	\end{MultiCols}
\end{enumerate}
\end{cexo}

\pagebreak

\section{Applications classiques}

\subsection{Démontrer une divisibilité}

\begin{cexo}
On souhaite démontrer que, pour tout entier $n$, $13^n-1$ est un multiple de 4. On travaille donc \textbf{modulo 4}.
\end{cexo}

\begin{cresol}
\begin{itemize}[leftmargin=*]
	\item on a directement que $13  \equiv 0 \Modulo{4}$ et de ce fait $13^n  \equiv 1^n \Modulo{4} \equiv 1 \Modulo{4}$ ce qui donne $13^n - 1  \equiv 1 - 1 \Modulo{4} \equiv 0 \Modulo{4}$ ;
	\item c'est donc que 4 divise $13^n-1$.
\end{itemize}
\end{cresol}

\begin{cpython}
On peut tester, avec \logopythonvect, la divisibilité des entiers $13^n-1$ par 4, pour $n$ allant de 0 à \num{1000} :

\begin{CodePiton}[Largeur=16cm,Alignement=center]{}
def testdivisib():
	res = True
	for i in range(0,1001) :
		if (13**i-1)%4 != 0 : #si une divisibilité n'est pas bonne, on passe en False
			res = False
	return res
\end{CodePiton}

\begin{python}
def testdivisib():
	res = True
	for i in range(0,1001) :
		if (13**i-1)%4 != 0 :
			res = False
	return res

\end{python}

\begin{ConsolePiton}<Alignement=center,Largeur=16cm>{}
testdivisib()
\end{ConsolePiton}
\end{cpython}

\subsection{\og Équation \fg{} congruence}

\begin{cexo}
On souhaite résoudre l'équation $7x \equiv 3 \Modulo{26}$. L'idée est \og d'\textbf{enlever} \fg{} le 7 !
\end{cexo}

\begin{cresol}
\begin{itemize}[leftmargin=*]
	\item comme pour les matrices, on revient au fait que \og diviser revient à multiplier par l'inverse \fg ;
	\item en essayant avec la calculatrice, on trouve que $7 \times 15 \equiv 1 \Modulo{26}$ ;
	\item de ce fait, $7x \equiv 3 \Modulo{26} \Rightarrow \underbrace{15 \times 7}_{1}x \equiv 15 \times 3 \Modulo{26} \Rightarrow x \equiv 45 \Modulo{26} \equiv 19 \Modulo{26} \Rightarrow x \equiv 19 \Modulo{26}$ !
\end{itemize}
\end{cresol}

\begin{cpython}
On peut utiliser \logopythonvect{} pour déterminer l'inverse (quand elle existe) d'un entier $a$ modulo $n$ :

\begin{CodePiton}[Largeur=16cm,Alignement=center]{}
def inversemodulo(a,n):          # version non optimisée
	res = False
	for i in range(1,n+1) :
		if (a*i)%n == 1 :
			res = i
	return res
\end{CodePiton}
%
\begin{python}
def inversemodulo(a,n):
	res = False
	for i in range(1,n+1) :
		if (a*i)%n == 1 :
			res = i
	return res

\end{python}

\begin{ConsolePiton}<Alignement=center,Largeur=16cm>{}
inversemodulo(7,26)
\end{ConsolePiton}
\end{cpython}

\end{document}