% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U2]},typedoc=CHAPITRE~,numdoc=02,titre={Logique propositionnelle}]
%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}
%divers


\begin{document}

\pagestyle{fancy}

\part{CH02 - Logique - Calcul propositionnel}

\section{Introduction}

\begin{cintro}[Compteur=false]
Dans ce chapitre, on présente quelques rudiments de logique mathématique (appelée aussi \textbf{logique formelle}), dont l'étude remonte au moins jusqu'à \textit{Aristote}. La connaissance de ces notions sert à :
\begin{itemize}[leftmargin=*]
	\item comprendre certains raisonnements mathématiques, même à un niveau très élémentaire : beaucoup d'erreurs, en mathématiques, proviennent souvent d'une ignorance des règles de base de la logique ;
	\item formuler des instructions algorithmiques, même dans un langage de programmation évolué. Là encore, les maladresses de l'informaticien débutant relèvent souvent d'une méconnaissance de la logique.
\end{itemize}
\end{cintro}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{Aristote}{}\textit{Aristote} ($-384$ ; $-322$, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{GR}\!) n’est pas à proprement parler un mathématicien mais par sa vision philosophique des sciences, il a participé au développement des mathématiques. \\ Dans ses traités, \textbf{\textit{Les Topiques}} et \textbf{\textit{Les Analytiques}}, Aristote expose des réflexions dans le domaine de la logique et du raisonnement. Il y définit le syllogisme, sorte de schéma logique menant à une conclusion à partir de la conjonction de deux propositions considérées comme vraies.
\end{chistoire}

\begin{crmq}[Compteur=false]
La logique formelle étant binaire (on dit aussi : \textbf{booléenne}, du nom du mathématicien anglais George Boole (1815--1864), elle est souvent très éloignée du  \textbf{bon sens} quotidien, et emploie certains \textit{mots} (comme les mots \og et \fg, \og ou \fg, etc) dans un sens qui n'est pas toujours celui du langage ordinaire.
\end{crmq}

\begin{chistoire}[ComplementTitre={ - Anecdote}]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{Boole}{}\textit{George Boole} (1815-1864, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{UK}\!) enseignait au \textit{Queen's College} à Cork et un jour qu'il faisait le trajet de 3~km à pied depuis son domicile et qu'il pleuvait, il attrapa une pneumonie. Sa femme, pensant qu'un remède doit ressembler à la cause, le mit au lit et lui jeta des bassines d'eau pendant plusieurs jours !
\end{chistoire}

\section{Propositions}

\subsection{Définition}

\begin{cdefi}
Une proposition est une affirmation à laquelle on peut attribuer une unique \textbf{valeur de vérité} parmi les deux valeurs possibles : \textbf{VRAI} ou \textbf{FAUX} (en abrégé :  V ou F).
\end{cdefi}

\begin{cexemple}[Pluriel]
$\bullet~~$P : \og 9 est un carré parfait \fg \hfill{}$\bullet~~$Q : \og $2^2 + 3^2 = 5^2$ \fg \hfill{}$\bullet~~$R : \og Il pleut \fg \hfill{}~
\end{cexemple}

\begin{cpython}
En \calgpython, on a les valeurs de vérité \cpy{True} et \cpy{False}.

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
2**2 + 3**2 == 5**2
\end{ConsolePiton}
\end{cpython}

\subsection{Remarque et exemples}

\begin{cexemple}[Pluriel,ModifLabel= { et Remarques}]
Par exemple, la phrase  \og 4 est un nombre réel positif \fg{} (symbolisée mathématiquement  par  \og $4  \pg 0$ \fg) est une proposition VRAIE. Par conséquent la phrase  \og $4 < 0$ \fg{} est une proposition FAUSSE

\smallskip

Les valeurs de vérité \textsf{V} ou \textsf{F} sont souvent notées respectivement \textsf{1} et \textsf{0}.
\end{cexemple}

\begin{cidee}[Compteur=false]
De façon intuitive, une proposition est tout simplement une phrase qui peut être qualifiée de "vraie" ou bien de "fausse" à l'exclusion de toute autre possibilité.

C'est ce qu'on appelle le principe du \textit{tiers exclu}, qui signifie, littéralement, qu'il n'y a pas de troisième possibilité. La logique formelle présuppose toujours qu'une proposition est soit vraie soit fausse, indépendamment de notre jugement ou de notre perception. C'est déjà un premier point essentiel, qui différencie le "vrai" et le "faux" de la logique formelle des notions de "vérité" et de  "fausseté" de la vie quotidienne, qui sont souvent versatiles et subjectives.
\end{cidee}

\begin{crmq}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Il existe en mathématiques de nombreuses propositions dont on ignore la valeur de vérité.
	\item Certaines affirmations, même sémantiquement cohérentes, ne sont pas des propositions. Par exemple, la phrase "tout ce que je dis est faux" n'est pas une proposition, on ne peut pas associer de valeur de vérité à cette affirmation.
	\item Des phrases impératives ou interrogatives ne sont pas considérées comme des propositions.
	\item En mathématiques, il existe des propositions que l'on ne démontre pas et qu'on appelle des \textbf{axiomes}. Les autres propositions d'une théorie mathématique (celles dont on  démontre la vérité) sont appelées théorèmes ou, tout simplement, "propositions" (sous-entendu : propositions vraies).
\end{itemize}
\end{crmq}

\section{Connecteurs logiques}

\subsection{Intro}

\begin{ccadre}[Compteur=false]
Étant données des propositions P, Q, R, \dots, on peut construire des nouvelles propositions plus complexes en formant des liaisons entre celles-ci à l'aide de signes (qu'on appelle des connecteurs logiques) et de quelques règles logiques. On forme ainsi ce qu'on nomme le "calcul propositionnel" (ou logique des propositions), dont l'usage est fondamental aussi bien en automatique qu'en informatique ou en théorie des langages. 
\end{ccadre}

\subsection{Négation}

\begin{cdefi}
Si P est une proposition, on note $\lnot P$ (ou encore $\overline{P}$) (lire « non P », ou « contraire de P ») l'assertion qui est vraie si et seulement si P est fausse.
\end{cdefi}

\begin{cillustr}
Ce connecteur de négation peut se représenter par une table de correspondance, appelée \textbf{table de vérité} :
\begin{center}
	\begin{tblr}{stretch=1.25,hlines,vlines,colspec={Q[1cm,m,c]Q[1cm,m,c]},row{1}={bg=gray!25}}
		P & $\lnot P$ \\
		V&F \\
		F&V \\
	\end{tblr}
\end{center}
\end{cillustr}

\begin{cexemple}[Pluriel]
$\bullet~~$P : \og $x \pp 4$ \fg{} $\Rightarrow \lnot P$ :

$\bullet~~$R : \og Le soleil brille \fg{} $\Rightarrow \lnot R$ :

$\bullet~~$Q : \og Toutes les Ferrari sont rouges \fg{} $\Rightarrow \lnot Q$ :

$\bullet~~$T :  \og $\pi>0$ \fg{} est VRAIE, donc $\lnot T$ est FAUSSE

$\bullet~~$Table de vérité de $\lnot \left( \lnot P \right)$
\end{cexemple}

\begin{cpython}
En \calgpython, l'opérateur de négation est \cpy{not}.

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
not (True) ; not (False) ; not (2**2 + 3**2 == 5**2)
\end{ConsolePiton}
\end{cpython}

\subsection{Équivalence}

\begin{cdefi}
La proposition $P  \ssi Q$ (lire "P est équivalent à Q") est la proposition qui est vraie si et seulement P et Q ont la même valeur de vérité.
\end{cdefi}

\begin{cillustr}
On a la table de vérité suivante :
\begin{center}
	\begin{tblr}{stretch=1.25,hlines,vlines,colspec={Q[1cm,m,c]Q[1cm,m,c]Q[1.5cm,m,c]},row{1}={bg=gray!25}}
		P & Q & $P \ssi Q$ \\
		V&V &V \\
		V&F &F \\
		F&V &F \\
		F&F &V \\
	\end{tblr}
\end{center}
\end{cillustr}

\begin{crmq}[Compteur=false]
D'après l'exercice du paragraphe précédent, $\left( \lnot \left( \lnot P \right) \right) \ssi P$
\end{crmq}

\begin{cprop}
Pour signifier une équivalence, on utilise souvent les termes "si et seulement si" : $P \ssi Q$ se traduit par : "P est vraie" si et seulement si "Q est vraie".

En pratique, $P \ssi Q$  peut être compris comme une sorte d'\textbf{égalité logique} (résolution d'équations\ldots).
\end{cprop}

\begin{cpython}
En \calgpython, l'opérateur d'équivalence est \cpy{==}.

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
True == False ; False == False ; True == True
\end{ConsolePiton}
\end{cpython}

\subsection{Conjonction}

\begin{cdefi}
Si P et Q sont deux propositions, on note $P \land Q $ (lire "P et Q") la 
proposition qui est vraie si et seulement si les propositions P et Q sont 
toutes les deux vraies.  Sinon, elle est fausse.
\end{cdefi}

\begin{cillustr}
On a la table de vérité suivante :
\begin{center}
	\begin{tblr}{stretch=1.25,hlines,vlines,colspec={Q[1cm,m,c]Q[1cm,m,c]Q[1.5cm,m,c]},row{1}={bg=gray!25}}
		P & Q & $P \land Q$ \\
		V&V &V \\
		V&F &F \\
		F&V &F \\
		F&F &F \\
	\end{tblr}
\end{center}
\end{cillustr}

\begin{crmq}[Compteur=false]
On peut faire l'analogie entre l'opération $\land$ de la logique formelle et le \textbf{montage en série} de deux interrupteurs en électricité.
\end{crmq}

\begin{cexemple}[Pluriel]
On sait que Jean n'a pas obtenu son permis de conduire et que Sophie a eu son code.

Soit P : "Jean n'a pas obtenu son permis" et soit Q : "Sophie a eu son code". Alors $P \land Q$ est VRAIE

\smallskip

Soient A la proposition : "$3 > 5$" et B :  "$2 + 4 = 6$". Alors $A \land B$ est FAUSSE
\end{cexemple}

\begin{cpython}
En \calgpython, l'opérateur de conjonction est \cpy{and} ou \cpy{\&}.

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
True and True
True and False
False and False
A = (3>5) ; B = (2+4==6) ; A and B
True & True
True & False
False & False
\end{ConsolePiton}
\end{cpython}

\subsection{Disjonction}

\begin{cdefi}
Si P et Q sont deux propositions, on note $P\lor Q$ (lire "P ou Q") la proposition qui est fausse si et seulement si  P et Q sont toutes les deux fausses. Sinon, elle est vraie.
\end{cdefi}

\newpage

\begin{cillustr}
On a la table de vérité suivante :
\begin{center}
	\begin{tblr}{stretch=1.25,hlines,vlines,colspec={Q[1cm,m,c]Q[1cm,m,c]Q[1.5cm,m,c]},row{1}={bg=gray!25}}
		P & Q & $P \lor Q$ \\ 
		V&V &V \\
		V&F &V \\
		F&V &V \\
		F&F &F \\
	\end{tblr}
\end{center}
\end{cillustr}

\begin{crmq}[Compteur=false]
On peut faire l'analogie entre l'opération $\lor$ de la logique formelle et le \textbf{montage en parallèle} de deux interrupteurs en électricité.
\end{crmq}

\begin{cidee}
La table de vérité ci-contre montre que $P \lor Q$ est vraie si et seulement si au moins une des deux propositions P et Q est vraie. Il faut donc distinguer l'expression « P ou Q » employée ici (qui n'exclut pas que les deux termes soient vrais) du « ou » du langage ordinaire qui est souvent utilisé dans un sens \textit{exclusif} qu'il n'a pas en logique. Par exemple, dans la vie quotidienne, si un menu indique : « fromage ou dessert », il est implicitement entendu qu'on ne peut pas prendre les deux. On dit que le « ou » logique est \textit{inclusif}.
\end{cidee}

\begin{cexemple}[Pluriel]
$\bullet~~P \lor (\lnot P)$ est VRAIE (soit encore $P \lor (\lnot P) \ssi \mathcallig{V}$ : principe du tiers exclu, qui est une \textit{tautologie}).

$\bullet~~P \land (\lnot P)$ est FAUSSE (soit encore $P \land (\lnot P) \ssi \mathcallig{F}$ : principe de la non-contradiction).

\smallskip

On démontre facilement ces résultats à l'aide d'une table de vérité.
\end{cexemple}

\begin{cpython}
En \calgpython, l'opérateur de disjonction est \cpy{or} ou bien \cpy{|}.

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
True or True, True | True
True or False, True | False
False or False, False | False
\end{ConsolePiton}
\end{cpython}

\subsection{Lois de Morgan}

\begin{cthm}
Soient P et Q deux propositions
\begin{itemize}[leftmargin=*]
	\item $\lnot \left( P \land Q \right) \ssi \left( \lnot P\right) \lor \left( \lnot Q\right)$ ou encore $\overline{P \land Q} \ssi \overline{P} \lor \overline{Q}$
	\item $\lnot \left( P \lor Q \right) \ssi \left( \lnot P\right) \land \left( \lnot Q\right)$ ou encore $\overline{P \lor Q} \ssi \overline{P} \land \overline{Q}$
\end{itemize}
\end{cthm}

\begin{crmq}[Compteur=false]
Ces propriétés sont fondamentales pour simplifier le contraire de propositions composées de \textit{conjonctions} et/ou de \textit{disjonctions}.
\end{crmq}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Démonstration par table de vérité
	\begin{center}
		\begin{tblr}{stretch=1.25,hlines,vlines,colspec={Q[1cm,m,c]Q[1cm,m,c]*{3}{Q[1.35cm,m,c]}*{2}{Q[2cm,m,c]}},row{1}={bg=gray!25}}
			P & Q & $\lnot P$ & $\lnot Q$ & $P \land Q$ & $\lnot \left( P \land Q \right)$ & $\left( \lnot P\right) \lor \left( \lnot Q\right)$ \\ 
			V & V & F & F & V & F & F \\
			\dots &  &  &  &  &  &  \\
			\dots &  &  &  &  &  &  \\
		\end{tblr}
	\end{center}
	\item \og Cet arbre est un hêtre ou un charme \fg
	\item \og J'ai eu la moyenne en Anglais et en Maths \fg
	\item $(x<0)$ ou $(n=4)$
	\item (age $>$ 18) et (age $\pp$ 25)
	\item ($n$ est pair) ou ($n=7$)
	\item Le bulletin météorologique affirme : \og Demain, il pleuvra et il y aura du vent \fg
	\item \og Je baisserai les impôts ou j'éradiquerai le chômage \fg
\end{itemize}
\end{cexemple}

%\subsection{Connecteur OUX}
%
%\begin{cdefi}
%La proposition $\left( \lnot P \land Q \right) \lor \left( P \land \lnot Q \right)$ n'est vraie que lorsque les deux propositions P et Q ont des valeurs de vérité différentes, c'est-à-dire lorsque l'une est vraie et l'autre est fausse. Il s'agit en fait de la proposition P \textbf{OUX} Q, le connecteur "OUX" étant le "OU exclusif".
%\end{cdefi}
%
%\begin{cprop}
%On a (A OUX B) OUX B = A (table de vérité)
%\end{cprop}
%
%\begin{crmq}[ - Exemple]
%Cette propriété  rend  le connecteur OUX très utile en informatique, notamment en cryptographie :
%\begin{itemize}
%	\item Soit un message à coder : A=0110 1100. On choisit une clé (succession aléatoire de 0 et de 1) de la même longueur que le message. Cette clé est secrète et n'est connue que de l'expéditeur du message et de son destinataire.
%	\item Soit B=1011 0110 cette clé. Le message codé est alors C=A OUX B=1101 1010.
%	\item Le décodage s'effectue en faisant C OUX B=0110 1100=A
%\end{itemize}
%Ce système, bien que très simple dans son principe, peut s'avérer inviolable si la suite de chiffres de la clé est vraiment aléatoire. Cette dernière ne doit en outre être utilisée qu'une seule fois (on parle de masque jetable ou encore de «one-time pad»). Dans cette phrase, c'est surtout le mot «aléatoire» qui s'avère être le plus difficile à mettre en œuvre. Mais lorsque la clé est vraiment aléatoire ce système est parfaitement sûr. C'est le seul chiffrement aboutissant à une sécurité absolue, en théorie.
%\end{crmq}

\section{Propriétés}

\subsection{Premières propriétés des connecteurs}

\begin{cprop}[Pluriel]
Soient P, Q et R des propositions :
\begin{itemize}
	\item $P \land Q \ssi Q \land P$ et $P \lor Q \ssi Q \lor P$ \hfill{}Commutativité
	\item $P \land (Q \lor R) \ssi (P \land Q) \lor (P \land R)$\hfill{}Double distributivité
	
	$P \lor (Q \land R) \ssi (P \lor Q) \land (P \lor R)$
	\item $P \land (Q \land R) \ssi (P \land Q) \land R \ssi P \land Q \land R$\hfill{}Associativé
	
	$P \lor (Q \lor R) \ssi (P \lor Q) \lor R \ssi P \lor Q \lor R$
\end{itemize}
\end{cprop}

\begin{crmq}[Compteur=false,ComplementTitre={ - Principe de la dualité}]
Les règles de logique, sauf l'involution de la négation, fournissent chacune une paire de propriétés, telle que la deuxième est obtenue de la première en permutant $\land$ et $\lor$ ainsi que V et F.

C'est ce qu'on appelle le principe de dualité : à chaque fois qu'on établit une propriété, on établit aussi, du même coup, une propriété duale.
\end{crmq}

\begin{cprop}[Pluriel]
Soit $\mathcallig{V}$ une proposition vraie, et $\mathcallig{F}$ une proposition fausse. Alors, pour toute proposition $P$ :
\begin{itemize}[leftmargin=*]
	\item $(P \land \mathcallig{V}) \ssi P $,
	\item $(P \land \mathcallig{F}) \ssi \mathcallig{F} $,
	\item $(P \lor \mathcallig{V}) \ssi \mathcallig{V}$,
	\item $(P \lor \mathcallig{F}) \ssi P$.
\end{itemize}
On dit que $\mathcallig{V}$ est \underline{l’élément neutre} du connecteur $\land$, et \underline{l’élément absorbant} du connecteur $\lor$.

De même $\mathcallig{F}$ qui est \underline{l’élément neutre} du connecteur $\lor$, et \underline{l’élément absorbant} du connecteur $\land$.
\end{cprop}

\subsection{Implication}

\begin{cprop}
La proposition $(\lnot P \lor Q)$ [lire "non P ou Q"] est aussi notée $(P \Rightarrow Q$)  [lire  "P implique Q" ou encore "Si P est vraie alors Q est vraie"]. Le symbole "$\Rightarrow$" est appelé implication.
\end{cprop}

\begin{cillustr}
Table de vérité :
\begin{center}
	\begin{tblr}{stretch=1.25,hlines,vlines,colspec={Q[1cm,m,c]Q[1cm,m,c]*{2}{Q[1.35cm,m,c]}},row{1}={bg=gray!25}}
		P & Q & $\lnot P$ & $\lnot P \lor Q$ \\ 
		V&V &F &V\\
		V&F &F &F\\
		F&V &V &V\\
		F&F &V &V\\
	\end{tblr}
	~~~~~
	\begin{tblr}{stretch=1.25,hlines,vlines,colspec={Q[1cm,m,c]Q[1cm,m,c]Q[1.35cm,m,c]},row{1}={bg=gray!25}}
		P & Q & $P \Rightarrow Q$ \\
		V&V &V \\
		V&F &F \\
		F&V &V \\
		F&F &V \\
	\end{tblr}
\end{center}
Autrement dit la proposition $P \Rightarrow Q$ est la proposition qui n'est fausse que lorsque P est vraie \uline{et} Q est fausse.
\end{cillustr}

\begin{cvoc}
Dans l'implication $P\Rightarrow Q$, P est une condition \textbf{suffisante} de Q et Q est une condition \textbf{nécessaire} de P.
\end{cvoc}

\begin{cpython}
En \calgpython, l'implication n'est pas implémentée (enfin si\ldots{} mais c'est \og contre-intuitif \fg), on peut la \og programmer \fg.

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
P = True ; Q = True ; print(f"{P} => {Q} : {not P or Q}")
P = True ; Q = False ; print(f"{P} => {Q} : {not P or Q}")
P = False ; Q = True ; print(f"{P} => {Q} : {not P or Q}")
P = True ; Q = True ; print(f"{P} => {Q} : {not P or Q}")
\end{ConsolePiton}
\end{cpython}

\subsection{Réciproque et négation d'une implication}

\begin{cdefi}
L'implication "$Q \Rightarrow P$" est la réciproque de l'implication "$P \Rightarrow Q$".
\end{cdefi}

\subsection{Contrapposée d'une implication}

\begin{cdefi}
La contraposée de l'implication $P \Rightarrow Q$ est l'implication $\lnot Q \Rightarrow \lnot P$
\end{cdefi}

\begin{crmq}[s]
$\bullet~~$Il ne pas confondre contrapposée et réciproque !

$\bullet~~$L'équivalence $(P \ssi Q)$ est la conjonction des deux implications
$(P \Rightarrow Q) \land (Q \Rightarrow P)$
\end{crmq}

%\section{Calcul des prédicats}
%
%\subsection{Définition}
%
%\defi{
%Un \textbf{prédicat} est une proposition dont la valeur de vérité dépend d'une ou plusieurs variables.
%}
%
%\espacepara
%
%\subsection{Quantificateur universel}
%
%\defi{
%Pour toute variable $x$, $P(x)$ est vrai", ou encore : "Quelle que soit la variable $x$, $P(x)$ est vrai" se traduit symboliquement par : $$\forall x,\,P(x)$$
%Le symbole $\forall$ (qui se lit "pour tout" ou "quel que soit") est appelé quantificateur universel. 
%
%On lit : "Pour tout $x$, on a $P(x)$" ou "Quel que soit $x$, on a $P(x)$".
%}
%
%\rmq{
%Si $P(x)$ est un prédicat et E un ensemble, $(\forall x \in E,\,P(x))$ est une proposition.
%}
%
%\methode{
%Pour démontrer qu'une proposition telle que : $(\forall x,\,P(x))$ est VRAIE, il faut examiner \textbf{tous} les cas et prouver par une \textbf{démonstration} que $P(x)$ est vrai.
% 
%Pour démontrer qu'une telle proposition est FAUSSE, il suffit de trouver un cas dans lequel $P(x)$ est faux (un \textit{contre exemple}).
%}
%
%\espacepara
%
%\subsection{Quantificateur existentiel}
%
%\defi{
%"Il existe une variable $x$ pour laquelle $P(x)$ est vrai", ou encore : "Il existe une variable $x$ telle que $P(x)$ est vrai" se traduit symboliquement par : $$\exists x,\,P(x).$$
%Le symbole $\exists$ (qui se lit "il existe") est appelé quantificateur existentiel.
%
%On lit : "Il existe $x$ pour lequel on a $P(x)$" ou "Il existe $x$ tel qu'on a $P(x)$".
%}
%
%\rmq{
%Si $P(x)$ est un prédicat et E un ensemble, $(\exists x \in E,\,P(x))$ est une proposition.
%}
%
%\methode{ 
%Pour démontrer qu'une proposition telle que : $(\exists x \in E,\,P(x))$ est VRAIE, il suffit de trouver un exemple pour lequel $P(x)$ est vrai (un cas "qui marche").
% 
%Pour démontrer qu'une telle proposition est FAUSSE , il faut démontrer que $P(x)$ est faux dans \textit{tous} les cas.
%}
%
%\espacepara
%
%\subsection{Ordre des quantificateurs}
%
%\prop[s]{
%Il faut savoir : 
%\begin{itemize}[leftmargin=*]
%	\item $\lnot (\forall x \in E,\,P(x)) \ssi (\exists x \in E,\, \lnot P(x))$
%	\item $\lnot (\exists x \in E,\,P(x)) \ssi (\forall x \in E,\, \lnot P(x))$
%\end{itemize}
%}
%
%\rmq{
%Ce résultat autorise certains automatismes lorsqu'il s'agit d'exprimer la négation de propositions comportant un (ou plusieurs) quantificateur(s). 
%Pour exprimer la négation d'une telle proposition, on remplace $\forall$ par $\exists$ et inversement, $\lor$  par $\land$  et inversement, $P$ par $\lnot P$, \dots{} tout en respectant l'\textbf{ordre} des quantificateurs.
%}
%
%\espacepara
%
%\subsection{Ordre des quantificateurs}
%
%\prop{
%Dans une proposition comportant plus d'un quantificateur, il ne faut pas modifier l'ordre de ces quantificateurs.
%}

\end{document}