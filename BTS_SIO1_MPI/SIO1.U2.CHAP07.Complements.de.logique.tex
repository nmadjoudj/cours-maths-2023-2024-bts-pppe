% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage{ProfSio}
\useproflyclib{ecritures,piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{wrapstuff}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO1},matiere={[U2]},typedoc={CHAPITRE~},numdoc={7},titre={Compléments de logique}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
%\setlength{\multicolsep}{6.0pt plus 2.0pt minus 1.5pt}% 50% of original values

\begin{document}

\pagestyle{fancy}

\part{CH07 - Compléments de logique}

\section{Calcul des prédicats}

\subsection{Définition}

\begin{cdefi}
Un \textbf{prédicat} est une proposition dont la valeur de vérité dépend d'une ou plusieurs variables.
\end{cdefi}

\begin{cexemple}[Pluriel]
On note le prédicat \og $P(x)$ : $x<4$ \fg :
\begin{itemize}[label=\small$\blacktriangleright$]
	\item on a $P(1)$ est VRAI, car $1<4$ ;
	\item on a $P(5)$ est FAUX, car $5 \not< 4$.
\end{itemize}
On note le prédicat \og $Q(x;y)$ : $x<y$ \fg :
\begin{itemize}[label=\small$\blacktriangleright$]
	\item on a $Q(4;5)$ est VRAI, car $4<5$ ;
	\item on a $Q(-10;-20)$ est FAUX, car $-10 \not< -20$.
\end{itemize}
\end{cexemple}

\begin{crmq}
Dans ces écritures, $x$ est une variable muette, elle peut être remplacée par une autre lettre, sans ce que cela change quoi que ce soit au prédicat.
\end{crmq}

\subsection{Quantificateur universel}

\begin{cdefi}
\og Pour toute variable $x$, $P(x)$ \fg{} est vrai, ou encore, \fg Quelle que soit la variable $x$, $P(x)$ est vrai \fg{} se traduit par : $$\forall x,\,P(x).$$
Le symbole $\forall$ (qui se lit \og pour tout \fg{} ou \og quel que soit \fg) est appelé \textbf{quantificateur universel}. 

On lit : \og Pour tout $x$, on a $P(x)$ \fg{} ou \og Quel que soit $x$, on a $P(x)$ \fg.
\end{cdefi}

\begin{crmq}
Si $P(x)$ est un prédicat et E un ensemble, $(\forall x \in E,\,P(x))$ est une proposition.
\end{crmq}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $\og \forall x \in \R, \: x<4 \fg$ ;
	\item $\og \forall x \in \R, \: (x+1)^2=x^2+2x+1 \fg$.
\end{itemize}
\end{cexemple}

\begin{cmethode}
Pour démontrer qu'une proposition telle que : $(\forall x,\,P(x))$ est VRAIE, il faut examiner \textbf{tous} les cas et prouver par une \textbf{démonstration} que $P(x)$ est vrai.

\smallskip
 
Pour démontrer qu'une telle proposition est FAUSSE, il suffit de trouver \textbf{un} cas dans lequel $P(x)$ est faux (un \textit{contre exemple}).
\end{cmethode}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $\og \forall x \in \R, \: x<4 \fg$ est FAUSSE car (par exemple), pour $x=5$, $x<4$ est FAUSSE ;
	\item $\og \forall x \in \R, \: (x+1)^2=x^2+2x+1 \fg$ est VRAIE, car $(x+1)^2=x^2+2x+1$ est une identité remarquable (donc vraie).
\end{itemize}
\end{cexemple}

\subsection{Quantificateur existentiel}

\begin{cdefi}
\og Il existe une variable $x$ pour laquelle $P(x)$ est vrai \fg, ou encore  \fg Il existe une variable $x$ telle que $P(x)$ est vrai \fg{} se traduit symboliquement par : $$\exists x,\,P(x).$$
Le symbole $\exists$ (qui se lit \og il existe \fg) est appelé \textbf{quantificateur existentiel}.

On lit : \og Il existe $x$ pour lequel on a $P(x)$ \fg{} ou \fg Il existe $x$ tel qu'on a $P(x)$ \fg.
\end{cdefi}

\begin{crmq}
Si $P(x)$ est un prédicat et E un ensemble, $(\exists x \in E,\,P(x))$ est une proposition.
\end{crmq}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $\og\exists x \in \R, \: 3x+1=13 \fg$ ;
	\item $\og\exists n \in \N, \: 3n+1=12 \fg$.
\end{itemize}
\end{cexemple}

\begin{cmethode} 
Pour démontrer qu'une proposition telle que : $(\exists x \in E,\,P(x))$ est VRAIE, il suffit de trouver un exemple pour lequel $P(x)$ est vrai (un cas \og qui marche \fg).

\smallskip
 
Pour démontrer qu'une telle proposition est FAUSSE , il faut démontrer que $P(x)$ est faux dans \textit{tous} les cas.
\end{cmethode}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $\og\exists x \in \R, \: 3x+1=13 \fg$ est VRAIE car on a $3x+1=13 \ssi 3x=12 \ssi x=4 \in \R$ ;
	\item $\og\exists n \in \N, \: 3n+1=12 \fg$ est FAUSSE car on a $3n+1=12 \ssi 3n=11 \ssi n=\nicefrac{11}{3} \not\in \N$.
\end{itemize}
\end{cexemple}

\section{Propriétés des prédicats}

\subsection{Succession de quantificateurs}

\begin{cidee}
On peut \og enchaîner \fg{} les quantificateurs dans une proposition.
\end{cidee}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Soit $S$ : $\og \forall x \in \R, \: \exists y \in \R, \: x<y \fg$. 
	\begin{itemize}[label=\small$\blacktriangleright$]
		\item On peut traduire S par : \og Pour tout réel $x$, il existe un réel $y$ tel que $x<y$ ;
		\item cette proposition est VRAIE car pour tout réel $x$, il suffit de considérer $y=x+1$.
	\end{itemize}
	\item Soit $T$ : $\og \exists y \in \R, \: \forall x \in \R, \: x<y \fg$.
	\begin{itemize}[label=\small$\blacktriangleright$]
		\item On peut traduire T par \og Il existe un réel $y$, tel que pour tout réel $x$, $x<y$ \fg ;
		\item cette proposition est FAUSSE car il n'existe aucun réel supérieur à tous les autres et à lui-même.
	\end{itemize}
\end{itemize}
\end{cexemple}

\begin{cprop}
Dans une proposition comportant plusieurs fois le \textit{même} quantificateur, on peut échanger leur ordre.

Dans une proposition comportant plusieurs quantificateurs \textit{différents}, il ne faut pas modifier l'ordre de ces quantificateurs !
\end{cprop}

\subsection{Négation de quantificateurs}

\begin{cprop}[Pluriel]
Il faut savoir : 
\begin{itemize}[leftmargin=*]
	\item $\lnot (\forall x \in E,\,P(x)) \ssi (\exists x \in E,\, \lnot P(x))$ ou bien, avec la notation \og barre \fg{}, $\overline{\forall x \in E,\,P(x)} \ssi (\exists x \in E,\, \overline{P(x)})$
	\item $\lnot (\exists x \in E,\,P(x)) \ssi (\forall x \in E,\, \lnot P(x))$ ou bien, avec la notation \og barre \fg{}, $\overline{\exists x \in E,\,P(x)}
	 \ssi (\forall x \in E,\, \overline{P(x)})$
\end{itemize}
\end{cprop}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item La négation de $\exists x \in \R, \: x^2 \neq 9 \fg$ est $\forall x \in \R, \: x^2 = 9 \fg$.
	\item La négation de \og Tous les chats sont gris \fg{} est \og Il existe des chats non gris \fg.
\end{itemize}
\end{cexemple}

\begin{cprop}
On obtient la négation d'une proposition formée d'une suite de quantificateurs suivie d'un prédicat en :
\begin{itemize}
	\item changeant partout $\forall$ par $\exists$ et $\exists$ par $\forall$, sans changer l'ordre ; 
	\item puis en changeant le prédicat par sa négation. 
\end{itemize}
\end{cprop}

\begin{cexemple}
Soit la proposition $P$ : $\og \forall n \in \N, \: \exists p \in \N, \: p=\tfrac{n}{2} \fg$.
\begin{itemize}[label=\small$\blacktriangleright$]
	\item La négation de $P$ est $\og \exists n \in \N, \: \forall p \in \N, \: p\neq\tfrac{n}{2} \fg$.
	\item $P$ est FAUSSE car il existe des entiers non divisibles par 2, comme par exemple $n=3$.
\end{itemize}
\end{cexemple}

\section{Notions d'ensemble}

\subsection{Définition}

\begin{cdefi}
Un \textbf{ensemble} est une collection d’objets tous distincts, appelés \textbf{éléments} de l’ensemble.
\end{cdefi}

\begin{cnota}
Pour dire que $x$ est un \textbf{élément} de E, on écrit   \og$ x \in E$ \fg, qui se lit « $x$ appartient à l'ensemble E » ou « $x$ est un élément de l'ensemble E ». Dans le cas contraire, on écrit  \og $x \notin E$ \fg.
\end{cnota}

\begin{crmq}[Pluriel]
Il est conseillé de désigner les ensembles par des lettres majuscules et leurs éléments par des minuscules.

L’ensemble \textbf{vide} (qui ne contient aucun élément) est noté $\varnothing$ ou $\left\lbrace~\right\rbrace$.
\end{crmq}

\subsection{Cardinal}

\begin{cdefi}
Si le nombre d’éléments d’un ensemble E est \textbf{fini}, on dit que E est un ensemble fini.

Le nombre d’éléments de E est alors appelé \textbf{cardinal} de E et noté $\text{Card}(E)$.
\end{cdefi}

\subsection{Modes de définition}

\begin{cprop}[Pluriel]
Un ensemble peut être défini : 
\begin{itemize}
\item par la \textbf{liste} de ses éléments : on dit alors qu’il est défini par \textbf{extension} ;
\item par une \textbf{propriété caractéristique} de ses éléments : on dit qu’il est défini par \textbf{compréhension}.
\end{itemize}
\end{cprop}

\begin{cexemple}[Pluriel]
$\bullet~~E= \EcritureEnsemble[\strut]{a/b/1/2/3}$. Les éléments sont séparés par des (points-) virgules et placés entre des \textbf{accolades}.

Dans cette écriture, l'ordre dans lequel sont indiqués les éléments de E n'a aucune importance. 

On peut aussi noter : $E= \EcritureEnsemble[\strut]{a/2/3/b/1}$.

$\bullet~~F=\{\strut x \in \R,~x \pp 4\} = \IntervalleOF{-\infty}{4}$ : F est l’ensemble de tous les réels inférieurs ou égaux à 4.
\end{cexemple}

\subsection{Représentation}

\begin{cdefi}
Un ensemble peut être représenté par un \textbf{diagramme} d’Euler-Venn.

\begin{center}
\begin{tikzpicture}[scale=1,line width=1.25pt]
	\foreach \Point/\label in {(0.5,0.75)/a,(1,1.25)/b,(2,1.75)/c,(1.23,0.33)/3,(3,0.74)/2}
	\draw \Point pic {PLdotcross=2pt/45} node[right=1.25pt] {$\label$};
	\draw (2,1) circle[x radius=2,y radius=1] ;
	\draw (3.25,2) node {E} ;
\end{tikzpicture}
\end{center}

Les croix désignent les éléments de E.
\end{cdefi}

\section{Opérations sur les ensembles}

\subsection{Égalité, inclusion}

\begin{cdefi}
Deux ensembles A et B qui contiennent les mêmes éléments sont dits \textbf{égaux} et on écrit $A=B$. Dans ce cas : \[ \forall x,~(x \in A) \ssi (x \in B).\]
\end{cdefi}

\begin{cdefi}
On dit qu'un ensemble A est \textbf{inclus} dans un ensemble E, ou que A est un \textbf{sous-ensemble} ou une partie de E, et on note $A \subset E$, lorsque tout élément de A  est nécessairement dans E , c’est-à-dire : \[ (A \subset E) \ssi ((x \in A) \Rightarrow (x \in E)).\]
\end{cdefi}

\begin{cillustr}
\begin{center}
\begin{tikzpicture}[line width=1.25pt]
	\draw (2,1) circle[x radius=2,y radius=1] ;
	\draw (1.5,1.25) circle[x radius=1.15,y radius=0.45] ;
	\draw (3.25,2) node {E} ;
	\draw (2,0.6) node {A} ;
\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cprop}[Pluriel]
\begin{itemize}[leftmargin=*]
\item On a $E \subset E$ et $\varnothing \subset E$.
\item Si A n’est pas inclus dans E, on écrit $A \not\subset E$ ; il existe au moins un élément de A qui n’est pas dans E : \[ (A \not\subset E) \ssi (\exists x \in E \text{, } x \notin A).\]
\item Les parties de E qui ne contiennent qu’un élément sont appelées des \textbf{singletons}.
\end{itemize}
\end{cprop}

\begin{cexemple}[s]
Soient $E= \EcritureEnsemble[\strut]{a/b/c/1/2}$ et $A= \EcritureEnsemble[\strut]{a/2}$.
\begin{itemize}
\item on a bien $A \subset E$ ;
\item $\{ \strut a \}$ est un singleton, on a donc $a \in E$  et $\{ a \} \subset E$ ;
\item si $B=\EcritureEnsemble[\strut]{2/a}$, on a bien $A=B$.
\end{itemize}
\end{cexemple}

\begin{cprop}[ComplementTitre={ - Double Inclusion}]
Pour montrer que deux ensembles sont égaux, on peut utiliser la méthode de la \textbf{Double Inclusion} : \[ \big(A=B\big) \ssi \big((A \subset B)  \land (B \subset A\big).\]
\end{cprop}

\subsection{Ensemble des parties d'un ensemble}

\begin{cdefi}
Toutes les parties d'un ensemble E forment un nouvel ensemble noté $\mathscr{P}(E)$ appelé \textbf{ensemble des parties} de E, et qui est donc un « ensemble d'ensembles ». On a donc $A \subset E \ssi  A \in \mathscr{P}(E)$.
\end{cdefi}

\begin{cexemple}
Soit $E = \{1;2;3\}$. On voit immédiatement que l'ensemble $\mathscr{P}(E)$ des parties de E est l'ensemble : \[ \mathscr{P}(E) = \left\lbrace \vphantom{A_2^2} \varnothing;\{\strut 1\};\{\strut2\};\{\strut3\};\{\strut1;2\};\{\strut1;3\};\{\strut2;3\};\{\strut1;2;3\} \right\rbrace .\]
\end{cexemple}

\begin{cthm}
Si $\text{Card}(E) = n$ alors $\text{Card} \big( \mathscr{P}(E) \big) =2^n$.
\end{cthm}

\subsection{Complémentaire}

\begin{cdefi}
Soit A une partie de E.

On appelle \textbf{complémentaire} de A dans E  l’ensemble des éléments de E qui ne sont pas dans A. On le note $\overline{A}$ : \[ \overline{A} = \left\lbrace  x \in E \text{ et } x \notin A \right\rbrace.\]
\end{cdefi}

\begin{cillustr}
\begin{center}
	\begin{tikzpicture}
		\draw[line width=1.25pt,pattern={Lines[angle=-50,distance=5pt,line width=1pt]},pattern color=gray] (2,1) circle[x radius=2,y radius=1] ;
		\draw[line width=1.25pt,black,fill=white] (1.5,1.25) circle[x radius=1.15,y radius=0.45] ;
		\draw (1.5,1.25) node {A} ;
		\draw (3.25,2) node {E} ;
		\draw (4,1) node[right] {La partie hachurée représente $\overline{A}$} ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cprop}
La complémentation est une opération \textbf{unaire}. Et on a $\varnothing=\overline{E}$ et  $E=\overline{\varnothing}$.
\end{cprop}

\begin{cexemple}
Soient $E=\{\strut a;b;1;2;3\}$ et $A=\{\strut a;3;b\}$ une partie de E ; alors $\overline{A} = \{\strut 1;2\}$.
\end{cexemple}

\subsection{Intersection et réunion}

\begin{cprop}
Soient A et B deux parties d’un ensemble E.

La \textbf{réunion} de A et de B, notée $A \cup B$, est l’ensemble qui contient les éléments de A ainsi que ceux de B : \[ A \cup B= \left\lbrace x \in E,~(x \in A) \lor (x \in B) \right\rbrace.\]
\end{cprop}

\begin{cillustr}
\begin{center}
	\begin{tikzpicture}[line width=1.25pt]
		\draw[pattern={Lines[angle=-45,distance=5pt,line width=1pt]},pattern color=gray] (2,1) circle[x radius=2,y radius=1] ;
		\draw[pattern={Lines[angle=45,distance=5pt,line width=1pt]},pattern color=gray] (4.5,0.15) circle[x radius=2.15,y radius=0.85] ;
		\draw (7,0.5) node[right] {La partie hachurée représente $A \cup B$} ;
		\draw (2,1) circle[x radius=2,y radius=1] ;
		\draw (4.5,0.15) circle[x radius=2.15,y radius=0.85] ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cdefi}
Soient A et B deux parties d’un ensemble E.

L’\textbf{intersection} de A et de B est l’ensemble qui contient les éléments \textbf{communs} à A et à B : \[ A \cap  B=\left\lbrace x \in E,~(x \in A) \land (x \in B) \right\rbrace.\]
\end{cdefi}

\begin{cexemple}
Soient $E=\{\strut 1;2;3;4;5;6\}$, $A=\{\strut 2;3;4\}$ et $B=\{\strut 3;4;6\}$. Alors : $A \cup B=\{\strut 2;3;4;6\}$.
\end{cexemple}

\begin{cillustr}
\begin{center}
	\begin{tikzpicture}[line width=1.25pt]
		\draw (2,1) circle[x radius=2,y radius=1] ;
		\draw (4.5,0.15) circle[x radius=2.15,y radius=0.85] ;
		\draw (7,0.5) node[right] {La partie hachurée représente $A \cap B$} ;
		\clip (2,1) circle[x radius=2,y radius=1] ;
		\draw[pattern={Lines[angle=-45,distance=5pt,line width=1pt]},pattern color=gray] (4.5,0.15) circle[x radius=2.15,y radius=0.85] ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cprop}[Pluriel]
On a les propriétés suivantes :
\begin{itemize}[]
	\item $A \subset A \cup B$ et $B \subset A \cup B$.
	\item $A \cap B \subset A$ et $A \cap B \subset B$.
	\item Deux ensembles A et B sont dits disjoints lorsqu'ils n’ont aucun élément commun, c’est-à-dire $A \cap B = \varnothing$.
	\item Ne pas confondre « A et B disjoints » (aucun élément commun) avec « A et B distincts » (pas égaux).
\end{itemize}
\end{cprop}

\begin{cexemple}[Pluriel]
Avec les ensembles précédents, on a $A \cap B=\{3;4\}$.
\end{cexemple}

\begin{crmq}
On remarquera  la similitude (volontaire !) du graphisme des symboles d'union $\cup$ et de disjonction $\lor$, ainsi que des symboles d'intersection $\cap$ et de conjonction $\land$.
\end{crmq}

\begin{cpython}
En \calgpython, on peut travailler sur des ensembles, grâce au type \cpy{set} :

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Logo,Alignement=center>{}
#définitions des opérateurs ensemblistes grâce aux opérateurs booléens
def inter(A,B):
	return A & B

def union(A,B):
	return A | B

def contr(A):
	return E-A

#des ensembles
E=set('abcdefghijklmnopqrstuvwxyz')
V=set("aeiou")
C=set("bcdfghjklmnpqrstvwxyz")
A=set("mathematiques")

#opérations sur les ensembles V, C et A (dans E)

print("A=",A)

print("nonC=",contr(C))

print("V et C =",inter(V,C))

print("A et C =",inter(A,C))

print("V ou A =",union(V,A))

print("(nonA) et V =",inter(contr(A),V))
\end{ConsolePiton}
\end{cpython}

\end{document}