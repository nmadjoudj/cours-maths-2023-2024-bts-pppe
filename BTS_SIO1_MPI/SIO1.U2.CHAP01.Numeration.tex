% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U2]},typedoc=CHAPITRE~,numdoc=01,titre={Arithmétique - Numération}]
%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}
%divers

\begin{document}

\pagestyle{fancy}

\part{CH01 - Arithmétique - Numération}

\section{Introduction}

\begin{cintro}[Compteur=false]
Les nombres entiers sont de deux sortes : 
\begin{itemize}
	\item les nombres \textbf{cardinaux} qui permettent de dénombrer des objets et de faire des calculs, 
	\item les nombres \textbf{ordinaux} qui permettent d'établir un ordre entre des objets d'un ensemble ordonné.
\end{itemize}
On s'intéresse, en arithmétique, à la fonction cardinale des nombres.
\end{cintro}

\begin{chistoire}[Compteur=false,SousTitre={Numération dans l'histoire}]
Pour compter des objets, l'homme a utilisé des cailloux (calculus en latin, qui a donné \textbf{calcul} ; cf. calculs rénaux), mais aussi différentes parties de son corps, en particulier les doigts. Ainsi a été créée la numération \textit{décimale} (méthode de dénombrement par dizaines).

\smallskip

Certaines civilisations du passé (les Celtes, les Mayas, les Aztèques) ont compté par \textit{vingtaines}. Ainsi, le calendrier maya comportait des mois de 20 jours et envisageait des cycles de 20 ans. Chaque puissance de 20 portait un nom particulier (comme dans la numération décimale : dix, cent, mille...).

Il nous reste des Celtes des nombres comme quatre-vingts, quatre-vingt-dix, l'hôpital des quinze-vingts\ldots

La numération décimale est dite de \textit{base 10} ;  la numération \textit{vigésimale} a pour base 20.

Le système \textit{sexagésimal}, c'est-à-dire de base 60, a été utilisé chez les Sumériens puis chez les Babyloniens et chez les Grecs par les astronomes. Il nous en reste les unités de mesure du temps et de mesure d'angles.

Lorsque nous dénombrons des œufs, nous comptons en base 12.

\smallskip

Pour représenter les nombres, nous utilisons des \textit{symboles} : les \textbf{chiffres}.
Les chiffres que nous utilisons sont les chiffres arabo-indiens, adoptés à partir de 1202, date de la publication du Liber Abaci de Leonardo Fibonacci.

On connaît aussi les chiffres \textit{romains} : I, II, III, IIII, V, VI, VII, VIII, VIIII, X, XI, ..., XV, ..., XX, ..., L, ..., C, ..., D, M.

Chez les Romains, le principe  de  numération est dit \textit{additif} : pour connaître la valeur d'un nombre, on ajoute les valeurs des symboles (chiffres) qui le composent LXVIII signifie $50 + 10 + 5 + 3 = 68$.

\smallskip

Nous utilisons aujourd'hui une \textit{numération de position} dans laquelle c'est la position du chiffre dans le nombre qui donne sa valeur. Ainsi, dans 124, 51, 10 078 ou 713, le chiffre "1" n'a pas la même "valeur".
Ce principe de numération permet d'effectuer les opérations usuelles grâce à des \textit{algorithmes} simples.
\end{chistoire}

\section{Bases et numération de position}

\subsection{Bases}

\begin{cdefi}
La base d'un système de numération est le nombre de chiffres utilisés pour représenter les nombres.

En numération de position, la place d'un chiffre dans le nombre indique sa valeur.
\end{cdefi}

\begin{cdefi}
Dans une base $b$, on utilise $b$ chiffres (de 0 à $b-1$) pour écrire les nombres.

La position de chaque chiffre dans le nombre correspond à une puissance de la base $b$.

Un nombre quelconque $n$ s'écrit alors, dans la base $b$ : $$n=(a_p a_{p-1} a_{p-2} \dots a_1 a_0 )_b=a_p \times b^p+a_{p-1} \times b^{p-1}+a_{p-2} \times b^{p-2}+ \dots +a_1 \times b^1+a_0 \times b^0$$ où les $a_i$ sont les $p+1$ chiffres (non nécessairement tous distincts) qui composent le nombre $n$.
\end{cdefi}

\begin{crmq}
Cette écriture, appelée \textit{écriture polynomiale}, permet de convertir un nombre de la base $b$ vers la base 10.
\end{crmq}

\subsection{Conversion d'une base vers la base 10}

\begin{cmethode}
Pour convertir un nombre de la base $b$ vers la base 10 :
\begin{itemize}[leftmargin=*]
	\item multiplier chacun des chiffres du nombre par la puissance de $b$ correspondant à sa position dans le nombre ;
	\item faire la somme des produits obtenus.
\end{itemize}
\end{cmethode}

\section{La base 2 - Numération binaire}

\subsection{Conversion d'un nombre binaire en décimal}

\begin{cprop}
En base 2, on utilise uniquement les deux chiffres 0 et 1. 

La position d'un chiffre dans le nombre binaire correspond à une puissance de 2.

\smallskip

Pour effectuer une conversion du binaire vers la base 10, on utilise la méthode donnée dans le paragraphe précédent à partir de l'écriture polynomiale.
\end{cprop}

\begin{crmq}
On peut également \og apprendre \fg{} les valeurs des premières puissances de 2 pour aller plus vite !
\end{crmq}

\begin{cpython}
En \calgpython, on peut travailler de la base 2 à la base 10, avec la commande \cpy{int} par exemple :

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
int(0b10), int(0b1010), int(0b111111), int(0b11100001)
\end{ConsolePiton}

\smallskip

Le fait que le nombre soit en \textit{binaire} est signifié, en \calgpython, par \cpy{0b...}.
\end{cpython}

\subsection{Conversion d'un nombre décimal en binaire}

\begin{cmethode}[ComplementTitre={ - Avec le tableau des puissances de 2}]
Écrire un nombre en binaire revient à l'exprimer comme une somme de puissances de 2.
\end{cmethode}

\begin{crmq}
Cette méthode peut cependant se révéler longue et fastidieuse lorsque le nombre à convertir est très grand. La méthode suivante, ou méthode des \textit{divisions successives}, est alors plus efficace.
\end{crmq}

\begin{cmethode}[ComplementTitre={ - Les divisions successives par 2}]
Pour convertir un nombre décimal en base 2 on effectue les divisions euclidiennes successives, et on \og remonte les restes \fg.
\end{cmethode}

\begin{crmq}
Cette méthode est valable en remplaçant la base 2 par toute autre base $b$ !
\end{crmq}

\begin{cmethode}[ComplementTitre={ générale}]
Pour convertir un entier $n$ de la base 10 vers la base $b$ :
\begin{itemize}
	\item diviser l'entier $n$ par $b$ et noter le quotient et le reste de la division ;
	\item diviser par $b$ le quotient obtenu à l'étape précédente et noter à nouveau le quotient et le reste ;
	\item poursuivre le processus jusqu'à ce que le quotient soit égal à 0 ;
	\item noter les restes successifs de gauche à droite en commençant par le dernier reste.
\end{itemize}
\end{cmethode}

\begin{cpython}
En \calgpython, on peut travailler de la base 10 à la base 2, avec la commande \cpy{bin} par exemple :

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
bin(2), bin(10), bin(63), bin(225)
\end{ConsolePiton}
\end{cpython}

\subsection{Calculs en binaire}

\begin{cprop}[SousTitre={Addition}]
Le principe est le même qu'en base 10. Il est utile, pour commencer, de connaître les additions suivantes :
\begin{itemize}
	\item $0 + 0 = 0$ ; $1 + 0 = 1$ ; $1 + 1 = 10$
	\item $10 + 1 = 11$ ; $11 + 1 = 100$
	\item $100 + 1 = 101$ ; $101 + 1 = 110$ ; $110 + 1 = 111$
\end{itemize}
On pose l'opération puis on calcule en colonne comme en base 10, sans oublier les éventuelles retenues !
\end{cprop}

\begin{cprop}[SousTitre={Soustraction}]
En base 2 comme en base 10, les nombres négatifs sont précédés du signe $-$.

Pour soustraire en binaire, on peut utiliser la méthode du \og complément à 2 \fg{} :

\begin{itemize}
	\item on inverse les bits de l'écriture binaire de sa valeur absolue (sans le $-$) ;
	\item on ajoute 1 au résultat (les dépassements sont ignorés).
\end{itemize}
\end{cprop}

\begin{cexemple}
$5-3=2$ en décimal ! et $101 - 11 = 101 - 011 = 101 + (100 + 1) = 101 + (101) = \cancel{1}010 = 10$ !
\end{cexemple}

\begin{cmethode}[SousTitre={Multiplication}]
Les seules multiplications à effectuer sont des multiplications par 0 ou par 1 ; celles-ci se calculent comme en base 10 :  $0 \times 0=0$ ; $0 \times 1=0$ ; $1 \times 0=0$ et $1 \times 1=1$. L'addition finale s'effectue comme indiqué précédemment.
\end{cmethode}

\begin{cprop}
Les puissances de 2, en binaire, sont $(2^0 )_{10}=(1)_2$ ; $(2^1)_{10}=(10)_2$ ; $(2^2 )_{10}=(100)_2$ ; $(2^3)_{10}=(1000)_2$ ; etc

D'une manière générale, $(2^n)_{10}=(10 \dots 0 )_2$ ($n$ zéros), et :
\begin{itemize}
	\item On sait multiplier par 1 en base 2  (de même qu'en base 10)
	\item Multiplier par $(10)_2$ revient à ajouter un zéro à la droite de l'écriture du nombre binaire ; multiplier par $(100)_2$ revient à ajouter deux zéros etc
	\item Multiplier un nombre décimal par $(2^n )_{10}$ se traduit en binaire par ajouter $n$ zéros
	\item Diviser un nombre décimal par $(2^n )_{10}$ revient à \dots
\end{itemize}
\end{cprop}

\pagebreak

\section{La base 16 - Numération hexadécimale}

\subsection{Conversion de la base 16 vers la base 10}

\begin{cmethode}
La méthode est la même que pour passer du binaire à la base 10 : on écrit le nombre sous forme polynomiale, avec les puissances de 16.
\end{cmethode}

\begin{cpython}
En \calgpython, on peut travailler de la base 16 à la base 10, avec la commande \cpy{int} par exemple :

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
int(0x10), int(0x47), int(0xCA), int(0xAFF)
\end{ConsolePiton}

\smallskip

Le fait que le résultat soit \textit{hexa} est signifié, en \calgpython, par \cpy{0x...}.
\end{cpython}

\subsection{Conversion de la base 10 vers la base 16}

\begin{cmethode}
le plus simple est d'effectuer les divisions successives par 16 jusqu'à obtenir un quotient égal à 0.
\end{cmethode}

\begin{cpython}
En \calgpython, on peut travailler de la base 10 à la base 16, avec la commande \cpy{hex} par exemple :

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
hex(10), hex(16), hex(64), hex(255)
\end{ConsolePiton}
\end{cpython}

\subsection{Conversion de la base 2 vers la base 16}

\begin{cmethode}
On procède en regroupant les chiffres par paquets de quatre, en commençant par la droite et en étendant, si nécessaire, à gauche par des zéros. On utilise pour cela le tableau donné ci-dessous :

\begin{center}
	\begin{tblr}{rowsep=1pt,hlines,vlines,cells={font=\scriptsize\sffamily},colspec={*{3}{Q[1.5cm,c,m]}},row{1}={bg=lightgray!25}}
		\textbf{Base 10} & \textbf{Base 2} & \textbf{Base 16} \\
		0	&0000	&0 \\
		1	&0001	&1 \\
		2	&0010	&2 \\
		3	&0011	&3 \\
		4	&0100	&4 \\
		5	&0101	&5 \\
		6	&0110	&6 \\
		7	&0111	&7 \\
	\end{tblr}
	~~~~
	\begin{tblr}{rowsep=1pt,hlines,vlines,cells={font=\scriptsize\sffamily},colspec={*{3}{Q[1.5cm,c,m]}},row{1}={bg=lightgray!25}}
		\textbf{Base 10} & \textbf{Base 2} & \textbf{Base 16} \\
		8	&1000	&8 \\
		9	&1001	&9 \\
		10	&1010	&A \\
		11	&1011	&B \\
		12	&1100	&C \\
		13	&1101	&D \\
		14	&1110	&E \\
		15	&1111	&F \\
	\end{tblr}
\end{center}
\end{cmethode}

\subsection{Conversion de la base 16 vers la base 2}

\begin{cmethode}
On remplace chaque chiffre de la base 16 en binaire à 4 chiffres (en complétant éventuellement à gauche par des zéros).
\end{cmethode}

\end{document}