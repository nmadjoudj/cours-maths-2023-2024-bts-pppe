% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton,ecritures}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{customenvs}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U2]},typedoc=CHAPITRE~,numdoc=04,titre={Primalité.PGCD}]
%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\usepackage{xlop}
%\usepackage{tabularx}
\ifdef{\cc}{}{\newcommand{\cc}{\SetCell{bg=lightgray!50}}}
\ifdef{\diveucl}%
	{\renewcommand{\diveucl}[4]{\begin{tabular}{r|l}#1&#2\\\cline{2-2}#3&#4\\\end{tabular}}}%
	{\newcommand{\diveucl}[4]{\begin{tabular}{r|l}#1&#2\\\cline{2-2}#3&#4\\\end{tabular}}}%

\begin{document}

\pagestyle{fancy}

\part{CH04 - Arithmétique - Primalité, PGCD}

\begin{python}
def estpremier(n) :
	if n == 1 :
		return False
	elif n == 2 or n==3 :
		return True
	else :
		res = True
		for div in range(2,n) :
			if n%div == 0 :
				res = False
	return res

\end{python}

\medskip

\begin{ccadre}[Compteur=false]
Dans ce chapitre, on travaille dans l'ensemble des entiers naturels, $\N$.
\end{ccadre}

\section{Nombres premiers}

\subsection{Définitions}

\begin{cdefi}[Pluriel]
Un entier naturel $p$ est \textbf{premier} s’il admet \uline{exactement} deux diviseurs : 1 et lui-même.
 
Un nombre qui n’est pas premier est dit \textbf{composé}.

\smallskip

On admettra qu'il existe une infinité de nombres premiers.
\end{cdefi}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item 0 et 1 ne sont pas premiers. Pourquoi ?
	\item 2 est premier ; c’est le seul nombre premier qui soit pair. Pourquoi ?
	\item 13 est premier : en effet, les seuls entiers  inférieurs ou égaux à 13 qui divisent 13 sont 1 et 13.
	\item 15 n’est pas premier : il a quatre diviseurs : 1, 3, 5, 15.
	\item Les entiers premiers inférieurs ou égaux à 30 sont : 2, 3, 5, 7, 11, 13, 17, 19, 23, 29.
\end{itemize}
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut utiliser une \calg{boucle} ou une \calg{fonction booléenne} :  

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
a = 29
res = True
for div in range(2,a) :
	if a%div == 0 :
		res = False

print(res)

def estpremier(n) :
	if n == 1 :
		return False
	elif n == 2 or n==3 :
		return True
	else :
		res = True
		for div in range(2,n) :
			if n%div == 0 :
				res = False
		return res

estpremier(2)
estpremier(3)
estpremier(15)
estpremier(29)
\end{ConsolePiton}
\end{cpython}

\subsection{Caractérisation}

\begin{cthm}
Soit $n$ un entier naturel tel que $n \pg 2$. Alors :
\begin{itemize}
	\item soit $n$ est premier ;
	\item soit, s’il est composé, il admet un diviseur premier $p$ tel que $2 \pp p \pp \sqrt{n}$.
\end{itemize} 
\end{cthm}

\begin{ccscq}
Si un entier n'admet aucun diviseur premier inférieur ou égal à sa racine carrée, alors il est premier.
\end{ccscq}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $\sqrt{271} \approx 16,46$ :
	
	On teste la divisibilité de 271 par tous les nombres premiers inférieurs ou égaux à 16 : 2, 3, 5, 7, 11, 13.
	Les critères de divisibilité nous permettent de trouver rapidement que 271 n'est pas divisible par 2, ni par 3, 5, 11. À l'aide de la calculatrice, on constate que 271 n'est pas divisible par 7, ni par 11.
	
	Ainsi, 271 n'est divisible par aucun entier premier inférieur ou égal à sa racine : il est donc premier.
	\item En ce qui concerne 471, on constate immédiatement que $4 + 7 + 1 = 12$, multiple de 3.
	
	471 est divisible par 3 : il n'est donc pas premier.
\end{itemize}
\end{cexemple}

\subsection{Algorithme de recherche de nombres premiers}

\begin{cmethode}
Le \textbf{crible d'Eratosthène} permet de déterminer tous les entiers premiers inférieurs à un entier $N$ donné, en procédant par élimination des entiers non premiers (jusqu'au rang $N$).

On commence par écrire la liste de tous les entiers naturels de 1 jusqu'à N, et on y supprime méthodiquement tous les multiples d'un entier (multiples de 2, puis multiples de 3, puis multiples de 5, etc). En supprimant tous les multiples, à la fin il ne restera que les entiers qui ne sont multiples d'aucun entier, et qui sont donc les nombres premiers.
\end{cmethode}

\begin{cillustr}
\hfill
\begin{tblr}[expand=\cc]{hlines,vlines,rowsep=0pt,colsep=0pt,rows={0.7cm},columns={0.7cm},cells={m,c,font=\ttfamily}}
	&\cc 2&\cc 3&4&\cc 5&6&\cc 7&8&9&10\\
	\cc 11&12&\cc 13&14&15&16&\cc 17&18&\cc 19&20\\
	21&22&\cc 23&24&25&26&27&28&\cc 29&30\\
	\cc 31&32&33&34&35&36&\cc 37&38&39&40\\
	\cc 41&42&\cc 43&44&45&46&\cc 47&48&49&50\\
	51&52&\cc 53&54&55&56&57&58&\cc 59&60\\
	\cc 61&62&63&64&65&66&\cc 67&68&69&70\\
	\cc 71&72&\cc 73&74&75&76&77&78&\cc 79&80\\
	81&82&\cc 83&84&85&86&87&88&\cc 89&90\\
	91&92&93&94&95&96&\cc 97&98&99&100
\end{tblr}
\hfill~

\medskip

Les nombres premiers inférieurs à 100 sont :
\begin{Centrage}
	2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97.
\end{Centrage}
\end{cillustr}

\begin{cpython}
En \calgpython{}, on peut balayer les entiers et utiliser la fonction \cpy{estpremier} :

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
for i in range(1,101) :
	if estpremier(i) :
		print(i,end=";")

\end{ConsolePiton}
\end{cpython}

\section{Décomposition, ensemble des diviseurs}

\subsection{Décomposition en produit de facteurs premiers}

\begin{cthm}
Tout entier naturel strictement supérieur à 1 peut être écrit comme un produit de nombres premiers de façon unique, à l'ordre près des facteurs. On appelle ce produit la \og décomposition en facteurs premiers \fg.
\end{cthm}

\begin{cexemple}[Pluriel]
$58=2 \times 29$

$180=2^2 \times 3^2 \times 5$
\end{cexemple}

\begin{cmethode}
Soit un entier $n > 1$ :
\begin{itemize}[leftmargin=*]
	\item si $n$ est premier, la décomposition s'arrête ici ;
	\item sinon, trouver le plus petit nombre premier $p$ qui divise $n$, l'ajouter à la liste des facteurs premiers et recommencer avec la valeur $\nicefrac{n}{p}$.
\end{itemize} 
On rappelle qu'il n'est pas nécessaire de tester les nombres premiers strictement supérieurs à $\sqrt{n}$. 
\end{cmethode}

\begin{cexemple}[Pluriel]
Décomposer 12 en produit de facteurs premiers :

On écrit, dans la colonne de droite (et si possible dans l'ordre croissant) les diviseurs premiers (2, 3, 5, 7 etc) des quotients écrits dans la colonne de gauche, jusqu'à ce que le dernier quotient obtenu soit 1 :

\begin{center}
	\begin{tabular}{c|c}
		12  & 2 \\
		6 & 2 \\
		3 & 3 \\
		1 & \\
	\end{tabular}
\end{center}
Ainsi : $12=2 \times 2 \times 3=2^2 \times 3$

\smallskip

De même :
\begin{itemize}[leftmargin=*]
	\item $54=2 \times 3 \times 3 \times 3=2 \times 3^3$ ;
	\item $558=2 \times 3^2 \times 31$ ;
	\item $6~468=2^2 \times 3 \times 7 \times 11$.
\end{itemize}
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut utiliser un balayage et une liste pour décomposer un entier $n$ : 

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
n = 6468
decompo = []
d = 2
while n%2 ==0 :              #les divisions par 2
	decompo.append(d)
	q = int(n/d)
	n = q

d = 3
while d <= n :
	while n%d == 0 :         #les divisions par 3, 5, ...
		decompo.append(d)
		q = int(n/d)
		n = q
	d = d + 2
	
print(decompo)
\end{ConsolePiton}
\end{cpython}

\subsection{Ensemble des diviseurs d'un entier naturel}

\begin{cmethode}
$48=1 \times 48=2 \times 24=3 \times 16=4 \times 12=6 \times 8$

L’ensemble des diviseurs de 48 est par conséquent $\mathscr{D}_{48}=\{1;2;3;4;6;8;12;16;24;48 \}$

\smallskip

De même on peut montrer que $\mathscr{D}_{60}=\{1;2;3;4;5;6;10;12;15;20;30;60 \}$
\end{cmethode}

\begin{cmethode}
On utilise la décomposition en facteurs premiers et on réalise un \textbf{arbre}.
\end{cmethode}

\begin{cillustr}
On sait que $12=2^2 \times 3$. Les diviseurs de 12 ont nécessairement les mêmes facteurs premiers que 12, avec des exposants inférieurs ou égaux à ceux présents dans la décomposition de 12.

\begin{center}
	\begin{tikzpicture}[xscale=1,yscale=1]
	% Styles (MODIFIABLES)
	\tikzstyle{fleche}=[->,>=latex,thick]
	\tikzstyle{noeud}=[]
	\tikzstyle{feuille}=[]
	\tikzstyle{etiquette}=[]
	% Dimensions (MODIFIABLES)
	\def\DistanceInterNiveaux{3}
	\def\DistanceInterFeuilles{2}
	% Dimensions calculées (NON MODIFIABLES)
	\def\NiveauA{(0)*\DistanceInterNiveaux}
	\def\NiveauB{(1.25)*\DistanceInterNiveaux}
	\def\NiveauC{(2.5)*\DistanceInterNiveaux}
	\def\NiveauD{(3.1)*\DistanceInterNiveaux}
	\def\InterFeuilles{(-0.3)*\DistanceInterFeuilles}
	% Noeuds (MODIFIABLES : Styles et Coefficients d'InterFeuilles)
	\coordinate (R) at ({\NiveauA},{(2.5)*\InterFeuilles}) ;
	\node[noeud] (Ra) at ({\NiveauB},{(1)*\InterFeuilles}) {$3^0$};
	\node[noeud] (Raa) at ({\NiveauC},{(0)*\InterFeuilles}) {$2^0$};
	\node[feuille] (Raaa) at ({\NiveauD},{(0)*\InterFeuilles}) {$3^0 \times 2^0 = 1\phantom{2}$};
	\node[noeud] (Rab) at ({\NiveauC},{(1)*\InterFeuilles}) {$2^1$};
	\node[feuille] (Raba) at ({\NiveauD},{(1)*\InterFeuilles}) {$3^0 \times 2^1 = 2\phantom{2}$};
	\node[noeud] (Rac) at ({\NiveauC},{(2)*\InterFeuilles}) {$2^2$};
	\node[feuille] (Raca) at ({\NiveauD},{(2)*\InterFeuilles}) {$3^0 \times 2^2 = 4\phantom{2}$};
	\node[noeud] (Rb) at ({\NiveauB},{(4)*\InterFeuilles}) {$3^1$};
	\node[noeud] (Rba) at ({\NiveauC},{(3)*\InterFeuilles}) {$2^0$};
	\node[feuille] (Rbaa) at ({\NiveauD},{(3)*\InterFeuilles}) {$3^1 \times 2^0 = 3\phantom{2}$};
	\node[noeud] (Rbb) at ({\NiveauC},{(4)*\InterFeuilles}) {$2^1$};
	\node[feuille] (Rbba) at ({\NiveauD},{(4)*\InterFeuilles}) {$3^1 \times 2^1 = 6\phantom{2}$};
	\node[noeud] (Rbc) at ({\NiveauC},{(5)*\InterFeuilles}) {$2^2$};
	\node[feuille] (Rbca) at ({\NiveauD},{(5)*\InterFeuilles}) {$3^1 \times 2^2 = 12$};
	% Arcs (MODIFIABLES : Styles)
	\draw[fleche] (R)--(Ra) node[etiquette] { };
	\draw[fleche] (Ra)--(Raa) node[etiquette] { };
	\draw[fleche] (Ra)--(Rab) node[etiquette] { };
	\draw[fleche] (Ra)--(Rac) node[etiquette] { };
	\draw[fleche] (R)--(Rb) node[etiquette] { };
	\draw[fleche] (Rb)--(Rba) node[etiquette] { };
	\draw[fleche] (Rb)--(Rbb) node[etiquette] { };
	\draw[fleche] (Rb)--(Rbc) node[etiquette] { };
	\end{tikzpicture}
\end{center}

On obtient $\mathscr{D}_{12}=\{1;2;3;4;6;12\}$
\end{cillustr}

\begin{cexemple}[Pluriel]
On peut montrer que :
\begin{itemize}[leftmargin=*]
	\item $\mathscr{D}_{50}=\{1;2;5;10;25;50 \}$ ;
	\item $\mathscr{D}_{42} = \{1;2;3;6;7;14;21;42 \}$.
\end{itemize}
\end{cexemple}

\begin{crmq}[Pluriel]
Pour trouver le \textbf{nombre} de diviseurs d'un entier N à partir de sa décomposition en facteurs premiers, il suffit de multiplier entre eux les exposants, augmentés de 1, des facteurs premiers apparaissant dans la décomposition de N.

Ainsi, le nombre de diviseurs de 12 est égal à $(1+1) \times (2+1)=2 \times 3=6$

De même $50=2 \times 5^2 $et le nombre de diviseurs de 50 est égal à $(1+1) \times (2+1)=6$

et $42=2 \times 3 \times 7$ et le nombre de diviseurs de 42 est égal à $(1+1) \times (1+1) \times (1+1)=8$
\end{crmq}

\begin{cpython}
En \calgpython{}, on peut utiliser un balayage et une liste pour travailler sur les diviseurs : 

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
n = 42
liste_div = []
for i in range(1,n+1) :
	if n%i == 0 :
		liste_div.append(i)

print(liste_div)
print(f"{n} admet {len(liste_div)} diviseurs")
\end{ConsolePiton}
\end{cpython}

\section{PGCD de deux entiers naturels non nuls}

\subsection{Définition}

\begin{cdefi}
Le PGCD des deux entiers $a$ et $b$, noté $PGCD(a;b)$ est le plus grand des diviseurs communs à $a$ et à $b$.
\end{cdefi}

\begin{cexemple}
La liste des diviseurs de 24 est : $1 ; 2 ; 3 ; 4 ; 6 ; 8 ; 12 ; 24$.

La liste des diviseurs de 36 est : $1 ; 2 ; 3 ; 4 ; 6 ; 9 ; 12 ; 18 ; 36$.

36 et 24 ont pour diviseurs communs : $1 ; 2 ; 3 ; 4 ; 6 ; 12$.

Le plus grand de ces diviseurs communs est 12 donc $PGCD(36;24)=12$.
\end{cexemple}

\subsection{Calcul du PGCD avec la décomposition en facteurs premiers}

\begin{cmethode}
Pour calculer $PGCD(a;b)$ :
\begin{itemize}[leftmargin=*]
	\item on écrit la décomposition en facteurs premiers de $a$ et de $b$ ;
	\item on multiplie entre eux les facteurs premiers communs aux deux décompositions, affectés du plus petit exposant figurant dans ces décompositions.
\end{itemize} 
\end{cmethode}

\begin{cexemple}[Pluriel]
Calculons $PGCD(36;24)$ avec $36=2^2 \times 3^2$ et $24=2^3 \times 3$ :

\hspace{5mm}$PGCD(36;24)=2^2 \times 3=12$

\smallskip

Calculons $PGCD(2\,100 ; 29\,250)$ avec $2\,100=2^2 \times 3 \times 5^2 \times 7$ et $29\,250=2 \times 3^2 \times 5^3 \times 13$ :

\hspace{5mm}$PGCD(2\,100 ; 29\,250)=2 \times 3 \times 5^2=150$
\end{cexemple}

\begin{ccalco}[Compteur=false]
\begin{center}
	\includegraphics[height=2.5cm]{graphics/chap04_pcgd_35}~~\includegraphics[height=2.5cm]{graphics/chap04_pgcd_83ce}~~\includegraphics[height=2.5cm]{graphics/chap04_pgcd_82}~~\includegraphics[height=2.5cm]{graphics/chap04_pgcd_nwks}
\end{center}
\end{ccalco}

\begin{cpython}
En \calgpython{}, il existe la fonction \cpy{gcd} du module \cpy{math} : 

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
from math import gcd
gcd(2100,29250)
\end{ConsolePiton}
\end{cpython}

\begin{cprop}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Si $a$ divise $b$, alors $PGCD(a ; b)=a$.
	\item Les diviseurs communs à $a$ et à $b$ sont les diviseurs de leur PGCD.
	\item Si $a>b$, et si $a=bq+r$ alors $PGCD(a ; b)=PGCD(b ; r)$ et  $b>r$.
\end{itemize}
\end{cprop}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item $PGCD(15 ; 45)=15$ car $45=3 \times 15$.
	\item Les diviseurs communs à 2\,100 et 29\,250 sont les diviseurs de 150 :
	\begin{itemize}
		\item $150=2 \times 3 \times 5^2$ ;
		\item il a $2 \times 2 \times 3=12$ diviseurs $\{1 ; 2 ; 3 ; 5 ; 6 ; 25 ; 10 ; 15 ; 30 ; 50 ; 75 ; 150\}$ ;
		\item $\{1 ; 2 ; 3 ; 5 ; 6 ; 25 ; 10 ; 15 ; 30 ; 50 ; 75 ; 150\}$ est l'ensemble des diviseurs communs à 2\,100 et 29\,250.
	\end{itemize}
\end{itemize}
\end{cexemple}

\subsection{Calcul du PGCD avec l'algorithme d'Euclide}

\begin{cmethode}
Pour calculer $PGCD(a ; b$) où $a>b$ :
\begin{itemize}[leftmargin=*]
	\item on effectue la division euclidienne de $a$ par $b$ :  on obtient un reste $r$ avec  $r<b$ :
	\begin{itemize}
		\item si $r=0$, alors $a$ est divisible par $b$ et $PGCD(a ; b)=b$ ;
		\item si $r \neq 0$, alors $PGCD(a ; b)=PGCD(b ; r)$ ;
	\end{itemize}
	\item on effectue la division euclidienne de $b$ par $r$ : on obtient un nouveau reste $r_1$ avec $r_1<r$ :
	\begin{itemize}
		\item si $r_1=0$ alors $b$ est divisible par $r$ et $PGCD(a ; b)=PGCD(b ; r)=r_1$ ;
		\item si $r_1 \neq 0$ alors on réitère le processus avec $r$ et $r_1$ ;
	\end{itemize}
	\item on s'arrête lorsque l'on trouve un reste égal à 0 ; le $PGCD(a ; b)$ est alors le dernier reste non nul obtenu.
\end{itemize}
\end{cmethode}

\begin{cexemple}[Pluriel]
Calculer $PGCD(252 ; 120)$ : \diveucl{252}{120}{12}{10}~~~puis~\diveucl{120}{12}{10}{0} ; ainsi $PGCD(252 ; 120)=12$

De même :
\begin{itemize}
	\item $PGCD(978 ; 254)=2$ ;
	\item $PGCD(420 ; 540)=60$.
\end{itemize}
\end{cexemple}

\subsection{Nombres premiers entre eux}

\begin{cdefi}
$a$ et $b$ sont dits premiers entre eux lorsque leur seul diviseur commun est 1.
\end{cdefi}

\begin{cprop}[Pluriel]
$a$ et $b$ sont premiers entre eux si et seulement si leur PGCD est égal à 1. 

Deux nombres premiers distincts sont premiers entre eux.
\end{cprop}

\begin{cexemple}
18 et 35 sont premiers entre eux. 
\end{cexemple}

\begin{cpython}
En \calgpython{}, une petite \cpy{fonction booléenne} appelant \cpy{gcd} peu être utilisée :

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
from math import gcd
def premiers_entre_eux(a,b) :
	pgcd = gcd(a,b)
	if pgcd == 1 :
		return True
	else :
		return False

premiers_entre_eux(18,35)
premiers_entre_eux(18,40)
\end{ConsolePiton}
\end{cpython}

\section{Complément sur la cryptologie}

\begin{chistoire}[Compteur=false]
{\polancienne Étymologiquement, le mot \textit{cryptographie} provient du grec : \textit{kruptos} (caché) et \textit{graphein} (écrire). Le \textit{cryptographe} essaie donc de mettre en place des systèmes cryptographiques, ou cryptosystèmes, fiables pour chiffrer (ou sécuriser) des messages circulant dans un réseau de communication. De son côté, le \textit{cryptanalyste} tente de disséquer le système utilisé afin de trouver des failles et d’obtenir une information à partir du message codé, appelé cryptogramme. Cryptographie et cryptanalyse font tous deux partie du domaine général qu’est la \textit{cryptologie} : la science du secret.\\
On se place dans la situation suivante : deux personnes, habituellement dénommées \textit{Alice} et \textit{Bob}, échangent des informations via un réseau et un intrus, \textit{Charlie} ou \textit{Eve}, espionne les transmissions. Dans ce contexte, les quatre buts principaux de la cryptographie sont :
\begin{itemize}[label=\scriptsize\faKey,leftmargin=*]
	\item la \textcolor{red}{\textbf{confidentialité}} : les textes codés et envoyés par Alice et Bob ne doivent pas être compris par Charlie ;
	\item l’\textcolor{red}{\textbf{authentification}} : Bob doit pouvoir être sûr que l’auteur du message est bien Alice et personne d'autre ;
	\item l’\textcolor{red}{\textbf{intégrité}} : le message d’Alice reçu par Bob n’a pas pu être modifié par Charlie lors de la transmission ;
	\item la \textcolor{red}{\textbf{non-répudiation}} : Alice ne peut ni nier être l’auteur ni avoir envoyé son message une fois transmis.
\end{itemize}
Il y a deux grandes familles de cryptographie : la cryptographie à \textit{clef secrète} (ou symétrique) et la cryptographie à \textit{clef publique} (ou asymétrique). Les systèmes de cryptographie utilisent notamment des \textbf{nombres premiers} ainsi que des \textbf{PGCD}, d'où leur étude en SIO !}
\smallskip

\hfill{}{\footnotesize Source : CNRS}
\end{chistoire}

\end{document}