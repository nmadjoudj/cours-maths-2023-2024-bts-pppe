% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\useproflyclib{piton}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{menukeys}
\let\tab\relax
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U2]},typedoc=ALGORITHMIQUE~,numdoc=01,titre={Instructions de base et types}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%fancy
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}

\begin{document}

\pagestyle{fancy}

\part{CH01 - Instructions de base et types}

\section{Introduction}

\subsection{Algorithmes}

\begin{cintro}[Compteur=false]
Le mot \og algorithme \fg{} vient du nom du mathématicien et astronome Al Khuwarizmi (latinisé au Moyen Âge en \textit{Algoritmi}), qui, au IX\up{e} siècle écrivit le premier ouvrage systématique sur la solution des équations linéaires et quadratiques.
\end{cintro}

\begin{cdefi}
Un \uline{algorithme} est une suite finie d'opérations ou d'instructions à appliquer dans un ordre déterminé à un nombre fini de données pour résoudre un problème en un nombre fini d'étapes.
\end{cdefi}

\begin{cexemple}[Pluriel]
On peut trouver des exemples d'algorithmes dans la vie courante :

\begin{itemize}
	\item recette de cuisine,
	\item notice de montage de meuble.
\end{itemize}
\end{cexemple}

\begin{cillustr}[Pluriel,Compteur=false]
\hfill
\begin{tblr}{c|c}
	\includegraphics[height=5cm]{chap01_algo_cuisine} & \includegraphics[height=5cm]{chap01_algo_ikea} \\
	{\small Recette du site \texttt{Marmiton.org}\textcopyright{}} & {\small Montage d'un meuble  \texttt{IKEA}\textcopyright{}}
\end{tblr}
\hfill~
\end{cillustr}

\begin{crmq}
Les premiers algorithmes mathématiques sont apparus bien avant les premiers ordinateurs et étaient exécutés à la main. Dans ce cours nous allons apprendre à écrire les algorithmes en \textit{langage de programmation} de manière à ce que l'ordinateur les exécute
\end{crmq}

\begin{cdefi}
Un algorithme écrit en langage de programmation s'appelle un \uline{programme}.
\end{cdefi}

\subsection{Langages informatiques}

\begin{cintro}[Compteur=false]
Le langage de l'ordinateur est le langage \textbf{binaire}, qui est composé uniquement de 0 et de 1.
\end{cintro}

\begin{cexemple}
On peut par exemple traduire le mot \og Bonjour \fg{} (codage UTF-8) par le code
binaire : 

\hfill\og 01000010011011110110111001101010011011110111010101110010 \fg.\hfill~
\end{cexemple}

\begin{cidee}[Compteur=false]
Pour éviter de devoir écrire nos algorithmes en binaire, on a recours à ce que l'on appelle
un \textit{langage de programmation} (comme par exemple \textsf{Python}, \textsf{C}, \textsf{C++}, etc).

Un programme, appelé \textit{interpréteur} ou \textit{compilateur} selon les cas, se charge ensuite de traduire ces algorithmes en langage binaire compréhensible par la machine.
\end{cidee}

\begin{ccadre}[Compteur=false]
Durant l'année, nous allons utiliser deux langages pour nos algorithmes :

\begin{itemize}
	\item le \calg{pseudo-code} : un langage naturel, exprimé en français, que nous utiliserons pour
	élaborer nos algorithmes sur papier ;
	\item \calgpython{} : un langage informatique multi-plateforme placé sous licence libre, créé en 1989 par le programmateur Guido van Rossum.
\end{itemize}
\end{ccadre}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{rossum}{}\textit{Guido van Rossum} (1956 --, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{NL}\!) est un développeur connu pour être le créateur et leader du projet du langage de programmation \calgpython. Il est également l'auteur du navigateur web Grail entièrement programmé en \calgpython{} ; il n'a pas été actualisé depuis 1999. Le nom fait référence au film \textit{Monty Python and the Holy Grail}.
\end{chistoire}

\section{Environnement de développement}

\subsection{IDE}

\begin{ccadre}[Compteur=false]
Durant l'année, nous allons utiliser la version 3 de \calgpython. Pour faciliter l'écriture d'algorithmes, nous allons travailler dans un environnement de développement intégré (en anglais
IDE, pour Integrated Development Environment).
\end{ccadre}

\begin{clog}[Compteur=false]
Au cours de l'année, nous allons utiliser le logiciel \textsf{Thonny}.

\textsf{Thonny} est un environnement de développement léger, muni d'un débogueur permettant de visualiser le contenu de chaque variable. Il est portable et peut s'installer sans droits d'administration. Il est disponible à l'adresse : \url{https://thonny.org/}.
\end{clog}

\begin{crmq}[Pluriel,Compteur=false]
À noter qu'il existe d'autres possibilités :

\begin{itemize}
	\item \textsf{Idle}, un IDE installé avec toute distribution \calgpython{} (\url{https://www.python.org/downloads/}) ;
	\item \textsf{Spyder}, un peu lourd et lent mais assez convivial, qui s'installe avec le pack \textsf{Anaconda} (\url{http://continuum.io/downloads}) ou avec le pack \textsf{Miniconda} (\url{https://conda.io/miniconda.html}) puis taper dans la console \textsf{conda} : \cshell{conda install spyder} ;
	\item \textsf{Pythontutor} qui est un environnement de développement en ligne (
	\url{http://www.pythontutor.com/visualize.html#mode=edit}) qui permet de visualiser le contenu de la mémoire au fur et à mesure.
\end{itemize}
\end{crmq}

\subsection{Présentation de Thonny}

\begin{clog}[Compteur=false]
Les environnements de développement sont essentiellement constitués de deux \textit{blocs} : l'\textit{éditeur de texte} et la \textit{console de l'interpréteur}.

\smallskip

\begin{wrapstuff}[r]
\includegraphics[width=8cm]{chap01_thonny}
\end{wrapstuff}

\textbf{L'éditeur de texte}

L'éditeur de texte est la zone du programmeur. En effet, les programmes sont tapés dans des fichiers
textes, que l'on peut sauvegarder, modifier et distribuer à loisir.

Dans \textsf{Thonny}, on peut exécuter le programme contenu dans le fichier texte à l'aide du bouton
d'exécution.

\medskip

\textbf{La console de l'interpréteur}

La console est la zone de l'utilisateur. Elle se repère aux trois chevrons (\cpy{>>>}) qu'elle contient.

En général, on se sert de cette zone essentiellement pour visualiser le résultat de l'exécution de nos programmes, et pour interagir avec le programme (saisie). Cependant, on peut aussi se servir de la console pour tester l'effet d'une commande.
\end{clog}

\begin{ccadre}
Tous les exemples du module sont à taper dans l'éditeur de texte, sauf si il est explicitement écrit de tester les exemples dans la console.
\end{ccadre}

\begin{crmq}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Le symbole \cpy{>>>} dans la console s'appelle une \textit{invite de commande}, il signifie que l'interpréteur est prêt à recevoir une instruction.
	\item Les fichiers créés par l'éditeur de texte portent l'extension \cshell{.py}, et il est important de sauvegarder son travail régulièrement.
	\item Pour ajouter des commentaires, on utilise le symbole \cpy{\#} ; le texte suivant le symbole \cpy{\#} (sur une seule ligne) n'est pas pris en compte par l'interpréteur.
	\item En \calgpython, les espaces en début de ligne (on parle d'\textit{indentation}) ont un sens. Sauf précision contraire, toutes les instructions doivent donc être alignées complètement à gauche.
\end{itemize}
\end{crmq}

\begin{clog}[Compteur=false]
Dans \textsf{Thonny}, on peut mettre en \textit{commentaire} une portion du texte en sélectionnant celle-ci, en allant dans le menu \menu[,]{Édition,Commenter}.

On peut également réaliser l'opération inverse en allant dans le menu \menu[,]{Édition,Décommenter}.
\end{clog}

\begin{cattention}[Compteur=false]
Les résultats des instructions tapées dans la console sont affichés automatiquement.

Mais pour afficher le résultat d'une instruction tapée dans l'éditeur de texte, il faut utiliser
\cpy{\piton{print}}, qui est la procédure permettant d'afficher.

\begin{itemize}[leftmargin=*]
	\item dans la console, on peut taper \cpy{\piton{1+2}} ;
	\item dans l'éditeur de texte, on tape \cpy{\piton{print(1+2)}}.
\end{itemize}
\end{cattention}

\subsection{Règles de nommage des fichiers et de sauvegarde}

\begin{cattention}[Compteur=false]
Les ordinateurs du lycée peuvent être réinitialisés à tout moment, toute donnée stockée sur l'un d'eux étant alors perdue. Il est recommandé de sauvegarder son travail :

\begin{itemize}
	\item sur l'espace partagé du serveur ;
	\item sur une clé USB (attention toutefois avec les virus) ;
	\item sur un espace de stockage en ligne (drive, etc).
\end{itemize}

Dans tous les cas, tout travail effectué en classe doit être sauvegardé.
\end{cattention}

\begin{crmq}[Compteur=false]
Par ailleurs, tout fichier rendu devra être nommé de la façon suivante : \cshell{nom\_devoir\_date.py}.

Par exemple, un élève dont le nom est \textsf{Azerty}, qui rend un fichier pour le \textsf{DS1} le
\textsf{15/09/2042} devra nommer son fichier  \cshell{azerty\_ds1\_15\_09\_2042.py}.

\smallskip

De plus, pour une meilleure organisation, il est par ailleurs recommandé de créer un \textit{dossier dédié}.
\end{crmq}

\section{Variables}

\subsection{Ce qu'est une variable}

\begin{cdefi}
Une variable correspond à un emplacement dans la mémoire de l'ordinateur auquel on donne :

\begin{itemize}
	\item \textbf{un nom} : qui doit commencer par une lettre et ne contenir que des lettres non accentuées, des chiffres et du caractère \og \_ \fg{} ;
	\item \textbf{une valeur} : qui est modifiable ;
	\item \textbf{un type} : entier, réel, etc, que l'on peut obtenir en \calgpython{} avec la fonction \cpy{type()}.
\end{itemize}
\end{cdefi}

\begin{crmq}[Pluriel]
$\bullet~~$\calgpython{} est sensible à la casse, autrement dit la variable \cpy{nom\_de\_variable} et la variable \cpy{Nom\_De\_Variable} ne sont pas les mêmes.

$\bullet~~$Il est très important de donner des noms de variable qui permettent de retrouver ce que la variable contient. Par exemple si je veux créer une variable qui contient un âge, je ne l'appelle pas \cpy{toto}, ou \cpy{x}, mais \cpy{age}.

\smallskip

Les évaluations pourront tenir compte de choix cohérents pour les noms de variables.
\end{crmq}

\subsection{Affichage}

\begin{calgo}
Pour afficher la valeur d'une variable, une valeur ou du texte dans la console, on utilise :

\medskip

\begin{minipage}{0.45\linewidth}
\begin{PseudoCodeAlt}*[7cm]{center}
Afficher : <expression à afficher>
\end{PseudoCodeAlt}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane,Lignes=false]{}
print(<expression à afficher>)
\end{CodePiton}
\end{minipage}
\end{calgo}

\begin{cexemple}
\begin{minipage}{0.45\linewidth}
En \calg{pseudo-code} :

\smallskip

\begin{PseudoCodeAlt}[7cm]{center}
a = 10
Afficher : a, 5, "Bonjour"
\end{PseudoCodeAlt}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
En \calgpython{} :

\smallskip

\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane]{}
a = 10
print(a, 5, "Bonjour")
\end{CodePiton}
\end{minipage}
\end{cexemple}

\begin{cpython}
La console permet -- par exemple -- de vérifier le bon fonctionnement des commandes :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
a = 10
print(a, 5, "Bonjour")
\end{ConsolePiton}
\end{cpython}

\begin{crmq}[Compteur=false,ComplementTitre={s importantes}]
$\bullet~~$Attention à bien distinguer une variable d'une chaîne de caractères :

\begin{itemize}
	\item la chaîne de caractères est un texte écrit entre guillemets et qui sera affiché tel quel ;
	\item lorsqu'on met un nom de variable (sans guillemets), c'est la valeur de cette variable qui
	sera affichée.
\end{itemize}

$\bullet~~$En \calgpython, les différents éléments à afficher sont mis entre parenthèses et séparés par
des virgules, comme par exemple \cpy{\piton{print(42, "Bob", "1+1 =", 1+1)}}.

\end{crmq}

\subsection{Affectation d'une valeur à une variable}

\begin{cdefi}
L'\textbf{affectation} est une opération permettant de définir la valeur initiale d'une variable
(on parle alors d'\textit{initialisation}), puis de modifier la valeur de cette variable.

\medskip

\begin{minipage}{0.45\linewidth}
En \calg{pseudo-code}, on note :

\begin{PseudoCodeAlt}*[7cm]{center}
nom_variable = valeur
\end{PseudoCodeAlt}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
En \calgpython{}, on note :
	
\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane,Lignes=false]{}
nom_variable = valeur
\end{CodePiton}
\end{minipage}
\end{cdefi}

\begin{cexemple}
\begin{minipage}{0.45\linewidth}
En \calg{pseudo-code} :

\smallskip

\begin{PseudoCodeAlt}[7cm]{center}
annee = 2021
Afficher : annee
annee_prochaine = annee + 1
Afficher : annee_prochaine
\end{PseudoCodeAlt}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
En \calgpython{} :

\smallskip

\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane]{}
annee = 2021
print(annee)
annee_prochaine = annee + 1
print(annee_prochaine)
\end{CodePiton}
\end{minipage}
\end{cexemple}

\begin{cpython}
La console permet -- par exemple -- de vérifier le bon fonctionnement des commandes :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
annee = 2021
print(annee)
annee_prochaine = annee + 1
print(annee_prochaine)
\end{ConsolePiton}
\end{cpython}

\begin{crmq}[Pluriel]
$\bullet~~$On peut affecter à une variable la valeur d'une autre variable au moment de l'affectation.

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
a = 42
b = a
a = 10
print(a, b)
\end{ConsolePiton}

$\bullet~~$Une même variable peut apparaître à droite et à gauche de l'affectation.

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
a = 10
a = a * 4
print(a)
\end{ConsolePiton}

$\bullet~~$On fera toujours très attention à mettre la variable dont on veut changer la valeur à gauche, et la valeur que l'on veut donner à cette variable à droite !

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
10 = a
\end{ConsolePiton}
\end{crmq}

\begin{cdefi}
On appelle \textbf{incrémentation} l'opération (\textit{très courante}) consistant à ajouter 1 ou une
valeur entière fixe à une variable.

\begin{itemize}
	\item Par exemple \calg{a \textleftarrow{} a + 1} en \calg{pseudo-code}.
	\item Par exemple \cpy{\piton{a = a + 1}} en \calgpython.
\end{itemize}
\end{cdefi}

\subsection{Saisie}

\begin{cdefi}
Pour rendre un programme \textit{interactif}, on peut, pendant l'exécution de celui-ci, demander à l'utilisateur de saisir un texte ou une valeur grâce à une \textit{instruction de saisie}.

\smallskip

Dès qu'arrive une instruction de saisie, le programme se met en pause. L'utilisateur doit alors saisir quelque chose dans la console et appuyer sur la touche \menu{\return} pour valider.

Pour expliciter ce qu'il faut saisir, une instruction de saisie est en général accompagnée par l'affichage d'un message explicatif.

\smallskip

La valeur saisie doit par ailleurs être stockée dans une variable de manière à pouvoir être réutilisée dans la suite du programme.

\smallskip

Pour demander de saisir une valeur et la stocker dans une variable nommée \calg{a} (ou \cpy{a}), on utilise les instructions suivantes :

\smallskip

\begin{minipage}[t]{0.45\linewidth}
En \calg{pseudo-code} :

\smallskip

\begin{PseudoCodeAlt}[7cm]{center}
Afficher : <message>
Saisir a
\end{PseudoCodeAlt}
\end{minipage}
\hfill
\begin{minipage}[t]{0.45\linewidth}
En \calgpython{} :
\begin{itemize}
	\item pour saisir un texte :
	
	\begin{CodePiton}[Gobble=tabs,Style=Classique,Largeur=7cm,Alignement=center,Filigrane]{}
		a = input("message")
	\end{CodePiton}
	\item pour saisir un nombre :
	
	\begin{CodePiton}[Gobble=tabs,Style=Classique,Largeur=7cm,Alignement=center,Filigrane]{}
		a = eval(input("message"))
	\end{CodePiton}
\end{itemize}
\end{minipage}
\end{cdefi}

\begin{crmq}
$\bullet~~$Dans les instruction précédentes, le nom de variable \calg{a} (ou \cpy{a}) doit bien sûr être remplacé par un nom de variable bien choisi.

$\bullet~~$Dans les instruction précédentes, le \calg{"message"} (ou \cpy{"message"}) doit être remplacé par un message expliquant à l'utilisateur ce qu'il doit saisir.
\end{crmq}

\begin{cexemple}
$\bullet~~$En \calg{pseudo-code} :

\smallskip

\begin{PseudoCodeAlt}[12cm]{center}
Afficher : "Veuillez saisir votre prénom : "
Saisir : nom
Afficher : "Veuillez saisir votre âge : "
Saisir : age
Afficher : "Bonjour ", nom , ", vous avez ", age , " ans."
\end{PseudoCodeAlt}

$\bullet~~$En \calgpython{} :

\smallskip

\begin{CodePiton}[Style=Classique,Largeur=12cm,Alignement=center,Filigrane]{}
nom = input("Veuillez saisir votre prénom : ")
age = eval(input("Veuillez saisir votre âge : "))
#Affichage classique
print("Bonjour ", nom , ", vous avez ", age , " ans.")
#Affichage formaté
print(f"Bonjour {nom}, vous avez {age} ans.")
\end{CodePiton}
\end{cexemple}

\begin{cpython}
La console permet -- par exemple -- de vérifier le bon fonctionnement des commandes (les saisies ne sont pas ici présentées \textit{comme en vrai}) :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
nom = "Maurice"
age = 75
#Affichage classique
print("Bonjour ", nom , ", vous avez ", age , " ans.")
#Affichage formaté
print(f"Bonjour {nom}, vous avez {age} ans.")
\end{ConsolePiton}
\end{cpython}

\begin{crmq}[Compteur=false,ComplementTitre={ importante !}]
La valeur retournée par la fonction \cpy{\piton{input()}} est toujours un texte (du type chaîne de caractère, \cpy{str} en \calgpython) et non un nombre !

Or \calgpython{} ne gère pas les chaînes de caractères et les nombres de la même façon.

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
age = "75"
print(type(age))
print(age *2)
\end{ConsolePiton}

\medskip

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
age = 75
print(type(age))
print(age *2)
\end{ConsolePiton}
\end{crmq}

\subsection{Opérations sur les variables, variables numériques}

\begin{cmethode}
Sous \calgpython, le type de la variable est donné par le \textit{type} de la valeur qu'on lui affecte. 

On peut changer de type (on parle de \calg{transtypage}) à l'aide de fonctions :

\begin{itemize}
	\item Type \textbf{entier} en \calgpython{} : \cpy{\piton{int()}}.
	\item Type \textbf{réel} en \calgpython{} : \cpy{\piton{float()}} (Le séparateur décimal est un point !).
	\item Type \textbf{chaîne} en \calgpython{} : \cpy{\piton{str()}}.
	\item Type \textbf{booléen} en \calgpython{} : \cpy{\piton{bool()}}.
\end{itemize}
\end{cmethode}

\begin{cpython}
\calgpython{} permet d'effectuer les opérations classiques sur les nombres. En plus de celles-ci,
deux opérations importantes sont à connaître :

\begin{itemize}
	\item la \textbf{division entière}, qui donne la partie entière de la division (ou le quotient de la
	division euclidienne pour des entiers) ; elle s'obtient par le symbole \cpy{\piton{//}} ;
	\item le \textit{modulo}, qui est le reste de la division entière ; il s'obtient par le symbole \cpy{\piton{\%}}.
\end{itemize}

Le tableau ci-dessous récapitule les symboles permettant d'effectuer les différentes opérations en \calgpython :

\medskip

\begin{tblr}{width=\linewidth,colspec={X[m,c]Q[c,m]Q[c,m]X[m,c]X[m,c]X[m,c]X[m,c]},hlines,vlines}
	addition & soustraction & multiplication & division & puissance & division entière & modulo \\
	\cpy{\piton{+}} & \cpy{\piton{-}} & \cpy{\piton{*}} & \cpy{\piton{/}} & \cpy{\piton{**}} & \cpy{\piton{//}} & \cpy{\piton{\%}} \\
\end{tblr}

\medskip

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
1+1
10-3
6*7
12/4
10**3
13//3  # retourne 4 car 13/3 vaut environ 4,33 dont la partie entière est 4
13%3   # retourne 1 car 13 = 3 * 4 + 1
\end{ConsolePiton}
\end{cpython}

\begin{crmq}
Attention, les calculs avec des nombres réels (flottants) donnent parfois des résultats approximatifs à cause de la façon dont sont stockés ces nombres !

\smallskip

Il faut essayer de privilégier les calculs entiers, et manier les réels avec une extrême prudence (et ce quelque soit le langage) !

\medskip

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
0.1+0.1
0.1+0.2
\end{ConsolePiton}
\end{crmq}

%\begin{calgo}[Compteur=false]
%Un algorithme en \calg{pseudo-code} se présente donc sous la forme suivante :
%
%\begin{PseudoCodeAlt}[10cm]{center}
%Algorithme : (*\dotfill*)
%Rôle       : (*\dotfill*)
%Variables  : (*\dotfill*)
%Début
%	(*\dotfill*)
%	(*\dotfill*)
%Fin
%\end{PseudoCodeAlt}
%\end{calgo}

\end{document}