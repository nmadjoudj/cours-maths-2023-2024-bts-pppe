% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{customenvs}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO1},matiere={[UF2]},typedoc={CHAPITRE~},numdoc={4},titre={Dérivation}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\usetikzlibrary{hobby}

\begin{document}

\tikzset{tangent/.style={% https://tex.stackexchange.com/a/25940
	decoration={%
		markings,mark=at position #1 with{
			\coordinate (tangent point-\pgfkeysvalueof{/pgf/decoration/mark info/sequence number}) at (0pt,0pt);
			\coordinate (tangent unit vector-\pgfkeysvalueof{/pgf/decoration/mark info/sequence number}) at (1,0pt);
			\coordinate (tangent orthogonal unit vector-\pgfkeysvalueof{/pgf/decoration/mark info/sequence number}) at (0pt,1);}
	},
	postaction=decorate},
	use tangent/.style={shift=(tangent point-#1),%
	x=(tangent unit vector-#1),
	y=(tangent orthogonal unit vector-#1)
	},
	use tangent/.default=1}

\pagestyle{fancy}

\part{CH04 - Dérivation}

\section{Variations d'une fonction}

\subsection{Introduction}

\begin{cintro}[Compteur=false]
Étudier, sans la courbe, les variations d'une fonction est fastidieux, mais grâce à un nouvel outil, la \textbf{dérivation}, on peut le faire \og assez facilement \fg{}.
\end{cintro}

\begin{cidee}[Compteur=false]
L'idée est de \og remplacer \fg{} (localement) la courbe par sa tangente.
\begin{center}
	\begin{tikzpicture}[x=1cm,y=0.5cm]
		%styles
		\tkzSetUpPoint[shape=circle,size=4pt,color=violet,fill=violet]
		\tikzset{tan style/.style={<->,>=latex}}
		\tkzInit[xmin=-2,xmax=8,xstep=1,ymin=-4,ymax=6.1,ystep=1]
		%courbe
		\tkzFct[very thick,red,domain=-2:8,samples=250]{2*cos(2*x)+cos(x)+3*cos(3*x)};
		%tangentes croissantes
		\foreach \x/\lg in {-1/0.25,2/0.3,4/0.75,6/0.15}{\tkzDrawTangentLine[very thick,color=blue,kr=\lg,kl=\lg](\x)}
		%tangentes décroissantes
		\foreach \x/\lg in {1/0.25,3/0.3,5/0.3}{\tkzDrawTangentLine[very thick,color=CouleurVertForet,kr=\lg,kl=\lg](\x)}
		%tangente hor
		\tkzDrawTangentLine[very thick,color=darkgray,kr=0.75,kl=0.75](0)
		%points
		\foreach \va in {-1,0,1,2,3,4,5,6}{\tkzDefPointByFct[draw](\va)}
	\end{tikzpicture}
\end{center}
On peut donc constater que :
\begin{itemize}
	\item les \og \textcolor{blue}{tangentes bleues montent} \fg{} et que la courbe \og suit la même direction \fg{} ;
	\item les \og \textcolor{CouleurVertForet}{tangentes vertes descendent} \fg{} et que la courbe \og suit la même direction \fg{} ;
	\item la \og \textcolor{darkgray}{tangente grise est horizontale} \fg{} et que la courbe \og change de direction \fg{}.
\end{itemize}
La tangente et la courbe vont donc dans la \og même direction \fg{} !
\end{cidee}

\begin{crappel}[Pluriel,ComplementTitre={ - Idée}]
Une tangente est une \textbf{droite}, et savoir si une droite \og monte ou descend \fg{} est simple : il suffit de connaître sa \textbf{pente} (ou son \textbf{coefficient directeur}).

On rappelle que l'équation (réduite) d'une droite est de la forme $y=\mathcolor{red}{m}x+\mathcolor{blue}{p}$ avec $\begin{dcases} \mathcolor{red}{m} \text{ la pente} \\ \mathcolor{blue}{p} \text{ l'ordonnée à l'origine} \end{dcases}$.
\begin{itemize}[leftmargin=*]
	\item si $m > 0$, la droite est croissante ;
	\item si $m < 0$, la droite est décroissante ;
	\item si $m = 0$, la droite est horizontale.
\end{itemize}

\smallskip

Il ne reste donc \og plus qu'à \fg{} trouver un moyen de déterminer la pente des tangentes !
\end{crappel}

\subsection{Nombre dérivé}

\begin{cdefi}[ComplementTitre={ - Propriété}]
Soit $f$ une fonction définie sur un intervalle I, et soit $a \in I$.

On appelle \textbf{nombre dérivé} de $f$ en $a$, la pente (si elle existe !) de la tangente à la courbe $\mathscr{C}_f$ en $a$.

Dans le cas où la tangente existe, on dit que $f$ est dérivable en $a$ et on note $f'(a)$ ce nombre dérivé.
\end{cdefi}

\begin{cprop}
Si $f$ est dérivable en $a$, une équation de la tangente $\Gamma_a$ à la courbe $\mathscr{C}_f$ au point d'abscisse $a$ est :

\hfill~$\Gamma_a$ : $y=f'(a) \times (x-a)+f(a)$.\hfill~
\end{cprop}

\begin{crmq}[Pluriel]
La formule \textbf{locale} qui permet de calculer $f'(a)$ est assez technique$\dots$

Pour pallier cette technicité, on a va utiliser des formules (globales) qui donnent \textbf{tous} les $f'(a)$ !

\smallskip

Pour déterminer l'équation de la tangente, on \textbf{remplace} les $a$, et on calcule $f'(a)$ puis $f(a)$ !
\end{crmq}

\subsection{Fonction dérivée}

\begin{cprop}[Pluriel]
On a les fonctions dérivées suivantes (les ensembles de définition et dérivabilité ne sont pas indiqués) :
\begin{center}
	%\renewcommand{\arraystretch}{1.25}
	\begin{tblr}{stretch,1.15,hlines,vlines,width=10cm,colspec={X[m,c]X[m,c]},row{1}={bg=lightgray!75}}
		$f(x)=\dots$ & $f'(x)=\dots$ \\
		$k$&$0$ \\
		$x$&$1$ \\
		$x^2$&$2x$ \\
		$x^3$&$3x^2$ \\
		$\phantom{(n \pg 1)}$\hfill{}$x^n$\hfill{}($n \pg 1$)&$nx^{n-1}$ \\
		$\tfrac{1}{x}$&$-\tfrac{1}{x^2}$ \\
		$\tfrac{1}{x^2}$&$-\tfrac{2}{x^3}$ \\
		$\phantom{(n \pg 1)}$\hfill{}$\tfrac{1}{x^n}$\hfill{}($n \pg 1$)&$-\tfrac{n}{x^{n+1}}$ \\
		$\sqrt{x}$&$\tfrac{1}{2\sqrt{x}}$ \\
		$\e^x$&$\e^x$ \\
		$\ln(x)$&$\tfrac{1}{x}$ \\
	\end{tblr}
\end{center}
\end{cprop}

\begin{cthm}
On a les formules de dérivations suivantes ($u$ et $v$ sont des \textbf{fonctions}) :
\begin{center}
	%\renewcommand{\arraystretch}{1.25}
	\begin{tblr}{stretch,1.15,hlines,vlines,width=10cm,colspec={X[m,c]X[m,c]},row{1}={bg=lightgray!75}}
		Fonction & Dérivée \\
		$u+v$&$u'+v'$ \\
		$\phantom{k \text{ cstte}}$\hfill{}$ku$\hfill{}($k$ cstte)&$ku'$ \\
		$u \times v$ & $u'v+v'u$ \\
		$\tfrac{u}{v}$ & $\tfrac{u'v-v'u}{v^2}$ \\
		$\tfrac{1}{v}$ & $\tfrac{-v'}{v^2}$ \\
		$u^n$ & $nu'u^{n-1}$ \\
		$\sqrt{u}$&$\tfrac{u'}{2\sqrt{u}}$ \\
		$\e^u$&$u'\e^u$ \\
		$\ln(u)$&$\tfrac{u'}{u}$ \\
	\end{tblr}
\end{center}
\end{cthm}

\begin{cexemple}[Pluriel,ComplementTitre={, de base}]
Pour dériver une fonction, on repère la ou les formules à utiliser, puis on raisonne éventuellement sur les composées !
\begin{itemize}[leftmargin=*,itemsep=6pt]
	\item $f(x)=x+x^3+\ln(x)$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules de bases ;
		\item $f'(x)=1+3x^2+\dfrac{1}{x}$.
	\end{itemize}
	\item $g(x)=4\e^x-10x^2$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules de base, en laissant les constantes ;
		\item $g'(x)=4 \times \e^x - 10 \times 2x = 4\e^x - 20x$.
	\end{itemize}
	\item $h(x)=\ln(3x+1)$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise la formule du $\ln$ avec $u=3x+1$ et donc $u'=3$
		\item $g'(x)=\dfrac{u'}{u}=\dfrac{3}{3x+1}$.
	\end{itemize}
	\item $i(x)=10\e^{-0,3x+5}$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise la formule de l'exponentielle avec $u=-0,3x+5$ et donc $u'=-0,3$ ; on laisse le 10 ;
		\item $i'(x)=10 \times u'\e^{u} = 10 \times \left(-0,3\right)\e^{-0,3x+5} = -3\e^{-0,3x+5}$.
	\end{itemize}
	\item $j(x)=\dfrac{x+2}{x^2+5}$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on va utiliser $\nicefrac{u}{v}$ avec $\begin{dcases} u=x+2 \\v=x^2+5 \end{dcases}$ et donc $\begin{dcases} u'=1 \\v'=2x \end{dcases}$ ;
		\item $j'(x)=\dfrac{u'v-v'u}{v^2}=\dfrac{1 \times (x^2+5) - 2x \times (x+2)}{(x^2+5)^2}=\dfrac{x^2+5-2x^2-4x}{(x^2+5)^2}=\dfrac{-x^2-4x+5}{(x^2+5)^2}$.
	\end{itemize}
\end{itemize}
\end{cexemple}

\begin{crmq}[Pluriel]
Pour dériver une \textbf{exponentielle}, on la laisse telle quelle et on fait sortir la dérivée de ce qui se trouve à l'intérieur !

\smallskip

Pour la dérivée d'un \textbf{quotient}, on ne développe pas le dénominateur, on le laisse sous forme d'un carré !

\smallskip

Si des choses sont simplifiables, on n'hésite pas à les \textbf{simplifier} !
\end{crmq}

\begin{cexemple}[Pluriel,ComplementTitre={, classiques}]
\begin{itemize}[leftmargin=*,itemsep=6pt]
	\item $f(x)=(3x+2)\e^{-2x}$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules du produit et de l'exponentielle ;
		\item on a $\begin{dcases} u=3x+2 \\v=\e^{-2x} \end{dcases}$ et donc $\begin{dcases} u'=3 \\v'=-2\e^{-2x} \end{dcases}$
		\item $f'(x)=u'v+v'u = 3 \times \e^{-2x} + \left( -2\e^{-2x} \right) \times (3x+2) = 3\e^{-2x} - 6x\e^{-2x} - 4\e^{-2x} = (-6x-1)\e^{-2x}$.
	\end{itemize}
	\item $g(x)=2x^2 + 5x + 3\ln(x)$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules de base, ici on pensera à mettre au même dénominateur ;
		\item $g'(x)=2 \times 2x + 5 + 3 \times \dfrac1x = 4x+5+\dfrac3x = \dfrac{4x^2}{x}+\dfrac{5x}{x}+\dfrac3x=\dfrac{4x^2+5x+3}{x}$.
	\end{itemize}
\end{itemize}
\end{cexemple}

\pagebreak

\section{Étude générale}

\subsection{Méthode}

\begin{cthm}
Pour étudier une fonction $f$ sur un intervalle I :
\begin{itemize}
	\item on calcule sa dérivée $f'(x)$ sur I ;
	\item on étudie, si besoin en \og transformant \fg{}, le \textbf{signe} de $f'(x)$ sur I ;
	\item on dresse le \textbf{tds} de $f'(x)$ sur et on en déduit le \textbf{tdv} de $f$ sur I grâce à $\mathcolor{red}{f' \oplus \Rightarrow f \nearrow}$  et $\mathcolor{blue}{f' \ominus \Rightarrow f \searrow}$ ;
	\item on complète le tdv de $f$ avec les images (ou les limites).
\end{itemize}
\end{cthm}

\subsection{Exemple}

\begin{cillustr}
Soit $f$ la fonction définie que $\intervFF{0}{7}$ par $f(x)=x^3-11x^2+39x-20$.
\begin{itemize}
	\item $f$ est dérivable et $f'(x)=3x^2-22x+39$ ;
	\item on utilise $\Delta$ pour déterminer le signe de $f'(x)$ ;
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit{$x$/0.7,$f'(x)$/0.7}{$0$,$3$,$\tfrac{13}{3}$,$7$}
			\tkzTabLine{,+,z,-,z,+,}
		\end{tikzpicture}
	\end{center}
	\item le théorème fondamental permet de dresser le tableau de variations de $f$ ;
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit{$x$/0.7,$f'(x)$/0.7,$f$/1.4}{$0$,$3$,$\tfrac{13}{3}$,$7$}
			\tkzTabLine{,+,z,-,z,+,}
			\tkzTabVar{-/$-20$,+/$25$,-/$m$,+/$57$}
		\end{tikzpicture}
	\end{center}
	avec $m \approx 23,8$.
	\item on peut proposer la courbe suivante pour terminer.
	\begin{center}
%		\tunits{1}{0.1}
%		\tdefgrille{0}{7}{1}{0.5}{-20}{60}{10}{5}
		\begin{tikzpicture}[x=1cm,y=0.1cm,xmin=0,xmax=7,ymin=-20,ymax=60,ygrille=10,ygrilles=5]
			\GrilleTikz\AxesTikz[ElargirOx=0,ElargirOy=0]
			\AxexTikz{0,1,...,6}\AxeyTikz{-20,-10,...,50}
			\draw[line width=1.25pt,color=blue,samples=250,domain=0:7] plot (\x,{\x*\x*\x-11*\x*\x+39*\x-20}) ;
			\draw[line width=1.25pt,dashed](4.333,0)--(4.333,23.8) ;
			\draw[line width=1.25pt,dashed](3,0)--(3,25) ;
			\foreach \Point in {(3,25),(4.333,23.8),(4.333,0)}
				\filldraw \Point circle[radius=3pt] ;
		\end{tikzpicture}
	\end{center}
\end{itemize}
\end{cillustr}

\end{document}