% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{customenvs}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO1},matiere={[UF2]},typedoc={CHAPITRE~},numdoc={5},titre={Probabilités}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\usepackage{tzplot}
\tikzset{>=to}
\newfontfamily\polhistory{QTAbbie}

\begin{document}

\pagestyle{fancy}

\part{CH05 - Probabilités élémentaires}

\section{Introduction}

\begin{cintro}[Compteur=false]
On fait remonter à la correspondance de 1654 entre Pascal et Fermat, sur un problème de jeu de hasard, l'acte de naissance du calcul des probabilités.

Après trois siècles de recherche, le calcul des probabilités a pu fournir, au début du $XX^{e}$ siècle, les bases théoriques nécessaires au développement de la statistique et a investi de très nombreux domaines de la vie scientifique, économique et sociale. 
\end{cintro}

\begin{ccadre}[Compteur=false]
Le but des probabilités est d'essayer de \textbf{rationaliser} le hasard : quelles sont les chances d'obtenir un résultat suite à une expérience aléatoire ?

Quelles chances ai-je d'obtenir  \og pile \fg{} en lançant une pièce de monnaie ? Quelles chances ai-je d'obtenir  \og 6 \fg{} en lançant un dé ? Quelles chances ai-je de valider la grille gagnante du loto ?
\end{ccadre}

\begin{chistoire}[ComplementTitre={ - Anecdote},Compteur=false]
\begin{minipage}{0.33\linewidth}
\begin{center}
	\includegraphics[width=4cm]{chap05_table}
\end{center}
\end{minipage}\hfill
\begin{minipage}{0.65\linewidth}
\begin{center}
	\includegraphics[height=1.5cm]{chap05_pascalfermat}
\end{center}
\polhistory\scriptsize Le \textit{chevalier de Méré} (17\up*{e} siècle) était un joueur et souhaitait connaître les stratégies amenant à un plus grand nombre de succès. Il soumet ce problème : « Deux joueurs misent chacun 32 pistoles dans un jeu en trois manches gagnantes. La partie est interrompue alors que le premier joueur a remporté deux manches et le second une seule. Quel doit être alors le gain de chacun des deux joueurs ? » Pascal et Fermat relèvent le défi et on retrouve les réponses dans leurs échanges épistolaires.
 Le problème décrit par le chevalier de Méré est un problème historique que l’on trouve déjà dans le \textit{Summa de Arithmetica} de Luca Pacioli (1494). Pascal et Fermat sont les premiers à y apporter une solution mathématique généralisable.\\ C’est cependant le Hollandais Christiaan Huygens qui publie en 1657 le premier traité mathématique consacré aux probabilités : \textit{Tractatus de Rariociniis in Alea Ludo}. De nos jours, les probabilités constituent une partie très importante des mathématiques.
\end{minipage}

\smallskip

~\hfill{}{\tiny Source : \textit{LeLivreScolaire}}
\end{chistoire}

\section{Vocabulaire des événements}

\subsection{Bases}

\begin{cdefi}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Chaque résultat possible d'une expérience aléatoire est appelé \underline{éventualité} liée à l'expérience aléatoire.
	\item L'ensemble formé par les éventualités est appelé \underline{univers}, il est très souvent noté $\Omega$.
	\item Un \underline{événement} d'une expérience aléatoire est une partie quelconque de l'univers.
	\item Un événement ne comprenant qu'une seule éventualité est un \underline{événement élémentaire}.
	\item L'événement qui ne contient aucune éventualité est l'\underline{événement impossible}, noté $\varnothing$.
	\item L'événement composé de toutes les éventualités est appelé \underline{événement certain}.
	\item Pour tout événement $A$ il existe un événement noté $\overline{A}$ et appelé \underline{événement contraire} de $B$.
	
	Il est composé des éléments de $\Omega$ qui ne sont pas dans A. On a en particulier $A\cup\overline{A}=\Omega$ et $A\cap\overline{A}=\varnothing$.
\end{itemize}
\end{cdefi}

\begin{cexemple}
Lancer d'un dé à six faces :
\begin{itemize}[label=\RIGHTarrow]
	\item \og obtenir $2$ \fg{} est une éventualité de cette expérience aléatoire.
	\item Univers : $\Omega=\{1;2;3;4;5;6\}$.
	\item $A=$ \og obtenir un $5$ \fg{} est un événement élémentaire que l'on peut noter $A=\{5\}$.
	\item $B=$ \og obtenir un numéro pair \fg{} est un événement que l'on peut noter $B=\{2;4;6\}$.
	\item \og obtenir $7$ \fg{} est un événement impossible.
	\item \og obtenir un nombre positif \fg{} est un événement certain.
	\item $\overline{B}=$ \og obtenir un nombre impair \fg{} est l'événement contraire de $B$.
\end{itemize}
\end{cexemple}

\begin{crmq}
Dans toute la suite du cours, on suppose que $\Omega$ est l’univers associé à une expérience aléatoire, et A et B deux événements associés à cet univers.
\end{crmq}

\subsection{Intersection et réunion d'événements}

\begin{cdefi}
\underline{Intersection d'événements} : événement constitué des éventualités appartenant à $A$ et à $B$ noté $A\cap B$ (se lit \og $A$ inter $B$ \fg{} ou \og $A$ et $B$ \fg).

\underline{Réunion d'événements} : événement constitué des éventualités appartenant à $A$ ou à $B$ noté $A\cup B$ (se lit \og $A$ union $B$ \fg{} ou \og $A$ ou $B$ \fg).
\end{cdefi}

\begin{cexemple}
On considère l'ensemble des chiffres. On note $A$ \og obtenir un chiffre pair \fg{} et $B$  \og obtenir un chiffre strictement inférieur à six \fg :
\begin{itemize}[label=\RIGHTarrow]
	\item $A \cap B=$ \og obtenir un chiffre pair et inférieur strictement à six \fg{} : $A \cap B=\{2;4\}$.
	\item $A \cup B=$ \og obtenir un chiffre pair ou inférieur strictement à six \fg{} : $A \cup B=\{1;2;3;4;5;6;8;10\}$.
\end{itemize}
\end{cexemple}

\begin{crmq}
Si $A\cap B=\varnothing$, on dit que les événements sont \textit{disjoints} ou \textit{incompatibles}.
\end{crmq}

\subsection{Représentation des événements}

\begin{cillustr}[ComplementTitre={ - Diagramme en patates (ou diagramme de Venn)}]
\begin{center}
	\begin{tikzpicture}[thick,x=0.85cm,y=0.85cm]
		\tzellipse(3,2)(2 and 1.2)
		\tzellipse(6,2)(2.2 and 1.4)
		\begin{scope}[transparency group,fill opacity=0.5]
			\draw[black,fill=lightgray] (12,2) ellipse (2 and 1.2) ;
			\draw[black,fill=lightgray](15,2) ellipse (2.2 and 1.4) ;
		\end{scope}
		\draw[black] (12,2) ellipse (2 and 1.2) ;
		\draw[black](15,2) ellipse (2.2 and 1.4) ;
		\draw (4.4,2) node[font=\footnotesize] {$A \cap B$} ;
		\draw (13.4,2) node[font=\footnotesize] {$A \cup B$} ;
		\clip (3,2) ellipse (2 and 1.2) ;
		\tzellipse[fill=lightgray,fill opacity=0.5](6,2)(2.2 and 1.4)
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cillustr}[ComplementTitre={ - Tableau}]
On peut utiliser un tableau (à double entrée) pour représenter une situation de probabilités.
\end{cillustr}

\begin{cillustr}[ComplementTitre={ - Arbre}]
On peut utiliser un arbre pour représenter une situation de probabilités.
\end{cillustr}

\pagebreak

\section{Calculs de probabilités}

\subsection{Définition}

\begin{cdefi}
La \textit{probabilité} d'un événement est la somme des probabilités des événements élémentaires qui le constitue.

\smallskip

On dit qu'il y a \textit{équiprobabilité} lorsque tous les événements élémentaires ont la même probabilité.

\smallskip

Dans ce cas, on a : $P(A)=\dfrac{\textrm{nombre d'éléments de }A}{\textrm{nombre d'éléments de }\Omega}=\dfrac{\textrm{Card}(A)}{\textrm{Card}(\Omega)}$.
\end{cdefi}

\begin{crmq}
Dans un exercice, pour signifier qu'on est dans une situation d'\textit{équiprobabilité} on a généralement dans l'énoncé un expression du type :
\begin{itemize}
	\item on lance un dé \underline{non pipé},
	\item dans une urne, il y a des boules \underline{indiscernables} au toucher,
	\item on rencontre au \underline{hasard} une personne parmi\dots
\end{itemize}
\end{crmq}

\subsection{Propriétés}

\begin{cprop}[Pluriel]
Soit $A$ et $B$ deux événements, on a les propriétés suivantes :

\begin{itemize}[leftmargin=*]
	\item $P(\varnothing)=0$.
	\item $P(\Omega)=1$.
	\item $0 \leqslant P(A) \leqslant 1$.
	\item $P(\overline{A})=1-P(A)$.
	\item $P(A\cup B)=P(A)+P(B)-P(A\cap B)$.
\end{itemize}
\end{cprop}

\begin{cexemple}
On considère l'ensemble $E$ des entiers de $1$ à $20$. On choisit l'un de ces nombres au hasard. \\
$A$ est l'événement : « le nombre est multiple de $3$ » :

\begin{itemize}[label=\RIGHTarrow]
	\item $A=\{3;6;9;12;15;18\}$.
\end{itemize}

$B$ est l'événement : « le nombre est multiple de $2$ » :

\begin{itemize}[label=\RIGHTarrow]
	\item $B=\{2;4;6;8;10;12;14;16;18;20\}$.
\end{itemize}

Calcul des probabilités :

\begin{itemize}[label=\RIGHTarrow]
	\item $P(A)=\dfrac{6}{20}=\dfrac{3}{10}=0,3$.
	\item $P(\overline{A})=1-P(A)=1-\dfrac{3}{10}=\dfrac{7}{10}=0,7$.
	\item $P(B)=\dfrac{10}{20}=\dfrac{1}{2}=0,5$.
	\item $P(A \cap B)=\dfrac{3}{20}=0,15$.
	\item $P(A \cup B)=P(A)+P(B)-P(A\cap B)=\dfrac{6}{20}+\dfrac{10}{20}-\dfrac{3}{20}=\dfrac{13}{20}=0,65$.
\end{itemize}
\end{cexemple}

\pagebreak

\section{Probabilités conditionnelles}

\subsection{Définition}

\begin{cdefi}
On suppose que $P(B)\neq 0$.

\begin{itemize}[leftmargin=*]
	\item On appelle \underline{probabilité conditionnelle de $A$ relativement à $B$} ou \underline{de $A$ sachant $B$} la probabilité que l'événement $A$ se réalise sachant que $B$ est réalisé.
	\item Cette probabilité vaut $P_B(A)=\dfrac{P(A\cap B)}{P(B)}$.
\end{itemize}
\end{cdefi}

\begin{cexemple}
On considère l'expérience aléatoire consistant à lancer un dé à $6$ faces, équilibré. On suppose que toutes les faces sont équiprobables, et on définit les événements :

\begin{itemize}
	\item $B$ : \og la face obtenue porte un numéro pair \fg{} ;
	\item $A$ : \og la face obtenue porte un numéro multiple de $3$\fg{}.
\end{itemize}

Déterminons la probabilité d'obtenir un numéro multiple de $3$, sachant qu'on a un numéro pair de deux manières différentes.

\begin{itemize}[label=\RIGHTarrow]
	\item L'événement \og A sachant B \fg{} correspond à l'événement \og obtenir un numéro multiple de 3 \fg{} parmi les éventualités de $B$, autrement dit parmi $\{ 2;4;6 \}$. Il n'y a donc que l'issue \og obtenir $6$ \fg{} qui correspond.
	
	Et comme on est en situation d'équiprobabilité, on obtient $P_B(A)=\dfrac{1}{3}$.
	\item Par le calul, on a $P(B)=\dfrac{3}{6}$ et $P(A\cap B)=\dfrac{1}{6}$ donc, d'après la formule : $P_B(A)=\dfrac{P(A\cap B)}{P(B)}=\dfrac{\nicefrac{1}{6}}{\nicefrac{1}{2}}=\dfrac{1}{3}$.
\end{itemize}
\end{cexemple}

\subsection{Propriétés}

\begin{cprop}[ComplementTitre={ - Formule des probabilités composées}]
Pour tous événements $A$ et $B$ de probabilité non nulle, on a : $$P(A\cap B)=P(B) \times P_B(A)=P(A) \times P_A(B).$$ 
\end{cprop}

\begin{cprop}[Pluriel]
Soit $S$ un événement de probabilité non nulle, on a :

\begin{itemize}[leftmargin=*]
	\item $0\leqslant P_S(A)\leqslant 1$ ;
	\item $P_S(\Omega)=1$ ;
	\item $P_S(\varnothing)=0$ ;
	\item $P_S(\bar{A})=1-P_S(A)$ ;
	\item $P_S(A\cup B)=P_S(A)+P_S(B) - P_S(A\cap B)$ ;
	\item Si $A$ et $B$ sont des événements incompatibles, alors $P_S(A\cup B)=P_S(A)+P_S(B)$ ;
	\item $P_S(\overline{A} \cap \overline{B})=P_S(\overline{A \cup B})=1-P_S(A\cup B)$.
\end{itemize}
\end{cprop}

\begin{cthm}[ComplementTitre={ - Formule des probabilités totales}]
Pour tous $A$ et $B$ de probabilité non nulle : $$P(A)=P(B) \times P_B(A)+P(\overline{B})\times P_{\overline{B}}(A).$$
\end{cthm}

\subsection{Événements indépendants}

\begin{cdefi}
On dit que $A$ et $B$ sont des événements \underline{indépendants} si et seulement si $P(A\cap B)=P(A) \times P(B)$.
\end{cdefi}

\begin{cexemple}
On considère le tirage au hasard d'une carte d'un jeu de $32$ cartes.

$A=$ \og Tirer un as \fg \quad  \quad $B=$ \og Tirer un cœur \fg{} \quad  \quad $C=$ \og Tirer un as rouge \fg{}.

Indépendance de $A$ et $B$ :

\begin{itemize}[label=\RIGHTarrow]
	\item $P(A)=\dfrac{4}{32}=\dfrac{1}{8}$ et $P(B)=\dfrac{8}{32}=\dfrac{1}{4}$.   
	\item $P(A\cap B)=\dfrac{1}{32}=P(A)\times P(B)$, les événements $A$ et $B$ sont donc indépendants.   
\end{itemize}

Indépendance de $B$ et $C$ :

\begin{itemize}[label=\RIGHTarrow]
	\item $P(B)=\dfrac{1}{4}$ et $P(C)=\dfrac{2}{32}=\dfrac{1}{16}$.   
	\item $P(B\cap C)=\dfrac{1}{32}\neq P(B)\times P(C)$, les événements $B$ et $C$ ne sont donc pas indépendants.   
\end{itemize}
\end{cexemple}

\begin{crmq}
Dans le cas où $A$ et $B$ sont des événements de probabilités non nulles, on a :

\begin{itemize}[leftmargin=*]
	\item $P(A\cap B)=P(B) \times P_B(A)=P(A) \times P_A(B)=P(A) \times P(B)$, d'où $P_B(A)=P(A)$ et $P_A(B)=P(B)$.
\end{itemize}
\end{crmq}

\begin{cprop}
Si $A$ et $B$ sont des événements indépendants, alors : $A$ et $\overline{B}$ ; $\overline{A}$ et $B$ ; $\overline{A}$ et $\overline{B}$ sont également des événements indépendants.
\end{cprop}

\end{document}