% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO1},matiere={[UF2]},typedoc=CHAPITRE~,numdoc=01,titre={Statistiques à une variable}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}
\urlstyle{same}

\begin{document}

\pagestyle{fancy}

\part{CH01 - Statistiques à une variable}

\section{Approche historique}

\begin{chistoire}[Compteur=false,SousTitre={Source : Wikipedia}]<fontupper=\large\polancienne>
\begin{wrapstuff}[r,top=1]
	\includegraphics[height=3.5cm]{chap01_activite_nightindale}
\end{wrapstuff}
On attribue à l'histoire de la statistique ou des statistiques la date de commencement de 1749, bien que l’interprétation du terme « \textit{statistique} » a changé au cours du temps.

Aux temps plus anciens, cette science ne consistait qu’à la collection d’informations des États, d’où l’étymologie du nom, de l’allemand \textit{Statistik}, dérivé de l’italien \textit{statista} (\og Homme d’État \fg).

Plus tard, cette définition est étendue à tout type d’information collectée et, encore plus tard, les sciences statistiques incluent l’analyse et l’interprétation de ces données. En termes modernes, les statistiques incluent les ensembles de données, telles celles de la comptabilité nationale et les registres de températures, ainsi que le travail d’analyse, lequel requiert les méthodes de l’inférence statistique.

\smallskip

Florence Nightingale est une pionnière de la présentation visuelle de l'information. Elle utilise entre autres les diagrammes circulaires, les \og Pie Chart \fg, développés par William Playfair en 1801.

Après la guerre de Crimée, elle se met à utiliser une version améliorée de ces diagrammes afin d'illustrer les causes saisonnières de mortalité des patients de l'hôpital militaire qu'elle gère.
\end{chistoire}

\section{Définitions et notations}

\subsection{Notations}

\begin{cintro}[Compteur=false]
Dans ce chapitre, on étudie des séries à caractères \textbf{quantitatifs} discrètes (à valeurs séparées) ou \textbf{continues} (dont les valeurs sont regroupées en \textit{classes}, ou \textit{intervalles}).

Dans le cas d’une série continue, on fait toujours l’hypothèse que la \textit{répartition} des valeurs est \textit{uniforme} à l’intérieur de chaque classe.
\end{cintro}

\begin{cnota}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item X est le caractère étudié et $x_1$, $x_2$, \dots, $x_p$ les \textit{valeurs} du caractère ou les centres des classes dans le cas où les valeurs sont regroupées en classes. Ces valeurs sont \uline{rangées dans l’ordre croissant}.
	\item $n_1$, $n_2$, \dots, $n_p$ sont les \textit{effectifs} respectifs des valeurs $x_1$, $x_2$, \dots, $x_p$.
	\item $f_1$, $f_2$, \dots, $f_p$ sont les \textit{fréquences} respectives des valeurs $x_1$, $x_2$, \dots, $x_p$.
	\item N est l’effectif total, et $N=n_1+n_2+\dots+n_p$
\end{itemize}
\end{cnota}

\subsection{Fréquences}

\begin{cprop}[Pluriel]
La fréquence de la valeur $x_i$ est donnée par $f_i=\dfrac{n_i}{N}$.

Ce nombre est compris dans  l’intervalle $\intervFF{0}{1}$ et est souvent écrit sous la forme d’un \textit{pourcentage}.

La somme de toutes les fréquences est égale à 1 : $\sum_{i=1}^p f_i=f_1+f_2+\dots+f_p=1$.
\end{cprop}

\subsection{Effectif cumulé, fréquence cumulée}

\begin{cdefi}[Pluriel,ComplementTitre={ - Cas d’une série discrète}]
C'est le nombre d'individus pour lesquels la valeur du caractère est \textit{inférieure ou égale} à $x_i$.

On a $S_i= n_1+n_2+\dots+n_i$ et de ce fait on a $S_p  = N$ (dernier effectif cumulé).

\smallskip

La fréquence cumulée en $x_i$ est donnée par $F_i=f_1+f_2+\dots+f_i=\dfrac{S_i}{N}.$
\end{cdefi}

\begin{cdefi}[ComplementTitre={ - Cas d’une série continue}]
Pour l'effectif cumulé d'une classe $\intervOF{a}{b}$, il s'agit de l'effectif cumulé en $b$ (borne droite de l'intervalle), c'est-à-dire le nombre d'individus pour lesquels la valeur du caractère est inférieure ou égale à $b$. 

Ainsi, lorsqu'on calcule des effectifs cumulés pour une série statistique groupée en classes, ces effectifs cumulés doivent être \textbf{impérativement} affectés aux \textit{bornes de droite} de ces classes.
\end{cdefi}

\begin{crmq}[Compteur=false,ComplementTitre={ - Hypothèse de répartition uniforme}]
Pour des raisons pratique, on fait l'hypothèse de \textit{répartition uniforme} à l'intérieur de chaque classe, c'est-à-dire qu'il y a \og proportionnalité entre les effectifs et les largeurs \fg.
%La distribution de l'effectif de la classe $\intervOF{a}{b}$ est \textit{uniforme} signifie en pratique que, si $c$ et $d$ sont deux nombres tels que $a \pp c < d \pp b$, alors l'effectif de l'intervalle $\intervOF{c}{d}$ vérifie la relation de proportionnalité : $$\dfrac{\text{effectif de } \intervOF{c}{d}}{d-c}=\dfrac{\text{effectif de } \intervOF{a}{b}}{b-a}.$$
\end{crmq}

\section{Paramètres d'une série}

\subsection{Médiane et quartiles}

\begin{cdefi}
La valeur médiane d’une série statistique est la valeur M séparant la population en deux moitiés : les 50\,\% ayant une valeur inférieure ou égale à M et les 50\,\% ayant une valeur supérieure ou égale à M.
\end{cdefi}

\begin{cmethode}[Pluriel]
Cas d’une série \textbf{discrète} : 

Si l’effectif est impair, la médiane est la valeur de rang $\nicefrac{(N+1)}{2}$ ; si l’effectif est pair, la médiane est la moyenne des valeurs de rang $\nicefrac{N}{2}$ et $\nicefrac{N}{2}+1$.

\smallskip

Cas d’une série \textbf{continue} :

La médiane est la valeur correspondant à la fréquence cumulée égale à 0,5.
\end{cmethode}

\begin{cdefi}[Pluriel]
De même, le premier quartile est la valeur $Q_1$ telle que 25\,\% de la population a une valeur du caractère inférieure ou égale à $Q_1$, les 75\,\% restants ayant une valeur supérieur ou égale à $Q_1$.

Le troisième quartile est la valeur $Q_3$ telle que 75\,\% de la population a une valeur du caractère inférieure ou égale à $Q_3$, les 25\,\% restants ayant une valeur supérieur ou égale à $Q_3$.
\end{cdefi}

\begin{cmethode}[Pluriel]
Cas d’une série \textbf{discrète} : 

Si $\nicefrac{N}{4}$ est un entier, $Q_1$ est la valeur de rang $\nicefrac{N}{4}$ et $Q_3$ est la valeur de rang $\nicefrac{3N}{4}$.

Si $\nicefrac{N}{4}$ n’est pas un entier, $Q_1$ est est la valeur de rang immédiatement supérieur à $\nicefrac{N}{4}$ et $Q_3$ est la valeur de rang immédiatement supérieur à $\nicefrac{3N}{4}$.

\smallskip

Cas d’une série \textbf{continue} :

$Q_1$ est la valeur correspondant à la fréquence cumulée égal à $0,25$ et $Q_3$ est la valeur correspondant  à la fréquence cumulée égale à $0,75$.
\end{cmethode}

\begin{cdefi}[Pluriel]
Intervalle interquartile : c’est l’intervalle $\intervFF{Q_1}{Q_3}$.\\
Écart interquartile : c’est la valeur de $Q_3-Q_1$. 
\end{cdefi}

\subsection{Moyenne}

\begin{cdefi}
Pour une série \textbf{discrète}, la moyenne des valeurs $x_1$, $x_2$, \dots, $x_p$ d'effectifs $n_1$, $n_2$, \dots, $n_p$  (effectif total N) est le réel : $$\bar{x}=\dfrac{n_1 x_1+n_2 x_2+\dots+n_p x_p}{n_1+n_2+\dots+n_p}=\dfrac{1}{N} \sum_{i=1}^p n_i x_i.$$

\smallskip

Pour une série \textbf{continue}, on applique la même formule en prenant pour les valeurs $x_i$ les centres des classes.
\end{cdefi}

\subsection{Variance et écart-type}

\begin{cintro}[Compteur=false]
La moyenne d'un caractère statistique ne suffit pas pour caractériser le comportement de ses valeurs.

Par exemple, les étudiants d'un premier groupe peuvent avoir obtenu des résultats très homogènes (c'est-à-dire des notes comprises dans un petit intervalle autour de 10) alors que ceux d'un deuxième groupe peuvent avoir des résultats beaucoup plus dispersés autour de 10. L'\textbf{écart-type} est un nombre qui mesure de cette \textbf{dispersion}, au sens où plus l'écart-type est élevé plus les valeurs sont dispersées autour de la moyenne.
\end{cintro}

\begin{cdefi}[Pluriel]
La \textbf{variance} d’une série statistique de caractère X prenant les valeurs (ou centres des classes) $x_1$, $x_2$, \dots, $x_p$ d’effectifs respectifs $n_1$, $n_2$, \dots, $n_p$ et de moyenne $\overline{x}$ est le nombre $$V(x)=\dfrac{1}{N} \sum_{i=1}^p \left(x_i-\bar{x}\right)^2=\dfrac{1}{N} \sum_{i=1}^p n_i x_i^2 - \overline{x}^2.$$%
On en déduit l'\textbf{écart-type} de la série $\sigma(X)=\sqrt{V(X)}$ qui est toujours un nombre positif.
\end{cdefi}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{demoivre}{}L'écart type est une grandeur dont l'invention remonte au XIX\up{e} siècle, qui vit la statistique se développer au Royaume-Uni. C'est à \textit{Abraham de Moivre} (1667/1754, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{FR}\!) qu'est attribuée la découverte du concept de mesure de la dispersion qui apparaît dans son ouvrage \textit{The Doctrine of Chances} en 1718.
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{pearson}{}Mais le terme d'écart type (« standard deviation ») a été employé pour la première fois par \textit{Karl Pearson} (1857/1936, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{GB}\!) en 1893 devant la Royal Society. C'est aussi lui qui utilise pour la première fois le symbole $\sigma$ pour représenter l'écart type.
\end{chistoire}

\section{Représentations graphiques}

\begin{cintro}[Compteur=false]
Il existe, comme présenté en cours, plusieurs représentations graphiques possibles pour des séries statistiques. 

Pour des séries à valeurs discrètes, on peut utiliser :
\begin{itemize}
	\item le nuage de points ou la \og courbe \fg{} ;
	\item le diagramme en bâtons ou en barres ;
	\item le diagramme circulaire.
\end{itemize}

Pour des séries à valeurs continues, on va utiliser :
\begin{itemize}
	\item le diagramme circulaire ;
	\item l'histogramme.
\end{itemize}
\end{cintro}

\begin{cexemple}
On considère la série (discrète) suivante :

\begin{center}
	\begin{tblr}{width=15cm,hlines,vlines,colspec={Q[c,m]*{14}{X[m,c]}}}
		Notes & $02$ & $03$ & $04$ & $05$ & $06$ & $07$ & $08$ & $09$ & $10$ & $11$ & $13$ & $15$ & $16$ \\
		Eff. & $1$ & $2$ & $1$ & $1$ & $2$ & $3$ & $5$ & $6$ & $2$ & $3$  & $2$ & $1$ & $1$ \\
	\end{tblr}
\end{center}
\end{cexemple}

\begin{cillustr}[Pluriel]
\begin{center}
	\begin{tikzpicture}[>=latex,x=0.55cm,y=0.55cm,xmin=0,xmax=20,xgrille=1,xgrilles=1,ymin=0,ymax=7,ygrille=1,ygrilles=1]
		%axes et grilles
		\GrilleTikz[Affp=false][][dotted,line width=0.6pt,gray]
		\draw[->,line width=1.25pt] (\xmin,0) -- (\xmax,0);
		\draw[->,line width=1.25pt] (0,\ymin) -- (0,\ymax);
		\foreach \x in {0,1,...,19}
			\draw[line width=1.25pt] (\x,4pt) -- (\x,-4pt) node[below] {\num{\x}};
		\foreach \y in {0,1,...,6}
			\draw[line width=1.25pt] (4pt,\y) -- (-4pt,\y) node[left] {\footnotesize \num{\y}};
		%données
		\foreach \Point in {(2,1),(3,2),(4,1),(5,1),(6,2),(7,3),(8,5),(9,6),(10,2),(11,3),(13,2),(15,1),(16,1)}
			\draw[ultra thick] \Point[font=\Large,red] node {$+$};
	\end{tikzpicture}
	
	\smallskip
	
	\begin{tikzpicture}[>=latex,x=0.55cm,y=0.55cm,xmin=0,xmax=20,xgrille=1,xgrilles=1,ymin=0,ymax=7,ygrille=1,ygrilles=1]
		%axes et grilles
		\GrilleTikz[Affp=false][][dotted,line width=0.6pt,gray]
		\draw[->,line width=1.25pt] (\xmin,0) -- (\xmax,0);
		\draw[->,line width=1.25pt] (0,\ymin) -- (0,\ymax);
		\foreach \x in {0,1,...,19}
		\draw[line width=1.25pt] (\x,4pt) -- (\x,-4pt) node[below] {\num{\x}};
		\foreach \y in {0,1,...,6}
		\draw[line width=1.25pt] (4pt,\y) -- (-4pt,\y) node[left,font=\footnotesize] {\num{\y}};
		%données
		\draw[line width=1.25pt,red] (2,1) -- (3,2) -- (4,1) -- (5,1) -- (6,2) -- (7,3) -- (8,5) -- (9,6) -- (10,2) -- (11,3) -- (13,2) -- (15,1) -- (16,1) ;
	\end{tikzpicture}

	\smallskip
	
	\begin{tikzpicture}[>=latex,x=0.55cm,y=0.55cm,xmin=0,xmax=20,xgrille=1,xgrilles=1,ymin=0,ymax=7,ygrille=1,ygrilles=1]
		%axes et grilles
		\GrilleTikz[Affp=false][][dotted,line width=0.6pt,gray]
		\draw[->,line width=1.25pt] (\xmin,0) -- (\xmax,0);
		\draw[->,line width=1.25pt] (0,\ymin) -- (0,\ymax);
		\foreach \x in {0,1,...,19}
		\draw[line width=1.25pt] (\x,4pt) -- (\x,-4pt) node[below] {\num{\x}};
		\foreach \y in {0,1,...,6}
		\draw[line width=1.25pt] (4pt,\y) -- (-4pt,\y) node[left,font=\footnotesize] {\num{\y}};
		%données
		\foreach \x/\y in {2/1,3/2,4/1,5/1,6/2,7/3,8/5,9/6,10/2,11/3,13/2,15/1,16/1}
			\draw[line width=3pt,red] (\x,0) -- (\x,\y);
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cexemple}
On considère la série (continue) suivante :

\begin{center}
	\begin{tblr}{width=10cm,colspec={Q[m,c]*{4}{X[m,c]}},hlines,vlines}
		Notes & $[0;5[$ & $[5;10[$ & $[10;15[$ & $[15;20[$ \\
		Effectif & $4$ & $17$ & $7$ & $2$ \\
	\end{tblr}
\end{center}
\end{cexemple}

\begin{cillustr}
Pour le digramme circulaire, on travaille par proportionnalité par rapport à \og 360° $\rightleftarrows$ 30 (effectif total) \fg{}.
\begin{itemize}
	\item effectif de 4 : angle de $\tfrac{360 \times 4}{30} = 12 \times 4 = 48$° ;
	\item effectif de 17 : angle de $\tfrac{360 \times 17}{30} = 12 \times 17 = 204$° ;
	\item effectif de 7 : angle de $\tfrac{360 \times 7}{30} = 12 \times 7 = 84$° ;
	\item effectif de 2 : angle de $\tfrac{360 \times 2}{30} = 12 \times 2 = 24$°.
\end{itemize}

\begin{center}
	\begin{tikzpicture}[x=3cm,y=3cm]
		\draw[line width=1pt,fill=red!10] (0,0) circle(1);
		\filldraw[fill=blue!10,line width=1pt] (0,0) -- (1,0) arc (0:48:1) -- (0,0);
		\filldraw[fill=orange!10,line width=1pt] (0,0) -- (-24:1) arc (-24:0:1) -- (0,0);
		\filldraw[fill=green!10,line width=1pt] (0,0) -- (-108:1) arc (-108:-24:1) -- (0,0);
		\draw (24:0.6) node[rotate=24] {\textcolor{blue}{\ppoint{[0;\,5[}}} ;
		\draw (150:0.6) node[rotate=-30] {\textcolor{red}{\ppoint{[5;\,10[}}} ;
		\draw (294:0.6) node[rotate=-66] {\textcolor{green}{\ppoint{[10;\,15[}}} ;
		\draw (348:0.6) node[rotate=-12] {\textcolor{orange}{\ppoint{[15;\,20[}}} ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{crappel}[Compteur=false]
Pour l'histogramme, c'est l'\textbf{aire} de chaque rectangle qui est proportionnelle à l'effectif (ou à la fréquence) associée à chaque classe.

Lorsque les classes ont la même \textbf{amplitude}, c'est la \textit{hauteur} qui est proportionnelle à l'effectif.
\end{crappel}

\begin{cillustr}
Ici les amplitudes sont toute égales à $5$ :

\begin{center}
	%\tunits{0.6}{0.6}
	%\tdefgrille{0}{20}{1}{1}{0}{9}{1}{1}
	\begin{tikzpicture}[>=latex,x=0.6cm,y=0.6cm,xmin=0,xmax=20,xgrille=1,xgrilles=1,ymin=0,ymax=9,ygrille=1,ygrilles=1]
		%rectangles
		\draw[fill=blue!10,line width=1pt] (0,0) rectangle (5,2) ;
		\draw[fill=red!10,line width=1pt] (5,0) rectangle (10,8.5) ;
		\draw[fill=green!10,line width=1pt] (10,0) rectangle (15,3.5) ;
		\draw[fill=orange!10,line width=1pt] (15,0) rectangle (20,1) ;
		%axes et grilles
		\GrilleTikz[Affp=false][][dotted,line width=0.6pt,gray]
%		\tgrilles[dotted,line width=0.6pt,gray]
		\draw[->,line width=1.25pt] (\xmin,0) -- (\xmax,0);
		\draw[->,line width=1.25pt] (0,\ymin) -- (0,\ymax);
		\foreach \x in {0,1,...,19}
			\draw[line width=1.25pt] (\x,4pt) -- (\x,-4pt) node[below] {\num{\x}};
		\foreach \y in {0,4,...,16}
			\FPeval{yy}{\y/2}
			\draw[line width=1.25pt] (4pt,\yy) -- (-4pt,\yy) node[left] {\num{\y}};
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cillustr}
Dans l'exemple suivant, les classes n'ont pas la même amplitude, on \og s'arrange \fg{} donc pour créer une unité d'aire permettant de représenter la série.

\begin{itemize}
	\item on choisit 200 pour unité de classe (généralement celui de la classe la plus petite) ;
	\item on détermine le nombre d’intervalles unitaires (I.U.) de chaque classe ;
	\item on détermine la hauteur des rectangles en divisant l’effectif de la classe par le nombre d’intervalles unitaires.
\end{itemize}

\begin{center}
	\begin{tblr}{width=\linewidth,hlines,vlines,colspec={Q[m,c]*{6}{X[m,c]}},cell{1}{2-Z}={font=\small}}
		Valeurs & $[9\,00;1\,200[$ & $[1\,200;1\,400[$ & $[1\,400;1\,600[$ & $[1\,600;1\,800[$ & $[1\,800;2\,000[$ & $[2\,000;2\,400[$ \\
		Effectif & $30$ & $30$ & $60$ & $40$ & $20$ & $20$ \\
		Largeur classe & $300$ & $200$ & $200$ & $200$ & $200$ & $400$ \\
		Nb int. unit. & $1,5$ & $1$ & $1$ & $1$ & $1$ & $2$ \\
		Eff. par int. unit. & 20 & 30 & 60 & 40 & 20 & 10 \\
	\end{tblr}
\end{center}

\begin{center}
	%\tunits{0.8}{0.8}
	%\tdefgrille{0}{17}{1}{1}{0}{7}{1}{1}
	\begin{tikzpicture}[>=latex,x=0.8cm,y=0.8cm,xmin=0,xmax=17,xgrille=1,xgrilles=1,ymin=0,ymax=7,ygrille=1,ygrilles=1]
		\draw[fill=red!10,line width=1pt] (1,0) rectangle (4,2) node[pos=.5,red,font=\Large\sffamily] {30} ;
		\draw[fill=green!10,line width=1pt] (4,0) rectangle (6,3) node[pos=.5,green,font=\Large\sffamily] {30} ;
		\draw[fill=yellow!10,line width=1pt] (6,0) rectangle (8,6) node[pos=.5,orange,font=\Large\sffamily] {60} ;
		\draw[fill=red!10,line width=1pt] (8,0) rectangle (10,4) node[pos=.5,red,font=\Large\sffamily] {40} ;
		\draw[fill=green!10,line width=1pt] (10,0) rectangle (12,2) node[pos=.5,green,font=\Large\sffamily] {20} ; \draw[fill=yellow!10,line width=1pt] (12,0) rectangle (16,1) node[pos=.5,orange,font=\Large\sffamily] {20} ;
		%grille et axes
		\GrilleTikz[Affp=false][][dotted,line width=0.6pt,gray]
		%\tgrilles[dotted,line width=0.6pt,gray]
		\draw[->,line width=1.25pt] (\xmin,0) -- (\xmax,0);
		\draw[->,line width=1.25pt] (0,\ymin) -- (0,\ymax) node[below right] {Eff./I.U.};
		\foreach \x in {800,1000,1200,1400,1600,1800,2000,2200,2400}
			\FPeval{xx}{(\x-800)/100}
			\draw[line width=1.25pt] (\xx,4pt) -- (\xx,-4pt) node[below] {\num{\x}};
		\foreach \y in {20,40,60}
			\FPeval{yy}{\y/10}
			\draw[line width=1.25pt] (4pt,\yy) -- (-4pt,\yy) node[left] {\num{\y}};
		%notice
		\draw[line width=1pt,fill=lightgray!50] (15,5) rectangle (16,6) ;
		\draw (15.5,6) node[above] {5 unités} ;
		\draw[line width=1pt,<->] (15,3) -- (17,3) ;
		\draw (16,3) node[above] {1 I.U} ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cintro}[Compteur=false]
Il existe également deux représentations \og outils \fg{} qui permettent d'étudier plus finement une série :
\begin{itemize}
	\item le diagramme en boîte à moustaches ;
	\item la courbe des ECC (ou des FCC).
\end{itemize}
Dans les deux cas de figure, on est sur une exploitation ou une recherche de paramètres statistiques (médiane, quartiles).
\end{cintro}

\begin{crappel}[Compteur=false]
Le polygone (ou la courbe) des effectifs cumulés (ou fréquences cumulées) est formé de segments de droite joignant les points :
\begin{itemize}
	\item d’abscisses : la borne supérieure d’une classe pour le polygone des ECC ;
	\item d’ordonnées : l’effectif cumulé d’une classe ;
	\item partant du point d'ordonnée 0 et d'abscisse la borne inférieure de la première classe.
\end{itemize}
\end{crappel}

\begin{cexemple}
On donne la série statistique suivante :

\begin{center}
	\begin{tblr}{stretch=1.15,width=0.95\linewidth,colspec={Q[l,m]*{8}{X[m,c]}},vlines,cells={font=\small}}
		\hline
		Valeurs $x_i$ & $[0;15[$ & $[15;25[$& $[25;35[$ &$[35;40[$&$[40;45[$&$[45;55[$&$[55;65[$ & $[65;75[$ \\ \hline
		Effectif $n_i$ & $15$ & $20$ & $50$ & $30$ & $35$ & $25$ & $15$ & $10$ \\ \hline \hline
		Fréquence en \% & $7,5$ & $10$ & $25$ & $15$ & $17,5$ & $12,5$ & $7,5$ & $5$ \\ \hline
		FCC en \% & $7,5$ & $17,5$ & $42,5$ & $57,5$ & $75$ & $87,5$ & $95$ & $100$ \\ \hline
	\end{tblr}
\end{center}
\end{cexemple}

\begin{cillustr}
\begin{center}
	%\tunits{0.18}{9}
	%\tdefgrille{0}{80}{10}{5}{0}{1.05}{0.1}{0.05}
	\begin{tikzpicture}[>=latex,x=0.18cm,y=9cm,xmin=0,xmax=80,xgrille=10,xgrilles=5,ymin=0,ymax=1.05,ygrille=0.1,ygrilles=0.05]
		%ECC
		%axes & grille
		\GrilleTikz[Affp=false][][dotted,line width=0.6pt,gray]
		%\tgrilles[dotted,line width=0.6pt,gray] ;
		\draw[dotted,line width=0.6pt,gray] (0,1.05) -- (\xmax,1.05);
		\draw[->,line width=1.25pt] (\xmin,0) -- (\xmax,0);
		\draw[->,line width=1.25pt] (0,\ymin) -- (0,\ymax);
		\foreach \x in {0,5,...,75}
			\draw[line width=1.25pt] (\x,4pt) -- (\x,-4pt) node[below] {\num{\x}};
		\foreach \y in {0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1}
			\draw[line width=1.25pt] (4pt,\y) -- (-4pt,\y) node[left] {\num{\y}};
		%courbe
		\draw[line width=1.25pt,color=red] (0,0) -- (15,0.075) -- (25,0.175) -- (35,0.425) -- (40,0.575) -- (45,0.75) -- (55,0.875) -- (65,0.95) -- (75,1) ;
		\foreach \Point in {(0,0),(15,0.075),(25,0.175),(35,0.425),(40,0.575),(45,0.75),(55,0.875),(65,0.95),(75,1)}
		\draw[blue,fill=blue] \Point circle(2pt);
		%traits de construction
		\draw[dashed,line width=1.25pt,VertForet,<-] (37.5,0) -- (37.5,0.5) -- (0,0.5) ; %MED
		\draw[dashed,line width=1.25pt,violet,<-] (28,0) -- (28,0.25) -- (0,0.25) ; %Q1
		\draw[dashed,line width=1.25pt,violet,<-] (45,0) -- (45,0.75) -- (0,0.75) ; %Q3
		\foreach \Point in {(28,0.25),(37.5,0.5),(45,0.75)}
			\draw[gray,fill=gray] \Point circle(2pt);
		%textes
		\draw (28,0) node[above left] {\textcolor{violet}{$\approx 28$}} ;
		\draw (37.5,0) node[above right] {\textcolor{VertForet}{$\approx 37,5$}} ;
		%BOÎTE
		%bords
		\draw[line width=1.5pt] (0,-0.2) -- (0,-0.4) ;
		\draw[line width=1.5pt] (75,-0.2) -- (75,-0.4) ;
		%moustaches
		\draw[line width=1.5pt] (0,-0.3) -- (28,-0.3) ;
		\draw[line width=1.5pt] (45,-0.3) -- (75,-0.3) ;
		%mediane
		\draw[VertForet,line width=1.5pt] (37.5,-0.2) -- (37.5,-0.4) ;
		%boite
		\draw[line width=1.5pt,violet] (28,-0.2) rectangle (45,-0.4) ;
		%params
		\draw (0,-0.15) node[above] {\textsf{min}} ;
		\draw (75,-0.15) node[above] {\textsf{Max}} ;
		\draw (28,-0.15) node[above] {\textcolor{violet}{\textsf{Q1}}} ;
		\draw (45,-0.15) node[above] {\textcolor{violet}{\textsf{Q3}}} ;
		\draw (37.5,-0.15) node[above] {\textcolor{VertForet}{\textsf{Méd}}} ;
	\end{tikzpicture}
\end{center}

\end{cillustr}

\begin{clog}[Logo=windows,Compteur=false]
Le logiciel gratuit \textsf{SineQuaNon}, fonctionnant sous Windows $\vcenter{\hbox{\includegraphics[height=0.75\baselineskip]{windows}}}$, et disponible à l'adresse internet \cshell{\url{http://patrice-rabiller.fr/SineQuaNon/menusqn.htm}}, permet de traiter et d'exploiter des données statistiques, qu'elles soient quantitatives, qualitatives, discrètes ou continues\dots.

L'interface est un peu \textit{austère} mais les fonctionnalités sont bien présentes, et très pertinentes !
%
\begin{center}
	\includegraphics[height=2.75cm]{td_rep_sine_a}~~\includegraphics[height=2.75cm]{td_rep_sine_b}~~\includegraphics[height=2.75cm]{td_rep_sine_c}
\end{center}
\end{clog}

\end{document}