% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[UF2]},typedoc={CHAPITRE~},numdoc={5},titre={Suites}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\part{CH05 - Suites numériques}

\section{Notion de suite}

\subsection{Définition et représentation graphique}

\begin{cdefi}[Pluriel]
Une suite est une \textbf{liste} de nombres réels numérotés par les entiers naturels. On la note généralement $\Suite{u}$.

On appelle $n$ l’indice (ou rang) et $u_n$ est le terme d’indice $n$ de la suite $\Suite{u}$.

Le terme initial ou premier terme de la suite est soit $u_0$, soit $u_1$ selon les exercices.
\end{cdefi}

\begin{cnota}
On note une suite entre parenthèses, soit $\Suite{u}$ ou parfois $\Suite{u}_{n \in \N}$.

$u_n$ est le terme d'indice $n$ de la suite $\Suite{u}$.

Ainsi, $u_{n-1}$ est le terme qui précède $u_n$, et $u_{n+1}$ est le terme qui suit $u_n$. 
\end{cnota}

\begin{cillustr}
On peut représenter une suite sur un graphique en mettant les valeurs de $n$ en abscisse et les valeurs correspondantes de $u_n$ en ordonnée.
\end{cillustr}

\begin{crmq}
Il est très utile de savoir tracer la représentation graphique et obtenir le tableau de valeurs d'une suite à l'aide de la calculatrice.
\end{crmq}

\subsection{Mode de génération d'une suite}

\begin{cprop}[Pluriel]
Une suite peut être définie  :
\begin{itemize}
	\item de manière \textbf{explicite} : par une formule du type $u_n=f(n)$ ;
	\item par \textbf{récurrence} : à l'aide d'une formule du type $u_{n+1}=f \left( u_n \right)$ (de proche en proche).
\end{itemize}
\end{cprop}

\begin{crmq}
Il est en général plus naturel de définir une suite par récurrence. Néanmoins, il est souvent plus pratique d'avoir la définition explicite d'une suite, car celle-ci permet de calculer la valeur de n'importe quel terme en un seul calcul. 
\end{crmq}

\subsection{Comportement global d'une suite}

\begin{cdefi}
\begin{itemize}[leftmargin=*]
	\item On dit que la suite $\Suite{u}$ est croissante si pour tout entier $n$, $u_n \pp u_{n+1}$.
	
	La suite est dite strictement croissante si l'inégalité est toujours stricte.
	\item On dit que la suite $\Suite{u}$ est décroissante si pour tout entier $n$, $u_n \pg u_{n+1}$.
	
	La suite est dite strictement décroissante si l'inégalité est toujours stricte.
\end{itemize}
\end{cdefi}

\newpage

\section{Suites classiques}

\subsection{Suites arithmétiques}

\begin{cdefi}
On dit qu’une suite est \textbf{arithmétique} si chaque terme est obtenu à partir du précédent en ajoutant un \textbf{même} nombre $r$, appelé raison. Pour tout entier $n$, $u_{n+1} = u_n + r$ avec $u_0$ (ou $u_1$) donné.
\end{cdefi}

\begin{cprop}[Pluriel]
Soit $u_0$ le premier terme et $r$ la raison d’une suite arithmétique, alors le terme de rang $n$ est $u_n = u_0 + nr$.
 
Soit $u_1$ le premier terme et $r$ la raison d’une suite arithmétique alors le terme de rang $n$ est $u_n = u_1 + (n-1)r$.

Cette expression permet de déterminer n’importe quel terme de la suite d’indice donné.

Si la suite $\Suite{u}$ est arithmétique de raison $r$ alors pour tout $n$ et $p$ entiers, on a $u_n = u_p +(n-p)r$ .
\end{cprop}

\begin{cmethode}
Pour justifier qu'une suite est arithmétique, on montre que $u_{n+1}- u_n$ est un nombre constant $r$, indépendant de $n$, qui est appelé la raison. On peut aussi écrire une phrase qui décrive le fonctionnement de la suite.
\end{cmethode}

\begin{cthm}
Soit $\Suite{u}$ une suite arithmétique de raison $r$. Alors :
\begin{itemize}
	\item Si $r<0$ alors la suite $\Suite{u}$ est décroissante ;
	\item Si $r=0$ alors la suite $\Suite{u}$ est constante ;
	\item Si $r>0$ alors la suite $\Suite{u}$ est croissante.
\end{itemize}
\end{cthm}

\begin{cprop}
La formule suivante est valable pour tout somme S de termes consécutifs d'une suite arithmétique \[S=\text{(nombre de termes de S)} \times \dfrac{\text{(premier terme de S)} + \text{(dernier terme de S)}}{2}.\]
En particulier, si $\Suite{u}$ est de :
\begin{itemize}
	\item premier terme $u_0$, alors $S=u_0+u_1+\dots+u_n=(n+1) \times \dfrac{u_0+u_n}{2}$.
	\item premier terme $u_1$, alors $S=u_1+u_2+\dots+u_n=n \times \dfrac{u_1+u_n}{2}$.
\end{itemize}
\end{cprop}

\subsection{Suites géométriques}

\begin{cdefi}
Une suite géométrique est une suite numérique dont chaque terme est obtenu en multipliant le terme précédent par un même nombre constant $q$ ($q \neq 0$), appelé raison. Pour tout entier $n$, $u_{n+1} = q \times u_n$ avec $u_0$ (ou $u_1$) donné.
\end{cdefi}

\begin{cprop}[Pluriel]
Soit $u_0$ le premier terme et $q$ la raison d’une suite géométrique, alors le terme de rang $n$ est $u_n = u_0 \times q^n$.

Soit $u_1$ le premier terme et $q$ la raison d’une suite géométrique alors le terme de rang $n$ est $u_n = u_1 \times q^{n-1}$.

Cette expression permet de déterminer n’importe quel terme de la suite d’indice donné.

Si la suite $\Suite{u}$ est géométrique de raison $q$ alors pour tous $n$ et $p$ entiers, on a $u_n = u_p  \times q^{n-p}$.
\end{cprop}

\begin{cmethode}
Si aucun terme n’est nul, on justifie que $\Suite{u}$ est géométrique en montrant que $\tfrac{u_{n+1}}{u_n}$ est un nombre constant $q$, indépendant de $n$, appelé la raison. On peut aussi écrire une phrase qui décrit le fonctionnement.
\end{cmethode}

\begin{cprop}
Une suite géométrique de premier positif est croissante lorsque $q >1$.

Une suite géométrique de premier positif est décroissante lorsque $0<q<1$.
\end{cprop}

\begin{cprop}
La formule suivante est valable pour tout somme S de termes consécutifs d'une suite géométrique \[S=\text{(premier de S)} \times \dfrac{1-\text{(raison)}^\text{(nombre de  termes de S)}}{1-\text{(raison)}}.\]
En particulier, si $\Suite{u}$ est de :
\begin{itemize}
	\item premier terme $u_0$, alors $S=u_0 \times \dfrac{1-q^{n+1}}{1-q}$ ;
	\item premier terme $u_1$, alors $S=u_1 \times \dfrac{1-q^{n}}{1-q}$.
\end{itemize}
\end{cprop}

\section{limite d'une suite}

\subsection{Limite finie}

\begin{cdefi}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item  On dit que la suite $\Suite{u}$ a pour {\bf limite} 0, et on note $\lim\limits_{n \to +\infty} u_n =0$, si $u_n$ peut être rendu arbitrairement proche de 0 pourvu que $n$ soit suffisamment grand.
	\item On dit que $\Suite{u}$ a pour \textbf{limite} $l$, et on note $\lim\limits_{n \to +\infty} u_n =l$, si la suite $\Suite{v}$, de terme général $v_n=u_n-l$, a pour limite 0.
\end{itemize}
\end{cdefi}

\subsection{Limite infinie}

\begin{cdefi}
On dit que la suite $\Suite{u}$ a pour limite $+\infty$, et on note $\lim\limits_{n \to +\infty} u_n=+\infty$, si $u_n$ peut être rendu arbitrairement grand pourvu que $n$ soit suffisamment grand.
\end{cdefi}

\subsection{Limite d'une suite géométrique}

\begin{cprop}[Pluriel]
Soit $q \in \R$ et soit $\Suite{u}$ la suite définie par $u_n=q^n$. Alors :
\begin{itemize}
	\item Si $q \leq -1$, $(u_n)$ n'a pas de limite ;
	\item Si $-1<q<1$, $\lim\limits_{n \to +\infty} u_n=0$ ;
	\item Si $q=1$, $\lim\limits_{n \to +\infty} u_n=u_0$ \qquad (suite constante).
	\item Si $q>1$, $\lim\limits_{n \to +\infty} u_n=+\infty$.
\end{itemize}
\end{cprop}

\begin{crmq}
On déduit de la propriété précédente la limite d'une suite géométrique en fonction de la raison et du signe du terme initial de celle-ci.
\end{crmq}

\end{document}