% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[UF2]},typedoc={CHAPITRE~},numdoc={3},titre={Fiabilité}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\usetikzlibrary{decorations.markings}

\begin{document}

\pagestyle{fancy}

\part{CH03 - Fiabilité, loi exponentielle}

\section{Fonction de défaillance, fonction de fiabilité}

\subsection{Introduction}

\begin{ccadre}[Compteur=false]
Lors de l’acquisition d’un appareil électronique ou électroménager par exemple, le choix est effectué selon certains critères dont l’un peut-être la \textit{\textbf{fiabilité}} : un modèle est plus \textit{\textbf{fiable}} qu’un autre signifie que, « \textit{en général} », un appareil de ce modèle fonctionne correctement plus longtemps qu’un appareil de l’autre modèle. Il s’agit d’une \textit{tendance}, non d’une \textit{certitude}. Pour définir la fiabilité, on est amené à parler de \textit{probabilité}.
\end{ccadre}

\begin{cvoc}[Compteur=false]
\begin{wrapstuff}[r,top=0,abovesep=-0.25\baselineskip]\includegraphics[height=1cm]{afnor}\end{wrapstuff}
Pour l’\textbf{AFNOR} (Association Française pour la NORmalisation) : « La fiabilité est la caractéristique d’un dispositif qui s’exprime par la probabilité pour ce dispositif d’accomplir une fonction requise, dans des conditions données, pendant  une période donnée. »
\end{cvoc}

\begin{cidee}
La durée de vie d'un matériel donné sera modélisée par une variable aléatoire continue T prenant (théoriquement) ses valeurs dans l'intervalle $\IntervalleFO{0}{+\infty}$. Pour une valeur $t$ dans $\IntervalleFO{0}{+\infty}$, on utilise les notations suivantes :
\begin{itemize}
	\item $F(t) = p(T \pp t)$ ;
	\item $R(t) = 1 - F(t) = p(T > t)$.
\end{itemize}

\smallskip

La probabilité $F(t) = p(T \pp t)$ s'interprète comme la probabilité que le matériel connaisse une défaillance avant l'instant $t$ : pour cette raison F est appelée la \textbf{fonction de défaillance} du matériel (en anglais, défaillance se dit \textit{failure}, d'où la lettre F).

\smallskip

Inversement, la probabilité $R(t) = 1 - F(t) = p(T > t)$ s'interprète comme la probabilité que le matériel ait fonctionné correctement jusqu'à l'instant $t$ : R est donc appelée \textbf{fonction de fiabilité} du matériel (en anglais, fiabilité se dit \textit{reliability}, d'où la lettre R).
\end{cidee}

\begin{crmq}
Pour pouvoir calculer les valeurs de la fonction R de fiabilité d'un matériel donné, il faut connaître la \textit{loi} de T.

Conformément au programme, nous n'étudierons qu'un cas particulier (mais qui est le plus important et le plus fréquent en pratique), qui est présenté dans l’exemple suivant.
\end{crmq}

\subsection{Un exemple}

\begin{cexemple}
Sur un grand nombre d'ampoules électriques identiques mises en service au même moment, on a relevé, toutes les 500 heures, la proportion d'ampoules encore en fonctionnement après une durée de $t$ heures :

\begin{center}
	\begin{tblr}{width=11cm,colspec={Q[l,m]*{7}{X[m,c]}},hlines,vlines,stretch=1.125,cells={font=\sffamily}}
		$\mathsf{t}$ (en heures) & 0 & 500 & 1000 & 1500 & 2000 & 2500 & 3000 \\
		Proportion $\mathsf{R(t)}$ & 1 & 0,78 & 0,60 & 0,47 & 0,37 & 0,28 & 0,22 \\
	\end{tblr}
\end{center}
\end{cexemple}

\begin{cillustr}
\begin{center}
	%\tunits{0.002}{5}
	%\tdefgrille{0}{3500}{500}{250}{0}{1}{0.1}{0.1}
	\begin{tikzpicture}[x=0.002cm,y=5cm,xmin=0,xmax=3500,xgrille=500,xgrilles=250,ymin=0,ymax=1,ygrille=0.1,ygrilles=0.1]
		%\tgrilles[line width=0.5pt,densely dashed,cyan] ;
		\GrilleTikz
		\draw[->,line width=1.25pt] (\xmin,0) -- (\xmax,0);
		\draw[->,line width=1.25pt] (0,\ymin) -- (0,\ymax);
		\draw[line width=0.5pt,densely dashed,cyan] (\xmax,\ymin) -- (\xmax,\ymax);
		\foreach \x in {0,500,...,3000}
			\draw[line width=1.25pt] (\x,4pt) -- (\x,-4pt) node[below] {\num{\x}};
		\foreach \y in {0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9}
			\draw[line width=1.25pt] (4pt,\y) -- (-4pt,\y) node[left] {\num{\y}};
		\draw[line width=1pt,red,domain=0:3500,samples=200] plot(\x,{exp(-0.0005*\x)});
		\foreach \Point in {(0,1),(500,0.78),(1000,0.6),(1500,0.47),(2000,0.37),(2500,0.28),(3000,0.22)}
			\draw [ultra thick,blue] plot[mark=+,mark size=4pt] coordinates {\Point};
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cidee}
Un modèle linéaire ne paraissant pas adapté pour le nuage de points  précédent, on recherche un modèle \textbf{exponentiel} du type $R(t) = k \e^{-\lambda t}$ en procédant à un changement de variable $\ln(R(t))$.

\smallskip

La méthode des moindres carrés donne $R(t)=\e^{\num{-0,0005}t}$ qui est très fiable (excellent coeff. de corrélation !)
%
\begin{center}
	\includegraphics[height=2cm]{chap03_expo_reg_a}~~\includegraphics[height=2cm]{chap03_expo_reg_b}
\end{center}
\end{cidee}

\section{Définition, propriétés}

\subsection{Définition}

\begin{cdefi}
Lorsque la fonction de fiabilité R s'écrit $R(t) = \e^{-\lambda t}$ où $\lambda$ est un réel strictement positif (comme dans l'exemple précédent où on avait $\lambda=\num{0,0005}$), on dit que la variable aléatoire T (la durée de vie du matériel) suit une \textbf{loi exponentielle} de paramètre $\lambda$. Conformément au programme, c'est la \textit{seule situation} rencontrée.

\smallskip

$T \sim \mathcallig{E}_{\lambda}$ signifie que la fonction de fiabilité est la fonction R définie sur $\IntervalleFO{0}{+\infty}$ par : $R(t) = \e^{-\lambda t}.$
\end{cdefi}

\subsection{Calculs de probabilités}

\begin{cexemple}
On reprend l'exemple précédent, on prélève au hasard une ampoule de ce lot et on admet que sa durée de vie définit une variable aléatoire T qui suit une loi exponentielle de paramètre $\lambda=0,0005$.

\begin{itemize}[leftmargin=*]
	\item La probabilité de l'événement A : « l'ampoule fonctionne correctement au bout de 1 000 heures »  est : $$p(A) = R(1000) = \e^{\num{-0,000 5} \times 1000}=\e^{-0,5} \approx \num{0,6065}$$
	\item La probabilité de l'événement B : « l'ampoule fonctionne correctement au bout de 1 500 heures » est : $$p(B) = R(1500) = \e^{\num{-0,000 5} \times 1500}=\e^{-0,75} \approx \num{0,4724}$$
	\item La probabilité de l'événement C : « l'ampoule fonctionne correctement au bout de 500 heures » est : $$p(C) = R(500) = \e^{\num{-0,000 5} \times 500}=\e^{-0,25} \approx \num{0,7788}$$
\end{itemize}

Et $p_A (B) =  \dfrac{p(A \cap B)}{p(A)}=\dfrac{p(B)}{p(A)}=\dfrac{\e^{-0,75}}{\e^{-0,5}} = \e^{-0,25} \text{ qui est exactement } p(C)$.
\end{cexemple}

\begin{crmq}
Autrement dit, la probabilité que l'ampoule fonctionne correctement au bout de 1500 heures sachant qu'elle a déjà fonctionné correctement pendant les 1 000  « premières » heures, est exactement la même que la probabilité que l'ampoule fonctionne correctement au bout de 500 heures (sous-entendu : comme si, au départ, cette ampoule  « n'avait encore jamais fonctionné »). Bref, que l'ampoule ait déjà fonctionné ou non, la probabilité qu'elle fonctionne encore 500 heures de plus est toujours la même !
\end{crmq}

\begin{cprop}
La loi exponentielle est donc une loi \textbf{sans mémoire}, au sens où si T est une variable aléatoire qui suit une loi exponentielle de paramètre $\lambda$, et si $t_1 \pp t_2$ sont deux réels positifs, on constate que : $$p(T>t_1  +t_2 )= R(t_1  +t_2 )=R(t_1)R(t_2)=p(T>t_1) \times p(T >t_2 )$$
Donc : $$p_{(T > t_1)}(T > t_1+t_2)=R(t_2)=p(T >t_2 )$$
\end{cprop}

\begin{cidee}
Ainsi, la probabilité que le dispositif fonctionne encore correctement à la date $t_1  + t_2$  sachant qu'il a déjà fonctionné correctement  jusqu'à la date $t_1$, est exactement égale à la probabilité que le dispositif fonctionne encore correctement à la date $t_2$ (donc, a priori, comme si le dispositif avait fonctionné jusqu'à la date $t_2$ en étant « neuf » au départ).
\end{cidee}

\begin{crmq}
On retiendra donc que la modélisation des questions de fiabilité par une loi exponentielle  est plutôt adaptée aux situations où les dispositifs étudiés ne subissent pas de défauts  « d'usure », donc plus adaptée aux  dispositifs électriques ou électroniques qu'aux dispositifs purement « mécaniques ».
\end{crmq}

\subsection{Moyenne des temps de bon fonctionnement}

\begin{cprop}
Si T suit une loi exponentielle de paramètre $\lambda$, on démontre que son espérance $\Esper{T}$ et son écart-type $\sigma(T)$ vérifient : \[\Esper{T}=\sigma(T)=\dfrac{1}{\lambda}.\]
%
Or, $\Esper{T}$ s'interprète en termes statistiques comme une moyenne, cette valeur correspond donc ici à la durée moyenne de vie du matériel étudié. En fiabilité, cette durée de vie moyenne est appelée \textbf{MTBF} (Moyenne des Temps de Bon Fonctionnement, et, en anglais, Mean Time Before Failure). En résumé : \[MTBF=\dfrac{1}{\lambda}.\]%
\end{cprop}

\subsection{Travail sur le paramètre}

\begin{cmethode}[Pluriel]
Si le paramètre $\lambda$ n'est pas connu, il peut l'être grâce à :
%
\begin{itemize}
	\item la donnée de la MTBF ;
	\item la donnée d'une probabilité particulière, et de ce fait par une résolution d'équation.
\end{itemize}
\end{cmethode}

\end{document}