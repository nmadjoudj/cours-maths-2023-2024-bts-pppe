% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{worldflags}
\usepackage{customenvs}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[UF2]},typedoc={CHAPITRE~},numdoc={6},titre={Fonctions}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\usetikzlibrary{hobby}

\begin{document}

\tikzset{tangent/.style={% https://tex.stackexchange.com/a/25940
	decoration={%
		markings,mark=at position #1 with{
			\coordinate (tangent point-\pgfkeysvalueof{/pgf/decoration/mark info/sequence number}) at (0pt,0pt);
			\coordinate (tangent unit vector-\pgfkeysvalueof{/pgf/decoration/mark info/sequence number}) at (1,0pt);
			\coordinate (tangent orthogonal unit vector-\pgfkeysvalueof{/pgf/decoration/mark info/sequence number}) at (0pt,1);}
	},
	postaction=decorate},
	use tangent/.style={shift=(tangent point-#1),%
	x=(tangent unit vector-#1),
	y=(tangent orthogonal unit vector-#1)
	},
	use tangent/.default=1}

\pagestyle{fancy}

\part{CH06 - Compléments sur les fonctions}

\section{Généralités sur les fonctions}

\subsection{Courbe, tableau de valeurs}

\begin{cmethode}
Une fonction peut se donner :
\begin{itemize}
	\item par sa formule ($f(x)=\dots$) ;
	\item par sa courbe dans un repère ;
	\item par un tableau de valeurs.
\end{itemize}
\end{cmethode}

\begin{cexemple}
Soit $f(x)=x^2$. La courbe $\mathscr{C}_f$ est l'ensemble des points de coordonnées $\left( x;f(x) \right)$.
\begin{center}
	\begin{tblr}{width=9cm,hlines,vlines,colspec={*{8}{X[m,c]}}}
		$x$ & $-3$ & $-2$ & $-1$ & $0$ & $1$ & $2$ & $3$ \\
		$f(x)$ & 9 & 4 & 1 & 0 & 1 & 4 & 9 \\
	\end{tblr}

	\vspace{0.5cm}
	
%	\tunits{1}{0.5}
%	\tdefgrille{-3}{3}{1}{0.5}{0}{10}{1}{0.5}
	\begin{tikzpicture}[x=0.8cm,y=0.4cm,xmin=-3,xmax=3,ymin=0,ymax=10]
		\GrilleTikz[Affp=false]\AxesTikz[ElargirOx=0,ElargirOy=0]
		\AxexTikz{-3,-2,...,2}\AxeyTikz{2,4,...,8}
		\draw[line width=1.5pt,color=red,samples=250,domain=-3:3] plot (\x,{\x*\x}) ;
		\foreach \Point in {(-3,9),(-2,4),(-1,1),(0,0),(1,1),(2,4),(3,9)}
			\filldraw[blue] \Point circle(2.5pt) ;
		\draw (2.5,8.5) node[text=red,font=\Large] {$\mathscr{C}_f$} ;
	\end{tikzpicture}
\end{center}
\end{cexemple}

\subsection{Image, antécédent, équation}

\begin{cmethode}[Pluriel]
Déterminer l'\textbf{image} d'un réel $x$ par $f$ revient à déterminer la valeur de $f(x)$ :
\begin{itemize}[leftmargin=15pt]
	\item soit par \textbf{calculs} : on remplace $x$ ;
	\item soit dans le tableau de valeurs : on cherche $x$ dans la première ligne ;
	\item soit on utilise la courbe : on part des abscisses et on lit l'ordonnée du point de la courbe.
\end{itemize} 
Déterminer les (éventuels) antécédents de $y$ par $f$ revient à chercher \textbf{tous} les $x$ tels que que $y=f(x)$ :
\begin{itemize}[leftmargin=15pt]
	\item par \textbf{calculs} ; en résolvant l'équation $f(x)=y$ ;
	\item via le tableau de valeurs : on cherche $y$ dans la deuxième ligne ;
	\item avec la courbe : on regarde les points d'intersection de la courbe avec la droite horizontale de hauteur $y$.
\end{itemize}
\end{cmethode}

\begin{crmq}[Pluriel]
Les calculs \textbf{algébriques} donnent toujours les valeurs exactes, mais la résolution d'\textbf{équations} (pour les antécédents) n'est pas toujours simple !

\smallskip

Le travail sur la courbe est le plus simple, il s'agit de \textbf{lectures graphiques}, et il faut (autant que faire se peut) travailler avec la courbe pour vérifier ses résultats !
\end{crmq}

\begin{cillustr}
\begin{center}
%	\tunits{0.8}{0.8}
%	\tdefgrille{-5}{5}{1}{0.5}{-3}{4}{1}{0.5}
	\begin{tikzpicture}[x=0.8cm,y=0.8cm,xmin=-5,xmax=5,ymin=-3,ymax=4,>=latex]
		\GrilleTikz\AxesTikz[ElargirOx=0,ElargirOy=0]
		\AxexTikz[Police=\scriptsize]{-5,-4,...,4}\AxeyTikz[Police=\scriptsize]{-3,-2,...,3}
		\draw[line width=1.5pt,red] (-5,1) to[curve through={(-4.18,3) .. (-3.5,3.5) .. (-2.25,3) .. (-0.6,0) .. (0,-1.22) .. (1.5,-2) .. (4,0)}] (5,1.75);
		\draw (5,1.75) node[above left,text=red,font=\large] {$\mathscr{C}_f$} ;
		\draw[line width=1.5pt,blue] (\xmin,3) -- (\xmax,3) node[above left,text=blue,font=\large] {$y=3$} ;
		%constructions
		\def\ya{-1.33}
		\def\xb{-2.25}
		\def\xc{-4.18}
		\draw[line width=1pt,purple,densely dashed,->,] (3,0) -- (3,\ya) -- (0,\ya) ;
		\draw[line width=1pt,CouleurVertForet,densely dashed,->] (\xb,3) -- (\xb,0) ;
		\draw[line width=1pt,CouleurVertForet,densely dashed,->] (\xc,3) -- (\xc,0) ;
		\foreach \Point in {(3,\ya),(\xb,3),(\xc,3)}
			\filldraw[darkgray] \Point circle(2.5pt) ;
	\end{tikzpicture}
\end{center}
On peut donc \og lire \fg{} l'\textcolor{purple}{image} de 3 par $f$ : $f(3) \approx -1,35$.

On peut donc \og lire \fg{} les \textcolor{CouleurVertForet}{antécédents} de 3 par $f$ : environ $-2,25$ et $-4,2$.
\end{cillustr}

\subsection{Tableau de signes, tableau de variations}

\begin{cmethode}[Pluriel]
Le \textbf{tableau de signes} (tds) d'une fonction permet de consigner, dans un tableau (sic), les intervalles (endroits) sur lesquels la fonction est positive ($+$) ou négative ($-$).

Graphiquement, une fonction est positive si sa courbe est au-dessus de l'axe des abscisses, négative sinon.

\smallskip

Le \textbf{tableau de(s) variations} (tdv) d'une fonction permet de consigner, dans un tableau (sic), les intervalles (endroits) sur lesquels la fonction est croissante ($\nearrow$) ou décroissante ($\searrow$).

Graphiquement, une fonction est croissante si sa courbe \og monte \fg, décroissante sinon.
\end{cmethode}

\begin{cillustr}[ComplementTitre={ - Remarque}]
En travaillant sur la courbe donnée précédemment :
\begin{center}
	\begin{tikzpicture}
		\tkzTabInit{$x$/0.7,$f(x)$/0.7}{$-5$, ${-0,6}$, $4$ , $5$}
		\tkzTabLine{,+,z,-,z,+,}
	\end{tikzpicture}
	
	\bigskip
	
	\begin{tikzpicture}
		\tkzTabInit{$x$/0.7,$f$/1.4}{$-5$, ${-3,25}$, ${1,5}$ , $5$}
		\tkzTabVar{-/${-1}$,+/${3,5}$,-/$-2$,+/${1,75}$}
	\end{tikzpicture}
\end{center}
\smallskip
Il ne faut pas \og mélanger \fg{} le signe et les variations d'une fonction !

\hspace{5mm}$\rhd~~$une fonction peut être \textcolor{red}{décroissante et positive} ; 

\hspace{5mm}$\rhd~~$une fonction peut être \textcolor{blue}{croissante et négative} ;

\hspace{5mm}$\rhd~~$une fonction peut être \textcolor{purple}{positive et changer \og très souvent de variations \fg{}} ;

\hspace{5mm}$\rhd~~$toutes les \textcolor{CouleurVertForet}{combinaisons sont possibles} !

\begin{center}
	\begin{tikzpicture}[x=0.8cm,y=0.8cm,xmin=0,xmax=3,xgrille=0.5,xgrilles=0.5,ymin=-0.5,ymax=2.5,ygrille=0.5,ygrilles=0.5]%1
		\AxesTikz[ElargirOx=0,ElargirOy=0]
		\foreach \x in {1,2} \draw[line width=0.75pt] (\x,-4pt) -- (\x,4pt) ;
		\foreach \y in {0,1,2} \draw[line width=0.75pt] (-4pt,\y) -- (4pt,\y) ;
		\draw[line width=1.25pt,color=red,samples=250,domain=0.5:3] plot (\x,{1/\x+0.25}) ;
	\end{tikzpicture}~~~~
	\begin{tikzpicture}[x=0.8cm,y=0.8cm,xmin=0,xmax=3,xgrille=0.5,xgrilles=0.5,ymin=-2.5,ymax=0.5,ygrille=0.5,ygrilles=0.5]%2
		\AxesTikz[ElargirOx=0,ElargirOy=0]
		\foreach \x in {1,2} \draw[line width=0.75pt] (\x,-4pt) -- (\x,4pt) ;
		\foreach \y in {-2,-1,0} \draw[line width=0.75pt] (-4pt,\y) -- (4pt,\y) ;
		\draw[line width=1.25pt,color=blue,samples=250,domain=0.5:3] plot (\x,{-1/\x-0.15}) ;
	\end{tikzpicture}~~~~
	\begin{tikzpicture}[x=0.8cm,y=0.8cm,xmin=0,xmax=3,xgrille=0.5,xgrilles=0.5,ymin=-0.5,ymax=2.5,ygrille=0.5,ygrilles=0.5]%2
		\AxesTikz[ElargirOx=0,ElargirOy=0]
		\foreach \x in {1,2} \draw[line width=0.75pt] (\x,-4pt) -- (\x,4pt) ;
		\foreach \y in {0,1,2} \draw[line width=0.75pt] (-4pt,\y) -- (4pt,\y) ;
		\draw[line width=1.25pt,color=purple,samples=250,domain=0:3] plot (\x,{sin(deg(10*\x))+1.35}) ;
	\end{tikzpicture}~~~~
	\begin{tikzpicture}[x=0.8cm,y=0.8cm,xmin=0,xmax=3,xgrille=0.5,xgrilles=0.5,ymin=-2.5,ymax=0.5,ygrille=0.5,ygrilles=0.5]%4
		\AxesTikz[ElargirOx=0,ElargirOy=0]
		\foreach \x in {1,2} \draw[line width=0.75pt] (\x,-4pt) -- (\x,4pt) ;
		\foreach \y in {-2,-1,0} \draw[line width=0.75pt] (-4pt,\y) -- (4pt,\y) ;
		\draw[line width=1.25pt,color=CouleurVertForet,samples=250,domain=0:3] plot (\x,{sin(deg(5*\x))-1.25}) ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cmethode}[Pluriel]
Un tableau de variations (ou une courbe) permet de :
\begin{itemize}
	\item déterminer un maximum, un minimum  : valeurs Max et min de la \textsf{L2} du tdv ;
	\item déterminer le nombre de solutions d'une équation du type $f(x)=k$ : on \og place \fg{} $k$ sur les flèches ;
	\item déterminer le tds (sans passer par la courbe) : on place les \og 0 \fg{} et on \og suit les flèches \fg{}.
\end{itemize}
\end{cmethode}

\begin{cillustr}
On donne le tdv d'une fonction $f$ :
\begin{center}
	\begin{tikzpicture}
		\tikzset{arrow style/.style = {->,>=latex}}
		\tkzTabSetup[fromcolor=red,fromstyle=dashed]
		\tkzTabInit{$x$/0.7,$f$/1.6}{$-5$, ${-2}$, $3$ , $5$, $10$}
		\tkzTabVar{+/$2$,-/$-5$,+/$4$,-/$-3$,+/$-1$}
		\tkzTabVal[]{1}{2}{0.3}{}{\footnotesize $\mathcolor{blue}{1,75}$}
		\tkzTabVal[]{2}{3}{0.7}{}{\footnotesize $\mathcolor{blue}{1,75}$}
		\tkzTabVal[]{3}{4}{0.4}{}{\footnotesize $\mathcolor{blue}{1,75}$}
		\tkzTabVal[draw]{1}{2}{0.6}{$\mathcolor{red}{a}$}{\footnotesize $\mathcolor{red}{0}$}
		\tkzTabVal[draw]{2}{3}{0.4}{$\mathcolor{red}{b}$}{\footnotesize $\mathcolor{red}{0}$}
		\tkzTabVal[draw]{3}{4}{0.7}{$\mathcolor{red}{c}$}{\footnotesize $\mathcolor{red}{0}$}
	\end{tikzpicture}
\end{center}

\hspace{5mm}$\rhd~~$le maximum de $f$ est 4, atteint en $x=3$ ;

\hspace{5mm}$\rhd~~$le minimum de $f$ est $-5$, atteint en $x=-2$ ;

\hspace{5mm}$\rhd~~$l'équation $\mathcolor{blue}{f(x)=1,75}$ admet \textcolor{blue}{3} solutions ;

\hspace{5mm}$\rhd~~$l'équation $\mathcolor{red}{f(x)=0}$ admet \textcolor{red}{3} solutions (notées $a$, $b$ et $c$).

\smallskip

On peut donc en déduire le tds de $f(x)$ :
\begin{center}
	\begin{tikzpicture}
		\tkzTabInit{$x$/0.7,$f(x)$/0.7}{$-5$, $\mathcolor{red}{a}$, $\mathcolor{red}{b}$ , $\mathcolor{red}{c}$, $10$}
		\tkzTabLine{,+,z,-,z,+,z,-,}
	\end{tikzpicture}
\end{center}
\end{cillustr}

\section{Rappels et compléments sur les équations et les tds}

\subsection{Équations classiques}

\begin{cexemple}[Pluriel]
Pour les équations du 1\up{er} degré, on \og isole \fg{} le $x$ :
\begin{itemize}
	\item $3x+5=10 \ssi 3x=5 \ssi x=\nicefrac{5}{3}$.
	\item $2x+9=-2x+4 \ssi 2x+2x=4-9 \ssi 4x=-5 \ssi x=\nicefrac{-5}{4}$.
\end{itemize}
\end{cexemple}

\begin{cexemple}[Pluriel]
Pour les équations du 2\up{d} degré, on utilise $\Delta$ (ou la calculatrice et le module \ccalg{équation}) :
\begin{itemize}
	\item $x^2-6x+5=0$ : $\Delta=16$ et les deux racines sont $x_1=5$ et $x_2=1$.
	\item $3x^2+7x-10=0$ : $\Delta=169$ et les deux racines sont $x_1=1$ et $x_2=-\nicefrac{10}{3}$.
\end{itemize}
\end{cexemple}

\begin{cexemple}[Pluriel]
On peut rappeler la méthode liée aux équations-produit (produit nul) :
\begin{itemize}
	\item $(x-2)(4x+6)=0 \ssi x=2$ ou $x=-\nicefrac{6}{4}=-\nicefrac{3}{2}$.
	\item $(x+2)(x^2+6x+9)=0 \ssi x=-2$ ou $x=-3$ (grâce à $\Delta$).
\end{itemize}
On peut également rappeler la méthode liée aux équations/quotient (produit en croix) :
\begin{itemize}
	\item $\tfrac{3}{2x+5}=7 \Rightarrow 3\times1=(2x+5)\times7 \Rightarrow 3=14x+35 \Rightarrow 14x=-32 \Rightarrow x=\nicefrac{-32}{14}=\nicefrac{-16}{7}$.
	\item $\tfrac{2x}{x+1}=\tfrac{x+1}{x+3} \Rightarrow (x+1)(x+1)=2x(x+3) \Rightarrow x^2+2x+1=2x^2+6x \Rightarrow -x^2-4x+1=0$ et $\Delta$ donne $x=-2\pm\sqrt{5}$.
\end{itemize}
\end{cexemple}

\begin{cexemple}[Pluriel]
On peut commencer par rappeler le fait qu'une \textbf{\textcolor{red}{exponentielle est toujours strictement positive}} !

Pour les équations faisant intervenir \og $\e^{\ldots}$ \fg{} ou \og $\ln(\ldots)$ \fg, on isole et on utilise la fonction \og réciproque \fg{} :
\begin{itemize}
	\item $\e^x=5 \ssi x=\ln(5)$.
	\item $4\e^{2x}-5=0 \ssi 4\e^{2x}=5 \ssi \e^{2x}=1,25 \ssi 2x=\ln(1,25) \ssi x=\tfrac{\ln(1,25)}{2}$.
	\item $\ln(x)=-4 \ssi x=\e^{-4}$.
	\item $10\ln(x)+1=0 \ssi 10\ln(x)=-1 \ssi \ln(x)=-0,1 \ssi x=\e^{-0,1}$.
\end{itemize}
\end{cexemple}

\subsection{Étude de signes}

\begin{crappel}[Pluriel]
Pour étudier le signe d'une fonction, le plus simple est de travailler sur un tds :
\begin{itemize}[label=$\rightsquigarrow$]
	\item il ne faut avoir que des produits et/ou des quotients ;\hfill{}\textcolor{CouleurVertForet}{\scriptsize Si besoin on met au même dénominateur, on factorise, etc}
	\item on \og remplit \fg{} une ligne par facteur ;
	\item la dernière ligne repose sur la \textbf{règle des signes}.
\end{itemize}

\smallskip

Les expressions classiques à savoir étudier :
\begin{itemize}[label=$\rightsquigarrow$]
	\item un \textbf{carré} est toujours positif (il peut quand même s'annuler\dots) ;
	\item une \textbf{exponentielle} est toujours strictement positive ;
	\item les fonctions affines $mx+p$, pour lesquelles on \og utilise \fg{} le signe de $m$ après le zéro ;
	\item les trinômes $ax^2+bx+c$ pour lesquelles le signe de $a$ est à l'extérieur des éventuelles racines ;
	\item les expressions du type $\ln(x)+a$ ou $\e^x - a$.
\end{itemize}
\end{crappel}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*,label=$\rightsquigarrow$]
	\item $3x+18$ :
	\begin{Centrage}
		\begin{tikzpicture}
			\tkzTabInit{$x$/0.6,expr/0.6}{$-\infty$,$-6$,$+\infty$}
			\tkzTabLine{,-,z,+,}
		\end{tikzpicture}
	\end{Centrage}
	\item $-2x^2+10x+12$:
	\begin{Centrage}
		\begin{tikzpicture}
			\tkzTabInit{$x$/0.6,expr/0.6}{$-\infty$,$-1$,$6$,$+\infty$}
			\tkzTabLine{,-,z,+,z,-,}
		\end{tikzpicture}
	\end{Centrage}
	\item $(5x-2)\e^{2x+1}$ :
	\begin{Centrage}
		\begin{tikzpicture}
			\tkzTabInit{$x$/0.6,$5x-2$/0.6,$\e^{2x+1}$/0.6,expr/0.6}{$-\infty$,${0,4}$,$+\infty$}
			\tkzTabLine{,-,z,+,}
			\tkzTabLine{,+,t,+,}
			\tkzTabLine{,-,z,+,}
		\end{tikzpicture}
	\end{Centrage}
	\item $\dfrac{x^2-5x+6}{x+3}$ :
	\begin{Centrage}
%		\def\tkzTabDefaultBackgroundColor{SeaGreen!10}
		\begin{tikzpicture}[double distance=3pt]
			\tkzTabInit[]{$x$/0.6,$x^2-5x+6$/0.6,$x+3$/0.6,expr/0.6}{$-\infty$,${-3}$,$2$,$3$,$+\infty$}
			\draw (N21) node[text=red] {\tiny\textsf{D}};
			\draw (N31) node[text=red] {\tiny\textsf{N}};
			\draw (N41) node[text=red] {\tiny\textsf{N}};
			\tkzTabLine{,+,t,+,z,-,z,+,}
			\tkzTabLine{,-,z,+,t,+,t,+,}
			\tkzTabLine{,-,d,+,z,-,z,+,}
		\end{tikzpicture}
	\end{Centrage}
\end{itemize}
\end{cexemple}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*,label=$\rightsquigarrow$]
	\item $\ln(x+1)-1$ sur $\intervFO{0}{+\infty}$ :
	
	\hspace{5mm}$\rhd$ on résout $\ln(x+1)-1 = 0 \ssi \ln(x+1) = 1 \ssi x+1 = \e^{1}=\e \ssi x = \e - 1 \approx 1,72$ ;
	
	\hspace{5mm}$\rhd$ on utilise la calculatrice en remplaçant $x$ par une valeur avant $\e - 1$ et une valeur après $\e - 1$ ;
	
	\hspace{5mm}$\rhd$ avec $x=1$ on trouve $\ln(1+1) - 1 \approx -0,3 < 0$ et avec $x=3$ on trouve $\ln(3+1) - 1 \approx 0,4 > 0$
	
	\begin{center}
		\begin{tikzpicture}
		\tkzTabInit[]{$x$/0.6,expr/0.6}{$0$,${\e-1}$,$+\infty$}
		\tkzTabLine{,-,z,+,}
		\end{tikzpicture}
	\end{center}
	\item $-4x+20-\dfrac{16}{x}$ sur $\intervFF{0,5}{6,5}$ :
	\begin{itemize}
		\item en mettant au même dénominateur, on obtient $\dfrac{-4x^2}{x}+\dfrac{20x}{x}-\dfrac{16}{x}=\dfrac{-4x^2+20x-16}{x}$ ;
		\item le dénominateur ($x$) est strictement positif sur $\intervFF{0,5}{6,5}$ ;
		\item pour le numérateur, $\Delta=144$ et les deux racines sont 1 et 4 ;
	\end{itemize}
	Ainsi on obtient :
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit[deltacl=0.7]{$x$/0.6,expr/0.6}{${0,5}$,$1$,$4$,${6,5}$}
			\tkzTabLine{,-,z,+,z,-,}
		\end{tikzpicture}
	\end{center}
\end{itemize}
\end{cexemple}

\subsection{Rappels sur l'exponentielle et le logarithme}

\begin{cprop}[Pluriel]
Les propriétés de l'exponentielle sont les mêmes que celles des puissances :
\begin{itemize}
	\item $\e^0=1$.
	\item $\e^a \times \e^b = \e^{a+b}$.
	\item $\dfrac{\e^a}{\e^b}=\e^{a-b}$.
	\item $\left(\e^a\right)^n=\e^{na}$.
\end{itemize}
Pour celles du logarithme, elles \og s'apprennent \fg{} :
\begin{itemize}
	\item $\ln(1)=0$.
	\item $\ln(a)+\ln(b)=\ln(ab)$.
	\item $\ln(a)-\ln(b)=\ln\left(\dfrac{a}{b}\right)$.
	\item $\ln\left(a^n\right)=n \times \ln(a)$.
\end{itemize}
\end{cprop}

\newpage

\section{Variations d'une fonction}

\subsection{Introduction}

\begin{cintro}
Étudier, sans la courbe, les variations d'une fonction est fastidieux, mais grâce à un nouvel outil, la \textbf{dérivation}, on peut le faire \og assez facilement \fg{}.
\end{cintro}

\begin{cidee}
L'idée est de \og remplacer \fg{} (localement) la courbe par sa tangente.
\begin{center}
	\begin{tikzpicture}[x=1cm,y=0.5cm]
		%styles
		\tkzSetUpPoint[shape=circle,size=4pt,color=violet,fill=violet]
		\tikzset{tan style/.style={<->,>=latex}}
		\tkzInit[xmin=-2,xmax=8,xstep=1,ymin=-4,ymax=6.1,ystep=1]
		%courbe
		\tkzFct[very thick,red,domain=-2:8,samples=250]{2*cos(2*x)+cos(x)+3*cos(3*x)};
		%tangentes croissantes
		\foreach \x/\lg in {-1/0.25,2/0.3,4/0.75,6/0.15}{\tkzDrawTangentLine[very thick,color=blue,kr=\lg,kl=\lg](\x)}
		%tangentes décroissantes
		\foreach \x/\lg in {1/0.25,3/0.3,5/0.3}{\tkzDrawTangentLine[very thick,color=CouleurVertForet,kr=\lg,kl=\lg](\x)}
		%tangente hor
		\tkzDrawTangentLine[very thick,color=darkgray,kr=0.75,kl=0.75](0)
		%points
		\foreach \va in {-1,0,1,2,3,4,5,6}{\tkzDefPointByFct[draw](\va)}
	\end{tikzpicture}
\end{center}
On peut donc constater que :
\begin{itemize}
	\item les \og \textcolor{blue}{tangentes bleues montent} \fg{} et que la courbe \og suit la même direction \fg{} ;
	\item les \og \textcolor{CouleurVertForet}{tangentes vertes descendent} \fg{} et que la courbe \og suit la même direction \fg{} ;
	\item la \og \textcolor{darkgray}{tangente grise est horizontale} \fg{} et que la courbe \og change de direction \fg{}.
\end{itemize}
La tangente et la courbe vont donc dans la \og même direction \fg{} !
\end{cidee}

\begin{crappel}[Pluriel,ComplementTitre={ - Idée}]
Une tangente est une \textbf{droite} est savoir si une droite \og monte ou descend \fg{} est simple : il suffit de connaître sa \textbf{pente} (ou son \textbf{coefficient directeur}).

On rappelle que l'équation (réduite) d'une droite est de la forme $y=\mathcolor{red}{m}x+\mathcolor{blue}{p}$ avec $\begin{dcases} \mathcolor{red}{m} \text{ la pente} \\ \mathcolor{blue}{p} \text{ l'ordonnée à l'origine} \end{dcases}$.
\begin{itemize}[leftmargin=*]
	\item si $m > 0$, la droite est croissante ;
	\item si $m < 0$, la droite est décroissante ;
	\item si $m = 0$, la droite est horizontale.
\end{itemize}

\smallskip

Il ne reste donc \og plus qu'à \fg{} trouver un moyen de déterminer la pente des tangentes !
\end{crappel}

\subsection{Nombre dérivé}

\begin{cdefi}[ComplementTitre={ - Propriété}]
Soit $f$ une fonction définie sur un intervalle I, et soit $a \in I$.

On appelle \textbf{nombre dérivé} de $f$ en $a$, la pente (si elle existe !) de la tangente à la courbe $\mathscr{C}_f$ en $a$.

Dans le cas où la tangente existe, on dit que $f$ est dérivable en $a$ et on note $f'(a)$ ce nombre dérivé.
\end{cdefi}

\begin{cprop}
Si $f$ est dérivable en $a$, une équation de la tangente $\Gamma_a$ à la courbe $\mathscr{C}_f$ au point d'abscisse $a$ est :

\hfill~$\Gamma_a$ : $y=f'(a) \times (x-a)+f(a)$.\hfill~
\end{cprop}

\begin{crmq}[Pluriel]
La formule \textbf{locale} qui permet de calculer $f'(a)$ est assez technique$\dots$

Pour pallier cette technicité, on a va utiliser des formules (globales) qui donnent \textbf{tous} les $f'(a)$ !

\smallskip

Pour déterminer l'équation de la tangente, on \textbf{remplace} les $a$, et on calcule $f'(a)$ puis $f(a)$ !
\end{crmq}

\subsection{Fonction dérivée}

\begin{cprop}[Pluriel]
On a les fonctions dérivées suivantes (les ensembles de définition et dérivabilité ne sont pas indiqués) :
\begin{center}
	%\renewcommand{\arraystretch}{1.25}
	\begin{tblr}{stretch,1.15,hlines,vlines,width=10cm,colspec={X[m,c]X[m,c]},row{1}={bg=lightgray!75}}
		$f(x)=\dots$ & $f'(x)=\dots$ \\
		$k$&$0$ \\
		$x$&$1$ \\
		$x^2$&$2x$ \\
		$x^3$&$3x^2$ \\
		$\phantom{(n \pg 1)}$\hfill{}$x^n$\hfill{}($n \pg 1$)&$nx^{n-1}$ \\
		$\tfrac{1}{x}$&$-\tfrac{1}{x^2}$ \\
		$\tfrac{1}{x^2}$&$-\tfrac{2}{x^3}$ \\
		$\phantom{(n \pg 1)}$\hfill{}$\tfrac{1}{x^n}$\hfill{}($n \pg 1$)&$-\tfrac{n}{x^{n+1}}$ \\
		$\sqrt{x}$&$\tfrac{1}{2\sqrt{x}}$ \\
		$\e^x$&$\e^x$ \\
		$\ln(x)$&$\tfrac{1}{x}$ \\
	\end{tblr}
\end{center}
\end{cprop}

\begin{cthm}
On a les formules de dérivations suivantes ($u$ et $v$ sont des \textbf{fonctions}) :
\begin{center}
	%\renewcommand{\arraystretch}{1.25}
	\begin{tblr}{stretch,1.15,hlines,vlines,width=10cm,colspec={X[m,c]X[m,c]},row{1}={bg=lightgray!75}}
		Fonction & Dérivée \\
		$u+v$&$u'+v'$ \\
		$\phantom{k \text{ cstte}}$\hfill{}$ku$\hfill{}($k$ cstte)&$ku'$ \\
		$u \times v$ & $u'v+v'u$ \\
		$\tfrac{u}{v}$ & $\tfrac{u'v-v'u}{v^2}$ \\
		$\tfrac{1}{v}$ & $\tfrac{-v'}{v^2}$ \\
		$u^n$ & $nu'u^{n-1}$ \\
		$\sqrt{u}$&$\tfrac{u'}{2\sqrt{u}}$ \\
		$\e^u$&$u'\e^u$ \\
		$\ln(u)$&$\tfrac{u'}{u}$ \\
	\end{tblr}
\end{center}
\end{cthm}

\begin{cexemple}[Pluriel,ComplementTitre={, de base}]
Pour dériver une fonction, on repère la ou les formules à utiliser, puis on raisonne éventuellement sur les composées !
\begin{itemize}[leftmargin=*,itemsep=6pt]
	\item $f(x)=x+x^3+\ln(x)$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules de bases ;
		\item $f'(x)=1+3x^2+\dfrac{1}{x}$.
	\end{itemize}
	\item $g(x)=4\e^x-10x^2$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules de base, en laissant les constantes ;
		\item $g'(x)=4 \times \e^x - 10 \times 2x = 4\e^x - 20x$.
	\end{itemize}
	\item $h(x)=\ln(3x+1)$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise la formule du $\ln$ avec $u=3x+1$ et donc $u'=3$
		\item $g'(x)=\dfrac{u'}{u}=\dfrac{3}{3x+1}$.
	\end{itemize}
	\item $i(x)=10\e^{-0,3x+5}$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise la formule de l'exponentielle avec $u=-0,3x+5$ et donc $u'=-0,3$ ; on laisse le 10 ;
		\item $i'(x)=10 \times u'\e^{u} = 10 \times \left(-0,3\right)\e^{-0,3x+5} = -3\e^{-0,3x+5}$.
	\end{itemize}
	\item $j(x)=\dfrac{x+2}{x^2+5}$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on va utiliser $\nicefrac{u}{v}$ avec $\begin{dcases} u=x+2 \\v=x^2+5 \end{dcases}$ et donc $\begin{dcases} u'=1 \\v'=2x \end{dcases}$ ;
		\item $j'(x)=\dfrac{u'v-v'u}{v^2}=\dfrac{1 \times (x^2+5) - 2x \times (x+2)}{(x^2+5)^2}=\dfrac{x^2+5-2x^2-4x}{(x^2+5)^2}=\dfrac{-x^2-4x+5}{(x^2+5)^2}$.
	\end{itemize}
\end{itemize}
\end{cexemple}

\begin{crmq}[Pluriel]
Pour dériver une \textbf{exponentielle}, on la laisse telle quelle et on fait sortir la dérivée de ce qui se trouve à l'intérieur !

\smallskip

Pour la dérivée d'un \textbf{quotient}, on ne développe pas le dénominateur, on le laisse sous forme d'un carré !

\smallskip

Si des choses sont simplifiables, on n'hésite pas à les \textbf{simplifier} !
\end{crmq}

\begin{cexemple}[Pluriel,ComplementTitre={, classiques}]
\begin{itemize}[leftmargin=*,itemsep=6pt]
	\item $f(x)=(3x+2)\e^{-2x}$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules du produit et de l'exponentielle ;
		\item on a $\begin{dcases} u=3x+2 \\v=\e^{-2x} \end{dcases}$ et donc $\begin{dcases} u'=3 \\v'=-2\e^{-2x} \end{dcases}$
		\item $f'(x)=u'v+v'u = 3 \times \e^{-2x} + \left( -2\e^{-2x} \right) \times (3x+2) = 3\e^{-2x} - 6x\e^{-2x} - 4\e^{-2x} = (-6x-1)\e^{-2x}$.
	\end{itemize}
	\item $g(x)=2x^2 + 5x + 3\ln(x)$
	\begin{itemize}[label=$\rightsquigarrow$]
		\item on utilise les formules de base, ici on pensera à mettre au même dénominateur ;
		\item $g'(x)=2 \times 2x + 5 + 3 \times \dfrac1x = 4x+5+\dfrac3x = \dfrac{4x^2}{x}+\dfrac{5x}{x}+\dfrac3x=\dfrac{4x^2+5x+3}{x}$.
	\end{itemize}
\end{itemize}
\end{cexemple}

\subsection{Étude générale}

\begin{cthm}
Pour étudier une fonction $f$ sur un intervalle I :
\begin{itemize}
	\item on calcule sa dérivée $f'(x)$ sur I ;
	\item on étudie, si besoin en \og transformant \fg{}, le \textbf{signe} de $f'(x)$ sur I ;
	\item on dresse le \textbf{tds} de $f'(x)$ sur et on en déduit le \textbf{tdv} de $f$ sur I grâce à $\mathcolor{red}{f' \oplus \Rightarrow f \nearrow}$  et $\mathcolor{blue}{f' \ominus \Rightarrow f \searrow}$ ;
	\item on complète le tdv de $f$ avec les images (ou les limites).
\end{itemize}
\end{cthm}

\begin{cillustr}
Soit $f$ la fonction définie que $\intervFF{0}{7}$ par $f(x)=x^3-11x^2+39x-20$.
\begin{itemize}
	\item $f$ est dérivable et $f'(x)=3x^2-22x+39$ ;
	\item on utilise $\Delta$ pour déterminer le signe de $f'(x)$ ;
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit{$x$/0.7,$f'(x)$/0.7}{$0$,$3$,$\tfrac{13}{3}$,$7$}
			\tkzTabLine{,+,z,-,z,+,}
		\end{tikzpicture}
	\end{center}
	\item le théorème fondamental permet de dresser le tableau de variations de $f$ ;
	\begin{center}
		\begin{tikzpicture}
			\tkzTabInit{$x$/0.7,$f'(x)$/0.7,$f$/1.4}{$0$,$3$,$\tfrac{13}{3}$,$7$}
			\tkzTabLine{,+,z,-,z,+,}
			\tkzTabVar{-/$-20$,+/$25$,-/$m$,+/$57$}
		\end{tikzpicture}
	\end{center}
	avec $m \approx 23,8$.
	\item on peut proposer la courbe suivante pour terminer.
	\begin{center}
%		\tunits{1}{0.1}
%		\tdefgrille{0}{7}{1}{0.5}{-20}{60}{10}{5}
		\begin{tikzpicture}[x=1cm,y=0.1cm,xmin=0,xmax=7,ymin=-20,ymax=60,ygrille=10,ygrilles=5]
			\GrilleTikz\AxesTikz[ElargirOx=0,ElargirOy=0]
			\AxexTikz{0,1,...,6}\AxeyTikz{-20,-10,...,50}
			\draw[line width=1.25pt,color=blue,samples=250,domain=0:7] plot (\x,{\x*\x*\x-11*\x*\x+39*\x-20}) ;
			\draw[line width=1.25pt,dashed](4.333,0)--(4.333,23.8) ;
			\draw[line width=1.25pt,dashed](3,0)--(3,25) ;
			\foreach \Point in {(3,25),(4.333,23.8),(4.333,0)}
				\filldraw \Point circle[radius=3pt] ;
		\end{tikzpicture}
	\end{center}
\end{itemize}
\end{cillustr}

\newpage

\section{Intégration}

\subsection{Notion de primitive}

\begin{cexemple}
$F(x)=2x^3-x+5$ et $G(x)=2x^3-x-8$ ont la même dérivée, la fonction $f(x)=6x^2-1$.

On dit que F et G sont des \textbf{primitives} de $f$.
\end{cexemple}

\begin{cdefi}
Soit $f$ une fonction définie sur un intervalle I et soit F une fonction dérivable sur I.

On dit que F est une \textbf{primitive} de $f$ sur I si $F'(x)=f(x)$ (pour tout réel $x$ de I). 
\end{cdefi}

\subsection{Détermination de primitives}

\begin{cthm}
\begin{center}
	\begin{tblr}{stretch,1.15,hlines,vlines,width=12cm,colspec={X[m,c]X[m,c]X[m,c]},row{1}={bg=lightgray!75}}
		$f(x)=$&$F(x)=$&Validité sur\\
		$a$ \:\:($a \in \R$)&$ax$&$\R$ \\
		$x$&$\tfrac{1}{2}x^2$&$\R$ \\
		$x^2$&$\tfrac{1}{3}x^3$&$\R$ \\
		$x^n$ \:\: ($n \neq 0$ et $n \neq -1$)&$\tfrac{1}{n+1}x^{n+1}$&$\R$ si $n>0$ ; $R^*$ sinon \\
		$\tfrac{1}{x^2}$&$-\tfrac{1}{x}$&$\R^*$ \\
		$\tfrac{1}{x}$&$\ln(x)$&$\R_+^*$ \\
		$\e^x$&$\e^x$&$\R$ \\
	\end{tblr}
\end{center}
\end{cthm}

\begin{cprop}[Pluriel]
On a les primitives (composées) suivantes :
\begin{itemize}[]
	\item une primitive de $f(x)=\e^{ax+b}$ (avec $a \neq0$) est $F(x)=\tfrac{1}{a}\e^{ax+b}$ ;
	\item une primitive de $f(x)=\tfrac{u'}{u}$ (avec $u>0$) est $F(x)=\ln(u)$.
\end{itemize}
De plus, si $F$ est une primitive de $f$ sur I, si $G$ est une primitive de $g$ sur I et si $k$ est un réel alors : 
\begin{itemize}
	\item $F+G$ est une primitive de $f+g$ sur I ;
	\item $kF$ est une primitive de $kf$ sur I.
\end{itemize}
\end{cprop}

\begin{cmethode}
Pour déterminer une primitive d'une fonction :
\begin{itemize}[]
	\item on peut la trouver avec les formules \og classiques \fg{} ;
	\item on peut donner le résultat ou une partie du résultat pour les parties compliquées ;
	\item on peut donner une aide à l'aide d'un logiciel de calcul formel.
\end{itemize}
\begin{center}
	\includegraphics[width=13cm]{./graphics/chap06_xcas}
\end{center}
\end{cmethode}

\subsection{Intégrales}

\begin{cdefi}
Soit $f$ une fonction dérivable sur un intervalle I  et soient $a$ et $b$ deux réels de I.

L’intégrale de $a$ à $b$ de $f$ est le nombre $F(b)-F(a)$ où $F$ est une primitive de $f$ sur I. On le note : \[\int_a^b f(t) \dx[t] = F(b)-F(a).\]
\end{cdefi}

\begin{crmq}
On dit que $t$ est une variable muette, on peut la remplacer par $x$, $u$, \dots
\end{crmq}

\begin{ccalco}
La calculatrice permet de déterminer une valeur approchée d'une intégrale.

Sur \ccalg{TI} on peut choisir le nom de la variable, sur \ccalg{CASIO} c'est forcément $x$.
\end{ccalco}

\begin{cexemple}[Pluriel]
%\begin{tblr}{width=\linewidth,colspec={X[m,l]*{3}{X[m,c]}}
%	\begin{itemize}[leftmargin=5pt]
%		\item $\displaystyle\int_1^2 \ln(t) \dx[t] \approx 0,386$
%		\item $\displaystyle\int_{0,5}^1 \ln(x) \dx \approx -0,153$
%		\item $\displaystyle\int_0^3 4 \dx[u]= 12$
%		\item $\displaystyle\int_{-1}^2 \left( x+\dfrac12 \right)\dx = 3$
%		\item $\displaystyle\int_1^2 \dfrac{1}{x^2} \dx= 0,5$
%		\item $\displaystyle\int_1^{\ln(2)} \e^x \dx\approx -0,718$
%	\end{itemize}
%	&\includegraphics[width=4cm]{./graphics/chap05_int_a}&\includegraphics[width=4cm]{./graphics/chap05_int_b}&\includegraphics[width=4cm]{./graphics/chap05_int_c}\\
%\end{tblr}
\end{cexemple}

\subsection{Application au calcul d'aires}

\begin{cdefi}
Dans un repère orthogonal $(O;I,J)$, une unité d’aire (ou 1 u.a.) est l’aire, en cm\up{2}, du rectangle (ou carré) de côtés OI et OJ.

Ainsi, si $OI=a$ cm et $OJ=b$ cm, 1 u.a.$= a \times b$ cm\up{2}.
\end{cdefi}

\begin{cthm}
Soit $f$ une fonction \textbf{positive} dérivable sur un intervalle $\intervFF{a}{b}$ et $\mathscr{C}$ sa représentation graphique dans un repère orthogonal.

Alors, l’aire en unité d’aire de la partie du plan comprise entre la courbe représentant $f$, l’axe des abscisses et les verticales d’équation $x=a$ et $x=b$ est égale à \[\mathscr{A}=\int_a^b f(t) \dx[t].\]
\end{cthm}

\begin{cillustr}
\begin{center}
%	\tunits{0.6}{0.4}
%	\tdefgrille{-3}{7}{1}{1}{0}{6}{1}{1}
	\begin{tikzpicture}[x=0.6cm,y=0.4cm,xmin=-3,xmax=7,ymin=0,ymax=6]
		\fill [draw,gray,line width=1pt,pattern=north east lines,pattern color=gray] (-1,0) -- plot [domain=-1:5] (\x,{0.07*\x*\x*\x-0.44*\x*\x+0.33*\x+4}) -- (5,0) -- cycle;
		\AxesTikz[ElargirOx=0,ElargirOy=0]
		\draw[line width=1.25pt,red,samples=250,domain=-2:6] plot (\x,{0.07*\x*\x*\x-0.44*\x*\x+0.33*\x+4});
		\draw (-1,0) node[below] {$a$} ; \draw (5,0) node[below] {$b$} ;
		\draw (4,3.5) node[text=red,font=\large] {$\mathscr{C}$} ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\begin{cdefi}
La \textbf{valeur moyenne} d'une fonction $f$ continue (ou dérivable) sur l'intervalle $\intervFF{a}{b}$ est égale à $$m=\dfrac{1}{b-a} \int_a^b f(t) \dx[t]= \dfrac{F(b)-F(a)}{b-a} \text{ avec }F \text{ une primitive de } f \text{ sur } \intervFF{a}{b}.$$
\end{cdefi}

\pagebreak

\section{Limites d'une fonction et asymptotes}

\subsection{Introduction}

\begin{cidee}
Comme pour les suites, l'idée est de regarder ce qu'il se passe pour les valeurs d'une fonction aux endroits intéressants de son ensemble de définition. La notation est la même, à savoir $\lim_{x \to \dots} f(x) = \dots$.
\end{cidee}

\subsection{Limites des fonctions usuelles en l'infini}

\begin{cprop}[s]
On a les limites usuelles suivantes :
\begin{itemize}
	\item $\lim\limits_{x \to +\infty} x^2=+\infty$ (puis généralisation à $x^n$ suivant la parité de $n$)
	\item $\lim\limits_{x \to \pm \infty} \tfrac{1}{x}=0$ ;
	\item $\lim\limits_{x \to +\infty} \sqrt{x} =+\infty$ et $\lim\limits_{x \to +\infty} \ln(x) =+\infty$.
	\item $\lim\limits_{x \to +\infty} \e^x =+\infty$ et $\lim\limits_{x \to -\infty} \e^x =0$.
\end{itemize}
\end{cprop}

\begin{cdefi}[ComplementTitre={ - Propriété}]
La droite d'équation $y=L$ est asymptote (horizontale) à la courbe $\mathscr{C}_f$ en $+\infty$ si $\lim\limits_{x \to +\infty} f(x)=L$.

Et respectivement en $-\infty$ si $\lim\limits_{x \to -\infty} f(x)=L$.
\end{cdefi}

\begin{cillustr}
\begin{center}
%	\tunits{0.7}{0.7}
%	\tdefgrille{0}{15}{1}{1}{0}{5}{1}{1}
	\begin{tikzpicture}[x=0.7cm,y=0.7cm,xmin=0,xmax=15,ymin=0,ymax=5]
		\GrilleTikz[Affp=false]\AxesTikz[ElargirOx=0,ElargirOy=0]
		\AxexTikz{0,1,...,14}\AxeyTikz{0,1,...,4}
%		\tgrillep[line width=0.4pt,color=cyan!50] ;
%		\axestikz* ;
%		\axextikz{0,1,...,14} ; \axeytikz{0,1,...,4} ;
		\draw[samples=200,line width=1.25pt,red,domain=0.25:15] plot (\x,{1+1/\x}) ;
		\draw[densely dashed,line width=1.25pt,CouleurVertForet](0,1)--(15,1) ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\subsection{Limite en un réel}

\begin{cprop}[s]
On a les résultats suivants : \vspace{-0.25cm}
\begin{multicols}{4}
	\begin{itemize}
		\item $\lim\limits_{x \to 0^{+}} \tfrac{1}{x} =+\infty$ ;
		\item $\lim\limits_{x \to 0^{-}} \tfrac{1}{x} =-\infty$ ;
		\item $\lim\limits_{x \to 0} \tfrac{1}{x^2} =+\infty$ ;
		\item $\lim\limits_{x \to 0^+} \ln(x) =-\infty$.
	\end{itemize}
\end{multicols}\vspace{1pt}
\end{cprop}

\begin{cnota}
On peut noter, pour distinguer,  $\lim\limits_{\substack{x \to a \\ x<a}} f(x)$ ou $\lim\limits_{\substack{x \to a^{-}}} f(x)$ et $\lim\limits_{\substack{x \to a \\ x>a}} f(x)$ ou $\lim\limits_{\substack{x \to a^{+}}} f(x)$.
\end{cnota}

\begin{cdefi}[ComplementTitre={ - Propriété}]
Si une fonction $f$ admet une limite infinie en $a$ (à droite ou à gauche ou les deux), la droite d'équation $x = a$ est une asymptote verticale à la courbe $\mathscr{C}_f$.
\end{cdefi}

\begin{cillustr}
\begin{center}
%	\tunits{0.7}{0.7}
%	\tdefgrille{0}{5}{1}{1}{0}{5}{1}{1}
	\begin{tikzpicture}[x=0.7cm,y=0.7cm,xmin=0,xmax=5,ymin=0,ymax=5]
		\GrilleTikz[Affp=false]\AxesTikz[ElargirOx=0,ElargirOy=0]
		\AxexTikz{0,1,...,4}\AxeyTikz{0,1,...,4}
%	\begin{tikzpicture}[x=\xunit cm,y=\yunit cm]
%		\tgrillep[line width=0.4pt,color=cyan!50] ;
%		\axestikz* ;
%		\axextikz{0,1,...,4} ; \axeytikz{0,1,...,4} ;
		\draw[samples=200,line width=1.25pt,red,domain=1.2:5] plot (\x,{1/(\x-1)}) ;
		\draw[densely dashed,line width=1.25pt,CouleurVertForet](1,0)--(1,5) ;
	\end{tikzpicture}
\end{center}
\end{cillustr}

\subsection{Détermination de limites}

\begin{cmethode}[Pluriel]
Pour \og chercher \fg{} une limite, on peut \og remplacer \fg{} et \og essayer de faire les calculs \fg{}.

Les formules de calculs sont celles imaginées, et on peut utiliser, en respectant la règle des signes :

\hfill{}$"\infty+\infty=\infty"$ ; $"\infty \times \infty=\infty"$ ; $"k \times \infty=\infty"$ si $k \neq 0$ ; $"\nicefrac{k}{\infty} =0"$ ; $" \nicefrac{k}{0} =\infty"$ si $k \neq 0$\hfill{}~

Ces \og calculs \fg{} sont des abus d'écriture (elles sont utiles au brouillon), mais elles peuvent être tolérées en utilisant des guillemets.

La calculatrice permet de déterminer (ou vérifier) une limite en remplaçant par la variable par des valeurs de plus en plus grandes, ou de plus en plus proches de la valeur en laquelle on travaille.
\end{cmethode}

\begin{crmq}[Pluriel]
Certains \og calculs \fg{} n'existent pas (ce sont les formes indéterminées $"\infty-\infty"$ ; $"\nicefrac{\infty}{\infty}"$ ; $"0 \times \infty"$ et $"\nicefrac{0}{0}"$).

Dans ce cas, on peut utiliser des méthodes particulières souvent liées aux croissances comparées :

\hfill{}en l'infini, on a \: $\ln(x) \ll \sqrt{x} \ll x \ll x^2 \ll \dots \ll \e^x$\hfill{}~
\end{crmq}

\end{document}