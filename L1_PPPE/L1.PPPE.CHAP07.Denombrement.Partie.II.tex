% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=L1 PPPE,matiere={[U24]},typedoc=CHAPITRE~,numdoc=07,titre={Dénombrement (II)}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%---divers---
\DeclareMathOperator*{\card}{Card}
\usetikzlibrary{matrix,backgrounds}
\usepackage{wrapstuff}

\begin{document}

\pagestyle{fancy}

\part{CH07 - Combinatoire}

\section{\textit{p}-listes}

\subsection{Produit cartésien de deux ensembles}

\begin{cdefi}
Le \textit{produit cartésien} de deux ensembles finis E et F, noté $E \times F$ (qui se lit \og E croix F \fg), est l'ensemble des couples $(x;y)$ où $x \in E$ et $y \in F$.
\end{cdefi}

\begin{cexemple}
On donne $E=\EcritureEnsemble[\strut]{0/1}$ et $F=\EcritureEnsemble[\strut]{a/b}$.

Par un arbre de choix, ou un tableau à double entrée (ou directement !) : 

\begin{itemize}
	\item $E \times F = \EcritureEnsemble[\strut]{(0;a)/(0;b)/(1;a)/(1;b)}$ ;
	\item $F \times E = \EcritureEnsemble[\strut]{(a;0)/(a;1)/(b;0)/(b;1)}$.
\end{itemize}
\end{cexemple}

\begin{crmq}
Attention le produit cartésien $E \times F$ n'est pas égal au produit cartésien $F \times E$ car ses éléments sont des couples (donc ordonnés !).
\end{crmq}

\begin{cprop}[ComplementTitre={ - Principe multiplicatif}]
Soient E et F deux ensembles finis de cardinal respectif $m$ et $n$.

Alors le produit cartésien $E \times F$ est un ensemble de cardinal $m \times n$.

\smallskip

Autrement dit, $\card(E \times F)=\card(E) \times \card(F)$.
\end{cprop}

\subsection{\textit{p}-listes}

\begin{cdefi}[Pluriel]
Soit E un ensemble fini à $n$ éléments et $p$ un  entier naturel non nul.

On appelle $p$-liste d’éléments de E, toute liste ordonnée avec ou sans répétition de $p$ éléments de E choisis parmi les $n$ éléments de E.

\smallskip

Une $p$-liste est une suite ordonnée (tirages successifs) et avec remise, donc on peut retrouver le même élément plusieurs fois.
\end{cdefi}

\begin{cexemple}[Pluriel]
--- $E = \EcritureEnsemble[\strut]{0/1/2}$

On choisit successivement 4 éléments de E en le remettant à chaque fois dans l'ensemble de départ.

Une 4-liste est par exemple $(0;0;1;2)$ de E.

\smallskip

--- On compte en binaire, on utilise seulement le 0 et le 1.

Une 10-liste est une suite de 0 et de 1 présents 10 fois : 1100010011 est une 10-liste.
\end{cexemple}

\begin{cnota}
L'ensemble des $p$-listes est l'ensemble $E \times E \times \ldots \times E$ (écrit $p$ fois) et noté $E^p$ (produit cartésien de E par lui-même $p$ fois).
\end{cnota}

\begin{cthm}
Le nombre de $p$-listes dans un ensemble E fini est $\card\left(E^p\right) = {\left(\card(E)\right)}^p$.
\end{cthm}

\begin{cexemple}
Soit $E = \EcritureEnsemble[\strut]{a/b/c}$, avec $\card(E)=3$.

\begin{itemize}
	\item Si $p=1$, alors une 1-liste est appelée \textit{singleton}, donc l'ensemble des singletons de E est
	
	$\left\{\strut (a);(b);(c)\right\}$ et cet ensemble possède $3^1=3$ éléments.
	\item Si $p=2$, alors une 2-liste est appelée \textit{couple}, donc l'ensemble des couples de E est
	
	$\left\{\strut (a;a);(a;b);(a;c);(b;a);(b;b);(b;c);(c;a);(c;b);(c;c)\right\}$ et cet ensemble possède $3^2=9$ éléments.
	\item Si $p=3$, alors une 3-liste est appelée \textit{triplet}, donc l'ensemble des triplets de E est
	
	$\left\{\strut (a;a;a);(a;a;b);(a;a;c);(a;b;a);(a;b;b);\ldots;(c;c;c)\right\}$ et cet ensemble possède $3^3=27$ éléments.
	\item etc
\end{itemize}
\end{cexemple}

\begin{cnota}
Si $p=4$, on dit qu'une 4-liste est un \textit{quadruplet}.

Si $p=5$, on dit qu'une 5-liste est un \textit{quintuplet}.

Si $p$ est plus grand, on dit une $p$-liste. 
\end{cnota}

\begin{cexo}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Soit $E = \EcritureEnsemble[\strut]{0/1/2/3/4/5/6/7/8/9}$ représentant les chiffres d'un digicode.
	
	Un code d'entrée à 4 chiffres est un quadruplet ordonné et avec répétition, c'est donc une 4-liste il y a donc $10^4 = \num{10000}$ codes possibles.
	\item E est l’ensemble des lettres (majuscules) de l'alphabet français, un \frquote{mot} de cinq lettres est une succession de cinq lettres de l'alphabet ayant un sens ou non avec des répétitions possibles.
	\begin{itemize}
		\item Une 5-liste d'éléments de E est -- par exemple -- $(S;K;O;P;K)$ ;
		\item On peut écrire $26^5=\num{\fpeval{26**5}}$ \og 5-listes \fg{} !
	\end{itemize}
\end{itemize}
\end{cexo}

\section{Arrangements, combinaisons}

\subsection{Arrangements}

\begin{ccadre}
Dans cette partie on a $0 \pp p \pp n$.
\end{ccadre}

\begin{cdefi}
On appelle \textit{arrangement} d'un ensemble E à $n$ éléments toute $p$-liste d'éléments \textit{distincts} de $E$.
\end{cdefi}

\begin{crmq}
On retrouve en conséquence immédiate que $0 \pp p \pp n$ (en effet, les éléments doivent être distincts donc on ne peut pas choisir 2 fois le même élément !).
\end{crmq}

\begin{cthm}
Le nombre d'arrangements de $p$ éléments dans un ensemble à $n$ éléments est égal à :

\hfill$n \times (n-1) \times (n-2) \times \ldots \times (n-p+1)$\hfill~
\end{cthm}

\begin{cdemo}
Le principe est de remplir $p$ cases avec des éléments distincts d'un ensemble à $n$ éléments :

\begin{itemize}
	\item pour la première case on a $n$ choix ;
	\item pour la deuxième case, il reste $n-1$ éléments de E puisqu'on élimine celui mis dans la première case ; donc on a $(n-1)$ choix possibles ;
	\item pour la troisième case, il reste $n-2$ éléments de E puisqu'on élimine celui mis dans la deuxième case, donc on a  $n-2$ choix possibles ;
	\item $\ldots$ ainsi de suite jusque la $p$\up{ème} case.
\end{itemize}
\end{cdemo}

\begin{crmq}[ComplementTitre={ - Vocabulaire}]
On appelle \textit{permutation} de E tout arrangement de $n$ éléments parmi les $n$ éléments de $E$.

Le nombre de permutations dans E avec $\card(E)=n$, est donc égal à $n \times (n-1) \times (n-2) \times \ldots \times 2 \times 1$.

\smallskip

C'est le produit de l'entier $n$ par tous les entiers non nuls inférieurs ou égaux à $n$.

On note ce nombre $n!$, qui se lit « factorielle $n$ ». Par convention $0! = 1$.
\end{crmq}

\begin{ccscq}[ComplementTitre={ - Notation}]
Le nombre d’arrangements de $p$ éléments dans un ensemble à $n$ éléments est égal à \[ \frac{n!}{(n-p)!}. \]%
On peut le noter $A_n^p$.
\end{ccscq}

\begin{cexemple}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item On considère l'ensemble $E=\EcritureEnsemble[\strut]{1/2/3}$.
	
	Les arrangements de 2 éléments dans E sont : $(1;2)$, $(1;3)$, $(2,1)$, $(2;3)$, $(3;1)$ et $(3;2)$.
	
	Il y en a 6, et on a bien $6=3\times2=\frac{3\times2\times1}{1}$.
	\item On appelle mot de 3 lettres, toute suite de trois lettres toutes distinctes 
	choisies dans le mot PARIS, (un tel mot n’ayant pas forcément de sens).
	
	Le nombre de mots de trois lettres qu'on peut écrire est le nombre d'arrangements de 3 éléments dans un ensemble à 5 éléments, il y en a donc $5\times4\times3=\frac{5\times4\times3\times2\times1}{2\times1}=\Arrangement{3}{5}$.
	\item Lors d'une course de 20 chevaux, une personne joue un tiercé (suite de trois chevaux).
	
	Il y a donc $20\times19\times18=\frac{20\times19\times18\times\ldots\times2\times1}{17\times16\times\ldots\times2\times1}=\Arrangement{3}{20}$ tiercés.
\end{itemize}
\end{cexemple}

\subsection{Combinaisons}

\begin{cdefi}[Pluriel]
Soient $n$ et $p$ deux entiers naturels tel que $0 \pp p \pp n$.

Une \textit{combinaison} de $p$ éléments choisis parmi $n$ éléments est une suite \textit{non ordonnée} de $p$ éléments choisis parmi les $n$ éléments.
\end{cdefi}

\begin{cprop}
Le nombre de combinaisons de $p$ éléments choisis parmi $n$ éléments se note $\dbinom{n}{p}$, et :%
\[ \binom{n}{p}=\frac{n!}{p!\,(n-p)!}. \]%
Il se lit \og $p$ parmi $n$ \fg.
\end{cprop}

\begin{crmq}[Pluriel]
Une combinaison est une liste non ordonnée et sans répétition de $p$ éléments choisis dans un ensemble à $n$ éléments. On a donc $0 \pp p \pp n$.

\smallskip

Le nombre $\dbinom{n}{p}$ peut également se noter $C_n^p$.
\end{crmq}

\begin{cidee}[Compteur=false]
On a en fait la relation $$\text{Nb de combinaisons possibles}=\dfrac{\text{Nb d'arrangements possibles}}{\text{Nb de permutations possibles de chaque arrangement}}.$$
Soit encore $C_n^p=\dbinom{n}{p}=\dfrac{A_n^p}{p!}$.
\end{cidee}

\begin{cexemple}
Un tiercé dans le désordre est un sous-ensemble de 3 chevaux pris dans l’ensemble des 20 chevaux au départ de la course. Une arrivée dans le désordre est une combinaison de 3 chevaux choisis parmi les 15 au départ.

Il y a donc $\displaystyle\binom{20}{3}=\dfrac{20!}{3! \times 17!}=\Combinaison{3}{20}$.
\end{cexemple}

\vfill

\begin{chistoire}[Compteur=false,SousTitre={- Le triangle de Pascal -}]
\begin{wrapstuff}[r,top=0]
	\includegraphics[height=2.5cm]{ch07_pascal}
\end{wrapstuff}
Le triangle de Pascal est une présentation des coefficients binomiaux (combinaisons) dans un tableau triangulaire. Il a été nommé ainsi en l'honneur du mathématicien français Blaise Pascal (en médaillon). Il est connu sous l'appellation « triangle de Pascal » en Occident, bien qu'il ait été étudié par d'autres mathématiciens, parfois plusieurs siècles avant lui, en Inde, en Perse (où il est appelé « triangle de Khayyam »), au Maghreb, en Chine (où il est appelé « triangle de Yang Hui »), en Allemagne et en Italie (où il est appelé « triangle de Tartaglia »).

La construction du triangle est régie par la relation de Pascal :
\[ \mathcolor{blue}{{n \choose p}+{n \choose p+1}={n+1 \choose p+1}}. \]

\begin{center}
	\begin{tikzpicture}[my rule/.style={line width=0.6pt}]
	\newlength{\myCellSize}\setlength{\myCellSize}{0.7cm}
	\matrix (mat) [my rule,draw,inner sep=0,matrix of math nodes,nodes={minimum width=\myCellSize,text height=0.6\myCellSize,text depth=0.4\myCellSize}]
	{
		\raisebox{-0.8ex}{\small $n$}\kern 0.3em\raisebox{0.7ex}{\small $p$}
		& 0      & 1 & 2 & 3 & 4 & 5 & \cdots   \\
		0      & 1                                     \\
		1      & 1      & 1                            \\
		2      & 1      & 2 & 1                        \\
		3      & 1      & 3 & 3  & 1                   \\
		4      & 1      & 4 & 6  & 4  & 1              \\
		5      & 1      & 5 & 10 & 10 & 5 & 1          \\
		\vdots & \vdots &   &    &    &   &   & \ddots \\
	};
	\begin{scope}[my rule]
		\draw (mat-1-1.north east) -- (mat-8-1.south east);
		\draw (mat-1-1.south west) -- (mat-1-8.south east);
		\draw ([shift={(0.3pt, -0.3pt)}]mat-1-1.north west) -- (mat-1-1.south east);
	\end{scope}
	\begin{scope}[on background layer]
		\draw[blue,fill=blue!15,thick,rounded corners=2pt]
		(mat-6-3.north west) -| (mat-7-4.south east) -| (mat-6-3.south east) -| ([yshift=-2ex]mat-6-3.north west) -- cycle;
	\end{scope}
	\begin{scope}%[on background layer]
		\draw[blue] ($(mat-6-3)!0.5!(mat-6-4)$) node {$+$};
		\draw[blue] ($(mat-6-4)!0.5!(mat-7-4)$) node {$=$};
	\end{scope}
\end{tikzpicture}
\end{center}
\end{chistoire}

\end{document}