% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=L1 PPPE,matiere={[U24]},typedoc=CHAPITRE~,numdoc=06,titre={Quadrilatères, cercles}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\usetikzlibrary{shapes.multipart}
\usepackage{wrapstuff}
\usepackage[lua]{tkz-euclide}
\newcommand\imgvcenter[2][]{\raisebox{-0.5\height}{\includegraphics[#1]{#2}}}

\begin{document}

\pagestyle{fancy}

\part{CH06 - Polygones et cercles du plan}

\section{Généralités sur les polygones}

\subsection{Définitions et premières propriétés}

\begin{cdefi}
Un \textbf{polygone} est une ligne brisée fermée c’est-à-dire une figure composée de $n$ segments ($n \pg 3$) tels que chaque segment partage chacune de ses extrémités avec exactement un autre segment.

Les segments qui composent le polygone sont appelés les \textbf{côtés} et les extrémités de ces segments sont appelés les \textbf{sommets} du polygone.
\end{cdefi}

\begin{cexemple}[Pluriel]
Ci-dessous trois exemples de polygones.
\begin{center}
	\begin{tikzpicture}[line join=bevel]
		\draw[very thick,fill=black] (0,1) circle[radius=1.5pt] -- (1,3) circle[radius=1.5pt] -- (1.5,3) circle[radius=1.5pt] -- (3,2) circle[radius=1.5pt] -- (1.5,0) circle[radius=1.5pt] -- (0,1) ;
		\draw (1.5,0) node[below,font=\large] {$\mathscr{P}_1$} ;
	\end{tikzpicture}
	\hspace{5mm}
	\begin{tikzpicture}[line join=bevel]
		\draw[very thick,fill=black] (0,0) circle[radius=1.5pt] -- (0,3) circle[radius=1.5pt] -- (1,2.05) circle[radius=1.5pt] -- (2.25,3) circle[radius=1.5pt] -- (3,0) circle[radius=1.5pt] -- (2.5,0.85) circle[radius=1.5pt] -- (0,0) ;
		\draw (1.5,0) node[below,font=\large] {$\mathscr{P}_2$} ;
	\end{tikzpicture}
	\hspace{5mm}
	\begin{tikzpicture}[line join=bevel]
		\draw[very thick,fill=black] (0,2) circle[radius=1.5pt] -- (0,3) circle[radius=1.5pt] -- (3,1) circle[radius=1.5pt] -- (3,3) circle[radius=1.5pt] -- (1,0) circle[radius=1.5pt] -- (0,2) ;
		\draw (1.5,0) node[below,font=\large] {$\mathscr{P}_3$} ;
	\end{tikzpicture}
\end{center}
%
Les polygones $\mathscr{P}_1$ et $\mathscr{P}_3$ possèdent 5 côtés et 5 sommets alors que le polygone $\mathscr{P}_2$ possèdent 6 côtés et 6 sommets.
\end{cexemple}

\begin{cprop}
Un polygone a autant de côtés que des sommets.
\end{cprop}

\begin{cprop}
Soit $\mathscr{P}$ un polygone. Si $A$ et $B$ sont deux sommets non consécutifs de $\mathscr{P}$ alors le segment $[AB]$ s’appelle une diagonale de $\mathscr{P}$ .
\end{cprop}

\begin{cexo}
Tracer un polygone ayant 7 côtés puis tracer toutes ses diagonales. Combien en possède-t-il ?
\end{cexo}

\subsection{Propriétés}

\begin{cdefi}
Un polygone est dit \textbf{croisé} si deux de ses côtés non consécutifs se coupent.

Sinon, il est dit non croisé.
\end{cdefi}

\begin{cexemple}
Sur la figure de l’exemple précédent, $\mathscr{P}_1$ et $\mathscr{P}_2$ sont non croisés et $\mathscr{P}_3$ est croisé.
\end{cexemple}

\begin{cdefi}
Un polygone non croisé est dit \textbf{convexe} si, pour tous points $A$ et $B$ du polygone, le segment $[AB]$ ne contient pas de point extérieur au polygone.

Dans le cas contraire, un polygone est dit non convexe (ou \textbf{concave}).
\end{cdefi}

\begin{cexemple}
Sur la figure de l’exemple précédent, $\mathscr{P}_1$ est convexe et $\mathscr{P}_2$ et $\mathscr{P}_3$ sont non convexes.
\end{cexemple}

\begin{cprop}
Un polygone non croisé $\mathscr{P}$ est convexe si et seulement si l’une des proposition suivantes est vérifiée :

\begin{enumerate}[leftmargin=*]
	\item toutes les diagonales de $\mathscr{P}$ sont entièrement situées à l’intérieur de $\mathscr{P}$ ;
	\item pour tous sommets $A$ et $B$ consécutifs de $\mathscr{P}$, tous les sommets de $\mathscr{P}$ se trouvent du même côté de la droite $(AB)$ ;
	\item les angles intérieurs de $\mathscr{P}$ sont tous inférieurs à 180°.
\end{enumerate}
\end{cprop}

\begin{cdefi}
Un polygone est dit \textbf{régulier} si tous ses côtés ont la même longueur et tous ses angles ont la même mesure.
\end{cdefi}

\begin{crmq}
On peut montrer qu’un polygone est régulier si et seulement s’il existe une rotation $r$ telle que, pour tout sommet $A$ du polygone, l’image de $A$ par $r$ est le sommet suivant du polygone. Le centre de la rotation est alors appelé le centre du polygone régulier.

Un polygone régulier peut être ou non croisé. Lorsqu'il est croisé, on parle de polygone \textbf{étoilé}.
\end{crmq}

\begin{cillustr}
\hfill
\begin{tikzpicture}[line join=bevel]
	\tkzDefPoints{0/0/A,2/0/B}
	\tkzDefRegPolygon[side,sides=6,name=P](A,B)
	\tkzDrawPolygon[very thick](P1,P...,P6)
	\foreach \i in {1,2,...,6} \filldraw (P\i) circle[radius=1.5pt] ;
	\draw (1,0) node[below=6pt,font=\large] {polygone convexe régulier à 6 côtés} ;
\end{tikzpicture}
\hspace{1cm}
\begin{tikzpicture}[line join=bevel]
	\tkzDefPoints{0/0/A,1.5/0/B}
	\tkzDefRegPolygon[side,sides=7,name=P](A,B)
	\foreach \i in {1,2,...,7} \filldraw (P\i) circle[radius=1.5pt] ;
	\draw[very thick] (P1)--(P3)--(P5)--(P7)--(P2)--(P4)--(P6)--(P1) ;
	\draw (1,0) node[below=6pt,font=\large] {polygone étoilé à 7 côtés} ;
\end{tikzpicture}
\hfill~
\end{cillustr}

\begin{cprop}
La somme des angles d’un polygone non croisé ayant $n$ côtés est $180(n − 2)$ degrés.
\end{cprop}

\begin{crmq}
En particulier, on en déduit que les angles intérieurs d’un polygone convexe régulier à $n$ côtés mesurent $\dfrac{180(n-2)}{n}$ degrés.

\smallskip

Pour $n = 3$, on retrouve que la somme des angles d’un triangle est $180(3 - 2) = 180$ degrés.
\end{crmq}

\pagebreak

\subsection{Dénomination}

\begin{cdefi}[Pluriel]
\begin{tblr}{hlines,vlines,width=\linewidth,colspec={Q[3cm,m,c]Q[3cm,m,c]X[m,c]},row{1}={bg=lightgray!25},row{2-Z}={bg=white}}
	Nom 		& Nombre de côtés 	& Exemples \\
	triangle 	& 3 				& \imgvcenter[height=2cm]{ch06_triangles} \\
	quadrilatère& 4 				& \imgvcenter[height=2cm]{ch06_quadris} \\
	pentagone 	& 5 				& \imgvcenter[height=2cm]{ch06_pentas} \\
	hexagone 	& 6 				& \imgvcenter[height=2cm]{ch06_hexas} \\
	heptagone 	& 7 				& \imgvcenter[height=2cm]{ch06_heptas} \\
	octogone 	& 8 				& \imgvcenter[height=2cm]{ch06_octos} \\
	ennéagone (ou nonagone) & 9 	& \imgvcenter[height=2cm]{ch06_nonas} \\
	décagone 	& 10 				& \imgvcenter[height=2cm]{ch06_decas} \\
	hendécagone	& 11 				& \imgvcenter[height=2cm]{ch06_hendes} \\
	dodécagone	& 12 				& \imgvcenter[height=2cm]{ch06_dodecas} \\
\end{tblr}
\end{cdefi}

\pagebreak

\section{Quadrilatères usuels}

\subsection{Parallélogrammes}

\begin{cdefi}
On dit qu’un quadrilatère est un \textbf{parallélogramme} si ses deux diagonales se coupent en leur milieu $I$. Le point $I$ est alors appelé le centre du parallélogramme.
\end{cdefi}

\begin{crmq}[Pluriel]
$\bullet~~$Un parallélogramme est un polygone convexe.

$\bullet~~$La définition précédente n’exclut pas que les 4 sommets du quadrilatère soient alignés. Dans ce cas, on parle de parallélogramme aplati.
\end{crmq}

\begin{cmethode}
Pour tracer un parallélogramme $ABCD$ de côtés $a$ et $b$, on commence par tracer un segment $[AB]$ de longueur $a$ puis un segment $[AD]$ de longueur $b$. Ensuite, on pique la pointe du compas en $B$ et on trace un arc de cercle de rayon $b$, on pique la pointe du compas en $D$ et on trace un arc de cercle de rayon $a$ et le point d’intersection des deux arcs de cercle est le point $C$.
\end{cmethode}

\begin{cexo}
Tracer deux parallélogrammes différents dont les côtés mesurent \qty{5}{cm} et \qty{8}{cm}.
\end{cexo}

\begin{cprop}[Pluriel]
Soient $A$, $B$, $C$ et $D$ quatre points non alignés du plan. Alors, $ABCD$ est un \textbf{parallélogramme} non aplati si et seulement si l’une des propositions suivantes est vérifiée :

\begin{enumerate}[leftmargin=*]
	\item $(AB)$ est parallèle à $(CD)$ et $(AD)$ est parallèle à $(BC)$ ;
	\item $ABCD$ n’est pas croisé, $AB = CD$ et $AD = BC$ ;
	\item $AB = CD$ et $(AB)$ est parallèle à $(CD)$ ;
	\item $ABCD$ est convexe et ses angles opposés ont mêmes mesures ;
	\item les angles consécutifs de $ABCD$ sont supplémentaires deux à deux (c’est-à-dire la somme de deux angles consécutifs de $ABCD$ vaut toujours 180°).
\end{enumerate}
\end{cprop}

\begin{cexo}
On considère dans le plan 6 points distincts $A$, $B$, $C$, $D$, $E$ et $F$ tels que $ABCD$ et $BEFC$ soient des parallélogrammes.

Montrer que $(AE)$ et $(DF)$ sont parallèles.
\end{cexo}

\begin{cprop}
L’aire d’un parallélogramme de côté $b$ et de hauteur associée $h$ est $b \times h$.
\end{cprop}

\begin{cexo}
On considère un parallélogramme $ABCD$ tel que $AB = \qty{8}{cm}$ et $AC = \qty{5}{cm}$. On note $H$ le point d’intersection de la perpendiculaire $(AB)$ passant par $D$ avec $(AB)$. Sachant que $HB = \qty{5}{cm}$, déterminer l’aire, en cm\up{2}, du parallélogramme $ABCD$.
\end{cexo}

\subsection{Rectangles, losanges, carrés}

\begin{cdefi}[Pluriel]
On dit qu’un quadrilatère $ABCD$ est :
%
\begin{itemize}[leftmargin=*]
	\item un \textbf{rectangle} si $ABCD$ est un parallélogramme possédant un angle droit.
	\item un \textbf{losange} si $ABCD$ est un parallélogramme possédant deux côtés consécutifs de
	même longueur.
	\item un \textbf{carré} si $ABCD$ est à la fois un rectangle et un losange.
\end{itemize}
\end{cdefi}

\begin{cillustr}
\hfill
\begin{tikzpicture}[line join=bevel]
	\draw[very thick] (0,0) rectangle (3,2);
	\draw (1.5,0) node[below=6pt,font=\large] {un rectangle} ;
\end{tikzpicture}
\hspace{5mm}
\begin{tikzpicture}[line join=bevel]
	\draw[very thick] (0,0) -- (1.5,-1) -- (3,0) -- (1.5,1) -- cycle;
	\draw (1.5,-1) node[below=6pt,font=\large] {un losange} ;
\end{tikzpicture}
\hspace{5mm}\begin{tikzpicture}[line join=bevel]
	\draw[very thick] (0,0) rectangle (2,2);
	\draw (1,0) node[below=6pt,font=\large] {un carré} ;
\end{tikzpicture}
\hfill~
\end{cillustr}

\begin{cprop}
Un quadrilatère est un \textbf{rectangle} si et seulement si ses diagonales ont même longueur et se coupent en leur milieu.
\end{cprop}

\begin{cexo}
Soit $ABC$ un triangle rectangle en $A$. On note $I$ le milieu de $[BC]$ et $D$ le symétrique de $A$ par rapport à $I$.
%
\begin{enumerate}
	\item Montrer que $ABDC$ est un rectangle.
	\item En déduire que $AI = IB = IC$.
\end{enumerate}
\end{cexo}

\begin{cprop}
Un quadrilatère est un losange si et seulement si ses diagonales se coupent en leur milieu et sont perpendiculaires.
\end{cprop}

\begin{cexo}
Soit $ABCD$ un parallélogramme tel que $\widehat{CAB} = \qty{71}{\degree}$ et $\widehat{ABD} = \qty{19}{\degree}$.

Montrer que $ABCD$ est un losange.
\end{cexo}

\begin{ccscq}
Un quadrilatère est un carré si et seulement si ses diagonales sont de même longueur et se coupent en leur milieu en formant un angle droit.
\end{ccscq}

\begin{cexo}
On considère un triangle $ABC$ isocèle rectangle en $A$. On note $D$ et $E$ les symétriques respectifs de $B$ et $C$ par rapport à $A$.

Montrer que $BCDE$ est un carré.
\end{cexo}

\begin{cprop}
L’aire d’un rectangle de côtés $a$ et $b$ est $a \times b$ et l’aire d’un losange de diagonales $d$ et $D$ est $\dfrac{d \times D}{2}$.
\end{cprop}

\begin{cexo}
En utilisant seulement la règle graduée et le compas, construire un carré dont l’aire est \qty{8}{cm^2}.
\end{cexo}

\subsection{Trapèzes}

\begin{cdefi}
On dit que $ABCD$ est un \textbf{trapèze} de \textbf{bases} $[AB]$ et $[CD]$ si les droites $(AB)$ et $(CD)$ sont parallèles.

Si, de plus, $AD = BC$, on dit que $ABCD$ est un trapèze \textbf{isocèle}.

Si $(AD)$ est perpendiculaire à $(AB)$, on dit que $ABCD$ est un trapèze \textbf{rectangle}.
\end{cdefi}

\begin{cillustr}
\hfill
\begin{tikzpicture}[line join=bevel]
	\draw[very thick,fill=black] (0,0) circle[radius=1.5pt] -- (0.5,2) circle[radius=1.5pt] -- (2,2) circle[radius=1.5pt] -- (3.25,0) circle[radius=1.5pt] -- (0,0) ;
	\draw ({0.5*3.25},0) node[below=6pt,font=\large] {un trapèze quelconque} ;
\end{tikzpicture}
\hspace{5mm}
\begin{tikzpicture}[line join=bevel]
	\draw[very thick,fill=black] (0,0) circle[radius=1.5pt] -- (1,2) node[midway,sloped] {$/\!\!/$} circle[radius=1.5pt] -- (3,2) circle[radius=1.5pt] -- (4,0) node[midway,sloped] {$/\!\!/$} circle[radius=1.5pt] -- (0,0) ;
	\draw ({0.5*4},0) node[below=6pt,font=\large] {un trapèze isocèle} ;
\end{tikzpicture}
\hspace{5mm}
\begin{tikzpicture}[line join=bevel]
	\draw[very thick,fill=black] (0,0) circle[radius=1.5pt] -- (0,2) circle[radius=1.5pt] -- (2.15,2) circle[radius=1.5pt] -- (2.85,0) circle[radius=1.5pt] -- (0,0) ;
	\draw[very thick] (0,0) rectangle (8pt,8pt) ;
	\draw ({0.5*2.85},0) node[below=6pt,font=\large] {un trapèze rectangle} ;
\end{tikzpicture}
\hfill~
\end{cillustr}

\begin{crmq}
Un trapèze à la fois isocèle et rectangle est un rectangle.
\end{crmq}

\begin{cprop}
L’aire d’un trapèze de petite base $b$, de grande base $\mathscr{B}$ et de hauteur $h$ est $\dfrac{(b + \mathscr{B}) \times h}{2}$.

\smallskip

\hfill
\begin{tikzpicture}[line join=bevel]
	\draw[very thick] (0,0) -- (1.5,2) -- (4,2) -- (5,0) -- cycle ;
	\draw[very thick] (1.5,0) rectangle++ (8pt,8pt) ;
	\draw[very thick,dashed] (1.5,0) --++ (0,2) node[midway,right] {$h$} ;
	\draw ({0.5*5},0) node[below] {$\mathscr{B}$} ;
	\draw ({0.5*1.5+0.5*4},2) node[above] {$b$} ;
\end{tikzpicture}
\hfill~
\end{cprop}

\begin{cexo}
On considère un trapèze $ABCD$ de petite base $AB = \qty{2}{cm}$ et de grande base
$CD = \qty{7}{cm}$. On note $H$ le point d’intersection de la perpendiculaire à $(CD)$ passant par $A$ avec $(CD)$. Sachant que $AH = CH = \qty{4}{cm}$ :
%
\begin{enumerate}
	\item déterminer l’aire de $ABCD$ ;
	\item déterminer le périmètre de $ABCD$ ;
	\item déterminer les longueurs des deux diagonales de $ABCD$.
\end{enumerate}
\end{cexo}

\pagebreak

\subsection{Classement des quadrilatères}

\begin{cmethode}
On peut \textit{classer} les quadrilatères selon leurs propriétés :

\begin{itemize}
	\item un trapèze est un quadrilatère particulier ;
	\item un parallélogramme est un trapèze particulier ;
	\item un rectangle et un losange sont des parallélogrammes particuliers ;
	\item un carré est un rectangle particulier et un losange particulier.
\end{itemize}

Par exemple, un carré est un rectangle \textit{et} un losange \textit{et} un parallélogramme \textit{et} un trapèze \textit{et} un quadrilatère.
\end{cmethode}

\begin{cillustr}
\vspace*{0.5\baselineskip}
\hfill
\begin{tikzpicture}[line join=bevel]
	%LÉGENDE
%	\draw[xstep=0.5,ystep=0.5,very thin,lightgray] (-5,-10) grid (5,2) ; \filldraw (0,0) circle[radius=1pt] ;
	\draw[blue!75!black,->,>=latex,very thick] (-4,-1.5)--++(0,2) node[midway,right,font=\small\sffamily] {est un } ;
	%N1 Quadri
	\draw (0,0) node[font=\scriptsize\sffamily] {\begin{tabular}{c} Quadrilatère \\ quelconque \end{tabular}} ;
	\draw[thick] (-1.5,0.35)--++(1.75,0.5)--++(1.65,-1)--++(-3,-0.5)--cycle ;
	%N2 Trap
	\draw (0,-2) node[font=\scriptsize\sffamily] {Trapèze} ;
	\draw[thick] (-1.05,-1.5)--++(1.75,0)--++(1,-1)--++(-3,0)--cycle ;
	%N2 Trap
	\draw (0,-4) node[font=\scriptsize\sffamily] {Parallélogramme} ;
	\draw[thick] (-1.05,-3.5)--++(2.5,0)--++(-0.3,-1)--++(-2.5,0)--cycle ;
	%N3A Los
	\draw (-2,-6) node[font=\scriptsize\sffamily] {Losange} ;
	\draw[thick] (-2,-5.5)--++(1.25,-0.5)--++(-1.25,-0.5)--++(-1.25,0.5)--cycle ;
	%N3B Rect
	\draw (2,-6) node[font=\scriptsize\sffamily] {Rectangle} ;
	\draw[thick] (1,-5.5)rectangle++(2,-1) ;
	%N4 Car
	\draw (0,-8) node[font=\scriptsize\sffamily] {Carré} ;
	\draw[thick] (-0.5,-7.5)rectangle++(1,-1) ;
	%FLÈCHES
	\draw[blue!75!black,->,>=latex,very thick] (-0.15,-7.5)--({-1.375},-6.25) ;
	\draw[blue!75!black,->,>=latex,very thick] (0.15,-7.5)--({1.5},-6.5) ;
	\draw[blue!75!black,->,>=latex,very thick] ({-1.375},-5.75)--(-0.15,-4.5) ;
	\draw[blue!75!black,->,>=latex,very thick] ({1.5},-5.5)--(0.15,-4.5) ;
	\draw[blue!75!black,->,>=latex,very thick] (0,-3.5)--(0,-2.5) ;
	\draw[blue!75!black,->,>=latex,very thick] (0,-1.5)--++(0,1.025) ;
\end{tikzpicture}
\hfill~
%
%\begin{center}
%	\includegraphics[scale=0.33]{ch06_classement}
%\end{center}
%%\hfill
%%\begin{tikzpicture}
%%	\node[thick,starburst,starburst points=2,starburst point height=1cm,draw,blue] (QUADRI) at (0,0) {Quadrilatère$\vphantom{A_A^A}$};
%%	\node[thick,trapezium,trapezium left angle=45,trapezium right angle=60,draw,violet] (TRAPEZ) at (0,-4) {Trapèze};
%%\end{tikzpicture}
%%\hfill~
\end{cillustr}

\section{Cercles et disques}

\subsection{Généralités}

\begin{cdefi}
Soit $O$ un point du plan et soit $R$ un réel strictement positif.

Le \textbf{cercle} $\mathscr{C}$ de centre $O$ et de rayon $R$ est l’ensemble des points $M$ du plan tels que $OM = R$.

Le \textbf{disque} $\mathscr{D}$ de centre $O$ et de rayon $R$ est l’ensemble des points $M$ du plan tels que $OM \pp R$. Il s’agit donc de la surface délimitée par $\mathscr{C}$ et contenant $O$.

\smallskip

\hfill
\begin{tikzpicture}
	\draw[very thick] (0,0) circle[radius=2] ;
	\draw (60:2) node[above right] {$\mathscr{C}$} ;
	\filldraw (0,0) circle[radius=1.5pt] node[below right] {$O$} ;
	\draw[very thick,->,>=latex] (0,0) -- (155:2) node[midway,above] {$R$} ;
\end{tikzpicture}
\hfill~
\end{cdefi}

\begin{cdefi}
Soit $\mathscr{C}$ un cercle de centre $O$ et de rayon $R$. Soient $A$ et $B$ deux points du cercle.
%
\begin{enumerate}
	\item Le segment $[AB]$ s’appelle une \textbf{corde} du cercle $\mathscr{C}$.
	\item Le segment $[OA]$ s’appelle un \textbf{rayon} du cercle.
	\item Si $O$ est le milieu de $[AB]$ alors $[AB]$ s’appelle un \textbf{diamètre} de $\mathscr{C}$ et on dit que $A$ et $B$ sont diamétralement opposés.
\end{enumerate}
\end{cdefi}

\subsection{Propriétés}

\begin{cprop}
Le \textbf{périmètre} (ou circonférence ou encore longueur) d’un cercle de rayon $R$ est $2\pi R$.

L’aire d’un disque de rayon $R$ est $\pi R^2$.
\end{cprop}

\begin{cexo}
Un stade d’athlétisme est modélisé par le schéma ci-dessous.

\smallskip

\hfill
\begin{tikzpicture}[scale=0.75]
	\draw[very thick] (0,0) -- (5,0) arc (-90:90:1.75) --++ (-5,0) arc (90:270:1.75) ;
	\draw[very thick,<->,>=latex] (-1.75,3.85) --++(8.5,0) node[midway,fill=white,scale=0.75] {\qty{300}{m}} ;
	\draw[very thick,<->,>=latex] ({-1.75-0.7},0) --++(0,3.5) node[midway,fill=white,scale=0.75] {\qty{120}{m}} ;
\end{tikzpicture}
\hfill~

\smallskip

Sachant que les arcs sont des demi-cercles et que les bords rectilignes sont deux côtés opposés d’un rectangle, déterminer le périmètre et l’aire de ce stade.
\end{cexo}

\begin{cdefi}
Soit $\mathscr{C}$ un cercle de centre $O$, et soit $A \in \mathscr{C}$.

La droite $\mathscr{T}$ passant par $A$ et perpendiculaire à $(OA)$ est appelée la
\textbf{tangente} à $\mathscr{C}$ au point $A$.
\smallskip

\hfill
\begin{tikzpicture}[scale=0.75]
	\tkzDefPoint(0,0){O}
	\tkzDefPoint(28:2){A}
	\tkzDefRandPointOn[circle=center O radius 2]\tkzGetPoint{Z}
	\tkzDrawSegment[very thick](O,A)
	\tkzDrawCircle[black,very thick](O,Z)
	\draw (120:2) node[above left,scale=0.75] {$\mathscr{C}$} ;
	\draw (A) node[right,scale=0.75] {$A$} ;
	\tkzDefLine[tangent at=A](O)\tkzGetPoint{h}
	\tkzDrawLine[very thick,black,add=2 and 0.5](A,h)
	\tkzMarkRightAngle[very thick](O,A,h)
	\draw ($(A)+(-62:1.5)$) node[right,scale=0.75] {$\mathscr{T}$} ;
\end{tikzpicture}
\hfill~
\end{cdefi}

\begin{cprop}
Soit $\mathscr{C}$ un cercle de centre $O$ et de rayon $R$. Soit $A \in \mathscr{C}$ et $\mathscr{T}$ la tangente à $\mathscr{C}$ en $A$.

Alors, la droite $\mathscr{T}$ coupe le cercle $\mathscr{C}$ en une unique point (qui est le point $A$).
\end{cprop}

\begin{cexo}
On considère un cercle $\mathscr{C}$ de centre $O$ et de rayon \qty{5}{cm}. On considère sur $\mathscr{C}$ deux points $A$ et $B$ diamétralement opposés. La perpendiculaire à $(AB)$ passant par $O$ coupe le cercle en deux points : le point $C$ et le point $D$.
%
\begin{enumerate}
	\item Déterminer la nature précise du quadrilatère $ACBD$. Déterminer son aire et son périmètre.
	\item On note $\mathscr{T}$ la tangente à $\mathscr{C}$ en $D$. On considère sur $\mathscr{T}$ deux points $E$ et $F$ tels que $EF = \qty{3}{cm}$.
	
	Démontrer que $AEFB$ ou $AFEB$ est un trapèze et déterminer son aire.
\end{enumerate}
\end{cexo}

\end{document}