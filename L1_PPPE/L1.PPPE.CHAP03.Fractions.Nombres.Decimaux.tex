% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=L1 PPPE,matiere={[U14]},typedoc=CHAPITRE~,numdoc=03,titre={Fractions}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}
%divers

\begin{document}

\pagestyle{fancy}

\part{CH03 - Fractions, nombres décimaux}

\section{Introduction}

\begin{cintro}[Compteur=false]
Nous avons déjà travaillé les \textbf{ensembles} des nombres entiers :
%
\begin{itemize}
	\item $\N$ : ensemble des entiers naturels ;
	\item $\Z$ : ensemble des entiers relatifs. 
\end{itemize}
%
Dans ce dernier ensemble, l'addition suit des règles précises et construites et possède des propriétés mathématiques, il n'en est pas de même pour la multiplication, en effet si la multiplication de deux entiers est un entier, si 1 est l'élément neutre de la multiplication, cette dernière ne possède pas d'élément \textit{symétrique} (c'est-à-dire il existe des entiers $n$ pour lesquels il n’existe pas d’entier $m$ tel que $n \times m = 1$) !
\end{cintro}

\begin{cidee}[Compteur=false]
Cette contrainte est à l'origine de la création de l’ensemble des \textbf{rationnels}.
\end{cidee}

\section{L'ensemble des rationnels}

\subsection{Définitions}

\begin{cdefi}
On appelle \textbf{nombre rationnel}, tout nombre \textit{pouvant} s'écrire sous la forme d’un quotient de deux nombres entiers.
\end{cdefi}

\begin{cdefi}
Mathématiquement, si $q$ est un rationnel alors il existe deux entiers $a \in \Z$ et $b \in \N*$ tel que \[q=\frac{a}{b}\] où $a$ est appelé \textit{numérateur} et $b$ est appelé \textit{dénominateur}.
\end{cdefi}

\begin{crmq}[Pluriel]
On a les résultats suivants :

\begin{itemize}
	\item un rationnel est donc un nombre qui est soit positif soit négatif ; et par \textit{convention} le signe \og $-$ \fg{} sera positionné au numérateur ou devant la fraction et non au dénominateur ;
	\item tout nombre entier est un rationnel, il suffit de considérer $b = 1$ ;
	\item une fraction est dite \textit{décimale} lorsque son dénominateur est une puissance de 10.
\end{itemize}
\end{crmq}

\begin{cdefi}
L'ensemble des nombres rationnels est l'ensemble de tous les rationnels et est 
noté $\Q$.
\end{cdefi}

\subsection{Propriétés}

\begin{cthm}[ComplementTitre={ - « Produit en croix »}]
Deux fractions $\dfrac{a}{b}$ et $\dfrac{c}{d}$ (avec $c$ et $d$ entiers non nuls) sont égales si et seulement si le \textit{produit des extrêmes} est égal au \textit{produit des moyens}.

C'est-à-dire $\dfrac{a}{d}=\dfrac{c}{d} \ssi a\times d=b\times c$.
\end{cthm}

\begin{ccscq}[Compteur=false]
On en déduit qu'un rationnel possède une infinité d'écritures fractionnaires.
\end{ccscq}

\subsection{Fraction irréductible}

\begin{cdefi}
Parmi toutes les fractions qui sont égales, il en existe \textbf{une et une seule} dont le numérateur et le dénominateur n'ont pas de diviseur commun (on dit qu'ils sont \textit{premiers entre eux}).

Cette fraction est appelée \textbf{fraction irréductible}.
\end{cdefi}

\begin{cprop}
Entre deux fractions données, il existe une infinité de fractions.
\end{cprop}

\section{Premières propriétés des fractions}

\subsection{Forme décimale infinie, période}

\begin{cprop}
En effectuant la division, tout nombre rationnel admet une forme décimale infinie, elle possède un chiffre ou un groupe de chiffres se répétant à l'identique à l'infini appelé \textit{période} de ce nombre rationnel et le nombre de chiffres formant la période est appelé \textit{longueur} de la période.
\end{cprop}

\begin{cmethode}
Pour retrouver une écriture fractionnaire d'un nombre rationnel connaissant 
sa forme décimale, on effectue des multiplications par des puissances de 10 et des additions (ou soustractions) afin d'obtenir des entiers.
\end{cmethode}

\subsection{Simplification de fraction}

\begin{cmethode}
Pour déterminer la fraction irréductible égale à un rationnel donné, il faut déterminer le plus grand diviseur commun aux numérateur et dénominateur puis simplifier ou bien décomposer sous forme de produit de facteurs chacun des deux nombres numérateur et dénominateur, produit dans lequel se trouve des facteurs communs aux deux nombres et simplifier par ce facteur commun (on peut réitérer le procédé).
\end{cmethode}

\section{Les nombres décimaux relatifs}

\subsection{Définition}

\begin{cdefi}
Un nombre décimal est un rationnel pouvant s'écrire sous forme de fraction décimale.
\end{cdefi}

\begin{ccscq}
Un nombre décimal admet une forme décimale appelée écriture décimale dont la suite de chiffres située à droite de la virgule, dite de \textit{cadrage}, est finie.

\smallskip

L'écriture décimale est composée de sa \textbf{partie entière} (nombre situé à gauche de la virgule) et de sa \textbf{partie décimale} (située à droite de la virgule).
\end{ccscq}

\begin{crmq}[Pluriel]
Un nombre décimal est toujours un rationnel de période 0.

La fraction décimale n'est pas forcément la fraction irréductible
\end{crmq}

\subsection{Propriétés}

\begin{cthm}
Un nombre décimal est un rationnel (une fraction de deux entiers) dont le dénominateur est 
le produit de puissances de 2 et/ou de 5.
\end{cthm}

\begin{cdefi}
L'ensemble des nombres décimaux est noté $\D$.
\end{cdefi}

\end{document}