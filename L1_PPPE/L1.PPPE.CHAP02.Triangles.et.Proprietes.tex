% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage{tikz2d-fr}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\usepackage{cp-preambule}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=L1 PPPE,matiere={[U14]},typedoc=CHAPITRE~,numdoc=02,titre={Triangles}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\numeropagetot}}

\begin{document}

\pagestyle{fancy}

\part{CH02 - Triangles et propriétés}

\section*{Pré-requis}

\begin{crappel}[Pluriel,Compteur=false]
Dans ce chapitre, on a comme pré-requis :

\begin{itemize}
	\item un angle plat vaut 180° ;
	\item deux droites parallèles à une même troisième sont parallèles ;
	\item deux droites perpendiculaires à une même troisième sont parallèles ;
	\item angles alternes-internes, opposés par le sommet, alternes-externes.
\end{itemize}
\end{crappel}

\begin{cnota}
De manière classique, on \og codera \fg{} un triangle $ABC$ de la manière suivante :

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/0/B,3/3/C}
		\tkzDrawPolygon[mainlevee,thick](A,B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzPicAngle["$\widehat{A}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](B,A,C)
		\tkzPicAngle["$\widehat{B}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](C,B,A)
		\tkzPicAngle["$\widehat{C}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](A,C,B)
		\tkzLabelSegment(A,B){$c$}
		\tkzLabelSegment[above left](A,C){$b$}
		\tkzLabelSegment[auto](C,B){$a$}
	\end{tikzpicture}
\end{center}
\end{cnota}

\section{Existence d'un triangle}

\begin{cprop}
Pour qu’un triangle soit \textit{constructible}, il faut que la longueur du plus grand côté soit inférieure à la somme des deux autres. C’est ce qu’on appelle l’\textbf{inégalité triangulaire} :
%
\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/0/B,3/1.5/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
	\end{tikzpicture}
\end{center}
%
Dans le triangle ABC, on a $AB<AC+CB$.
\end{cprop}

\begin{cexemple}
Peut-on construire un triangle ABC tel que $AB=6$~cm, $AC=2,5$~cm et $BC=3$~cm ? Si oui, le construire.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,6/0/B}
		\tkzDrawSegment[thick](A,B)
		\tkzLabelPoints[left](A)
		\tkzLabelPoints[right](B)
		\tkzDrawPoints(A,B)
		\tkzDrawArc[R,thick,densely dashed](A,2.5)(-15,15)
		\tkzDrawArc[R,thick,densely dashed](B,3)(165,195)
		\draw[thick,red,->] (A)--($(A)+(10:2.5)$) node[sloped,midway,above] {$2,5$~cm};
		\draw[thick,red,->] (B)--($(B)+(170:3)$) node[sloped,midway,above] {$3$~cm};
	\end{tikzpicture}
\end{center}
%
Ce n'est pas possible, car on a $6>2,5+3$ ; donc les deux arcs de cercle ne peuvent pas se croiser !
\end{cexemple}

\begin{crmq}[Pluriel]
$\bullet~~$Si le point $M$ appartient au segment $[BC]$, alors l’inégalité triangulaire devient une égalité $AB=AC+CB$ :

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/0/B,2.25/0/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[below](A)
		\tkzLabelPoints[below](B)
		\tkzLabelPoints[above](C)
		\tkzDrawPoints(A,B,C)
	\end{tikzpicture}
\end{center}

$\bullet~~$Dans un triangle $ABC$, on peut écrire trois inégalités triangulaires, à savoir $\begin{dcases} AB<AC+CB \\ AC<AB+BC \\ BC<BA+AC \end{dcases}$.
\end{crmq}

\section{La règle des 180°}

\begin{cthm}
La somme des mesures des angles d’un triangle est toujours égale à 180°.
\end{cthm}

\begin{cidee}[Compteur=false]
On peut \og visualiser \fg{} cette propriété à l'aide des angles alternes-internes :
%
\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/0/B,3/3/C,-4/3/E,4/3/F}
		\tkzDrawLines[thick,add = 0.1 and 0.1](A,B A,C B,C)
		\tkzPicAngle[red,"$\widehat{A}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](B,A,C)
		\tkzPicAngle[red,"$\widehat{A}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](E,C,A)
		\tkzPicAngle[blue,"$\widehat{B}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](C,B,A)
		\tkzPicAngle[blue,"$\widehat{B}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](B,C,F)
		\tkzPicAngle[VertForet,"$\widehat{C}$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](A,C,B)
		\draw[thick] (-0.4,3)--(4.4,3) ;
	\end{tikzpicture}
\end{center}
\end{cidee}

\section{Nature d'un triangle}

\subsection{Triangle rectangle}

\begin{cdefi}[Pluriel]
Un triangle est dit triangle \textbf{rectangle} lorsque deux de ses côtés sont perpendiculaires. 

Si ABC est rectangle en A, alors :
\begin{itemize}
	\item l'angle $\widehat{A}$ vaut 90° ;
	\item $\widehat{B}+\widehat{C}=90°$, on dit que ces deux angles sont complémentaires ;
	\item les côtés perpendiculaires sont appelés côtés de l'angle droit, et l'autre côté est appelé l'hypoténuse. 
\end{itemize}

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/0/B,0/2.5/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzMarkRightAngle[thick](B,A,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
	\end{tikzpicture}
\end{center}
\end{cdefi}

\subsection{Triangle isocèle}

\begin{cdefi}[Pluriel]
un triangle est dit \textbf{isocèle} lorsqu'il possède deux côtés de même longueur.

On dit qu’un triangle $ABC$ est isocèle en $A$ lorsque les côtés $[AB]$ et $[AC$] ont la même mesure.

Dans ce cas, $A$ est appelé sommet principal du triangle et il est indispensable de le préciser derrière le mot isocèle.

Les angles des sommets autres que le sommet principal sont de même mesure et sont appelés angles à la base. 

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/B,4/0/C,2/5/A}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzMarkSegments[thick,mark=||](A,B A,C)
		\tkzLabelPoints[below left](B)
		\tkzLabelPoints[below right](C)
		\tkzLabelPoints[above](A)
		\tkzMarkAngle[thick,size=1,color=blue,mark=|](C,B,A)
		\tkzMarkAngle[thick,size=1,color=blue,mark=|](A,C,B)
	\end{tikzpicture}
\end{center}
\end{cdefi}

\begin{crmq}
Un triangle peut être à la fois isocèle \textbf{et} rectangle, on parle d'un triangle isocèle rectangle.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,3/0/B,0/3/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzMarkSegments[thick,mark=||](A,B A,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzMarkRightAngle[thick](C,A,B)
		\tkzPicAngle[red,"$45°$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](C,B,A)
		\tkzPicAngle[red,"$45°$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](A,C,B)
	\end{tikzpicture}
\end{center}
\end{crmq}

\subsection{Triangle équilatéral}

\begin{cdefi}
Un triangle est dit \textbf{équilatéral} lorsque ses trois côtés ont la même mesure.

De ce fait, un triangle équilatéral a ses trois angles égaux, de mesure 60°.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,3/0/B}
		\tkzDefTriangle[equilateral](A,B)\tkzGetPoint{C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzMarkSegments[thick,mark=||](A,B A,C B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
%		\tkzMarkRightAngle[thick](C,A,B)
%		\tkzPicAngle[red,"$45°$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](C,B,A)
%		\tkzPicAngle[red,"$45°$",draw,thick,angle eccentricity=1.35,angle radius=0.75cm](A,C,B)
	\end{tikzpicture}
\end{center}
\end{cdefi}

\begin{cprop}[ComplementTitre={ - Programme de construction, version 1}]
$\bullet~~$On place deux points distincts $A$ et $B$.

$\bullet~~$On trace le segment $[AB]$.

$\bullet~~$On trace le cercle de centre $A$ passant par $B$.

$\bullet~~$On trace le cercle de centre $B$ passant par $A$.

$\bullet~~$Ces deux cercles se coupent en deux points, dont l'un est noté $C$.

$\bullet~~$On trace les segments $[AC]$ et $[BC]$.

$\bullet~~$Le triangle $ABC$ est équilatéral.
%
\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,3/0.5/B}
		\tkzDefTriangle[equilateral](A,B)\tkzGetPoint{C}
		\tkzDrawPolygon[thick](A,B,C)
		%\tkzMarkSegments[thick,mark=||](A,B A,C B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzDrawArc[thick,color=gray,densely dashed,delta=20](A,B)(C)
		\tkzDrawArc[thick,color=gray,densely dashed,delta=20](B,C)(A)
		%\tkzDrawArc[R,Gray,thick,densely dashed](A,2.5)(-15,15)
	\end{tikzpicture}
\end{center}
\end{cprop}

\begin{cprop}[ComplementTitre={ - Programme de construction, version 2}]
$\bullet~~$On trace un cercle de centre $O$ passant par un point $A$.

$\bullet~~$On place le point $A'$ diamétralement opposé à $A$.

$\bullet~~$On trace la médiatrice du segment $[OA']$, elle coupe le cercle en deux
points $B$ et $C$.

$\bullet~~$On trace les segments $[AC]$, $[AB]$ et $[BC]$.

$\bullet~~$Le triangle $ABC$ est équilatéral.
%
\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/O,2/0.5/A}
		\tkzDefPointBy[symmetry=center O](A)
		\tkzGetPoint{D}
		\tkzDefLine[mediator](O,D)\tkzGetPoints{B}{C}
		\tkzLabelPoints[right](A)
		\tkzLabelPoint[left](D){$A'$}
		\tkzLabelPoints[above left](C)
		\tkzLabelPoints[below](O)
		\tkzLabelPoints[below left](B)
		\tkzDrawSegment[color=gray,dashed,thick](A,D)
		\tkzDrawCircle[color=gray,densely dashed,thick](O,A)
		\tkzDrawSegment[color=gray,densely dashed,thick,add = 0.1 and 0.1](B,C)
		\tkzDrawPolygon[thick](A,B,C)
		\tkzDrawPoint(O)
	\end{tikzpicture}
\end{center}
\end{cprop}

\section{Droites remarquables d'un triangle}

\subsection{Hauteurs}

\begin{cdefi}[Pluriel]
On appelle \textbf{hauteur} d’un triangle toute droite passant par un de ses sommets et perpendiculaire au côté opposé. L'intersection de la perpendiculaire et du côté opposé au sommet est appelé \textbf{pied de la hauteur}.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/0/B,3.15/1.75/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzDefTriangleCenter[ortho](A,B,C)\tkzGetPoint{H}
		\tkzDefSpcTriangle[orthic,name=H](A,B,C){a,b,c}
		\tkzDrawSegment[thick,color=red](C,Hc)
		\tkzMarkRightAngle[thick](C,Hc,A)
	\end{tikzpicture}
	\hspace{2cm}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/0/B,5.25/1.75/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above right](C)
		\tkzDefTriangleCenter[ortho](A,B,C)\tkzGetPoint{H}
		\tkzDefSpcTriangle[orthic,name=H](A,B,C){a,b,c}
		\tkzDrawSegment[thick,color=red](C,Hc)
		\tkzDrawSegment[thick,densely dotted,add=0 and 1](B,Hc)
		\tkzMarkRightAngle[thick](C,Hc,A)
	\end{tikzpicture}
\end{center}

Une hauteur peut se situer à l’extérieur du triangle.
\end{cdefi}

\begin{cprop}
Les trois hauteurs d'un triangle sont concourantes en $H$, appelé \textbf{orthocentre} du triangle.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/1/B,2.25/4/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[below](A)
		\tkzLabelPoints[below](B)
		\tkzLabelPoints[above](C)
		\tkzDefTriangleCenter[ortho](A,B,C)\tkzGetPoint{H}
		\tkzDefSpcTriangle[orthic,name=H](A,B,C){a,b,c}
		\tkzDrawSegments[thick,color=red](A,Ha B,Hb C,Hc)
		\tkzLabelPoints[color=red,left=6pt](H)
		\tkzMarkRightAngles[thick](A,Ha,B B,Hb,C C,Hc,A)
	\end{tikzpicture}
\end{center}
\end{cprop}

\subsection{Médianes}

\begin{cdefi}
On appelle \textbf{médiane} d’un triangle tout segment qui a pour extrémités un sommet et le milieu du côté opposé à ce sommet. Une médiane est toujours située à l’intérieur du triangle.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/1/B,2.5/3.45/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzDefTriangleCenter[centroid](A,B,C)\tkzGetPoint{G}
		\tkzDefMidPoint(A,B)\tkzGetPoint{I}
		\tkzDrawLines[color=blue,add=0 and 0.125,thick](C,I)
		\tkzMarkSegments[thick,mark=||](A,I I,B)
		\tkzLabelPoints[below right](I)
		\tkzLabelPoints[below right](I)
	\end{tikzpicture}
\end{center}
\end{cdefi}

\begin{cprop}[Pluriel]
Les trois médianes d’un triangle sont concourantes en un point appelé \textbf{centre de gravité} du triangle (point d’équilibre).

Le centre de gravité d’un triangle est situé aux deux tiers d’une médiane en partant du sommet.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/1/B,2.5/3.45/C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzDefTriangleCenter[centroid](A,B,C)\tkzGetPoint{G}
		\tkzDefMidPoint(A,B)\tkzGetPoint{I}
		\tkzDefMidPoint(A,C)\tkzGetPoint{J}
		\tkzDefMidPoint(B,C)\tkzGetPoint{K}
		\tkzDrawLines[color=blue,add=0 and 0.125,thick](C,I B,J A,K)
		\tkzMarkSegments[thick,mark=||](A,I I,B)
		\tkzMarkSegments[thick,mark=|](A,J J,C)
		\tkzMarkSegments[thick,mark=x](C,K K,B)
		%\tkzDrawPoints(I,J,K)
		\tkzLabelPoints[color=blue,below right](G)
	\end{tikzpicture}
\end{center}
\end{cprop}

\begin{cprop}
Dans un triangle ABC isocèle en $A$, la médiane et la hauteur issues de A sont confondues.

Dans un triangle équilatéral, la médiane et la hauteur issues d'un sommet sont confondues.
\end{cprop}

\pagebreak

\subsection{Médiatrices}

\begin{cdefi}
On appelle \textbf{médiatrice} d’un segment la droite perpendiculaire à ce segment et passant par son milieu.

C’est aussi l’ensemble des points \textbf{équidistants} de $A$ et de $B$.
\end{cdefi}

\begin{cprop}
Pour construire la médiatrice d’un segment $[AB]$, on a deux méthodes :

\textbf{Méthode 1} : avec une règle et une équerre, on trace une droite perpendiculaire passant par le milieu de $[AB]$.

\textbf{Méthode 2} : à l’aide d’un compas écarté de plus de la moitié de la longueur $AB$, on trace un arc de cercle de centre $A$ et un autre de centre $B$. Les deux points d’intersection obtenus sont deux points de la médiatrice.
%
\begin{center}
	\begin{tikzpicture}[scale=0.8]
		\tkzDefPoints{0/0/A,4/-1/B}
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](B)
		\tkzDefMidPoint(A,B)\tkzGetPoint{I}
		\tkzDrawSegment[thick](A,B)
		\tkzMarkSegments[thick,mark=||](A,I I,B)
		\tkzDrawPoints(I)%\tkzLabelPoints[below right](I)
		%\tkzDefLine[mediator](A,B)\tkzGetPoints{U}{V}
		\tkzDrawSegments[thick](A,I I,B)
		\tkzShowLine[thick,mediator,color=gray,dashed,length=2](A,B)
		\tkzDefLine[mediator](A,B)\tkzGetPoints{C}{D}
		\tkzDefPointWith[linear,K=.5](I,C)\tkzGetPoint{C}
		\tkzDefPointWith[linear,K=.5](I,D)\tkzGetPoint{D}
		\tkzDrawSegment[thick,color=purple,add=0.15 and 0.15](C,D)
		\tkzMarkRightAngle[thick](B,I,C)
	\end{tikzpicture}
\end{center}
\end{cprop}

\begin{cprop}
Les trois médiatrices d’un triangle sont concourantes en un point.
%
\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/-1/B,2.25/3/C}
		\tkzLabelPoints[left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzDefMidPoint(A,B)\tkzGetPoint{I}
		\tkzDefMidPoint(A,C)\tkzGetPoint{J}
		\tkzDefMidPoint(B,C)\tkzGetPoint{K}
		\tkzMarkSegments[thick,mark=||](A,I I,B)
		\tkzMarkSegments[thick,mark=x](A,J J,C)
		\tkzMarkSegments[thick,mark=|](B,K K,C)
		\tkzDrawPolygon[thick](A,B,C)
		\tkzDefLine[perpendicular=through I,K=0.35](A,B)\tkzGetPoint{U}
		\tkzDrawSegment[thick,color=purple,add=0.15 and 0.15](I,U)
		\tkzDefLine[perpendicular=through J,K=-0.45](A,C)\tkzGetPoint{V}
		\tkzDrawSegment[thick,color=purple,add=0.15 and 0.15](J,V)
		\tkzDefLine[perpendicular=through K,K=0.35](B,C)\tkzGetPoint{W}
		\tkzDrawSegment[thick,color=purple,add=0.15 and 0](K,W)
		\tkzMarkRightAngles[thick](B,I,U C,J,V B,K,W)
	\end{tikzpicture}
\end{center}
\end{cprop}

\section{Cercle circonscrit}

\subsection{Définition}

\begin{cdefi}
On appelle \textbf{cercle circonscrit} à un triangle le cercle passant par les trois sommets du triangle.

\begin{center}
	\begin{tikzpicture}[scale=0.8]
		\tkzDefPoints{0/0/A,4/-0.5/B,3/1.85/C}
		\tkzLabelPoints[left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzDrawPolygon[thick](A,B,C)
		\tkzDefCircle[circum](A,B,C)\tkzGetPoint{O}
		\tkzDrawCircle[thick,blue](O,A)
	\end{tikzpicture}
\end{center}
\end{cdefi}

\begin{cprop}
Le centre du cercle circonscrit à un triangle est le point d’intersection des médiatrices du triangle.

\begin{center}
	\begin{tikzpicture}
		\tkzDefPoints{0/0/A,4/-1/B,2.25/3/C}
		\tkzLabelPoints[left](A)
		\tkzLabelPoints[below right](B)
		\tkzLabelPoints[above](C)
		\tkzDefMidPoint(A,B)\tkzGetPoint{I}
		\tkzDefMidPoint(A,C)\tkzGetPoint{J}
		\tkzDefMidPoint(B,C)\tkzGetPoint{K}
		\tkzMarkSegments[thick,mark=||](A,I I,B)
		\tkzMarkSegments[thick,mark=x](A,J J,C)
		\tkzMarkSegments[thick,mark=|](B,K K,C)
		\tkzDrawPolygon[thick](A,B,C)
		\tkzDefLine[perpendicular=through I,K=0.35](A,B)\tkzGetPoint{U}
		\tkzDrawSegment[densely dashed,thick,color=purple,add=0.15 and 0.15](I,U)
		\tkzDefLine[perpendicular=through J,K=-0.45](A,C)\tkzGetPoint{V}
		\tkzDrawSegment[densely dashed,thick,color=purple,add=0.15 and 0.15](J,V)
		\tkzDefLine[perpendicular=through K,K=0.35](B,C)\tkzGetPoint{W}
		\tkzDrawSegment[densely dashed,thick,color=purple,add=0.15 and 0](K,W)
		\tkzMarkRightAngles[thick](B,I,U C,J,V B,K,W)
		\tkzDefCircle[circum](A,B,C)\tkzGetPoint{O}
		\tkzDrawCircle[thick,blue](O,A)
		\tkzLabelPoints[above,color=purple](O)
	\end{tikzpicture}
\end{center}
\end{cprop}

\subsection{Cercle circonscrit et triangle rectangle}

\begin{cprop}[Pluriel]
$\bullet~~$Si un triangle est rectangle, alors son hypoténuse est un diamètre de son cercle circonscrit.

Autrement dit, le centre du cercle circonscrit est le milieu de l’hypoténuse. 

\smallskip

$\bullet~~$De ce fait, l’hypoténuse mesure le double de la médiane issue de l’angle droit du triangle.

\smallskip

$\bullet~~$Si un triangle est inscrit dans un cercle, et a pour côté un diamètre de ce cercle, alors ce 
triangle est rectangle, et le diamètre est son hypoténuse.

\begin{center}
	\begin{tikzpicture}[scale=0.8]
		\tkzDefPoints{0/0/A,4/-1/B}
		\tkzDefMidPoint(A,B)\tkzGetPoint{O}
		\tkzLabelPoints[left](A)
		\tkzLabelPoints[below right](B)
		\tkzDrawSemiCircle[thick,color=gray](O,B)
		\tkzDefPointOnCircle[through=center O angle 38 point B]\tkzGetPoint{C}
		\tkzDrawPolygon[thick](A,B,C)
		\tkzLabelPoints[above right](C)
		\tkzLabelPoints[below](O)
		\tkzDrawSegment[thick,densely dashed](C,O)
		\tkzMarkRightAngle[thick](A,C,B)
		\tkzMarkSegments[thick,mark=||](A,O O,B O,C)
	\end{tikzpicture}
\end{center}
\end{cprop}

\section{Théorème de Pythagore}

\subsection{Théorème et réciproque}

\begin{cthm}
Si un triangle, est rectangle, alors le carré de la longueur du plus grand côté est égal à la somme des carrés des longueur des deux autres côtés.

Plus simplement : si un triangle $ABC$ est rectangle en $A$, alors on a $BC^2=AB^2+AC^2$.
\end{cthm}

\begin{cthm}
Si $BC^2=AB^2+AC^2$, alors $ABC$ est un triangle rectangle en $A$.
\end{cthm}

\subsection{Contraposée}

\begin{cidee}[Compteur=false]
Grâce au théorème de Pythagore sous sa forme directe, il est aussi possible de prouver qu’un triangle n’est pas rectangle, puisque si le triangle était rectangle, l’égalité de Pythagore serait vérifiée !

Alors si elle ne l’est pas, il est impossible que le triangle soit rectangle.

On appelle cela raisonner par \textit{contraposée}.
\end{cidee}

\end{document}